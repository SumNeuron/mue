// //<kbd class='code-version'>VERSION: 0.0.0</kbd>
let HttpRequest = Mue.HttpRequest;

var App = new Vue({
  el: '#app',
  components: {
    HttpRequest
  },
  template: `<http-request :url='url'/>`,
  data: function(){
    return {
      url: './versions/v000.js'
    };
  }
})
