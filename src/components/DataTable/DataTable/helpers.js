export function sortableValue(record, field) {
  let value = record[field];
  return value;
}

export function filterableValue(record, field) {
  let value = record[field];
  if (!value) { return ''; }
  return value.toString().toLowerCase();
}


function stringSort(a, b, ascQ) {
  let k = ascQ ? -1 : 1;
  if (a == b) {return 0;}
  return a < b ? 1 * k : -1 * k;
}

function valueQ(a) {
  if (
    a == undefined ||
    typeof a == undefined ||
    isNaN(a) && typeof(a) != 'string' ||
    a == null
  ) { return false; }
  return true;
}

function standarizeValue(a) {
  if (!valueQ(a)) {return undefined;}
  return a;
}

function numberSort(a, b, ascQ) {
  return ascQ ? a - b : b - a;
}

export function tableTimsort(ids, records, sort, sortingFunctions) {
  var fields = Object.keys(sort);
  if (!fields.length) {return ids;}

  var fieldFunctions = fields.map(
    function(field){

      let transform = (record, field)=>{return record[field];};
      let ascendingQ = sort[field].order == 'ascending';

      if (field in sortingFunctions) { transform = sortingFunctions[field]; }

      return function(a, b) {
        let x = standarizeValue(transform(a, field));
        let y = standarizeValue(transform(b, field));
        // console.log({a: a[field],t: transform(a, field),sv: standarizeValue(transform(a, field))});
        // console.log({b: b[field],t: transform(b, field),sv: standarizeValue(transform(b, field))});

        var type;
        if ([x, y].some(function(e){return typeof e == 'string';})) {type='string';}
        if ([x, y].some(function(e){return typeof e == 'number';})) {type='number';}
        // console.log('type', type);

        if (type == 'string') {
          if (!valueQ(x)) {x = '';}
          if (!valueQ(y)) {y = '';}
          return stringSort(x, y, ascendingQ);
        }
        else if (type == 'number') {
          if (!valueQ(x)) {x = Infinity;}
          if (!valueQ(y)) {y = Infinity;}
          return numberSort(x, y, ascendingQ);
        }
        else {
          return -1;
        }

      };
  });


  let sorted = ids.sort(function(a, b) {
    for (var i = 0; i < fieldFunctions.length; i++) {
      var f = fieldFunctions[i];
      var v = f(records[a], records[b]);
      if (!v) { continue; }
      else {break;}
    }
    return v;
  });
  // console.log('sorted', sorted);
  return sorted;
}

export function giRegexOnFields(filterText, ids, records, fields, fieldRenderFunctions) {
  var reg = new RegExp(filterText, 'gi');
  var use;

  if (filterText == '') {
    use = ids;
  }
  else {
    use = [];

    ids.map(function(id) {

      var record = records[id];

      var fieldJoin = fields.map(
        function(f) {
          if (f in fieldRenderFunctions) {
            return fieldRenderFunctions[f](id, record, f);
          }
          return filterableValue(record, f);
        }
      ).join('');

      var match = fieldJoin.match(reg);

      if (
        !(match == null || match.join('') == '')
      ) {
        use.push(id);
      }

    });
  }
  return use;
}


export function objectFields(ids, records) {
  var allRecordFields = [].concat(
    ...ids.map(
      function(recordId){
        return Object.keys(records[recordId]);
      })
  );
  return [...new Set(allRecordFields)];
}
