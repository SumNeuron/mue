"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
var getFilesList = function getFilesList(files) {
    var list = {};

    files.forEach(function (file) {
        // TODO: this will cause files to override each other 
        // whenever there are two files with different extensions and identical name
        list[file.stem] = [file.path];
    });

    return list;
};

exports.default = getFilesList;
module.exports = exports["default"];

//# sourceMappingURL=getFilesList.js.map