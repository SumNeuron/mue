
import ButtonMinusToClose from './components/ButtonMinusToClose/ButtonMinusToClose.vue';
import ButtonPlus from './components/ButtonPlus/ButtonPlus.vue';

import Crumbs from './components/Crumbs/Crumbs.vue';



import AdvancedFilterInput from './components/AdvancedFilter/AdvancedFilterInput/AdvancedFilterInput.vue';
import AdvancedFilterRow from './components/AdvancedFilter/AdvancedFilterRow/AdvancedFilterRow.vue';
import AdvancedFilterSelectConditional from './components/AdvancedFilter/AdvancedFilterSelectConditional/AdvancedFilterSelectConditional.vue';
import AdvancedFilterSelectFunction from './components/AdvancedFilter/AdvancedFilterSelectFunction/AdvancedFilterSelectFunction.vue';
import AdvancedFilterSelectLogic from './components/AdvancedFilter/AdvancedFilterSelectLogic/AdvancedFilterSelectLogic.vue';
import AdvancedFilterSelectProperty from './components/AdvancedFilter/AdvancedFilterSelectProperty/AdvancedFilterSelectProperty.vue';
import AdvancedFilterTable from './components/AdvancedFilter/AdvancedFilterTable/AdvancedFilterTable.vue';

import AnchorButton from './components/AnchorButton/AnchorButton.vue';
import AlertBox from './components/AlertBox/AlertBox.vue';

import DataTable from './components/DataTable/DataTable/DataTable.vue';
import DataTableCell from './components/DataTable/DataTableCell/DataTableCell.vue';
import DataTableRow from './components/DataTable/DataTableRow/DataTableRow.vue';


import HttpRequest from './components/HttpRequest/HttpRequest.vue';


import SectionNavigation from './components/SectionNavigation/SectionNavigation/SectionNavigation.vue';
import SectionNavigationArrowUp from './components/SectionNavigation/SectionNavigationArrowUp/SectionNavigationArrowUp.vue';
import SectionNavigationArrowDown from './components/SectionNavigation/SectionNavigationArrowDown/SectionNavigationArrowDown.vue';


import InputAlpha from './components/InputAlpha/InputAlpha/InputAlpha.vue';
import InputAlphaFilter from './components/InputAlpha/InputAlphaFilter/InputAlphaFilter.vue';

import DjangoUser from './components/DjangoUser/DjangoUser.vue';


var Mue = {};

// Mue.ButtonMinusToClose = ButtonMinusToClose;
// Mue.ButtonPlus = ButtonPlus;

Mue.Crumbs = Crumbs;
Mue.AlertBox = AlertBox;

// Mue.AdvancedFilterInput = AdvancedFilterInput;
// Mue.AdvancedFilterRow = AdvancedFilterRow;
// Mue.AdvancedFilterSelectConditional = AdvancedFilterSelectConditional;
// Mue.AdvancedFilterSelectFunction = AdvancedFilterSelectFunction;
// Mue.AdvancedFilterSelectLogic = AdvancedFilterSelectLogic;
// Mue.AdvancedFilterSelectProperty = AdvancedFilterSelectProperty;
Mue.AdvancedFilterTable = AdvancedFilterTable;

Mue.AnchorButton = AnchorButton;

Mue.DataTable = DataTable;
// Mue.DataTableCell = DataTableCell;
// Mue.DataTableRow = DataTableRow;

Mue.HttpRequest = HttpRequest;

Mue.SectionNavigation = SectionNavigation;
// Mue.SectionNavigationArrowUp = SectionNavigationArrowUp;
// Mue.SectionNavigationArrowDown = SectionNavigationArrowDown;

Mue.InputAlpha = InputAlpha;
// Mue.InputAlphaFilter = InputAlphaFilter;

Mue.DjangoUser = DjangoUser;

window.Mue = Mue;
