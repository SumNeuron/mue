let sortTokenObject = function(object) {
  return Object.entries(object).sort(function(a, b) {
    let ka = a[0];
    let kb = b[0];
    return (ka > kb || kb.length - ka.length);
  }).reduce(function(o, [k, v]) {
    o[k] = v;
    return o;
  }, {});
};

let conditionalRenders = {
  eq: '=',
  neq: '≠',
  gt: '>',
  lt: '<',
  gte: '≥',
  lte: '≤',
  includes: '⊃',
};

let logicRenders = {
  and: '⋀',
  or: '⋁'
};

let conditionalTokens = {
  eq: 'eq',
  equal: 'eq',
  '=': 'eq',
  '!=': 'neq',
  '≠': 'neq',
  '>': 'gt',
  '≥': 'gte',
  '>=': 'gte',
  '<': 'lt',
  '≤': 'lte',
  '<=': 'lte',
  'is': 'eq',
  'is equal to': 'eq',
  'is not': 'neq',
  neq: 'neq',
  'not equal to': 'neq',
  'not eq to': 'neq',
  'is not equal to': 'neq',
  gt: 'gt',
  'greater than': 'gt',
  'less than': 'lt',
  lt: 'lt',
  'less than or equal to': 'lte',
  'greater than or equal to': 'gte',
  'member of': 'includes',
  substring: 'includes',
  contains: 'includes',
  includes: 'includes'
};


let functionTokens = {
  max: 'max',
  maximum: 'max',
  minimum: 'min',
  min: 'min',
  length: 'length',
  len: 'length',
  mean: 'mean',
  median: 'median',
  average: 'mean',
  identity: 'identity'
};

let logicTokens = {
  and: 'and',
  or: 'or'
};


functionTokens = sortTokenObject(functionTokens);
logicTokens = sortTokenObject(logicTokens);
conditionalTokens = sortTokenObject(conditionalTokens);

let extractTokens = function(text, tokens, fallback = undefined, scope = undefined, dropUntilMatchQ = false) {
  let token = fallback;
  let str = scope === undefined ? text : text.slice(0, scope);
  for (var t in tokens) {
    if (str.includes(t)) {
      token = tokens[t];
      if (dropUntilMatchQ) {
        text = text.slice(str.indexOf(t) + t.length, str.length);
      } else {
        text = text.replace(t, '');
      }
      break;
    }
  }
  return [token, text];
};

export {
  functionTokens,
  logicTokens,
  extractTokens,
  sortTokenObject,
  conditionalTokens,
  conditionalRenders,
  logicRenders
};
