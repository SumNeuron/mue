// Plug-ins
import vue from 'rollup-plugin-vue';

// backwards compatibility
import babel from 'rollup-plugin-babel';

// linter
import eslint from 'rollup-plugin-eslint';

// for node and common js packages
import resolve from 'rollup-plugin-node-resolve'; // which allows us to load third-party modules in node_modules
import commonjs from 'rollup-plugin-commonjs'; // which coverts CommonJS modules to ES6, which stops them from breaking Rollup

// for env vars
import replace from 'rollup-plugin-replace';

// for minimization
import { uglify } from 'rollup-plugin-uglify';


// for css injection
import postcss from 'rollup-plugin-postcss';

import simplevars from 'postcss-simple-vars';
/*
This allows the use of Sass-style variables (e.g. $myColor: #fff;, used as color: $myColor;)
instead of the more verbose CSS syntax (e.g. :root { --myColor: #fff; }, used as color: var(--myColor);).
This is purely preferential; I like the shorter syntax better.
*/

import nested from 'postcss-nested';
/*
This allows rules to be nested. I actually don’t use this to nest rules;
I use it as a shortcut for creating BEM-friendly selectors and grouping my
blocks, elements, and modifiers into single CSS blocks.
*/

import cssnext from 'postcss-cssnext';
/*
This is a bundle of plugins that enables the most current CSS syntax
(according to the latest CSS specs), transpiling it to work, even in older
browsers that don’t support the new features
*/


import pkg from './package.json';



var plugins = [
  vue(),

  postcss({
   plugins: [
     simplevars(),
     nested(),
     cssnext({ warnForDuplicates: false, }),
   ],
    extensions: [ '.css' ],
  }),

  resolve({
    jsnext: true,
    main: true,
    browser: true,
    /*
      The jsnext property is part of an effort to ease the migration to ES2015 modules for Node packages.
      The main and browser properties help the plugin decide which files should be used for the bundle.
    */
  }),

  commonjs(),


  babel({
    exclude: 'node_modules/**',
  }),

  replace({
    exclude: 'node_modules/**',
    ENV: JSON.stringify(process.env.NODE_ENV || 'development'),
  }),
]

let input = 'src/index.js'
let output = {
  format: 'iife',
  sourcemap: 'inline',
  file: `build/js/${pkg.name}.v${pkg.version}.js`
}

let minOutput = {
  format: 'iife',
  sourcemap: 'inline',
  file: `build/js/${pkg.name}.min.v${pkg.version}.js`
}

let lint = {
  exclude: [
    'src/styles/**',
    '**.vue',
  ]
};


export default [
  // main
  {
    input: input,
    output: output,
    plugins: plugins.concat([]), //eslint(lint),
  },
  // min
  {
    input: input,
    output: minOutput,
    plugins: plugins.concat([uglify(),])
  },

];
