let crumbs = Mue.Crumbs

let home = String(window.location).includes('gitlab') ? 'Mue' : ''

new Vue({
  el: '#crumbs',
  template: '<crumbs :home="home"/>',
  components: { crumbs },
  data: { home: home }
})
