let crumbs = vdsm.crumbs

let home = String(window.location).includes('gitlab') ? 'vdsm' : ''

new Vue({
  el: '#crumbs',
  template: '<crumbs :home="home"/>',
  components: { crumbs },
  data: { home: home }
})
