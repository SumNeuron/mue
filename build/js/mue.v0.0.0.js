(function () {
  'use strict';

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  var script = {
    props: {},
    data: function data() {
      return {
        readyQ: false,
        color: 'danger',
        background: 'light'

      };
    },
    methods: {
      minus: function minus() {
        if (!this.readyQ) {
          this.readyQ = true;
          return;
        } else {
          this.readyQ = false;
          this.$emit('minus');
        }
      }
    }
  };

  function normalizeComponent(template, style, script, scopeId, isFunctionalTemplate, moduleIdentifier
  /* server only */
  , shadowMode, createInjector, createInjectorSSR, createInjectorShadow) {
    if (typeof shadowMode !== 'boolean') {
      createInjectorSSR = createInjector;
      createInjector = shadowMode;
      shadowMode = false;
    } // Vue.extend constructor export interop.


    var options = typeof script === 'function' ? script.options : script; // render functions

    if (template && template.render) {
      options.render = template.render;
      options.staticRenderFns = template.staticRenderFns;
      options._compiled = true; // functional template

      if (isFunctionalTemplate) {
        options.functional = true;
      }
    } // scopedId


    if (scopeId) {
      options._scopeId = scopeId;
    }

    var hook;

    if (moduleIdentifier) {
      // server build
      hook = function hook(context) {
        // 2.3 injection
        context = context || // cached call
        this.$vnode && this.$vnode.ssrContext || // stateful
        this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext; // functional
        // 2.2 with runInNewContext: true

        if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
          context = __VUE_SSR_CONTEXT__;
        } // inject component styles


        if (style) {
          style.call(this, createInjectorSSR(context));
        } // register component module identifier for async chunk inference


        if (context && context._registeredComponents) {
          context._registeredComponents.add(moduleIdentifier);
        }
      }; // used by ssr in case component is cached and beforeCreate
      // never gets called


      options._ssrRegister = hook;
    } else if (style) {
      hook = shadowMode ? function () {
        style.call(this, createInjectorShadow(this.$root.$options.shadowRoot));
      } : function (context) {
        style.call(this, createInjector(context));
      };
    }

    if (hook) {
      if (options.functional) {
        // register for functional component in vue file
        var originalRender = options.render;

        options.render = function renderWithStyleInjection(h, context) {
          hook.call(context);
          return originalRender(h, context);
        };
      } else {
        // inject component registration as beforeCreate hook
        var existing = options.beforeCreate;
        options.beforeCreate = existing ? [].concat(existing, hook) : [hook];
      }
    }

    return script;
  }

  var normalizeComponent_1 = normalizeComponent;

  var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\\b/.test(navigator.userAgent.toLowerCase());
  function createInjector(context) {
    return function (id, style) {
      return addStyle(id, style);
    };
  }
  var HEAD = document.head || document.getElementsByTagName('head')[0];
  var styles = {};

  function addStyle(id, css) {
    var group = isOldIE ? css.media || 'default' : id;
    var style = styles[group] || (styles[group] = {
      ids: new Set(),
      styles: []
    });

    if (!style.ids.has(id)) {
      style.ids.add(id);
      var code = css.source;

      if (css.map) {
        // https://developer.chrome.com/devtools/docs/javascript-debugging
        // this makes source maps inside style tags work properly in Chrome
        code += '\n/*# sourceURL=' + css.map.sources[0] + ' */'; // http://stackoverflow.com/a/26603875

        code += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(css.map)))) + ' */';
      }

      if (!style.element) {
        style.element = document.createElement('style');
        style.element.type = 'text/css';
        if (css.media) style.element.setAttribute('media', css.media);
        HEAD.appendChild(style.element);
      }

      if ('styleSheet' in style.element) {
        style.styles.push(code);
        style.element.styleSheet.cssText = style.styles.filter(Boolean).join('\n');
      } else {
        var index = style.ids.size - 1;
        var textNode = document.createTextNode(code);
        var nodes = style.element.childNodes;
        if (nodes[index]) style.element.removeChild(nodes[index]);
        if (nodes.length) style.element.insertBefore(textNode, nodes[index]);else style.element.appendChild(textNode);
      }
    }
  }

  var browser = createInjector;

  /* script */
  var __vue_script__ = script;

  /* template */
  var __vue_render__ = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { on: { click: _vm.minus } }, [_c("i", {
      staticClass: "fa",
      class: [{ "fa-minus-circle": !_vm.readyQ, "fa-times-circle": _vm.readyQ }, "bg-" + _vm.background, "text-" + _vm.color]
    })]);
  };
  var __vue_staticRenderFns__ = [];
  __vue_render__._withStripped = true;

  /* style */
  var __vue_inject_styles__ = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-62535790_0", { source: "\ni[data-v-62535790] {\n  font-size: 24px;\n  color:'#dc3545';\n}\ni[data-v-62535790]:hover, i[data-v-62535790]:focus{\n  color: '#c82333' !important;\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/ButtonMinusToClose/ButtonMinusToClose.vue"], "names": [], "mappings": ";AA2CA;EACA,eAAA;EACA,eAAA;AACA;AACA;EACA,2BAAA;AACA", "file": "ButtonMinusToClose.vue", "sourcesContent": ["<template>\n  <div\n    @click='minus'\n  >\n    <i\n      class='fa'\n      :class='[{\"fa-minus-circle\": !readyQ,\"fa-times-circle\": readyQ},\"bg-\"+background, \"text-\"+color]'\n      >\n    </i>\n  </div>\n</template>\n\n<script>\nexport default {\n  props: {\n\n  },\n  data: function() {\n    return {\n      readyQ: false,\n      color: 'danger',\n      background: 'light'\n\n\n    };\n  },\n  methods: {\n    minus: function() {\n      if (!this.readyQ) {\n        this.readyQ = true;\n        return;\n      }\n      else {\n        this.readyQ = false;\n        this.$emit('minus');\n      }\n\n    }\n  }\n};\n</script>\n\n<style scoped>\ni {\n  font-size: 24px;\n  color:'#dc3545';\n}\ni:hover, i:focus{\n  color: '#c82333' !important;\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__ = "data-v-62535790";
  /* module identifier */
  var __vue_module_identifier__ = undefined;
  /* functional template */
  var __vue_is_functional_template__ = false;
  /* style inject SSR */

  var ButtonMinusToClose = normalizeComponent_1({ render: __vue_render__, staticRenderFns: __vue_staticRenderFns__ }, __vue_inject_styles__, __vue_script__, __vue_scope_id__, __vue_is_functional_template__, __vue_module_identifier__, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  var script$1 = {
    props: {
      color: {
        default: 'success',
        type: String
      },
      background: {
        default: 'light',
        type: String
      }
    },
    methods: {
      plus: function plus() {
        this.$emit('plus');
      }
    }
  };

  /* script */
  var __vue_script__$1 = script$1;

  /* template */
  var __vue_render__$1 = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { on: { click: _vm.plus } }, [_c("i", {
      staticClass: "fa fa-plus-circle",
      class: ["bg-" + _vm.background, "text-" + _vm.color]
    })]);
  };
  var __vue_staticRenderFns__$1 = [];
  __vue_render__$1._withStripped = true;

  /* style */
  var __vue_inject_styles__$1 = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-b77598f8_0", { source: "\ni[data-v-b77598f8] {\n  font-size: 24px;\n  color: #28a745;\n}\ni[data-v-b77598f8]:hover, i[data-v-b77598f8]:focus {\n  color: #218838 !important;\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/ButtonPlus/ButtonPlus.vue"], "names": [], "mappings": ";AAiCA;EACA,eAAA;EACA,cAAA;AACA;AAEA;EACA,yBAAA;AACA", "file": "ButtonPlus.vue", "sourcesContent": ["<template>\n  <div\n    @click='plus'\n  >\n    <i\n      class='fa fa-plus-circle'\n      :class='[\"bg-\"+background, \"text-\"+color]'\n      >\n    </i>\n  </div>\n</template>\n\n<script>\nexport default {\n  props: {\n    color: {\n      default:'success',\n      type:String\n    },\n    background: {\n      default:'light',\n      type:String\n    }\n  },\n  methods: {\n    plus: function() {\n      this.$emit('plus');\n    }\n  }\n};\n</script>\n\n<style scoped>\ni {\n  font-size: 24px;\n  color: #28a745;\n}\n\ni:hover, i:focus {\n  color: #218838 !important;\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$1 = "data-v-b77598f8";
  /* module identifier */
  var __vue_module_identifier__$1 = undefined;
  /* functional template */
  var __vue_is_functional_template__$1 = false;
  /* style inject SSR */

  var ButtonPlus = normalizeComponent_1({ render: __vue_render__$1, staticRenderFns: __vue_staticRenderFns__$1 }, __vue_inject_styles__$1, __vue_script__$1, __vue_scope_id__$1, __vue_is_functional_template__$1, __vue_module_identifier__$1, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  var script$2 = {
    name: 'crumbs',
    props: {
      home: {
        type: String,
        default: ''
      },
      debugQ: {
        type: Boolean,
        default: false
      }
    },

    data: function data() {
      return {};
    },

    methods: {
      removeTrailingSlash: function removeTrailingSlash(parts) {
        return parts[parts.length - 1] == '' ? parts.slice(0, parts.length - 1) : parts;
      },
      removeFileName: function removeFileName(parts) {
        return parts[parts.length - 1].includes('.html') ? parts.slice(0, parts.length - 1) : parts;
      },
      producePart: function producePart(part, i, parts) {
        var that = this;
        var firstQ = i == 0;
        var lastQ = i == parts.length - 1;

        var crumb = firstQ ? that.home == '' ? 'home' : that.home : part;

        var href = firstQ ? that.home == '' ? Array(parts.length - 1).fill('../').join('') : that.home : parts.slice(0, i + 1).join('/');

        return { activeQ: lastQ, crumb: crumb, href: href };
      }
    },

    computed: {
      parts: function parts() {
        var that = this;

        var pathname = window.location.pathname;
        var parts = pathname.split('/');

        parts = that.removeTrailingSlash(parts);
        parts = that.removeFileName(parts);
        return parts;
      },

      crumbs: function crumbs() {
        var that = this;
        if (that.debugQ) {
          console.group('[crumbs]');
        }

        var home = this.home;

        var parts = that.parts;

        if (that.debugQ) {
          console.table(parts);
          console.table({
            homeProp: that.home,
            home: home,
            homeHasIndex0Q: home == parts[0]
          });
        }

        // // remove invalid home
        while (home != parts[0] && parts.length) {
          parts.splice(0, 1);
        }

        var data = parts.map(function (part, i) {
          return that.producePart(part, i, parts);
        });

        if (that.debugQ) {
          console.groupEnd();
        }
        return data;
      }
    }
  };

  /* script */
  var __vue_script__$2 = script$2;

  /* template */
  var __vue_render__$2 = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("ol", { staticClass: "breadcrumb" }, _vm._l(_vm.crumbs, function (crumb) {
      return _c("li", { staticClass: "breadcrumb-item", class: { active: crumb.activeQ } }, [_c("a", { attrs: { href: crumb.href } }, [_vm._v(_vm._s(crumb.crumb))])]);
    }), 0);
  };
  var __vue_staticRenderFns__$2 = [];
  __vue_render__$2._withStripped = true;

  /* style */
  var __vue_inject_styles__$2 = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-ea188498_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: { "version": 3, "sources": [], "names": [], "mappings": "", "file": "Crumbs.vue" }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$2 = "data-v-ea188498";
  /* module identifier */
  var __vue_module_identifier__$2 = undefined;
  /* functional template */
  var __vue_is_functional_template__$2 = false;
  /* style inject SSR */

  var Crumbs = normalizeComponent_1({ render: __vue_render__$2, staticRenderFns: __vue_staticRenderFns__$2 }, __vue_inject_styles__$2, __vue_script__$2, __vue_scope_id__$2, __vue_is_functional_template__$2, __vue_module_identifier__$2, browser, undefined);

  //
  //
  //
  //
  //

  var script$3 = {
    name: 'advanced-filter-input',
    data: function data() {
      return {
        value: this.selected
      };
    },
    methods: {
      updateInput: function updateInput(event) {
        this.$emit('updateInput', event.target.value);
      }
    }

  };

  /* script */
  var __vue_script__$3 = script$3;

  /* template */
  var __vue_render__$3 = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("input", {
      domProps: { value: _vm.value },
      on: {
        input: function input($event) {
          return _vm.updateInput($event);
        }
      }
    });
  };
  var __vue_staticRenderFns__$3 = [];
  __vue_render__$3._withStripped = true;

  /* style */
  var __vue_inject_styles__$3 = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-578e36ed_0", { source: "\ninput[data-v-578e36ed] {\n  background-color: transparent ;\n  border-color: transparent;\n  border-bottom-color: RGB(50, 200, 250);\n  padding: 0 5px;\n  color: RGB(32, 122, 247)\n}\ninput[data-v-578e36ed]:focus {\n  outline: none;\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/AdvancedFilter/AdvancedFilterInput/AdvancedFilterInput.vue"], "names": [], "mappings": ";AAuBA;EACA,8BAAA;EACA,yBAAA;EACA,sCAAA;EACA,cAAA;EACA;AACA;AAEA;EACA,aAAA;AACA", "file": "AdvancedFilterInput.vue", "sourcesContent": ["\n<template>\n  <input :value='value' @input='updateInput($event)'/>\n</template>\n\n<script>\nexport default {\n  name:'advanced-filter-input',\n  data: function() {\n    return {\n      value: this.selected,\n    };\n  },\n  methods: {\n    updateInput: function(event) {\n      this.$emit('updateInput', event.target.value);\n    }\n  }\n\n};\n</script>\n\n<style scoped>\ninput {\n  background-color: transparent ;\n  border-color: transparent;\n  border-bottom-color: RGB(50, 200, 250);\n  padding: 0 5px;\n  color: RGB(32, 122, 247)\n}\n\ninput:focus {\n  outline: none;\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$3 = "data-v-578e36ed";
  /* module identifier */
  var __vue_module_identifier__$3 = undefined;
  /* functional template */
  var __vue_is_functional_template__$3 = false;
  /* style inject SSR */

  var AdvancedFilterInput = normalizeComponent_1({ render: __vue_render__$3, staticRenderFns: __vue_staticRenderFns__$3 }, __vue_inject_styles__$3, __vue_script__$3, __vue_scope_id__$3, __vue_is_functional_template__$3, __vue_module_identifier__$3, browser, undefined);

  //∈, ∉

  var conditionals = {
    // faeq: {
    //   text:'∀ e, e =',
    // }
    eq: {
      text: '=',
      types: ['array', 'arraystring', 'arrayobject', 'arraynumber', 'number', 'string', 'undefined'],
      cond: function cond(a, b) {
        return a == b;
      }
    },
    neq: {
      text: '≠',
      types: ['array', 'arraystring', 'arrayobject', 'arraynumber', 'number', 'string', 'undefined'],
      cond: function cond(a, b) {
        return a != b;
      }
    },
    lt: {
      text: '<',
      types: ['number'],

      cond: function cond(a, b) {
        return a < b;
      }
    },
    gt: {
      text: '>',
      types: ['number'],

      cond: function cond(a, b) {
        return a > b;
      }
    },
    lte: {
      text: '≤',
      types: ['number'],

      cond: function cond(a, b) {
        return a <= b;
      }
    },
    gte: {
      text: '≥',
      types: ['number'],

      cond: function cond(a, b) {
        return a >= b;
      }
    },
    ss: {
      text: '⊂',
      types: ['array', 'arraystring', 'arraynumber', 'string'],
      cond: function cond(a, b) {
        return a.includes(b);
      }
    },
    nss: {
      text: '⟈',
      types: ['array', 'arraystring', 'arraynumber', 'string'],

      cond: function cond(a, b) {
        return !a.includes(b);
      }
    }
  };

  //
  var script$4 = {
    name: 'advanced-filter-select-conditional',
    props: {
      selected: {
        type: String,
        default: 'identity'
      },
      dataType: {
        type: String,
        default: 'undefined'
      }
    },
    data: function data() {
      return {
        value: this.selected,
        options: conditionals
      };
    },
    methods: {
      updateSelected: function updateSelected(event) {
        this.value = event.target.value;
        this.$emit('updateSelected', event.target.value);
      }
    }
  };

  /* script */
  var __vue_script__$4 = script$4;

  /* template */
  var __vue_render__$4 = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("select", {
      on: {
        input: function input($event) {
          return _vm.updateSelected($event);
        }
      }
    }, _vm._l(_vm.options, function (val, key, index) {
      return val.types.includes(_vm.dataType) ? _c("option", { domProps: { value: key, selected: key == _vm.value } }, [_vm._v("\n    " + _vm._s(val.text) + "\n  ")]) : _vm._e();
    }), 0);
  };
  var __vue_staticRenderFns__$4 = [];
  __vue_render__$4._withStripped = true;

  /* style */
  var __vue_inject_styles__$4 = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-503871b2_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: { "version": 3, "sources": [], "names": [], "mappings": "", "file": "AdvancedFilterSelectConditional.vue" }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$4 = "data-v-503871b2";
  /* module identifier */
  var __vue_module_identifier__$4 = undefined;
  /* functional template */
  var __vue_is_functional_template__$4 = false;
  /* style inject SSR */

  var AdvancedFilterSelectConditional = normalizeComponent_1({ render: __vue_render__$4, staticRenderFns: __vue_staticRenderFns__$4 }, __vue_inject_styles__$4, __vue_script__$4, __vue_scope_id__$4, __vue_is_functional_template__$4, __vue_module_identifier__$4, browser, undefined);

  //'array', 'arraystring', 'arrayobject', 'arraynumber',
  //'number', 'string', 'undefined'
  var functions = {
    'identity': {
      text: 'x → x',
      func: function func(x) {
        return x;
      },
      types: ['number', 'string', 'undefined']
    },
    'mean': {
      text: 'x → mean(x)',
      func: function func(numbers) {
        return numbers.map(function (z) {
          return z / numbers.length;
        }).reduce(function (adder, value) {
          return adder + value;
        });
      },
      types: ['arraynumber']
    },
    'max': {
      text: 'x → max(x)',
      func: function func(x) {
        var lg = -Infinity;
        x.map(function (d) {
          lg = d > lg ? d : lg;
        });
        return lg;
      },
      types: ['arraynumber']

    },
    'min': {
      text: 'x → min(x)',
      func: function func(x) {
        var sm = Infinity;
        x.map(function (d) {
          sm = d < sm ? d : sm;
        });
        return sm;
      },
      types: ['arraynumber']
    },
    'length': {
      text: 'x → len(x)',
      func: function func(x) {
        return x.length;
      },
      types: ['array', 'arraynumber', 'arraystring', 'arrayobject']
    }

  };

  //
  var script$5 = {
    name: 'advanced-filter-select-function',
    props: {
      selected: {
        type: String,
        default: 'identity'
      },
      dataType: {
        type: String,
        default: 'undefined'
      }
    },
    data: function data() {
      return {
        value: this.selected,
        options: functions
      };
    },
    methods: {
      updateSelected: function updateSelected(event) {
        this.value = event.target.value;
        this.$emit('updateSelected', event.target.value);
      }
    }
  };

  /* script */
  var __vue_script__$5 = script$5;

  /* template */
  var __vue_render__$5 = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("select", {
      on: {
        change: function change($event) {
          return _vm.updateSelected($event);
        }
      }
    }, [_vm._v("\n  " + _vm._s(_vm.selected) + " " + _vm._s(_vm.value) + "\n  "), _vm._l(_vm.options, function (val, key, index) {
      return val.types.includes(_vm.dataType) ? _c("option", { domProps: { value: key, selected: key == _vm.selected } }, [_vm._v("\n    " + _vm._s(val.text) + "\n  ")]) : _vm._e();
    })], 2);
  };
  var __vue_staticRenderFns__$5 = [];
  __vue_render__$5._withStripped = true;

  /* style */
  var __vue_inject_styles__$5 = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-3351d1ac_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: { "version": 3, "sources": [], "names": [], "mappings": "", "file": "AdvancedFilterSelectFunction.vue" }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$5 = "data-v-3351d1ac";
  /* module identifier */
  var __vue_module_identifier__$5 = undefined;
  /* functional template */
  var __vue_is_functional_template__$5 = false;
  /* style inject SSR */

  var AdvancedFilterSelectFunction = normalizeComponent_1({ render: __vue_render__$5, staticRenderFns: __vue_staticRenderFns__$5 }, __vue_inject_styles__$5, __vue_script__$5, __vue_scope_id__$5, __vue_is_functional_template__$5, __vue_module_identifier__$5, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  var options = {
    and: { text: 'and' },
    or: { text: 'or' }
  };

  var script$6 = {
    name: 'advanced-filter-select-logic',
    props: {
      selected: {
        type: String,
        default: 'and'
      }
    },
    // model: {
    //   prop: 'selected',
    //   event: 'updateSelected'
    // },
    data: function data() {
      return {
        value: this.selected,
        options: options
      };
    },
    methods: {
      updateSelected: function updateSelected(event) {
        this.value = event.target.value;
        this.$emit('updateSelected', event.target.value);
      }
    }

  };

  /* script */
  var __vue_script__$6 = script$6;

  /* template */
  var __vue_render__$6 = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("select", {
      on: {
        change: function change($event) {
          return _vm.updateSelected($event);
        }
      }
    }, _vm._l(_vm.options, function (val, key, index) {
      return _c("option", { domProps: { value: key, selected: key == _vm.value } }, [_vm._v("\n    " + _vm._s(val.text) + "\n  ")]);
    }), 0);
  };
  var __vue_staticRenderFns__$6 = [];
  __vue_render__$6._withStripped = true;

  /* style */
  var __vue_inject_styles__$6 = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-1e275a60_0", { source: "\nselect[data-v-1e275a60] {\n  background-color: transparent ;\n  border-color: transparent;\n  border-bottom-color: RGB(50, 200, 250);\n  -webkit-appearance: none;\n  text-transform: uppercase;\n  padding: 0 5px;\n  font-weight: bold;\n  color: RGB(32, 122, 247)\n}\nselect[data-v-1e275a60]:focus {\n  outline: none;\n}\noption[data-v-1e275a60] {\n  padding: 0 5px;\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/AdvancedFilter/AdvancedFilterSelectLogic/AdvancedFilterSelectLogic.vue"], "names": [], "mappings": ";AAgDA;EACA,8BAAA;EACA,yBAAA;EACA,sCAAA;EACA,wBAAA;EACA,yBAAA;EACA,cAAA;EACA,iBAAA;EACA;AACA;AAEA;EACA,aAAA;AACA;AAEA;EACA,cAAA;AAEA", "file": "AdvancedFilterSelectLogic.vue", "sourcesContent": ["\n<template>\n  <select @change='updateSelected($event)'>\n    <option\n      v-for='(val, key, index) in options'\n      :value='key'\n      :selected='key == value'\n    >\n      {{val.text}}\n    </option>\n  </select>\n</template>\n\n<script>\nlet options = {\n  and: {text: 'and'},\n  or: {text: 'or'}\n};\n\nexport default {\n  name:'advanced-filter-select-logic',\n  props: {\n    selected: {\n      type: String,\n      default: 'and'\n    }\n  },\n  // model: {\n  //   prop: 'selected',\n  //   event: 'updateSelected'\n  // },\n  data: function() {\n    return {\n      value: this.selected,\n      options: options\n    };\n  },\n  methods: {\n    updateSelected: function(event) {\n      this.value = event.target.value;\n      this.$emit('updateSelected', event.target.value);\n    }\n  }\n\n};\n</script>\n\n<style scoped>\nselect {\n  background-color: transparent ;\n  border-color: transparent;\n  border-bottom-color: RGB(50, 200, 250);\n  -webkit-appearance: none;\n  text-transform: uppercase;\n  padding: 0 5px;\n  font-weight: bold;\n  color: RGB(32, 122, 247)\n}\n\nselect:focus {\n  outline: none;\n}\n\noption {\n  padding: 0 5px;\n\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$6 = "data-v-1e275a60";
  /* module identifier */
  var __vue_module_identifier__$6 = undefined;
  /* functional template */
  var __vue_is_functional_template__$6 = false;
  /* style inject SSR */

  var AdvancedFilterSelectLogic = normalizeComponent_1({ render: __vue_render__$6, staticRenderFns: __vue_staticRenderFns__$6 }, __vue_inject_styles__$6, __vue_script__$6, __vue_scope_id__$6, __vue_is_functional_template__$6, __vue_module_identifier__$6, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //


  var script$7 = {
    name: 'advanced-filter-select-property',
    props: {
      properties: {
        type: Object,
        default: function _default() {
          return {};
        }
      },
      selected: {
        type: String,
        default: 'identity'
      },
      dataType: {
        type: String,
        default: ''
      }
    },
    data: function data() {
      return {
        value: this.selected,
        options: this.properties
      };
    },
    methods: {
      updateSelected: function updateSelected(event) {
        this.value = event.target.value;
        this.$emit('updateSelected', event.target.value);
      },
      formatType: function formatType() {
        var that = this;
        var type = that.dataType;
        var formatted = type;
        if (type.includes('array') && type.length > 5) {
          formatted = type.replace('array', 'array: ');
        }
        return formatted;
      }
    },
    computed: {
      type: function type() {
        return this.formatType();
      }
    }
  };

  /* script */
  var __vue_script__$7 = script$7;

  /* template */
  var __vue_render__$7 = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", {}, [_c("select", {
      on: {
        change: function change($event) {
          return _vm.updateSelected($event);
        }
      }
    }, _vm._l(_vm.options, function (val, key, index) {
      return _c("option", { domProps: { value: key, selected: key == _vm.value } }, [_vm._v("\n      " + _vm._s(val.text) + "\n    ")]);
    }), 0), _vm._v(" "), _c("span", [_vm._v(_vm._s(_vm.type))])]);
  };
  var __vue_staticRenderFns__$7 = [];
  __vue_render__$7._withStripped = true;

  /* style */
  var __vue_inject_styles__$7 = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-4e98e0c2_0", { source: "\nspan[data-v-4e98e0c2] {\n  display: block;\n  color: RGB(202, 149, 103);\n  font-size: 10px;\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/AdvancedFilter/AdvancedFilterSelectProperty/AdvancedFilterSelectProperty.vue"], "names": [], "mappings": ";AAkEA;EACA,cAAA;EACA,yBAAA;EACA,eAAA;AACA", "file": "AdvancedFilterSelectProperty.vue", "sourcesContent": ["<template>\n  <div class=\"\">\n    <select @change='updateSelected($event)'>\n      <option\n        v-for='(val, key, index) in options'\n        :value='key'\n        :selected='key == value'\n      >\n        {{val.text}}\n      </option>\n    </select>\n    <span>{{type}}</span>\n  </div>\n\n</template>\n\n<script>\n\nexport default {\n  name:'advanced-filter-select-property',\n  props: {\n    properties: {\n      type: Object,\n      default: function() {\n        return {};\n      }\n    },\n    selected: {\n      type: String,\n      default: 'identity'\n    },\n    dataType: {\n      type: String,\n      default: ''\n    }\n  },\n  data: function() {\n    return {\n      value: this.selected,\n      options: this.properties\n    };\n  },\n  methods: {\n    updateSelected: function(event) {\n      this.value = event.target.value;\n      this.$emit('updateSelected', event.target.value);\n    },\n    formatType: function() {\n      let that = this;\n      let type = that.dataType;\n      let formatted = type;\n      if (type.includes('array') && type.length > 5) {\n        formatted = type.replace('array', 'array: ');\n      }\n      return formatted;\n    }\n  },\n  computed: {\n    type: function() {\n      return this.formatType();\n    }\n  }\n};\n</script>\n\n<style scoped>\nspan {\n  display: block;\n  color: RGB(202, 149, 103);\n  font-size: 10px;\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$7 = "data-v-4e98e0c2";
  /* module identifier */
  var __vue_module_identifier__$7 = undefined;
  /* functional template */
  var __vue_is_functional_template__$7 = false;
  /* style inject SSR */

  var AdvancedFilterSelectProperty = normalizeComponent_1({ render: __vue_render__$7, staticRenderFns: __vue_staticRenderFns__$7 }, __vue_inject_styles__$7, __vue_script__$7, __vue_scope_id__$7, __vue_is_functional_template__$7, __vue_module_identifier__$7, browser, undefined);

  //

  var script$8 = {

    components: {
      AdvancedFilterSelectConditional: AdvancedFilterSelectConditional,
      AdvancedFilterSelectLogic: AdvancedFilterSelectLogic,
      AdvancedFilterSelectFunction: AdvancedFilterSelectFunction,
      AdvancedFilterSelectProperty: AdvancedFilterSelectProperty,
      AdvancedFilterInput: AdvancedFilterInput,
      ButtonMinusToClose: ButtonMinusToClose
    },

    props: {

      index: { type: Number },

      filter: {
        type: Object,
        default: function _default() {
          return {};
        },
        validator: function validator(value) {
          var keys = Object.keys(value);

          if (!keys.includes('logic')) {
            value.logic = 'and';
          }

          if (!keys.includes('function')) {
            value.function = 'identity';
          }

          if (!keys.includes('conditional')) {
            value.conditional = 'eq';
          }

          if (!keys.includes('input')) {
            value.input = '';
          }

          if (!keys.includes('property')) {
            value.property = '';
          }
          return true;
        }
      },

      properties: {
        type: Object,
        default: function _default() {
          return {};
        },
        validator: function validator(properties) {
          var props = Object.keys(properties);
          return props.every(function (prop) {
            var propKeys = Object.keys(properties[prop]);
            var conditional = propKeys.includes('exampleValue') && propKeys.includes('exampleValueType') && propKeys.includes('text');
            return conditional;
          });
        }
      }
    },

    data: function data() {
      return {
        logic: this.filter.logic,
        function: this.filter.function,
        conditional: this.filter.conditional,
        input: this.filter.input,
        property: this.filter.property
      };
    },
    methods: {
      removeMe: function removeMe() {
        this.$emit('removeThisFilter');
      },

      applyFilter: function applyFilter() {
        var filt = this.filter;
        // var f = functions[filt.function].func;

        if (this.filter.input === '') {
          return;
        }

        this.$emit('updateFilter', filt);
        //
        // if (filt.input != '') {
        //   this.$emit('updateFilter', filt);
        // } else {
        //   this.$emit('noInput', filt, this.index)
        // }
      },

      updateSelectLogic: function updateSelectLogic(newSelected) {
        this.filter.logic = newSelected;
        this.logic = newSelected;
        this.applyFilter();
        // this.$emit('updateFilter', this.filter, this.index);
      },

      updateSelectFunction: function updateSelectFunction(newSelected) {
        this.filter.function = newSelected;
        this.function = newSelected;
        this.applyFilter();
        // this.$emit('updateFilter', this.filter, this.index);
      },

      updateSelectProperty: function updateSelectProperty(newSelected) {
        this.filter.property = newSelected;
        this.property = newSelected;
        this.applyFilter();
        // this.$emit('updateFilter', this.filter, this.index);
      },

      updateSelectConditional: function updateSelectConditional(newSelected) {
        this.filter.conditional = newSelected;
        this.conditional = newSelected;
        this.applyFilter();
        // this.$emit('updateFilter', this.filter, this.index);
      },

      updateInput: function updateInput(newInput) {
        this.filter.input = newInput;
        this.input = newInput;
        this.applyFilter();
        // this.$emit('updateFilter', this.filter, this.index);
      }
    },
    computed: {
      propertyData: function propertyData() {
        var that = this;
        var prop = that.property;
        var propData = that.properties[prop];
        return propData;
      },
      propertyType: function propertyType() {
        var that = this;
        return that.propertyData.exampleValueType;
      },
      applicableConditionalType: function applicableConditionalType() {
        var that = this;
        var type = that.propertyType;
        if (type.includes('array')) {
          if (['min', 'max', 'mean', 'length'].includes(that.function)) {
            type = 'number';
          }
        }
        return type;
      }
    }
  };

  /* script */
  var __vue_script__$8 = script$8;

  /* template */
  var __vue_render__$8 = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("tr", [_c("td", [_vm.index != 0 ? _c("advanced-filter-select-logic", {
      attrs: { selected: _vm.filter.logic },
      on: { updateSelected: _vm.updateSelectLogic }
    }) : _vm._e()], 1), _vm._v(" "), _c("td", [_c("advanced-filter-select-function", {
      attrs: {
        selected: _vm.filter.function,
        "data-type": _vm.propertyType
      },
      on: { updateSelected: _vm.updateSelectFunction }
    })], 1), _vm._v(" "), _c("td", [_c("advanced-filter-select-property", {
      attrs: {
        selected: _vm.filter.property,
        properties: _vm.properties,
        "data-type": _vm.propertyType
      },
      on: { updateSelected: _vm.updateSelectProperty }
    })], 1), _vm._v(" "), _c("td", [_c("advanced-filter-select-conditional", {
      attrs: {
        selected: _vm.filter.conditional,
        "data-type": _vm.applicableConditionalType
      },
      on: { updateSelected: _vm.updateSelectConditional }
    })], 1), _vm._v(" "), _c("td", [_c("advanced-filter-input", {
      attrs: { value: _vm.filter.input },
      on: { updateInput: _vm.updateInput }
    })], 1), _vm._v(" "), _c("td", [_c("button-minus-to-close", { on: { minus: _vm.removeMe } })], 1)]);
  };
  var __vue_staticRenderFns__$8 = [];
  __vue_render__$8._withStripped = true;

  /* style */
  var __vue_inject_styles__$8 = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-3a642ea1_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: { "version": 3, "sources": [], "names": [], "mappings": "", "file": "AdvancedFilterRow.vue" }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$8 = "data-v-3a642ea1";
  /* module identifier */
  var __vue_module_identifier__$8 = undefined;
  /* functional template */
  var __vue_is_functional_template__$8 = false;
  /* style inject SSR */

  var AdvancedFilterRow = normalizeComponent_1({ render: __vue_render__$8, staticRenderFns: __vue_staticRenderFns__$8 }, __vue_inject_styles__$8, __vue_script__$8, __vue_scope_id__$8, __vue_is_functional_template__$8, __vue_module_identifier__$8, browser, undefined);

  var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) {
    return typeof obj;
  } : function (obj) {
    return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj;
  };

  var slicedToArray = function () {
    function sliceIterator(arr, i) {
      var _arr = [];
      var _n = true;
      var _d = false;
      var _e = undefined;

      try {
        for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) {
          _arr.push(_s.value);

          if (i && _arr.length === i) break;
        }
      } catch (err) {
        _d = true;
        _e = err;
      } finally {
        try {
          if (!_n && _i["return"]) _i["return"]();
        } finally {
          if (_d) throw _e;
        }
      }

      return _arr;
    }

    return function (arr, i) {
      if (Array.isArray(arr)) {
        return arr;
      } else if (Symbol.iterator in Object(arr)) {
        return sliceIterator(arr, i);
      } else {
        throw new TypeError("Invalid attempt to destructure non-iterable instance");
      }
    };
  }();

  var toConsumableArray = function (arr) {
    if (Array.isArray(arr)) {
      for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) arr2[i] = arr[i];

      return arr2;
    } else {
      return Array.from(arr);
    }
  };

  function determineType(data) {
    // start with JS basics
    var type = typeof data === 'undefined' ? 'undefined' : _typeof(data);

    /*
    number and string are sufficient to know functions and conditionals
    can be applied to them.
    */
    if (['number', 'string'].includes(type)) {
      return type;
    }

    /*
    if not a number or string, it might be an array.
    What the elements are of those array might modify what functions
    can be applied. E.g. an array of numbers allows for min, max, mean,
    etc to be applied. Whereas an array of strings does not.
    */
    if (type != 'object') {
      return type;
    }

    /* only dealing with object types at this point */
    if (Array.isArray(data)) {
      var _type = 'array';

      var typeofElements = data.map(function (e) {
        return typeof e === 'undefined' ? 'undefined' : _typeof(e);
      });
      var typeofElement = typeofElements[0];
      var allSameQ = typeofElements.every(function (e) {
        return e == typeofElement;
      });

      if (allSameQ) {
        _type += typeofElement;
      }

      return _type;
    }

    return type;
  }

  //

  var script$9 = {
    components: { AdvancedFilterRow: AdvancedFilterRow, ButtonMinusToClose: ButtonMinusToClose, ButtonPlus: ButtonPlus },

    props: {
      data: {
        default: function _default() {},
        type: Object
      }
    },
    data: function data() {
      return {
        filtered: Object.keys(this.data),
        filters: [],
        records: this.data
      };
    },
    computed: {
      recordFields: function recordFields() {
        var _ref;

        var that = this;

        var allRecordFields = (_ref = []).concat.apply(_ref, toConsumableArray(that.recordIds.map(function (recordId) {
          return Object.keys(that.records[recordId]);
        })));
        return [].concat(toConsumableArray(new Set(allRecordFields)));
      },
      recordIds: function recordIds() {
        return Object.keys(this.records);
      },
      numberOfRecords: function numberOfRecords() {
        return this.filtered.length;
      },
      properties: function properties() {
        var that = this;
        var recordFields = that.recordFields;
        var recordIds = that.recordIds;
        var recordFieldsData = {};

        recordFields.map(function (field) {

          var fieldData = {};

          var exampleRecord, exampleFieldValue;
          for (var i = 0; i < recordIds.length; i++) {
            var currentRecordId = recordIds[i];
            exampleRecord = that.records[currentRecordId];
            exampleFieldValue = exampleRecord[field];
            if (exampleFieldValue) {
              break;
            }
          }

          fieldData.exampleValue = exampleFieldValue;
          fieldData.exampleValueType = that.determineType(exampleFieldValue);
          fieldData.text = field;
          recordFieldsData[field] = fieldData;
        });
        return recordFieldsData;
      }
    },
    methods: {
      addFilter: function addFilter() {
        var that = this;
        that.filters.push({
          property: that.recordFields[0]
        });
      },
      removeAll: function removeAll() {},
      filterData: function filterData() {
        var that = this;
        var recordIds = that.recordIds;
        var filters = this.filters;
        var filterGroups = [];

        for (var i = 0; i < filters.length; i++) {
          var logic = filters[i].logic;
          if (logic == 'and') {
            filterGroups.push([filters[i]]);
          } else {
            filterGroups[filterGroups.length - 1].push(filters[i]);
          }
        }

        var filtered = recordIds.filter(function (id) {
          var record = that.records[id];
          var and = filterGroups.map(function (filts) {
            var or = filts.some(function (filt) {
              var f = functions[filt.function].func;
              var c = conditionals[filt.conditional].cond;
              var p = record[filt.property];
              var i = filt.input;
              var fx = f(p);
              return c(fx, i);
            });
            return or;
          });
          return and.every(function (e) {
            return e;
          });
        });
        return filtered;
      },
      updateFilter: function updateFilter(filter) {
        // let filters = this.filters;
        this.filtered = this.filterData();
      },

      determineType: determineType
    }
  };

  /* script */
  var __vue_script__$9 = script$9;

  /* template */
  var __vue_render__$9 = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { staticClass: "table-responsive" }, [_c("table", { staticClass: "table" }, [_c("thead", [_c("tr", [_c("th", { attrs: { scope: "col" } }, [_vm.filters.length > 1 ? _c("span", [_vm._v("Logic")]) : _vm._e()]), _vm._v(" "), _c("th", { attrs: { scope: "col" } }, [_vm._v("Function")]), _vm._v(" "), _c("th", { attrs: { scope: "col" } }, [_vm._v("Property")]), _vm._v(" "), _c("th", { attrs: { scope: "col" } }, [_vm._v("Conditional")]), _vm._v(" "), _c("th", { attrs: { scope: "col" } }, [_vm._v("Value")]), _vm._v(" "), _c("th", { attrs: { scope: "col" } }, [_c("button-minus-to-close", {
      on: {
        minus: function minus($event) {
          _vm.filters = [];
        }
      }
    })], 1)])]), _vm._v(" "), _c("tbody", [_vm._l(_vm.filters, function (filter, index) {
      return _c("advanced-filter-row", {
        key: filter.id,
        attrs: {
          index: index,
          filter: filter,
          properties: _vm.properties
        },
        on: {
          removeThisFilter: function removeThisFilter($event) {
            return _vm.filters.splice(index, 1);
          },
          updateFilter: _vm.updateFilter
        }
      });
    }), _vm._v(" "), _c("tr", [_c("th", [_c("button-plus", { on: { plus: _vm.addFilter } })], 1), _vm._v(" "), _c("th"), _c("th"), _c("th"), _c("th"), _c("th")])], 2), _vm._v(" "), _c("caption", [_vm._v("Number of records: " + _vm._s(_vm.numberOfRecords))])]), _vm._v(" "), _vm._l(_vm.filtered, function (k, i) {
      return _c("div", [_vm._v("\n    " + _vm._s(k) + ": " + _vm._s(_vm.records[k]) + "\n  ")]);
    })], 2);
  };
  var __vue_staticRenderFns__$9 = [];
  __vue_render__$9._withStripped = true;

  /* style */
  var __vue_inject_styles__$9 = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-277f285c_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: { "version": 3, "sources": [], "names": [], "mappings": "", "file": "AdvancedFilterTable.vue" }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$9 = "data-v-277f285c";
  /* module identifier */
  var __vue_module_identifier__$9 = undefined;
  /* functional template */
  var __vue_is_functional_template__$9 = false;
  /* style inject SSR */

  var AdvancedFilterTable = normalizeComponent_1({ render: __vue_render__$9, staticRenderFns: __vue_staticRenderFns__$9 }, __vue_inject_styles__$9, __vue_script__$9, __vue_scope_id__$9, __vue_is_functional_template__$9, __vue_module_identifier__$9, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  var script$a = {
    name: 'anchor-button',
    props: {
      iconQ: {
        type: Boolean,
        default: false
      },
      src: {
        type: String,
        default: ''
      },
      text: {
        type: String,
        default: 'Button'
      },
      href: {
        type: String,
        default: '#'
      },
      color: {
        type: String,
        default: ''
      }
    }
  };

  /* script */
  var __vue_script__$a = script$a;

  /* template */
  var __vue_render__$a = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("a", { staticClass: "button", class: [{ "has-icon": _vm.iconQ }, _vm.color] }, [_vm.iconQ ? _c("img", { attrs: { src: _vm.src } }) : _vm._e(), _vm._v("\n" + _vm._s(_vm.text) + "\n")]);
  };
  var __vue_staticRenderFns__$a = [];
  __vue_render__$a._withStripped = true;

  /* style */
  var __vue_inject_styles__$a = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-61c72258_0", { source: "\na[data-v-61c72258] {\n  text-decoration: none;\n  color: #34495e;\n  font-family: 'Source Sans Pro', 'Helvetica Neue', Arial, sans-serif;\n  font-size: 1.05em;\n    font-weight: 600;\n}\na.button[data-v-61c72258] {\n  padding: 0.75em 2em;\n  border-radius: 2em;\n  display: inline-block;\n\n  transition: all 0.15s ease;\n  box-sizing: border-box;\n  border: 1px solid;\n}\n.has-icon[data-v-61c72258] {\n  position: relative;\n  text-indent: 1.4em;\n  width: 2em;\n}\nimg[data-v-61c72258] {\n  /* width: 30px;\n  height:30px; */\n  position: absolute;\n  left: 0.4em;\n  top: 0.4em;\n  width: 2em;\n}\n.button[data-v-61c72258] {\n  margin: 1em 0;\n  font-size: 1.05em;\n  font-weight: 600;\n  letter-spacing: 0.1em;\n  min-width: 8em;\n  text-align: center;\n}\n.vue-green[data-v-61c72258] {\n  color: #fff !important;\n  background-color: #4fc08d !important;\n  border-color: #4fc08d !important;\n}\n.vue-white[data-v-61c72258] {\n  color: #42b983 !important;\n  background-color: #fff !important;\n  border-color: #4fc08d !important;\n}\n.vue-gray[data-v-61c72258] {\n  color: #7f8c8d !important;\n  background-color: #f6f6f6 !important;\n  border-color: #f6f6f6 !important;\n}\n.moz-blue[data-v-61c72258] {\n  color: #fff !important;;\n  background: #306efe linear-gradient(120deg,#30cde2 0,#306efe 100%) no-repeat !important;\n  border: 0 !important;\n\n  /* border-color: transparent !important; */\n  /* border-radius: 100px; */\n}\n.moz-blue-hollow[data-v-61c72258] {\n  color: #306efe!important;\n  border-color: #306efe!important;\n  /* border-radius: 100px!important; */\n  background-color: transparent !important;\n}\n.moz-white-hollow[data-v-61c72258] {\n  color: #fff!important;\n  border-color: #fff!important;\n  /* border-radius: 100px!important; */\n  background-color: transparent !important;\n}\n@media screen and (max-width: 900px) .button {\n  font-size: 0.9em !important;\nimg[data-v-61c72258] {\n    width: 20px;\n    height:20px;\n}\n}\n\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/AnchorButton/AnchorButton.vue"], "names": [], "mappings": ";AAuCA;EACA,qBAAA;EACA,cAAA;EACA,mEAAA;EACA,iBAAA;IACA,gBAAA;AACA;AAEA;EACA,mBAAA;EACA,kBAAA;EACA,qBAAA;;EAEA,0BAAA;EACA,sBAAA;EACA,iBAAA;AACA;AAEA;EACA,kBAAA;EACA,kBAAA;EACA,UAAA;AAEA;AAEA;EACA;gBACA;EACA,kBAAA;EACA,WAAA;EACA,UAAA;EACA,UAAA;AACA;AAEA;EACA,aAAA;EACA,iBAAA;EACA,gBAAA;EACA,qBAAA;EACA,cAAA;EACA,kBAAA;AACA;AAMA;EACA,sBAAA;EACA,oCAAA;EACA,gCAAA;AACA;AAEA;EACA,yBAAA;EACA,iCAAA;EACA,gCAAA;AACA;AAEA;EACA,yBAAA;EACA,oCAAA;EACA,gCAAA;AACA;AAEA;EACA,sBAAA;EACA,uFAAA;EACA,oBAAA;;EAEA,0CAAA;EACA,0BAAA;AACA;AAEA;EACA,wBAAA;EACA,+BAAA;EACA,oCAAA;EACA,wCAAA;AACA;AAEA;EACA,qBAAA;EACA,4BAAA;EACA,oCAAA;EACA,wCAAA;AACA;AAEA;EACA,2BAAA;AACA;IACA,WAAA;IACA,WAAA;AACA;AACA", "file": "AnchorButton.vue", "sourcesContent": ["<template>\n  <a\n    class='button'\n    :class='[{ \"has-icon\": iconQ}, color]'\n  >\n  <img v-if='iconQ' :src='src'></img>\n  {{text}}\n  </a>\n</template>\n\n<script>\nexport default {\n  name:'anchor-button',\n  props: {\n    iconQ: {\n      type: Boolean,\n      default: false\n    },\n    src: {\n      type: String,\n      default: ''\n    },\n    text: {\n      type: String,\n      default: 'Button'\n    },\n    href: {\n      type: String,\n      default: '#'\n    },\n    color: {\n      type:String,\n      default: ''\n    }\n  },\n};\n</script>\n\n<style scoped>\na {\n  text-decoration: none;\n  color: #34495e;\n  font-family: 'Source Sans Pro', 'Helvetica Neue', Arial, sans-serif;\n  font-size: 1.05em;\n    font-weight: 600;\n}\n\na.button {\n  padding: 0.75em 2em;\n  border-radius: 2em;\n  display: inline-block;\n\n  transition: all 0.15s ease;\n  box-sizing: border-box;\n  border: 1px solid;\n}\n\n.has-icon {\n  position: relative;\n  text-indent: 1.4em;\n  width: 2em;\n\n}\n\nimg {\n  /* width: 30px;\n  height:30px; */\n  position: absolute;\n  left: 0.4em;\n  top: 0.4em;\n  width: 2em;\n}\n\n.button {\n  margin: 1em 0;\n  font-size: 1.05em;\n  font-weight: 600;\n  letter-spacing: 0.1em;\n  min-width: 8em;\n  text-align: center;\n}\n\n\n\n\n\n.vue-green {\n  color: #fff !important;\n  background-color: #4fc08d !important;\n  border-color: #4fc08d !important;\n}\n\n.vue-white {\n  color: #42b983 !important;\n  background-color: #fff !important;\n  border-color: #4fc08d !important;\n}\n\n.vue-gray {\n  color: #7f8c8d !important;\n  background-color: #f6f6f6 !important;\n  border-color: #f6f6f6 !important;\n}\n\n.moz-blue {\n  color: #fff !important;;\n  background: #306efe linear-gradient(120deg,#30cde2 0,#306efe 100%) no-repeat !important;\n  border: 0 !important;\n\n  /* border-color: transparent !important; */\n  /* border-radius: 100px; */\n}\n\n.moz-blue-hollow {\n  color: #306efe!important;\n  border-color: #306efe!important;\n  /* border-radius: 100px!important; */\n  background-color: transparent !important;\n}\n\n.moz-white-hollow {\n  color: #fff!important;\n  border-color: #fff!important;\n  /* border-radius: 100px!important; */\n  background-color: transparent !important;\n}\n\n@media screen and (max-width: 900px) .button {\n  font-size: 0.9em !important;\n  img {\n    width: 20px;\n    height:20px;\n  }\n}\n\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$a = "data-v-61c72258";
  /* module identifier */
  var __vue_module_identifier__$a = undefined;
  /* functional template */
  var __vue_is_functional_template__$a = false;
  /* style inject SSR */

  var AnchorButton = normalizeComponent_1({ render: __vue_render__$a, staticRenderFns: __vue_staticRenderFns__$a }, __vue_inject_styles__$a, __vue_script__$a, __vue_scope_id__$a, __vue_is_functional_template__$a, __vue_module_identifier__$a, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //

  var script$b = {
    props: {
      type: {
        type: String,
        default: 'secondary'
      },
      error: {
        type: String,
        default: 'error'
      },
      message: {
        type: String,
        default: ''
      }
    }
  };

  /* script */
  var __vue_script__$b = script$b;

  /* template */
  var __vue_render__$b = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { class: "bg-" + _vm.type, attrs: { role: "alert-box" } }, [_c("strong", { domProps: { innerHTML: _vm._s(_vm.error) } }), _vm._v(" "), _c("span", { domProps: { innerHTML: _vm._s(_vm.message) } }), _vm._v(" "), _c("button", {
      on: {
        click: function click($event) {
          return _vm.$emit("remove");
        }
      }
    }, [_vm._v("×")])]);
  };
  var __vue_staticRenderFns__$b = [];
  __vue_render__$b._withStripped = true;

  /* style */
  var __vue_inject_styles__$b = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-39861ab4_0", { source: "div[data-v-39861ab4] {\n  -webkit-animation: Animate-data-v-39861ab4 10s ease infinite;\n  -moz-animation: Animate-data-v-39861ab4 10s ease infinite;\n  animation: Animate-data-v-39861ab4 10s ease infinite;\n  padding: 5px 10px;\n  border-radius: 5px;\n}\n.bg-warning[data-v-39861ab4] {\n  background: #f2994a; /* fallback for old browsers */\n  background: -webkit-linear-gradient(\n    to right,\n    #f2c94c,\n    #f2994a\n  ); /* Chrome 10-25, Safari 5.1-6 */\n  background: linear-gradient(\n    to right,\n    #f2c94c,\n    #f2994a\n  ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */\n}\n.text-warning[data-v-39861ab4] {\n  color: #f2994a; /* fallback for old browsers */\n  color: -webkit-linear-gradient(\n    to right,\n    #f2c94c,\n    #f2994a\n  ); /* Chrome 10-25, Safari 5.1-6 */\n  color: linear-gradient(\n    to right,\n    #f2c94c,\n    #f2994a\n  ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */\n}\n.bg-danger[data-v-39861ab4] {\n  background: #cb2d3e; /* fallback for old browsers */\n  background: -webkit-linear-gradient(\n    to bottom,\n    #cb2d3e,\n    #ef473a\n  ); /* Chrome 10-25, Safari 5.1-6 */\n  background: linear-gradient(\n    to bottom,\n    #cb2d3e,\n    #ef473a\n  ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */\n}\n.text-danger[data-v-39861ab4] {\n  color: #cb2d3e; /* fallback for old browsers */\n  color: -webkit-linear-gradient(\n    to bottom,\n    #cb2d3e,\n    #ef473a\n  ); /* Chrome 10-25, Safari 5.1-6 */\n  color: linear-gradient(\n    to bottom,\n    #cb2d3e,\n    #ef473a\n  ); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */\n}\nstrong[data-v-39861ab4]:after {\n  content: \": \";\n}\nbutton[data-v-39861ab4] {\n  font-weight: bold;\n  border: solid 1px transparent;\n  border-radius: 10px;\n  /* float: right; */\n  padding: 0px;\n  margin: 0px;\n}\nbutton[data-v-39861ab4]:hover {\n  opacity: 0.8;\n  -webkit-animation: Animate-data-v-39861ab4 10s ease infinite;\n  -moz-animation: Animate-data-v-39861ab4 10s ease infinite;\n  animation: Animate-data-v-39861ab4 10s ease infinite;\n}\n@-webkit-keyframes Animate-data-v-39861ab4 {\n0% {\n    background-position: 0% 50%;\n}\n50% {\n    background-position: 100% 50%;\n}\n100% {\n    background-position: 0% 50%;\n}\n}\n@-moz-keyframes Animate-data-v-39861ab4 {\n0% {\n    background-position: 0% 50%;\n}\n50% {\n    background-position: 100% 50%;\n}\n100% {\n    background-position: 0% 50%;\n}\n}\n@keyframes Animate-data-v-39861ab4 {\n0% {\n    background-position: 0% 50%;\n}\n50% {\n    background-position: 100% 50%;\n}\n100% {\n    background-position: 0% 50%;\n}\n}\n", map: undefined, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$b = "data-v-39861ab4";
  /* module identifier */
  var __vue_module_identifier__$b = undefined;
  /* functional template */
  var __vue_is_functional_template__$b = false;
  /* style inject SSR */

  var Alert = normalizeComponent_1({ render: __vue_render__$b, staticRenderFns: __vue_staticRenderFns__$b }, __vue_inject_styles__$b, __vue_script__$b, __vue_scope_id__$b, __vue_is_functional_template__$b, __vue_module_identifier__$b, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  var script$c = {
    components: {},

    props: ['filterText'],

    data: function data() {
      return {
        filter: this.filterText
      };
    },

    computed: {},

    methods: {
      input: function input(event) {
        this.filter = event.target.value;
        this.$emit('update:filterText', event.target.value);
      },
      clearFilter: function clearFilter() {
        this.filter = '';
        this.$emit('update:filterText', '');
      }
    }
  };

  /* script */
  var __vue_script__$c = script$c;

  /* template */
  var __vue_render__$c = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { staticClass: "filter" }, [_c("input", {
      staticClass: "data-table-filter",
      attrs: { type: "text", placeholder: "filter..." },
      domProps: { value: _vm.filter },
      on: {
        input: function input($event) {
          return _vm.input($event);
        }
      }
    }), _vm._v(" "), _vm.filter ? _c("span", {
      staticClass: "data-table-filter-clear",
      on: { click: _vm.clearFilter }
    }, [_vm._v("×")]) : _vm._e()]);
  };
  var __vue_staticRenderFns__$c = [];
  __vue_render__$c._withStripped = true;

  /* style */
  var __vue_inject_styles__$c = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-3cade9da_0", { source: ".data-table[data-v-3cade9da] {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.table-wrapper[data-v-3cade9da] {\n  /* overflow-x: scroll; */\n}\n.caption-row[data-v-3cade9da] {\n  display:inline-block; width:100%;\n}\n.table[data-v-3cade9da] {\n  display: flex;\n  flex-direction: column;\n\n  margin: 0 auto 25px;\n  border-collapse: collapse;\n  border-bottom: 2px solid #007593;\n  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1), 0px 10px 20px rgba(0, 0, 0, 0.05),\n    0px 20px 20px rgba(0, 0, 0, 0.05), 0px 30px 20px rgba(0, 0, 0, 0.05);\n  background-color: #e0e0e0;\n}\n.thead[data-v-3cade9da],\n.tfoot[data-v-3cade9da] {\n  background-color: #e0e0e0;\n  color: RGB(52, 58, 64);\n  font-weight: bold;\n  font-size: 1em;\n  cursor: pointer;\n  user-select: none;\n  display: flex;\n  flex-direction: column;\n  font-size: 1em;\n}\n.thead[data-v-3cade9da] {\n  /* text-decoration: underline; */\n}\n.thead .tr .th[data-v-3cade9da]:after {\n  text-decoration: none;\n}\n.tbody[data-v-3cade9da] {\n  display: block;\n  max-height: 250px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  flex-direction: column;\n  font-size: 0.85em;\n}\n.tfoot[data-v-3cade9da] {\n}\n.td[data-v-3cade9da],\n.th[data-v-3cade9da] {\n  word-wrap: break-word;\n  word-break: break-all;\n  text-overflow: ellipsis;\n  width: 100%;\n  flex-basis: 0;\n  flex-grow: 1;\n  border-collapse: collapse;\n  margin: 0px;\n  border: solid transparent 0px;\n  padding: 0px 5px;\n}\n.tr[data-v-3cade9da] {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  width: 100%;\n}\n.tbody .tr[data-v-3cade9da]:nth-child(even) {\n  background-color: RGB(242, 242, 242);\n}\n.tbody .tr[data-v-3cade9da]:nth-child(odd) {\n  background-color: #ffffff;\n}\n.thead.tr[data-v-3cade9da],\n.tfoot.tr[data-v-3cade9da] {\n  width: calc(100% - 15px);\n}\n.td[data-v-3cade9da] {\n}\n.th[data-v-3cade9da] {\n}\n.td[data-v-3cade9da]:hover {\n  background-color: RGB(184, 192, 200);\n}\n/* .td:not(:hover) {\n  opacity: 0.5;\n} */\n.title[data-v-3cade9da] {\n  float: left;\n  font-size: 1.25em;\n  color: black;\n  font-weight: bold;\n}\n@media (max-width: 800px) {\n.tr[data-v-3cade9da] {\n    flex-direction: column !important;\n}\n.tbody .tr[data-v-3cade9da] {\n    border-bottom: solid 2px black !important;\n}\n.tbody .td[data-v-3cade9da] {\n    border-bottom: solid 1px RGBA(1, 1, 1, 0.2) !important;\n}\n.tfoot[data-v-3cade9da],\n  .thead[data-v-3cade9da] {\n    font-size: 0.8em;\n}\n.tbody[data-v-3cade9da] {\n    font-size: 0.7em;\n}\n}\n.selectable-checkbox[data-v-3cade9da] {\n  align-self: center;\n  text-align: center;\n}\n.tpage[data-v-3cade9da] {\n}\n", map: undefined, media: undefined }), inject("data-v-3cade9da_1", { source: "\ndiv.filter[data-v-3cade9da] {\n  /* align-self: flex-end; */\n  text-align: right;\n  position: relative;\n}\n.data-table-filter[data-v-3cade9da] {\n  padding: 0 1.25em 0 0.75em;\n  height: 2em;\n  border: solid 2px #e0e0e0;\n  border-radius: 2em;\n  font-size: 0.85em;\n}\n.data-table-filter-caption[data-v-3cade9da] {\n  text-align: right;\n  position: relative;\n  font-size: 12px;\n  color: grey;\n}\n.data-table-filter[data-v-3cade9da]:focus {\n  outline: 0;\n  border-color: #007593;\n}\n.data-table-filter-clear[data-v-3cade9da] {\n  position: absolute;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 2em;\n  color: #007593;\n  font-weight: bold;\n  cursor: pointer;\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/DataTable/DataTableFilter/DataTableFilter.vue"], "names": [], "mappings": ";AA8CA;EACA,0BAAA;EACA,iBAAA;EACA,kBAAA;AACA;AAEA;EACA,0BAAA;EACA,WAAA;EACA,yBAAA;EACA,kBAAA;EACA,iBAAA;AACA;AAEA;EACA,iBAAA;EACA,kBAAA;EACA,eAAA;EACA,WAAA;AACA;AAEA;EACA,UAAA;EACA,qBAAA;AACA;AAEA;EACA,kBAAA;EACA,MAAA;EACA,QAAA;EACA,SAAA;EACA,aAAA;EACA,mBAAA;EACA,uBAAA;EACA,UAAA;EACA,cAAA;EACA,iBAAA;EACA,eAAA;AACA", "file": "DataTableFilter.vue", "sourcesContent": ["<template >\n  <div class='filter'>\n    <input\n      class='data-table-filter'\n      type='text'\n      :value='filter'\n      @input='input($event)'\n      placeholder='filter...'\n    >\n    <span\n      v-if='filter'\n      class='data-table-filter-clear'\n      @click='clearFilter'\n    >&times;</span>\n  </div>\n</template>\n\n<script>\nexport default {\n  components: {},\n\n  props: ['filterText'],\n\n  data: function() {\n    return {\n      filter: this.filterText\n    };\n  },\n\n  computed: {},\n\n  methods: {\n    input: function(event) {\n      this.filter = event.target.value;\n      this.$emit('update:filterText', event.target.value);\n    },\n    clearFilter: function() {\n      this.filter = '';\n      this.$emit('update:filterText', '');\n    }\n  }\n};\n</script>\n\n<style scoped src='../DataTable/data-table-style.css'></style>\n<style scoped>\ndiv.filter {\n  /* align-self: flex-end; */\n  text-align: right;\n  position: relative;\n}\n\n.data-table-filter {\n  padding: 0 1.25em 0 0.75em;\n  height: 2em;\n  border: solid 2px #e0e0e0;\n  border-radius: 2em;\n  font-size: 0.85em;\n}\n\n.data-table-filter-caption {\n  text-align: right;\n  position: relative;\n  font-size: 12px;\n  color: grey;\n}\n\n.data-table-filter:focus {\n  outline: 0;\n  border-color: #007593;\n}\n\n.data-table-filter-clear {\n  position: absolute;\n  top: 0;\n  right: 0;\n  bottom: 0;\n  display: flex;\n  align-items: center;\n  justify-content: center;\n  width: 2em;\n  color: #007593;\n  font-weight: bold;\n  cursor: pointer;\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$c = "data-v-3cade9da";
  /* module identifier */
  var __vue_module_identifier__$c = undefined;
  /* functional template */
  var __vue_is_functional_template__$c = false;
  /* style inject SSR */

  var DataTableFilter = normalizeComponent_1({ render: __vue_render__$c, staticRenderFns: __vue_staticRenderFns__$c }, __vue_inject_styles__$c, __vue_script__$c, __vue_scope_id__$c, __vue_is_functional_template__$c, __vue_module_identifier__$c, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //

  var script$d = {
    name: 'data-table-cell',
    props: {
      recordId: { type: String },
      record: { type: Object },
      field: { type: String },
      fieldRenderFunctions: {
        default: function _default() {
          return {};
        },
        type: Object
      }
    },
    computed: {
      html: function html() {
        var r = this.record;
        var f = this.field;
        var i = this.recordId;
        var funcs = this.fieldRenderFunctions;

        if (funcs != undefined && f in funcs) {
          return funcs[f](i, r, f);
        } else {
          return r[f];
        }
      }
    }
  };

  /* script */
  var __vue_script__$d = script$d;

  /* template */
  var __vue_render__$d = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", {
      staticClass: "td",
      domProps: { innerHTML: _vm._s(_vm.html) }
    });
  };
  var __vue_staticRenderFns__$d = [];
  __vue_render__$d._withStripped = true;

  /* style */
  var __vue_inject_styles__$d = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-6022bf7d_0", { source: ".data-table[data-v-6022bf7d] {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.table-wrapper[data-v-6022bf7d] {\n  /* overflow-x: scroll; */\n}\n.caption-row[data-v-6022bf7d] {\n  display:inline-block; width:100%;\n}\n.table[data-v-6022bf7d] {\n  display: flex;\n  flex-direction: column;\n\n  margin: 0 auto 25px;\n  border-collapse: collapse;\n  border-bottom: 2px solid #007593;\n  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1), 0px 10px 20px rgba(0, 0, 0, 0.05),\n    0px 20px 20px rgba(0, 0, 0, 0.05), 0px 30px 20px rgba(0, 0, 0, 0.05);\n  background-color: #e0e0e0;\n}\n.thead[data-v-6022bf7d],\n.tfoot[data-v-6022bf7d] {\n  background-color: #e0e0e0;\n  color: RGB(52, 58, 64);\n  font-weight: bold;\n  font-size: 1em;\n  cursor: pointer;\n  user-select: none;\n  display: flex;\n  flex-direction: column;\n  font-size: 1em;\n}\n.thead[data-v-6022bf7d] {\n  /* text-decoration: underline; */\n}\n.thead .tr .th[data-v-6022bf7d]:after {\n  text-decoration: none;\n}\n.tbody[data-v-6022bf7d] {\n  display: block;\n  max-height: 250px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  flex-direction: column;\n  font-size: 0.85em;\n}\n.tfoot[data-v-6022bf7d] {\n}\n.td[data-v-6022bf7d],\n.th[data-v-6022bf7d] {\n  word-wrap: break-word;\n  word-break: break-all;\n  text-overflow: ellipsis;\n  width: 100%;\n  flex-basis: 0;\n  flex-grow: 1;\n  border-collapse: collapse;\n  margin: 0px;\n  border: solid transparent 0px;\n  padding: 0px 5px;\n}\n.tr[data-v-6022bf7d] {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  width: 100%;\n}\n.tbody .tr[data-v-6022bf7d]:nth-child(even) {\n  background-color: RGB(242, 242, 242);\n}\n.tbody .tr[data-v-6022bf7d]:nth-child(odd) {\n  background-color: #ffffff;\n}\n.thead.tr[data-v-6022bf7d],\n.tfoot.tr[data-v-6022bf7d] {\n  width: calc(100% - 15px);\n}\n.td[data-v-6022bf7d] {\n}\n.th[data-v-6022bf7d] {\n}\n.td[data-v-6022bf7d]:hover {\n  background-color: RGB(184, 192, 200);\n}\n/* .td:not(:hover) {\n  opacity: 0.5;\n} */\n.title[data-v-6022bf7d] {\n  float: left;\n  font-size: 1.25em;\n  color: black;\n  font-weight: bold;\n}\n@media (max-width: 800px) {\n.tr[data-v-6022bf7d] {\n    flex-direction: column !important;\n}\n.tbody .tr[data-v-6022bf7d] {\n    border-bottom: solid 2px black !important;\n}\n.tbody .td[data-v-6022bf7d] {\n    border-bottom: solid 1px RGBA(1, 1, 1, 0.2) !important;\n}\n.tfoot[data-v-6022bf7d],\n  .thead[data-v-6022bf7d] {\n    font-size: 0.8em;\n}\n.tbody[data-v-6022bf7d] {\n    font-size: 0.7em;\n}\n}\n.selectable-checkbox[data-v-6022bf7d] {\n  align-self: center;\n  text-align: center;\n}\n.tpage[data-v-6022bf7d] {\n}\n", map: undefined, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$d = "data-v-6022bf7d";
  /* module identifier */
  var __vue_module_identifier__$d = undefined;
  /* functional template */
  var __vue_is_functional_template__$d = false;
  /* style inject SSR */

  var DataTableCell = normalizeComponent_1({ render: __vue_render__$d, staticRenderFns: __vue_staticRenderFns__$d }, __vue_inject_styles__$d, __vue_script__$d, __vue_scope_id__$d, __vue_is_functional_template__$d, __vue_module_identifier__$d, browser, undefined);

  //

  var script$e = {
    name: 'data-table-row',
    props: {
      recordId: { type: String },
      record: { type: Object },
      fields: { type: Array },
      selectableQ: { type: Boolean, default: false },
      selected: {
        type: Array, default: function _default() {
          return [];
        }
      },
      fieldRenderFunctions: {
        default: function _default() {
          return {};
        },
        type: Object
      }
    },
    data: function data() {
      return {};
    },
    components: {
      DataTableCell: DataTableCell
    },
    computed: {
      visibleColumns: function visibleColumns() {
        return this.columns.filter(function (column) {
          return !column.hidden;
        });
      },
      selectedQ: function selectedQ() {
        var id = this.recordId;
        var selected = this.selected;
        return selected.includes(id);
      }
    }
  };

  /* script */
  var __vue_script__$e = script$e;

  /* template */
  var __vue_render__$e = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", {
      staticClass: "tr",
      on: {
        click: function click($event) {
          return _vm.$emit("row:click", _vm.recordId);
        }
      }
    }, [_vm.selectableQ ? _c("div", { staticClass: "td selectable-checkbox" }, [_c("input", {
      attrs: { type: "checkbox", name: "recordId" },
      domProps: { checked: _vm.selectedQ },
      on: {
        change: function change($event) {
          return _vm.$emit("toggledSelected", _vm.recordId);
        }
      }
    })]) : _vm._e(), _vm._v(" "), _vm._l(_vm.fields, function (field) {
      return _c("data-table-cell", {
        key: field.id,
        attrs: {
          record: _vm.record,
          recordId: _vm.recordId,
          field: field,
          fieldRenderFunctions: _vm.fieldRenderFunctions
        }
      });
    })], 2);
  };
  var __vue_staticRenderFns__$e = [];
  __vue_render__$e._withStripped = true;

  /* style */
  var __vue_inject_styles__$e = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-0a8fd946_0", { source: ".data-table[data-v-0a8fd946] {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.table-wrapper[data-v-0a8fd946] {\n  /* overflow-x: scroll; */\n}\n.caption-row[data-v-0a8fd946] {\n  display:inline-block; width:100%;\n}\n.table[data-v-0a8fd946] {\n  display: flex;\n  flex-direction: column;\n\n  margin: 0 auto 25px;\n  border-collapse: collapse;\n  border-bottom: 2px solid #007593;\n  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1), 0px 10px 20px rgba(0, 0, 0, 0.05),\n    0px 20px 20px rgba(0, 0, 0, 0.05), 0px 30px 20px rgba(0, 0, 0, 0.05);\n  background-color: #e0e0e0;\n}\n.thead[data-v-0a8fd946],\n.tfoot[data-v-0a8fd946] {\n  background-color: #e0e0e0;\n  color: RGB(52, 58, 64);\n  font-weight: bold;\n  font-size: 1em;\n  cursor: pointer;\n  user-select: none;\n  display: flex;\n  flex-direction: column;\n  font-size: 1em;\n}\n.thead[data-v-0a8fd946] {\n  /* text-decoration: underline; */\n}\n.thead .tr .th[data-v-0a8fd946]:after {\n  text-decoration: none;\n}\n.tbody[data-v-0a8fd946] {\n  display: block;\n  max-height: 250px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  flex-direction: column;\n  font-size: 0.85em;\n}\n.tfoot[data-v-0a8fd946] {\n}\n.td[data-v-0a8fd946],\n.th[data-v-0a8fd946] {\n  word-wrap: break-word;\n  word-break: break-all;\n  text-overflow: ellipsis;\n  width: 100%;\n  flex-basis: 0;\n  flex-grow: 1;\n  border-collapse: collapse;\n  margin: 0px;\n  border: solid transparent 0px;\n  padding: 0px 5px;\n}\n.tr[data-v-0a8fd946] {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  width: 100%;\n}\n.tbody .tr[data-v-0a8fd946]:nth-child(even) {\n  background-color: RGB(242, 242, 242);\n}\n.tbody .tr[data-v-0a8fd946]:nth-child(odd) {\n  background-color: #ffffff;\n}\n.thead.tr[data-v-0a8fd946],\n.tfoot.tr[data-v-0a8fd946] {\n  width: calc(100% - 15px);\n}\n.td[data-v-0a8fd946] {\n}\n.th[data-v-0a8fd946] {\n}\n.td[data-v-0a8fd946]:hover {\n  background-color: RGB(184, 192, 200);\n}\n/* .td:not(:hover) {\n  opacity: 0.5;\n} */\n.title[data-v-0a8fd946] {\n  float: left;\n  font-size: 1.25em;\n  color: black;\n  font-weight: bold;\n}\n@media (max-width: 800px) {\n.tr[data-v-0a8fd946] {\n    flex-direction: column !important;\n}\n.tbody .tr[data-v-0a8fd946] {\n    border-bottom: solid 2px black !important;\n}\n.tbody .td[data-v-0a8fd946] {\n    border-bottom: solid 1px RGBA(1, 1, 1, 0.2) !important;\n}\n.tfoot[data-v-0a8fd946],\n  .thead[data-v-0a8fd946] {\n    font-size: 0.8em;\n}\n.tbody[data-v-0a8fd946] {\n    font-size: 0.7em;\n}\n}\n.selectable-checkbox[data-v-0a8fd946] {\n  align-self: center;\n  text-align: center;\n}\n.tpage[data-v-0a8fd946] {\n}\n", map: undefined, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$e = "data-v-0a8fd946";
  /* module identifier */
  var __vue_module_identifier__$e = undefined;
  /* functional template */
  var __vue_is_functional_template__$e = false;
  /* style inject SSR */

  var DataTableRow = normalizeComponent_1({ render: __vue_render__$e, staticRenderFns: __vue_staticRenderFns__$e }, __vue_inject_styles__$e, __vue_script__$e, __vue_scope_id__$e, __vue_is_functional_template__$e, __vue_module_identifier__$e, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  var script$f = {
    name: 'data-table-header-cell',
    props: ['field', 'sort'],
    computed: {
      cellClass: function cellClass() {
        var field = this.field;
        var sort = this.sort;
        return this.classObject(field, sort);
      }
    },
    methods: {
      classObject: function classObject(field, sort) {
        var val = sort[field];
        var cls = {};

        if (!(field in sort)) {
          return cls;
        }
        if (val.order === 'ascending') {
          cls.asc = true;
        }
        if (val.order === 'descending') {
          cls.des = true;
        }

        return cls;
      }
    }
  };

  /* script */
  var __vue_script__$f = script$f;

  /* template */
  var __vue_render__$f = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", {
      staticClass: "th",
      class: _vm.cellClass,
      domProps: { innerHTML: _vm._s(_vm.field) },
      on: {
        click: function click($event) {
          return _vm.$emit("headercell:click", _vm.field);
        }
      }
    });
  };
  var __vue_staticRenderFns__$f = [];
  __vue_render__$f._withStripped = true;

  /* style */
  var __vue_inject_styles__$f = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-10eb2fd8_0", { source: "\n.asc[data-v-10eb2fd8]:after {\n  content: '↑';\n  position: relative;\n  text-decoration: none;\n}\n.des[data-v-10eb2fd8]:after {\n  content: '↓';\n  position: relative;\n  text-decoration: none;\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/DataTable/DataTableHeaderCell/DataTableHeaderCell.vue"], "names": [], "mappings": ";AA4CA;EACA,YAAA;EACA,kBAAA;EACA,qBAAA;AACA;AAEA;EACA,YAAA;EACA,kBAAA;EACA,qBAAA;AACA", "file": "DataTableHeaderCell.vue", "sourcesContent": ["<template>\n  <div\n    v-html='field'\n    class='th'\n    :class='cellClass'\n    @click='$emit(\"headercell:click\", field)'\n  >\n  </div>\n</template>\n\n<script>\nexport default {\n  name: 'data-table-header-cell',\n  props: ['field', 'sort'],\n  computed: {\n    cellClass: function() {\n      var field = this.field;\n      var sort = this.sort;\n      return this.classObject(field, sort);\n    }\n  },\n  methods: {\n    classObject: function(field, sort) {\n      var val = sort[field];\n      var cls = {};\n\n      if (!(field in sort)) {\n        return cls;\n      }\n      if (val.order === 'ascending') {\n        cls.asc = true;\n      }\n      if (val.order === 'descending') {\n        cls.des = true;\n      }\n\n      return cls;\n    }\n  }\n};\n</script>\n\n\n<style scoped>\n.asc:after {\n  content: '↑';\n  position: relative;\n  text-decoration: none;\n}\n\n.des:after {\n  content: '↓';\n  position: relative;\n  text-decoration: none;\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$f = "data-v-10eb2fd8";
  /* module identifier */
  var __vue_module_identifier__$f = undefined;
  /* functional template */
  var __vue_is_functional_template__$f = false;
  /* style inject SSR */

  var DataTableHeaderCell = normalizeComponent_1({ render: __vue_render__$f, staticRenderFns: __vue_staticRenderFns__$f }, __vue_inject_styles__$f, __vue_script__$f, __vue_scope_id__$f, __vue_is_functional_template__$f, __vue_module_identifier__$f, browser, undefined);

  //

  var script$g = {
    name: 'data-table-header',
    props: ['fields', 'sort', 'selectableQ'],
    data: function data() {
      return {
        timsort: this.sort,
        selectedQ: false
      };
    },
    components: { DataTableHeaderCell: DataTableHeaderCell },

    methods: {
      headerCellClick: function headerCellClick(field) {
        var timsort = Object.assign({}, this.timsort);

        if (field in timsort) {
          var fieldSort = timsort[field];
          if (fieldSort.order == 'ascending') {
            fieldSort.order = 'descending';
          } else {
            delete timsort[field];
          }
        } else {
          timsort[field] = { order: 'ascending' };
        }

        this.timsort = timsort;
        this.$emit('header:click', this.timsort);
      }
    }
  };

  /* script */
  var __vue_script__$g = script$g;

  /* template */
  var __vue_render__$g = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { staticClass: "tr thead" }, [_vm.selectableQ ? _c("div", { staticClass: "th selectable-checkbox" }, [_c("input", {
      attrs: { type: "checkbox", name: "all" },
      domProps: { checked: _vm.selectedQ },
      on: {
        change: function change($event) {
          return _vm.$emit("selectAll");
        }
      }
    })]) : _vm._e(), _vm._v(" "), _vm._l(_vm.fields, function (field) {
      return _c("data-table-header-cell", {
        key: field.id,
        attrs: { field: field, sort: _vm.sort },
        on: { "headercell:click": _vm.headerCellClick }
      });
    })], 2);
  };
  var __vue_staticRenderFns__$g = [];
  __vue_render__$g._withStripped = true;

  /* style */
  var __vue_inject_styles__$g = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-2000460e_0", { source: ".data-table[data-v-2000460e] {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.table-wrapper[data-v-2000460e] {\n  /* overflow-x: scroll; */\n}\n.caption-row[data-v-2000460e] {\n  display:inline-block; width:100%;\n}\n.table[data-v-2000460e] {\n  display: flex;\n  flex-direction: column;\n\n  margin: 0 auto 25px;\n  border-collapse: collapse;\n  border-bottom: 2px solid #007593;\n  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1), 0px 10px 20px rgba(0, 0, 0, 0.05),\n    0px 20px 20px rgba(0, 0, 0, 0.05), 0px 30px 20px rgba(0, 0, 0, 0.05);\n  background-color: #e0e0e0;\n}\n.thead[data-v-2000460e],\n.tfoot[data-v-2000460e] {\n  background-color: #e0e0e0;\n  color: RGB(52, 58, 64);\n  font-weight: bold;\n  font-size: 1em;\n  cursor: pointer;\n  user-select: none;\n  display: flex;\n  flex-direction: column;\n  font-size: 1em;\n}\n.thead[data-v-2000460e] {\n  /* text-decoration: underline; */\n}\n.thead .tr .th[data-v-2000460e]:after {\n  text-decoration: none;\n}\n.tbody[data-v-2000460e] {\n  display: block;\n  max-height: 250px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  flex-direction: column;\n  font-size: 0.85em;\n}\n.tfoot[data-v-2000460e] {\n}\n.td[data-v-2000460e],\n.th[data-v-2000460e] {\n  word-wrap: break-word;\n  word-break: break-all;\n  text-overflow: ellipsis;\n  width: 100%;\n  flex-basis: 0;\n  flex-grow: 1;\n  border-collapse: collapse;\n  margin: 0px;\n  border: solid transparent 0px;\n  padding: 0px 5px;\n}\n.tr[data-v-2000460e] {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  width: 100%;\n}\n.tbody .tr[data-v-2000460e]:nth-child(even) {\n  background-color: RGB(242, 242, 242);\n}\n.tbody .tr[data-v-2000460e]:nth-child(odd) {\n  background-color: #ffffff;\n}\n.thead.tr[data-v-2000460e],\n.tfoot.tr[data-v-2000460e] {\n  width: calc(100% - 15px);\n}\n.td[data-v-2000460e] {\n}\n.th[data-v-2000460e] {\n}\n.td[data-v-2000460e]:hover {\n  background-color: RGB(184, 192, 200);\n}\n/* .td:not(:hover) {\n  opacity: 0.5;\n} */\n.title[data-v-2000460e] {\n  float: left;\n  font-size: 1.25em;\n  color: black;\n  font-weight: bold;\n}\n@media (max-width: 800px) {\n.tr[data-v-2000460e] {\n    flex-direction: column !important;\n}\n.tbody .tr[data-v-2000460e] {\n    border-bottom: solid 2px black !important;\n}\n.tbody .td[data-v-2000460e] {\n    border-bottom: solid 1px RGBA(1, 1, 1, 0.2) !important;\n}\n.tfoot[data-v-2000460e],\n  .thead[data-v-2000460e] {\n    font-size: 0.8em;\n}\n.tbody[data-v-2000460e] {\n    font-size: 0.7em;\n}\n}\n.selectable-checkbox[data-v-2000460e] {\n  align-self: center;\n  text-align: center;\n}\n.tpage[data-v-2000460e] {\n}\n", map: undefined, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$g = "data-v-2000460e";
  /* module identifier */
  var __vue_module_identifier__$g = undefined;
  /* functional template */
  var __vue_is_functional_template__$g = false;
  /* style inject SSR */

  var DataTableHeader = normalizeComponent_1({ render: __vue_render__$g, staticRenderFns: __vue_staticRenderFns__$g }, __vue_inject_styles__$g, __vue_script__$g, __vue_scope_id__$g, __vue_is_functional_template__$g, __vue_module_identifier__$g, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  var symbols = {
    sum: '∑',
    mean: '𝜇',
    set: '{}'
  };

  var script$h = {
    name: 'data-table-footer-cell',

    props: ['field', 'fieldData', 'width'],

    data: function data() {
      return {
        type: _typeof(this.fieldData[0]),
        selected: ''
      };
    },

    computed: {
      aggregate: function aggregate() {
        var type = this.type;
        var sel = this.selected;

        var data = this.fieldData;

        if (type == 'number') {
          var val = data.reduce(function (a, b) {
            return a + b;
          }, 0);
          if (sel == 'sum') {
            return val;
          } else if (sel == 'mean') {
            var mean = val / data.length;
            return Math.round(mean * 100) / 100;
          }
          return val;
        } else if (type == 'string') {
          return [].concat(toConsumableArray(new Set(data))).length;
        } else {
          return '';
        }
      },

      options: function options() {
        var type = this.type;
        var opts = [];

        if (type == 'number') {
          opts = ['sum', 'mean'];
        } else if (type == 'string') {
          opts = ['set'];
        } else {
          opts = [];
        }

        if (opts.length && this.selected == '') {
          this.selected = opts[0];
        }
        return opts;
      }
    },

    methods: {
      symbol: function symbol(option) {
        return symbols[option];
      },
      updateSelected: function updateSelected(event) {
        this.selected = event.target.value;
        // console.log(this.$el.childNodes);
      }
    }
  };

  /* script */
  var __vue_script__$h = script$h;

  /* template */
  var __vue_render__$h = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", {
      staticClass: "th",
      on: {
        click: function click($event) {
          return _vm.$emit("footercell:click", _vm.field);
        }
      }
    }, [_c("select", {
      attrs: {
        "data-toggle": "tooltip",
        "data-placement": "bottom",
        title: _vm.selected
      },
      on: {
        change: function change($event) {
          return _vm.updateSelected($event);
        }
      }
    }, _vm._l(_vm.options, function (option) {
      return _c("option", { domProps: { value: option, selected: option == _vm.selected } }, [_vm._v("\n    " + _vm._s(_vm.symbol(option)) + "\n  ")]);
    }), 0), _vm._v(" "), _c("span", [_vm._v(_vm._s(_vm.aggregate))])]);
  };
  var __vue_staticRenderFns__$h = [];
  __vue_render__$h._withStripped = true;

  /* style */
  var __vue_inject_styles__$h = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-5be6c5cb_0", { source: ".data-table[data-v-5be6c5cb] {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.table-wrapper[data-v-5be6c5cb] {\n  /* overflow-x: scroll; */\n}\n.caption-row[data-v-5be6c5cb] {\n  display:inline-block; width:100%;\n}\n.table[data-v-5be6c5cb] {\n  display: flex;\n  flex-direction: column;\n\n  margin: 0 auto 25px;\n  border-collapse: collapse;\n  border-bottom: 2px solid #007593;\n  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1), 0px 10px 20px rgba(0, 0, 0, 0.05),\n    0px 20px 20px rgba(0, 0, 0, 0.05), 0px 30px 20px rgba(0, 0, 0, 0.05);\n  background-color: #e0e0e0;\n}\n.thead[data-v-5be6c5cb],\n.tfoot[data-v-5be6c5cb] {\n  background-color: #e0e0e0;\n  color: RGB(52, 58, 64);\n  font-weight: bold;\n  font-size: 1em;\n  cursor: pointer;\n  user-select: none;\n  display: flex;\n  flex-direction: column;\n  font-size: 1em;\n}\n.thead[data-v-5be6c5cb] {\n  /* text-decoration: underline; */\n}\n.thead .tr .th[data-v-5be6c5cb]:after {\n  text-decoration: none;\n}\n.tbody[data-v-5be6c5cb] {\n  display: block;\n  max-height: 250px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  flex-direction: column;\n  font-size: 0.85em;\n}\n.tfoot[data-v-5be6c5cb] {\n}\n.td[data-v-5be6c5cb],\n.th[data-v-5be6c5cb] {\n  word-wrap: break-word;\n  word-break: break-all;\n  text-overflow: ellipsis;\n  width: 100%;\n  flex-basis: 0;\n  flex-grow: 1;\n  border-collapse: collapse;\n  margin: 0px;\n  border: solid transparent 0px;\n  padding: 0px 5px;\n}\n.tr[data-v-5be6c5cb] {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  width: 100%;\n}\n.tbody .tr[data-v-5be6c5cb]:nth-child(even) {\n  background-color: RGB(242, 242, 242);\n}\n.tbody .tr[data-v-5be6c5cb]:nth-child(odd) {\n  background-color: #ffffff;\n}\n.thead.tr[data-v-5be6c5cb],\n.tfoot.tr[data-v-5be6c5cb] {\n  width: calc(100% - 15px);\n}\n.td[data-v-5be6c5cb] {\n}\n.th[data-v-5be6c5cb] {\n}\n.td[data-v-5be6c5cb]:hover {\n  background-color: RGB(184, 192, 200);\n}\n/* .td:not(:hover) {\n  opacity: 0.5;\n} */\n.title[data-v-5be6c5cb] {\n  float: left;\n  font-size: 1.25em;\n  color: black;\n  font-weight: bold;\n}\n@media (max-width: 800px) {\n.tr[data-v-5be6c5cb] {\n    flex-direction: column !important;\n}\n.tbody .tr[data-v-5be6c5cb] {\n    border-bottom: solid 2px black !important;\n}\n.tbody .td[data-v-5be6c5cb] {\n    border-bottom: solid 1px RGBA(1, 1, 1, 0.2) !important;\n}\n.tfoot[data-v-5be6c5cb],\n  .thead[data-v-5be6c5cb] {\n    font-size: 0.8em;\n}\n.tbody[data-v-5be6c5cb] {\n    font-size: 0.7em;\n}\n}\n.selectable-checkbox[data-v-5be6c5cb] {\n  align-self: center;\n  text-align: center;\n}\n.tpage[data-v-5be6c5cb] {\n}\n", map: undefined, media: undefined }), inject("data-v-5be6c5cb_1", { source: "\nselect[data-v-5be6c5cb] {\n  background-color: transparent;\n  border-color: transparent;\n  /* border-bottom-color: RGB(50, 200, 250); */\n  -webkit-appearance: none;\n  text-transform: uppercase;\n  padding: 0 5px;\n  font-weight: bold;\n  color: RGB(32, 122, 247);\n  -moz-appearance:none;\n}\nselect[data-v-5be6c5cb]:hover {\n  color: RGB(50, 200, 250);\n}\nselect[data-v-5be6c5cb]:focus {\n  outline: none;\n}\noption[data-v-5be6c5cb] {\n  padding: 0 5px;\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/DataTable/DataTableFooterCell/DataTableFooterCell.vue"], "names": [], "mappings": ";AAyGA;EACA,6BAAA;EACA,yBAAA;EACA,4CAAA;EACA,wBAAA;EACA,yBAAA;EACA,cAAA;EACA,iBAAA;EACA,wBAAA;EACA,oBAAA;AACA;AAEA;EACA,wBAAA;AACA;AAEA;EACA,aAAA;AACA;AAEA;EACA,cAAA;AACA", "file": "DataTableFooterCell.vue", "sourcesContent": ["<template>\n  <div\n    class='th'\n    @click='$emit(\"footercell:click\", field)'\n  >\n\n\n  <select\n    @change='updateSelected($event)'\n    data-toggle='tooltip'\n    data-placement='bottom'\n    :title='selected'\n  >\n    <option\n      v-for='option in options'\n      :value='option'\n      :selected='option == selected'\n    >\n      {{symbol(option)}}\n    </option>\n  </select>\n\n\n  <span>{{aggregate}}</span>\n\n\n\n  </div>\n</template>\n\n<script>\nlet symbols = {\n  sum: '∑',\n  mean: '𝜇',\n  set: '{}'\n};\n\nexport default {\n  name: 'data-table-footer-cell',\n\n  props: ['field', 'fieldData', 'width'],\n\n  data: function() {\n    return {\n      type: typeof this.fieldData[0],\n      selected: ''\n    };\n  },\n\n  computed: {\n    aggregate: function() {\n      let type = this.type;\n      let sel = this.selected;\n\n      var data = this.fieldData;\n\n      if (type == 'number') {\n        var val = data.reduce((a, b) => a + b, 0);\n        if (sel == 'sum') {\n          return val;\n        } else if (sel == 'mean') {\n          var mean = val / data.length;\n          return Math.round(mean * 100) / 100;\n        }\n        return val;\n      } else if (type == 'string') {\n        return [...new Set(data)].length;\n      } else {\n        return '';\n      }\n    },\n\n    options: function() {\n      let type = this.type;\n      var opts = [];\n\n      if (type == 'number') {\n        opts = ['sum', 'mean'];\n      } else if (type == 'string') {\n        opts = ['set'];\n      } else {\n        opts = [];\n      }\n\n      if (opts.length && this.selected == '') {\n        this.selected = opts[0];\n      }\n      return opts;\n    }\n  },\n\n  methods: {\n    symbol: function(option) {\n      return symbols[option];\n    },\n    updateSelected: function(event) {\n      this.selected = event.target.value;\n      // console.log(this.$el.childNodes);\n    }\n  }\n};\n</script>\n\n<style scoped src='../DataTable/data-table-style.css'></style>\n<style scoped>\nselect {\n  background-color: transparent;\n  border-color: transparent;\n  /* border-bottom-color: RGB(50, 200, 250); */\n  -webkit-appearance: none;\n  text-transform: uppercase;\n  padding: 0 5px;\n  font-weight: bold;\n  color: RGB(32, 122, 247);\n  -moz-appearance:none;\n}\n\nselect:hover {\n  color: RGB(50, 200, 250);\n}\n\nselect:focus {\n  outline: none;\n}\n\noption {\n  padding: 0 5px;\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$h = "data-v-5be6c5cb";
  /* module identifier */
  var __vue_module_identifier__$h = undefined;
  /* functional template */
  var __vue_is_functional_template__$h = false;
  /* style inject SSR */

  var DataTableFooterCell = normalizeComponent_1({ render: __vue_render__$h, staticRenderFns: __vue_staticRenderFns__$h }, __vue_inject_styles__$h, __vue_script__$h, __vue_scope_id__$h, __vue_is_functional_template__$h, __vue_module_identifier__$h, browser, undefined);

  //

  var script$i = {
    name: 'data-table-footer',
    props: ['fieldData'],
    components: { DataTableFooterCell: DataTableFooterCell },
    computed: {}
  };

  /* script */
  var __vue_script__$i = script$i;

  /* template */
  var __vue_render__$i = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", {
      staticClass: "tr tfoot",
      on: {
        click: function click($event) {
          return _vm.$emit("footer:click");
        }
      }
    }, _vm._l(_vm.fieldData, function (data, field, index) {
      return _c("data-table-footer-cell", {
        key: field.id,
        attrs: { field: field, fieldData: data }
      });
    }), 1);
  };
  var __vue_staticRenderFns__$i = [];
  __vue_render__$i._withStripped = true;

  /* style */
  var __vue_inject_styles__$i = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-2c08c529_0", { source: ".data-table[data-v-2c08c529] {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.table-wrapper[data-v-2c08c529] {\n  /* overflow-x: scroll; */\n}\n.caption-row[data-v-2c08c529] {\n  display:inline-block; width:100%;\n}\n.table[data-v-2c08c529] {\n  display: flex;\n  flex-direction: column;\n\n  margin: 0 auto 25px;\n  border-collapse: collapse;\n  border-bottom: 2px solid #007593;\n  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1), 0px 10px 20px rgba(0, 0, 0, 0.05),\n    0px 20px 20px rgba(0, 0, 0, 0.05), 0px 30px 20px rgba(0, 0, 0, 0.05);\n  background-color: #e0e0e0;\n}\n.thead[data-v-2c08c529],\n.tfoot[data-v-2c08c529] {\n  background-color: #e0e0e0;\n  color: RGB(52, 58, 64);\n  font-weight: bold;\n  font-size: 1em;\n  cursor: pointer;\n  user-select: none;\n  display: flex;\n  flex-direction: column;\n  font-size: 1em;\n}\n.thead[data-v-2c08c529] {\n  /* text-decoration: underline; */\n}\n.thead .tr .th[data-v-2c08c529]:after {\n  text-decoration: none;\n}\n.tbody[data-v-2c08c529] {\n  display: block;\n  max-height: 250px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  flex-direction: column;\n  font-size: 0.85em;\n}\n.tfoot[data-v-2c08c529] {\n}\n.td[data-v-2c08c529],\n.th[data-v-2c08c529] {\n  word-wrap: break-word;\n  word-break: break-all;\n  text-overflow: ellipsis;\n  width: 100%;\n  flex-basis: 0;\n  flex-grow: 1;\n  border-collapse: collapse;\n  margin: 0px;\n  border: solid transparent 0px;\n  padding: 0px 5px;\n}\n.tr[data-v-2c08c529] {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  width: 100%;\n}\n.tbody .tr[data-v-2c08c529]:nth-child(even) {\n  background-color: RGB(242, 242, 242);\n}\n.tbody .tr[data-v-2c08c529]:nth-child(odd) {\n  background-color: #ffffff;\n}\n.thead.tr[data-v-2c08c529],\n.tfoot.tr[data-v-2c08c529] {\n  width: calc(100% - 15px);\n}\n.td[data-v-2c08c529] {\n}\n.th[data-v-2c08c529] {\n}\n.td[data-v-2c08c529]:hover {\n  background-color: RGB(184, 192, 200);\n}\n/* .td:not(:hover) {\n  opacity: 0.5;\n} */\n.title[data-v-2c08c529] {\n  float: left;\n  font-size: 1.25em;\n  color: black;\n  font-weight: bold;\n}\n@media (max-width: 800px) {\n.tr[data-v-2c08c529] {\n    flex-direction: column !important;\n}\n.tbody .tr[data-v-2c08c529] {\n    border-bottom: solid 2px black !important;\n}\n.tbody .td[data-v-2c08c529] {\n    border-bottom: solid 1px RGBA(1, 1, 1, 0.2) !important;\n}\n.tfoot[data-v-2c08c529],\n  .thead[data-v-2c08c529] {\n    font-size: 0.8em;\n}\n.tbody[data-v-2c08c529] {\n    font-size: 0.7em;\n}\n}\n.selectable-checkbox[data-v-2c08c529] {\n  align-self: center;\n  text-align: center;\n}\n.tpage[data-v-2c08c529] {\n}\n", map: undefined, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$i = "data-v-2c08c529";
  /* module identifier */
  var __vue_module_identifier__$i = undefined;
  /* functional template */
  var __vue_is_functional_template__$i = false;
  /* style inject SSR */

  var DataTableFooter = normalizeComponent_1({ render: __vue_render__$i, staticRenderFns: __vue_staticRenderFns__$i }, __vue_inject_styles__$i, __vue_script__$i, __vue_scope_id__$i, __vue_is_functional_template__$i, __vue_module_identifier__$i, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //


  var script$j = {
    name: 'data-table-pagination',
    props: {
      selected: {
        default: '5'
      },
      page: {
        default: 0
      },
      pages: {
        default: 0
      },
      pageOptions: {
        default: ['5', '10', '25', 'All']
      }

    },
    components: {},
    computed: {}

  };

  /* script */
  var __vue_script__$j = script$j;

  /* template */
  var __vue_render__$j = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { staticClass: "tr tpage" }, [_c("div", { staticClass: "tr" }, [_c("div", {}, [_c("span", [_vm._v("Records per page: ")]), _vm._v(" "), _c("select", {
      attrs: { selected: _vm.selected },
      on: {
        change: function change($event) {
          return _vm.$emit("pageSize", $event.target.value);
        }
      }
    }, _vm._l(_vm.pageOptions, function (opt) {
      return _c("option", { key: opt.id }, [_vm._v(_vm._s(opt))]);
    }), 0)])]), _vm._v(" "), _c("div", { staticStyle: { "flex-grow": "1" } }), _vm._v(" "), _c("div", { staticClass: "page-nav" }, [_c("a", {
      staticClass: "previous round",
      on: {
        click: function click($event) {
          return _vm.$emit("prev", -1);
        }
      }
    }, [_vm._v("‹")]), _vm._v(" "), _c("div", { staticClass: "page-text" }, [_vm._v("page " + _vm._s(_vm.page + 1) + " / " + _vm._s(_vm.pages))]), _vm._v(" "), _c("a", {
      staticClass: "next round",
      on: {
        click: function click($event) {
          return _vm.$emit("next", 1);
        }
      }
    }, [_vm._v("›")])])]);
  };
  var __vue_staticRenderFns__$j = [];
  __vue_render__$j._withStripped = true;

  /* style */
  var __vue_inject_styles__$j = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-79bbf5b2_0", { source: "\n.tpage[data-v-79bbf5b2] {\n  padding: 10px;\n  display: inline-flex;\n  flex-direction: row;\n}\n.page-nav[data-v-79bbf5b2] {\n  padding-left: 10px;\n  display: inline-flex;\n  flex-direction: row;\n}\n.page-text[data-v-79bbf5b2]{\n  margin-left: 2px;\n  margin-right: 2px;\n}\na[data-v-79bbf5b2] {\n text-decoration: none;\n display: inline-block;\n padding: 8px 16px;\n -webkit-user-select: none; /* Chrome all / Safari all */\n-moz-user-select: none;    /* Firefox all             */\n-ms-user-select: none;     /* IE 10+                  */\n user-select: none;        /* Likely future           */\n}\na[data-v-79bbf5b2]:hover {\n background-color: #ddd;\n color: black;\n}\n.previous[data-v-79bbf5b2] {\n background-color: #f1f1f1;\n color: black;\n}\n.next[data-v-79bbf5b2] {\n background-color: #f1f1f1;\n color: white;\n}\n.round[data-v-79bbf5b2] {\n border-radius: 50%;\n}\nselect[data-v-79bbf5b2] {\n    background-color: transparent;\n    border-color: transparent;\n    -webkit-appearance: none;\n    text-transform: uppercase;\n    padding: 0 5px;\n    font-weight: bold;\n    color: RGB(32, 122, 247);\n    -moz-appearance: none;\n}\n\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/DataTable/DataTablePagination/DataTablePagination.vue"], "names": [], "mappings": ";AAuDA;EACA,aAAA;EACA,oBAAA;EACA,mBAAA;AACA;AAEA;EACA,kBAAA;EACA,oBAAA;EACA,mBAAA;AACA;AACA;EACA,gBAAA;EACA,iBAAA;AACA;AAEA;CACA,qBAAA;CACA,qBAAA;CACA,iBAAA;CACA,yBAAA,EAAA,4BAAA;AACA,sBAAA,KAAA,4BAAA;AACA,qBAAA,MAAA,4BAAA;CACA,iBAAA,SAAA,4BAAA;AACA;AAEA;CACA,sBAAA;CACA,YAAA;AACA;AAEA;CACA,yBAAA;CACA,YAAA;AACA;AAEA;CACA,yBAAA;CACA,YAAA;AACA;AAEA;CACA,kBAAA;AACA;AAEA;IACA,6BAAA;IACA,yBAAA;IACA,wBAAA;IACA,yBAAA;IACA,cAAA;IACA,iBAAA;IACA,wBAAA;IACA,qBAAA;AACA", "file": "DataTablePagination.vue", "sourcesContent": ["<template>\n  <div class=\"tr tpage\">\n    <div class=\"tr\">\n      <div class=\"\">\n        <span>Records per page:&nbsp;</span>\n        <select :selected=\"selected\" @change=\"$emit('pageSize', $event.target.value)\">\n          <option\n          v-for=\"opt in pageOptions\"\n          :key=\"opt.id\"\n          >{{opt}}</option>\n        </select>\n      </div>\n    </div>\n\n    <div class=\"\" style='flex-grow:1'></div>\n\n    <div class=\"page-nav\">\n      <a @click=\"$emit('prev', -1)\" class=\"previous round\">&#8249;</a>\n      <div class='page-text'>page {{page+1}} / {{pages}}</div>\n      <a @click=\"$emit('next', 1)\" class=\"next round\">&#8250;</a>\n    </div>\n\n  </div>\n</template>\n\n<script>\n\nexport default {\n  name: 'data-table-pagination',\n  props: {\n    selected: {\n      default: '5'\n    },\n    page: {\n      default: 0\n    },\n    pages: {\n      default: 0\n    },\n    pageOptions: {\n      default: ['5', '10', '25', 'All']\n    }\n\n  },\n  components: {  },\n  computed: {},\n\n};\n</script>\n\n\n<style scoped>\n\n\n\n.tpage {\n  padding: 10px;\n  display: inline-flex;\n  flex-direction: row;\n}\n\n.page-nav {\n  padding-left: 10px;\n  display: inline-flex;\n  flex-direction: row;\n}\n.page-text{\n  margin-left: 2px;\n  margin-right: 2px;\n}\n\na {\n text-decoration: none;\n display: inline-block;\n padding: 8px 16px;\n -webkit-user-select: none; /* Chrome all / Safari all */\n-moz-user-select: none;    /* Firefox all             */\n-ms-user-select: none;     /* IE 10+                  */\n user-select: none;        /* Likely future           */\n}\n\na:hover {\n background-color: #ddd;\n color: black;\n}\n\n.previous {\n background-color: #f1f1f1;\n color: black;\n}\n\n.next {\n background-color: #f1f1f1;\n color: white;\n}\n\n.round {\n border-radius: 50%;\n}\n\nselect {\n    background-color: transparent;\n    border-color: transparent;\n    -webkit-appearance: none;\n    text-transform: uppercase;\n    padding: 0 5px;\n    font-weight: bold;\n    color: RGB(32, 122, 247);\n    -moz-appearance: none;\n  }\n\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$j = "data-v-79bbf5b2";
  /* module identifier */
  var __vue_module_identifier__$j = undefined;
  /* functional template */
  var __vue_is_functional_template__$j = false;
  /* style inject SSR */

  var DataTablePagination = normalizeComponent_1({ render: __vue_render__$j, staticRenderFns: __vue_staticRenderFns__$j }, __vue_inject_styles__$j, __vue_script__$j, __vue_scope_id__$j, __vue_is_functional_template__$j, __vue_module_identifier__$j, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  var symbols$1 = {
    ascending: '↑',
    descending: '↓'
  };
  var script$k = {
    name: 'data-table-caption-sort',
    props: ['timsort'],
    methods: {
      symbol: function symbol(order) {
        return symbols$1[order];
      }
    }
  };

  /* script */
  var __vue_script__$k = script$k;

  /* template */
  var __vue_render__$k = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("ol", [_vm._v("\n  Sorted by:\n  "), _vm._l(_vm.timsort, function (sort, field, i) {
      return _c("li", [_vm._v("\n    " + _vm._s(field) + "\n    "), _c("span", { class: sort.order }, [_vm._v(_vm._s(_vm.symbol(sort.order)))])]);
    })], 2);
  };
  var __vue_staticRenderFns__$k = [];
  __vue_render__$k._withStripped = true;

  /* style */
  var __vue_inject_styles__$k = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-cd28d93e_0", { source: ".data-table[data-v-cd28d93e] {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.table-wrapper[data-v-cd28d93e] {\n  /* overflow-x: scroll; */\n}\n.caption-row[data-v-cd28d93e] {\n  display:inline-block; width:100%;\n}\n.table[data-v-cd28d93e] {\n  display: flex;\n  flex-direction: column;\n\n  margin: 0 auto 25px;\n  border-collapse: collapse;\n  border-bottom: 2px solid #007593;\n  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1), 0px 10px 20px rgba(0, 0, 0, 0.05),\n    0px 20px 20px rgba(0, 0, 0, 0.05), 0px 30px 20px rgba(0, 0, 0, 0.05);\n  background-color: #e0e0e0;\n}\n.thead[data-v-cd28d93e],\n.tfoot[data-v-cd28d93e] {\n  background-color: #e0e0e0;\n  color: RGB(52, 58, 64);\n  font-weight: bold;\n  font-size: 1em;\n  cursor: pointer;\n  user-select: none;\n  display: flex;\n  flex-direction: column;\n  font-size: 1em;\n}\n.thead[data-v-cd28d93e] {\n  /* text-decoration: underline; */\n}\n.thead .tr .th[data-v-cd28d93e]:after {\n  text-decoration: none;\n}\n.tbody[data-v-cd28d93e] {\n  display: block;\n  max-height: 250px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  flex-direction: column;\n  font-size: 0.85em;\n}\n.tfoot[data-v-cd28d93e] {\n}\n.td[data-v-cd28d93e],\n.th[data-v-cd28d93e] {\n  word-wrap: break-word;\n  word-break: break-all;\n  text-overflow: ellipsis;\n  width: 100%;\n  flex-basis: 0;\n  flex-grow: 1;\n  border-collapse: collapse;\n  margin: 0px;\n  border: solid transparent 0px;\n  padding: 0px 5px;\n}\n.tr[data-v-cd28d93e] {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  width: 100%;\n}\n.tbody .tr[data-v-cd28d93e]:nth-child(even) {\n  background-color: RGB(242, 242, 242);\n}\n.tbody .tr[data-v-cd28d93e]:nth-child(odd) {\n  background-color: #ffffff;\n}\n.thead.tr[data-v-cd28d93e],\n.tfoot.tr[data-v-cd28d93e] {\n  width: calc(100% - 15px);\n}\n.td[data-v-cd28d93e] {\n}\n.th[data-v-cd28d93e] {\n}\n.td[data-v-cd28d93e]:hover {\n  background-color: RGB(184, 192, 200);\n}\n/* .td:not(:hover) {\n  opacity: 0.5;\n} */\n.title[data-v-cd28d93e] {\n  float: left;\n  font-size: 1.25em;\n  color: black;\n  font-weight: bold;\n}\n@media (max-width: 800px) {\n.tr[data-v-cd28d93e] {\n    flex-direction: column !important;\n}\n.tbody .tr[data-v-cd28d93e] {\n    border-bottom: solid 2px black !important;\n}\n.tbody .td[data-v-cd28d93e] {\n    border-bottom: solid 1px RGBA(1, 1, 1, 0.2) !important;\n}\n.tfoot[data-v-cd28d93e],\n  .thead[data-v-cd28d93e] {\n    font-size: 0.8em;\n}\n.tbody[data-v-cd28d93e] {\n    font-size: 0.7em;\n}\n}\n.selectable-checkbox[data-v-cd28d93e] {\n  align-self: center;\n  text-align: center;\n}\n.tpage[data-v-cd28d93e] {\n}\n", map: undefined, media: undefined }), inject("data-v-cd28d93e_1", { source: "\nol[data-v-cd28d93e] {\n  display: inline-block;\n  font-size: 12px;\n  color: grey;\n  margin: 0px;\n  padding: 0px;\n}\nli[data-v-cd28d93e] {\n  text-decoration: none;\n  list-style: none;\n  display: inline-block;\n}\nli[data-v-cd28d93e]:not(:first-child) {\n  margin-left: 1px;\n}\n.ascending[data-v-cd28d93e] {\n  color: red;\n}\n.descending[data-v-cd28d93e] {\n  color: blue;\n}\nli[data-v-cd28d93e]:not(:last-child):after {\n  content: '/';\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/DataTable/DataTableCaptionSort/DataTableCaptionSort.vue"], "names": [], "mappings": ";AA6BA;EACA,qBAAA;EACA,eAAA;EACA,WAAA;EACA,WAAA;EACA,YAAA;AACA;AACA;EACA,qBAAA;EACA,gBAAA;EACA,qBAAA;AACA;AAEA;EACA,gBAAA;AACA;AAEA;EACA,UAAA;AACA;AACA;EACA,WAAA;AACA;AAEA;EACA,YAAA;AACA", "file": "DataTableCaptionSort.vue", "sourcesContent": ["<template>\n  <ol>\n    Sorted by:\n    <li v-for='(sort, field, i) in timsort'>\n      {{field}}\n      <span :class='sort.order'>{{symbol(sort.order)}}</span>\n    </li>\n  </ol>\n\n</template>\n\n<script>\nlet symbols = {\n  ascending: '↑',\n  descending: '↓'\n};\nexport default {\n  name: 'data-table-caption-sort',\n  props: ['timsort'],\n  methods: {\n    symbol: function(order) {\n      return symbols[order];\n    }\n  }\n};\n</script>\n\n<style scoped src='../DataTable/data-table-style.css'></style>\n<style scoped>\nol {\n  display: inline-block;\n  font-size: 12px;\n  color: grey;\n  margin: 0px;\n  padding: 0px;\n}\nli {\n  text-decoration: none;\n  list-style: none;\n  display: inline-block;\n}\n\nli:not(:first-child) {\n  margin-left: 1px;\n}\n\n.ascending {\n  color: red;\n}\n.descending {\n  color: blue;\n}\n\nli:not(:last-child):after {\n  content: '/';\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$k = "data-v-cd28d93e";
  /* module identifier */
  var __vue_module_identifier__$k = undefined;
  /* functional template */
  var __vue_is_functional_template__$k = false;
  /* style inject SSR */

  var DataTableCaptionSort = normalizeComponent_1({ render: __vue_render__$k, staticRenderFns: __vue_staticRenderFns__$k }, __vue_inject_styles__$k, __vue_script__$k, __vue_scope_id__$k, __vue_is_functional_template__$k, __vue_module_identifier__$k, browser, undefined);

  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //
  //

  var script$l = {
    name: 'data-table-caption-sort',
    props: ['filtered', 'total']
  };

  /* script */
  var __vue_script__$l = script$l;

  /* template */
  var __vue_render__$l = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", [_vm._v("\n  Records:\n  "), _vm.filtered != _vm.total ? _c("span", { staticClass: "top" }, [_vm._v("\n    " + _vm._s(_vm.filtered) + "\n  ")]) : _vm._e(), _vm._v(" "), _c("span", [_vm._v(" " + _vm._s(_vm.total) + " ")])]);
  };
  var __vue_staticRenderFns__$l = [];
  __vue_render__$l._withStripped = true;

  /* style */
  var __vue_inject_styles__$l = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-0ca8018d_0", { source: ".data-table[data-v-0ca8018d] {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.table-wrapper[data-v-0ca8018d] {\n  /* overflow-x: scroll; */\n}\n.caption-row[data-v-0ca8018d] {\n  display:inline-block; width:100%;\n}\n.table[data-v-0ca8018d] {\n  display: flex;\n  flex-direction: column;\n\n  margin: 0 auto 25px;\n  border-collapse: collapse;\n  border-bottom: 2px solid #007593;\n  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1), 0px 10px 20px rgba(0, 0, 0, 0.05),\n    0px 20px 20px rgba(0, 0, 0, 0.05), 0px 30px 20px rgba(0, 0, 0, 0.05);\n  background-color: #e0e0e0;\n}\n.thead[data-v-0ca8018d],\n.tfoot[data-v-0ca8018d] {\n  background-color: #e0e0e0;\n  color: RGB(52, 58, 64);\n  font-weight: bold;\n  font-size: 1em;\n  cursor: pointer;\n  user-select: none;\n  display: flex;\n  flex-direction: column;\n  font-size: 1em;\n}\n.thead[data-v-0ca8018d] {\n  /* text-decoration: underline; */\n}\n.thead .tr .th[data-v-0ca8018d]:after {\n  text-decoration: none;\n}\n.tbody[data-v-0ca8018d] {\n  display: block;\n  max-height: 250px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  flex-direction: column;\n  font-size: 0.85em;\n}\n.tfoot[data-v-0ca8018d] {\n}\n.td[data-v-0ca8018d],\n.th[data-v-0ca8018d] {\n  word-wrap: break-word;\n  word-break: break-all;\n  text-overflow: ellipsis;\n  width: 100%;\n  flex-basis: 0;\n  flex-grow: 1;\n  border-collapse: collapse;\n  margin: 0px;\n  border: solid transparent 0px;\n  padding: 0px 5px;\n}\n.tr[data-v-0ca8018d] {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  width: 100%;\n}\n.tbody .tr[data-v-0ca8018d]:nth-child(even) {\n  background-color: RGB(242, 242, 242);\n}\n.tbody .tr[data-v-0ca8018d]:nth-child(odd) {\n  background-color: #ffffff;\n}\n.thead.tr[data-v-0ca8018d],\n.tfoot.tr[data-v-0ca8018d] {\n  width: calc(100% - 15px);\n}\n.td[data-v-0ca8018d] {\n}\n.th[data-v-0ca8018d] {\n}\n.td[data-v-0ca8018d]:hover {\n  background-color: RGB(184, 192, 200);\n}\n/* .td:not(:hover) {\n  opacity: 0.5;\n} */\n.title[data-v-0ca8018d] {\n  float: left;\n  font-size: 1.25em;\n  color: black;\n  font-weight: bold;\n}\n@media (max-width: 800px) {\n.tr[data-v-0ca8018d] {\n    flex-direction: column !important;\n}\n.tbody .tr[data-v-0ca8018d] {\n    border-bottom: solid 2px black !important;\n}\n.tbody .td[data-v-0ca8018d] {\n    border-bottom: solid 1px RGBA(1, 1, 1, 0.2) !important;\n}\n.tfoot[data-v-0ca8018d],\n  .thead[data-v-0ca8018d] {\n    font-size: 0.8em;\n}\n.tbody[data-v-0ca8018d] {\n    font-size: 0.7em;\n}\n}\n.selectable-checkbox[data-v-0ca8018d] {\n  align-self: center;\n  text-align: center;\n}\n.tpage[data-v-0ca8018d] {\n}\n", map: undefined, media: undefined }), inject("data-v-0ca8018d_1", { source: "\ndiv[data-v-0ca8018d] {\n  text-align: right;\n  position: relative;\n  font-size: 12px;\n  color: grey;\n}\n.top[data-v-0ca8018d] {\n}\n.top[data-v-0ca8018d]:after {\n  content: '/';\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/DataTable/DataTableCaptionFilter/DataTableCaptionFilter.vue"], "names": [], "mappings": ";AAsBA;EACA,iBAAA;EACA,kBAAA;EACA,eAAA;EACA,WAAA;AACA;AACA;AAAA;AACA;EACA,YAAA;AACA", "file": "DataTableCaptionFilter.vue", "sourcesContent": ["<template>\n  <div>\n    Records:\n    <span\n      v-if='filtered != total'\n      class='top'\n    >\n      {{filtered}}\n    </span>\n    <span> {{total}} </span>\n  </div>\n</template>\n\n<script>\nexport default {\n  name: 'data-table-caption-sort',\n  props: ['filtered', 'total']\n};\n</script>\n\n<style scoped src='../DataTable/data-table-style.css'></style>\n<style scoped>\ndiv {\n  text-align: right;\n  position: relative;\n  font-size: 12px;\n  color: grey;\n}\n.top { }\n.top:after {\n  content: '/';\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$l = "data-v-0ca8018d";
  /* module identifier */
  var __vue_module_identifier__$l = undefined;
  /* functional template */
  var __vue_is_functional_template__$l = false;
  /* style inject SSR */

  var DataTableCaptionFilter = normalizeComponent_1({ render: __vue_render__$l, staticRenderFns: __vue_staticRenderFns__$l }, __vue_inject_styles__$l, __vue_script__$l, __vue_scope_id__$l, __vue_is_functional_template__$l, __vue_module_identifier__$l, browser, undefined);

  function filterableValue(record, field) {
    var value = record[field];
    if (!value) {
      return '';
    }
    return value.toString().toLowerCase();
  }

  function stringSort(a, b, ascQ) {
    var k = ascQ ? -1 : 1;
    if (a == b) {
      return 0;
    }
    return a < b ? 1 * k : -1 * k;
  }

  function valueQ(a) {
    if (a == undefined || (typeof a === 'undefined' ? 'undefined' : _typeof(a)) == undefined || isNaN(a) && typeof a != 'string' || a == null) {
      return false;
    }
    return true;
  }

  function standarizeValue(a) {
    if (!valueQ(a)) {
      return undefined;
    }
    return a;
  }

  function numberSort(a, b, ascQ) {
    return ascQ ? a - b : b - a;
  }

  function tableTimsort(ids, records, sort, sortingFunctions) {
    var fields = Object.keys(sort);
    if (!fields.length) {
      return ids;
    }

    var fieldFunctions = fields.map(function (field) {

      var transform = function transform(record, field) {
        return record[field];
      };
      var ascendingQ = sort[field].order == 'ascending';

      if (field in sortingFunctions) {
        transform = sortingFunctions[field];
      }

      return function (a, b) {
        var x = standarizeValue(transform(a, field));
        var y = standarizeValue(transform(b, field));
        // console.log({a: a[field],t: transform(a, field),sv: standarizeValue(transform(a, field))});
        // console.log({b: b[field],t: transform(b, field),sv: standarizeValue(transform(b, field))});

        var type;
        if ([x, y].some(function (e) {
          return typeof e == 'string';
        })) {
          type = 'string';
        }
        if ([x, y].some(function (e) {
          return typeof e == 'number';
        })) {
          type = 'number';
        }
        // console.log('type', type);

        if (type == 'string') {
          if (!valueQ(x)) {
            x = '';
          }
          if (!valueQ(y)) {
            y = '';
          }
          return stringSort(x, y, ascendingQ);
        } else if (type == 'number') {
          if (!valueQ(x)) {
            x = Infinity;
          }
          if (!valueQ(y)) {
            y = Infinity;
          }
          return numberSort(x, y, ascendingQ);
        } else {
          return -1;
        }
      };
    });

    var sorted = ids.sort(function (a, b) {
      for (var i = 0; i < fieldFunctions.length; i++) {
        var f = fieldFunctions[i];
        var v = f(records[a], records[b]);
        if (!v) {
          continue;
        } else {
          break;
        }
      }
      return v;
    });
    // console.log('sorted', sorted);
    return sorted;
  }

  function giRegexOnFields(filterText, ids, records, fields, fieldRenderFunctions) {
    var reg = new RegExp(filterText, 'gi');
    var use;

    if (filterText == '') {
      use = ids;
    } else {
      use = [];

      ids.map(function (id) {

        var record = records[id];

        var fieldJoin = fields.map(function (f) {
          if (f in fieldRenderFunctions) {
            return fieldRenderFunctions[f](id, record, f);
          }
          return filterableValue(record, f);
        }).join('');

        var match = fieldJoin.match(reg);

        if (!(match == null || match.join('') == '')) {
          use.push(id);
        }
      });
    }
    return use;
  }

  function objectFields(ids, records) {
    var _ref;

    var allRecordFields = (_ref = []).concat.apply(_ref, toConsumableArray(ids.map(function (recordId) {
      return Object.keys(records[recordId]);
    })));
    return [].concat(toConsumableArray(new Set(allRecordFields)));
  }

  //

  var script$m = {
    components: {
      DataTableRow: DataTableRow,
      DataTableFilter: DataTableFilter,
      DataTableHeader: DataTableHeader,
      DataTableCaptionSort: DataTableCaptionSort,
      DataTableFooter: DataTableFooter,
      DataTableCaptionFilter: DataTableCaptionFilter,
      DataTablePagination: DataTablePagination
    },
    props: {
      pagination: {
        type: Boolean,
        default: false
      },
      includeFilter: {
        type: Boolean,
        default: true
      },
      data: {
        default: function _default() {
          return {};
        },
        type: Object
      },
      width: {
        default: 100
      },
      fieldRenderFunctions: {
        default: function _default() {
          return {};
        },
        type: Object
      },
      fieldSortingFunctions: {
        default: function _default() {
          return {};
        },
        type: Object
      },
      fieldAggregateFunctions: {
        default: function _default() {
          return {};
        },
        type: Object
      },
      title: {
        type: String,
        default: ''
      },
      selectableQ: {
        type: Boolean,
        default: false
      },
      startingPageSize: {
        type: String,
        default: '5'
      },
      pageOptions: {
        default: ['5', '10', '25', 'All']
      }
    },

    data: function data() {
      var inPageOpts = this.pageOptions.indexOf(this.startingPageSize) > -1;
      return {
        // actual data
        records: this.data,
        // keys in data
        recordIds: Object.keys(this.data),
        // fields across all records
        fields: objectFields(Object.keys(this.data), this.data),
        // value of filter
        filterText: '',

        // list of sorts
        timsort: {},

        sortedRecords: Object.keys(this.data),
        filteredRecords: Object.keys(this.data),
        sortedAndFilteredRecords: Object.keys(this.data),
        selected: [],

        pageSize: inPageOpts ? this.startingPageSize : this.pageOptions[0],
        currentPage: 0
      };
    },

    computed: {
      idsOfPage: function idsOfPage() {
        if (!this.pagination) return this.sortedAndFilteredRecords;
        if (this.pageSize == 'All') return this.sortedAndFilteredRecords;
        return this.sortedAndFilteredRecords.slice(this.currentPage * this.pageSize, (this.currentPage + 1) * this.pageSize);
      },
      numPages: function numPages() {
        if (this.pageSize == 'All') return 1;
        return Math.ceil(this.sortedAndFilteredRecords.length / this.pageSize);
      },

      fieldData: function fieldData() {
        var that = this;
        var fieldObject = {};

        if (this.selectableQ) {
          var selected = this.selected;
          fieldObject['checked'] = this.sortedAndFilteredRecords.map(function (id) {
            if (selected.includes(id)) {
              return 1;
            }
            return 0;
          });
        }

        that.fields.map(function (field) {
          that.sortedAndFilteredRecords.map(function (id) {
            var value = that.records[id][field];
            if (fieldObject[field] == undefined) {
              fieldObject[field] = [];
            }
            if (field in that.fieldRenderFunctions) {
              fieldObject[field].push(that.fieldRenderFunctions[field](id, that.records[id], field));
            } else {
              fieldObject[field].push(value);
            }
          });
        });
        return fieldObject;
      }
    },

    methods: {
      changePage: function changePage(val) {
        if (this.currentPage + val < 0) {
          return;
        }
        if (this.currentPage + val > this.numPages) {
          return;
        }
        this.currentPage += val;
      },
      changePageSize: function changePageSize(val) {
        this.pageSize = val;
        this.page = 0;
        // update page
      },

      headerClick: function headerClick(sort) {
        this.timsort = sort;
        // console.log('TIMSORT', this.timsort);
        this.sortedRecords = this.sortRecords(this.recordIds);
        this.filteredRecords = this.filterRecords(this.sortedRecords);
        this.sortedAndFilteredRecords = this.filteredRecords;
      },
      updateFilterText: function updateFilterText(filterText) {
        this.filterText = filterText;

        this.filteredRecords = this.filterRecords(this.sortedRecords);
        this.sortedAndFilteredRecords = this.filteredRecords;
      },

      sortAndFilter: function sortAndFilter() {
        var sorted = this.sortRecords(this.recordIds);
        return this.filteredRecords(sorted);
      },

      filterRecords: function filterRecords(ids) {
        return giRegexOnFields(this.filterText, ids, this.records, this.fields, this.fieldRenderFunctions);
      },

      sortRecords: function sortRecords(ids) {
        return tableTimsort(ids, this.records, this.timsort, this.fieldSortingFunctions);
      },

      selectAll: function selectAll() {
        if (this.selected.length == this.sortedAndFilteredRecords.length) {
          this.selected = [];
        } else {
          this.selected = this.sortedAndFilteredRecords.slice();
        }
      },
      toggledSelected: function toggledSelected(id) {

        var selected = this.selected;
        if (selected.includes(id)) {
          selected.splice(selected.indexOf(id), 1);
        } else {
          selected.push(id);
        }
      }
    }
  };

  /* script */
  var __vue_script__$m = script$m;

  /* template */
  var __vue_render__$m = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { staticClass: "data-table", style: { width: _vm.width + "%" } }, [_vm.includeFilter ? _c("div", { staticClass: "caption-row" }, [_c("span", { staticClass: "title" }, [_vm._v(_vm._s(_vm.title))]), _vm._v(" "), _c("data-table-filter", {
      attrs: {
        filterText: _vm.filterText,
        filtered: _vm.filteredRecords.length,
        total: _vm.recordIds.length
      },
      on: { "update:filterText": _vm.updateFilterText }
    })], 1) : _vm._e(), _vm._v(" "), _c("div", { staticClass: "caption-row" }, [_c("data-table-caption-sort", {
      staticStyle: { float: "left" },
      attrs: { timsort: _vm.timsort }
    }), _vm._v(" "), _c("data-table-caption-filter", {
      attrs: {
        filtered: _vm.filteredRecords.length,
        total: _vm.recordIds.length
      }
    })], 1), _vm._v(" "), _c("div", { staticClass: "table-wrapper" }, [_c("div", { staticClass: "table" }, [_c("data-table-header", {
      attrs: {
        fields: _vm.fields,
        sort: _vm.timsort,
        selectableQ: _vm.selectableQ
      },
      on: { "header:click": _vm.headerClick, selectAll: _vm.selectAll }
    }), _vm._v(" "), _c("div", { staticClass: "tbody" }, _vm._l(_vm.idsOfPage, function (recordId) {
      return _c("data-table-row", {
        key: recordId.id,
        attrs: {
          recordId: recordId,
          record: _vm.records[recordId],
          fields: _vm.fields,
          fieldRenderFunctions: _vm.fieldRenderFunctions,
          selectableQ: _vm.selectableQ,
          selected: _vm.selected
        },
        on: {
          toggledSelected: _vm.toggledSelected,
          "row:click": function rowClick($event) {}
        }
      });
    }), 1), _vm._v(" "), _c("data-table-footer", { attrs: { fieldData: _vm.fieldData } }), _vm._v(" "), _vm.pagination ? _c("data-table-pagination", {
      attrs: {
        selected: _vm.pageSize,
        page: _vm.currentPage,
        pages: _vm.numPages,
        pageOptions: _vm.pageOptions
      },
      on: {
        prev: _vm.changePage,
        next: _vm.changePage,
        pageSize: _vm.changePageSize
      }
    }) : _vm._e()], 1)])]);
  };
  var __vue_staticRenderFns__$m = [];
  __vue_render__$m._withStripped = true;

  /* style */
  var __vue_inject_styles__$m = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-9f60aeb0_0", { source: ".data-table[data-v-9f60aeb0] {\n  margin: auto;\n  margin-bottom: 20px;\n}\n.table-wrapper[data-v-9f60aeb0] {\n  /* overflow-x: scroll; */\n}\n.caption-row[data-v-9f60aeb0] {\n  display:inline-block; width:100%;\n}\n.table[data-v-9f60aeb0] {\n  display: flex;\n  flex-direction: column;\n\n  margin: 0 auto 25px;\n  border-collapse: collapse;\n  border-bottom: 2px solid #007593;\n  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.1), 0px 10px 20px rgba(0, 0, 0, 0.05),\n    0px 20px 20px rgba(0, 0, 0, 0.05), 0px 30px 20px rgba(0, 0, 0, 0.05);\n  background-color: #e0e0e0;\n}\n.thead[data-v-9f60aeb0],\n.tfoot[data-v-9f60aeb0] {\n  background-color: #e0e0e0;\n  color: RGB(52, 58, 64);\n  font-weight: bold;\n  font-size: 1em;\n  cursor: pointer;\n  user-select: none;\n  display: flex;\n  flex-direction: column;\n  font-size: 1em;\n}\n.thead[data-v-9f60aeb0] {\n  /* text-decoration: underline; */\n}\n.thead .tr .th[data-v-9f60aeb0]:after {\n  text-decoration: none;\n}\n.tbody[data-v-9f60aeb0] {\n  display: block;\n  max-height: 250px;\n  overflow-y: scroll;\n  overflow-x: hidden;\n  flex-direction: column;\n  font-size: 0.85em;\n}\n.tfoot[data-v-9f60aeb0] {\n}\n.td[data-v-9f60aeb0],\n.th[data-v-9f60aeb0] {\n  word-wrap: break-word;\n  word-break: break-all;\n  text-overflow: ellipsis;\n  width: 100%;\n  flex-basis: 0;\n  flex-grow: 1;\n  border-collapse: collapse;\n  margin: 0px;\n  border: solid transparent 0px;\n  padding: 0px 5px;\n}\n.tr[data-v-9f60aeb0] {\n  display: flex;\n  flex-direction: row;\n  justify-content: space-between;\n  width: 100%;\n}\n.tbody .tr[data-v-9f60aeb0]:nth-child(even) {\n  background-color: RGB(242, 242, 242);\n}\n.tbody .tr[data-v-9f60aeb0]:nth-child(odd) {\n  background-color: #ffffff;\n}\n.thead.tr[data-v-9f60aeb0],\n.tfoot.tr[data-v-9f60aeb0] {\n  width: calc(100% - 15px);\n}\n.td[data-v-9f60aeb0] {\n}\n.th[data-v-9f60aeb0] {\n}\n.td[data-v-9f60aeb0]:hover {\n  background-color: RGB(184, 192, 200);\n}\n/* .td:not(:hover) {\n  opacity: 0.5;\n} */\n.title[data-v-9f60aeb0] {\n  float: left;\n  font-size: 1.25em;\n  color: black;\n  font-weight: bold;\n}\n@media (max-width: 800px) {\n.tr[data-v-9f60aeb0] {\n    flex-direction: column !important;\n}\n.tbody .tr[data-v-9f60aeb0] {\n    border-bottom: solid 2px black !important;\n}\n.tbody .td[data-v-9f60aeb0] {\n    border-bottom: solid 1px RGBA(1, 1, 1, 0.2) !important;\n}\n.tfoot[data-v-9f60aeb0],\n  .thead[data-v-9f60aeb0] {\n    font-size: 0.8em;\n}\n.tbody[data-v-9f60aeb0] {\n    font-size: 0.7em;\n}\n}\n.selectable-checkbox[data-v-9f60aeb0] {\n  align-self: center;\n  text-align: center;\n}\n.tpage[data-v-9f60aeb0] {\n}\n", map: undefined, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$m = "data-v-9f60aeb0";
  /* module identifier */
  var __vue_module_identifier__$m = undefined;
  /* functional template */
  var __vue_is_functional_template__$m = false;
  /* style inject SSR */

  var DataTable = normalizeComponent_1({ render: __vue_render__$m, staticRenderFns: __vue_staticRenderFns__$m }, __vue_inject_styles__$m, __vue_script__$m, __vue_scope_id__$m, __vue_is_functional_template__$m, __vue_module_identifier__$m, browser, undefined);

  var httpReadyStateCodes = {
    0: 'unsent',
    1: 'opened',
    2: 'headers',
    3: 'loading',
    4: 'done'
  };
  var httpStatusCodes = {
    100: 'continue',
    101: 'switching protocols',
    102: 'processing',
    103: 'early hints',
    200: 'ok',
    201: 'created',
    202: 'accepted',
    203: 'non-authoritative information',
    204: 'no content',
    205: 'reset conten',
    206: 'partial content',
    207: 'multi-status',
    208: 'already reported',
    218: 'this is fine',
    226: 'IM used',
    300: 'multiple choices',
    301: 'moved permanently',
    302: 'found',
    303: 'see other',
    304: 'not motified',
    305: 'used proxy',
    306: 'switch proxy',
    307: 'temporary redirect',
    308: 'permanent redirect',
    400: 'bad request',
    401: 'unauthorized',
    402: 'payment required',
    403: 'forbidden',
    404: 'not found',
    405: 'method not allowed',
    406: 'no acceptable',
    407: 'proxy authentication required',
    408: 'requested timeout',
    409: 'conflict',
    410: 'gone',
    411: 'length required',
    412: 'precondition failed',
    413: 'payout too large',
    414: 'URI too long',
    415: 'unsupported media type',
    416: 'range not satisfiable',
    417: 'expectation failed',
    418: 'I\'m a teapot',
    421: 'misdirected requested',
    422: 'unprocessable Entity',
    423: 'locked',
    424: 'failed dependency',
    426: 'upgrade required',
    428: 'precondition required',
    429: 'too many requested',
    431: 'request header fields too large',
    451: 'unavailable for legal reasons',
    500: 'internal server error',
    501: 'not implemented',
    502: 'bad gateway',
    503: 'service unavailable',
    504: 'gateway timeout',
    505: 'http version not supported',
    506: 'variant also negotiates',
    507: 'insufficient storage',
    508: 'loop detected',
    510: 'not extended',
    511: 'network authentication required'
  };

  //

  var script$n = {
    props: {
      method: {
        type: String,
        default: 'GET',
        validator: function validator(value) {
          return ['GET', 'POST', 'PUT', 'DELETE'].includes(value);
        }
      },
      url: {
        type: String,
        required: true
      },
      async: {
        type: Boolean,
        default: true
      },
      success: {
        type: Function,
        default: function _default() {
          console.log(this.xhr.response);
        }
      },
      readystatechange: {
        type: Function,
        default: function _default(event) {
        }
      },
      automaticCloseQ: {
        type: Boolean,
        default: false
      }
    },
    data: function data() {
      return {
        xhr: new XMLHttpRequest(),
        httpStatusCodes: httpStatusCodes,
        httpReadyStateCodes: httpReadyStateCodes,
        color: 'primary',
        percent: 0,
        humanReadableLead: '',
        humanReadableMessage: '',
        humanReadableEnd: '',
        xhrMessage: '',
        showQ: true,
        completeQ: false
      };
    },
    computed: {
      readyStateTooltip: function readyStateTooltip() {
        var rs = this.xhr.readyState;
        var rsc = httpReadyStateCodes[rs];
        return 'Ready state ' + rs + ': ' + rsc;
      },
      statusTooltip: function statusTooltip() {
        var s = this.xhr.status;
        // s = s == 0 ? 218 : s
        var sc = httpStatusCodes[s];
        return 'Status ' + s + ': ' + sc;
      },
      transferComplete: function transferComplete() {
        return this.completeQ;
      }
    },
    methods: {
      open: function open() {
        this.xhr.open(this.method, this.url, this.async);
      },
      send: function send() {
        this.xhr.send();
      },
      goodbye: function goodbye() {
        this.showQ = false;
      }
    },

    created: function created() {
      var that = this;
      that.open();

      that.xhr.addEventListener('error', function (event) {
        that.color = 'danger';
        that.xhrMessage = 'An error has occured.';
      });

      this.xhr.addEventListener('progress', function (event) {
        if (event.lengthComputable) {
          var percentComplete = event.loaded / event.total * 100;
          that.percent = percentComplete;
        } else {
          that.percent = 100;
          that.xhrMessage = 'Unable to compute progress information since the total size is unknown.';
        }
      });

      that.xhr.addEventListener('abort', function (event) {
        that.color = 'danger';
        that.xhrMessage = 'The transfer has been canceled by the user.';
      });

      that.xhr.addEventListener('load', function (event) {
        that.color = 'success';
        that.xhrMessage = 'Transfer complete.';
        that.completeQ = true;
        if (that.automaticCloseQ) {
          that.showQ = false;
        }
        that.success();
      });

      that.xhr.addEventListener('readystatechange', function (event) {
        that.readystatechange(event);
      });

      that.send();
    }
  };

  /* script */
  var __vue_script__$n = script$n;

  /* template */
  var __vue_render__$n = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _vm.showQ ? _c("div", [_c("div", { staticClass: "text-muted" }, [_c("span", [_vm._v(_vm._s(_vm.humanReadableLead))]), _vm._v(" "), _c("span", { class: "text-" + _vm.color }, [_vm._v(_vm._s(_vm.humanReadableMessage))]), _vm._v(" "), _c("span", [_vm._v(_vm._s(_vm.humanReadableEnd))])]), _vm._v(" "), _c("div", { staticClass: "progress" }, [_c("div", {
      staticClass: "progress-bar progress-bar-striped progress-bar-animated",
      class: "bg-" + _vm.color,
      style: { width: String(_vm.percent) + "%" },
      attrs: {
        role: "progressbar",
        "aria-valuenow": _vm.percent,
        "aria-valuemin": "0",
        "aria-valuemax": "100"
      }
    })]), _vm._v(" "), _c("div", { staticClass: "text-right text-muted form-text text-small" }, [_c("span", { staticClass: "float-left" }, [_vm._v(_vm._s(_vm.xhrMessage))]), _vm._v(" "), _c("span", {
      staticClass: "badge",
      class: "badge-" + _vm.color,
      attrs: {
        "data-toggle": "tooltip",
        "data-placement": "right",
        title: _vm.readyStateTooltip
      }
    }, [_vm._v("\n        " + _vm._s(_vm.xhr.readyState) + "\n      ")]), _vm._v(" "), _c("span", {
      staticClass: "badge",
      class: "badge-" + _vm.color,
      attrs: {
        "data-toggle": "tooltip",
        "data-placement": "right",
        title: _vm.statusTooltip
      }
    }, [_vm._v("\n      " + _vm._s(_vm.xhr.status) + "\n    ")]), _vm._v(" "), _vm.transferComplete ? _c("span", {
      staticClass: "badge badge-secondary",
      attrs: {
        "data-toggle": "tooltip",
        "data-placement": "right",
        title: "Dismiss progress bar"
      },
      on: { click: _vm.goodbye }
    }, [_vm._v("\n      ×\n    ")]) : _vm._e()])]) : _vm._e();
  };
  var __vue_staticRenderFns__$n = [];
  __vue_render__$n._withStripped = true;

  /* style */
  var __vue_inject_styles__$n = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-be461128_0", { source: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", map: { "version": 3, "sources": [], "names": [], "mappings": "", "file": "HttpRequest.vue" }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$n = "data-v-be461128";
  /* module identifier */
  var __vue_module_identifier__$n = undefined;
  /* functional template */
  var __vue_is_functional_template__$n = false;
  /* style inject SSR */

  var HttpRequest = normalizeComponent_1({ render: __vue_render__$n, staticRenderFns: __vue_staticRenderFns__$n }, __vue_inject_styles__$n, __vue_script__$n, __vue_scope_id__$n, __vue_is_functional_template__$n, __vue_module_identifier__$n, browser, undefined);

  //
  //
  //
  //
  //
  //


  var script$o = {
    props: {
      offsetSelector: {
        type: String,
        default: ''
      },
      debugQ: {
        type: Boolean,
        default: false
      }
    },

    data: function data() {
      return {
        offset: 0
      };
    },
    computed: {},
    methods: {
      setOffset: function setOffset() {
        this.offset = this.offsetSelector == undefined ? 0 : $(this.offsetSelector).outerHeight() == undefined ? 0 : $(this.offsetSelector).outerHeight();
      },

      animateScrollTo: function animateScrollTo() {
        this.setOffset();
        var that = this;
        if (that.debugQ) {
          console.group('[section-down]');
        }
        if (that.debugQ) {
          console.table({ currentOffset: curOffset, offsetSelector: that.offsetSelector });
        }

        var sections = document.querySelectorAll('section:not([display="none"])');
        var current = undefined;
        var curOffset = this.offset;

        if (that.debugQ) {
          console.group('each section');
        }

        sections.forEach(function (s) {

          var winScroll = $(window).scrollTop() + 2;
          var curScroll = $(s).offset().top - curOffset;

          if (that.debugQ) {
            console.table({
              windowPosition: winScroll,
              sectionScrollPosition: curScroll,
              section: s
            });
          }

          if (winScroll < curScroll && current == undefined) {
            current = s;
          }
        });
        if (that.debugQ) {
          console.groupEnd();
        }
        if (that.debugQ) {
          console.groupEnd();
        }

        if (current != undefined) {
          $('html, body').animate({
            scrollTop: $(current).offset().top - curOffset
          }, 1000, function () {});
        }
      }

    }
  };

  /* script */
  var __vue_script__$o = script$o;

  /* template */
  var __vue_render__$o = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("button", {
      attrs: { type: "button", name: "button" },
      on: { click: _vm.animateScrollTo }
    }, [_c("i", { staticClass: "fa fa-chevron-down" })]);
  };
  var __vue_staticRenderFns__$o = [];
  __vue_render__$o._withStripped = true;

  /* style */
  var __vue_inject_styles__$o = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-9c3f1a3a_0", { source: "\nbutton[data-v-9c3f1a3a] {\n  background: radial-gradient(rgb(0, 198, 255), rgb(0, 114, 255));\n  background-color: transparent;\n  border: transparent 0px solid;\n\n  margin: 0;\n  padding: 0px;\n\n  width:32px;\n  height:32px;\n\n  border-radius: 50%;\n  transition: all 0.5s ease;\n}\ni[data-v-9c3f1a3a] { font-size: 16px;\n}\nbutton[data-v-9c3f1a3a]:focus {\n  outline: transparent 0px solid;\n}\nbutton[data-v-9c3f1a3a]:hover {\n  color:white;\n  opacity: 0.7;\n}\nbutton:hover i[data-v-9c3f1a3a] {\n  position: relative;\n  -moz-animation: bounce-data-v-9c3f1a3a 0.5s infinite linear;\n  -o-animation: bounce-data-v-9c3f1a3a 0.5s infinite linear;\n  -webkit-animation: bounce-data-v-9c3f1a3a 0.5s infinite linear;\n  animation: bounce-data-v-9c3f1a3a 0.5s infinite linear;\n}\n@-webkit-keyframes bounce-data-v-9c3f1a3a {\n0% { top: 0;\n}\n50% { top: 0.2em;\n}\n100% { top: 0;\n}\n}\n@-moz-keyframes bounce-data-v-9c3f1a3a {\n0% { top: 0;\n}\n50% { top: 0.2em;\n}\n100% { top: 0;\n}\n}\n@-o-keyframes bounce-data-v-9c3f1a3a {\n0% { top: 0;\n}\n50% { top: 0.2em;\n}\n100% { top: 0;\n}\n}\n@-ms-keyframes bounce-data-v-9c3f1a3a {\n0% { top: 0;\n}\n50% { top: 0.2em;\n}\n100% { top: 0;\n}\n}\n@keyframes bounce-data-v-9c3f1a3a {\n0% { top: 0;\n}\n50% { top: 0.2em;\n}\n100% { top: 0;\n}\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/SectionNavigation/SectionNavigationArrowDown/SectionNavigationArrowDown.vue"], "names": [], "mappings": ";AAiFA;EACA,+DAAA;EACA,6BAAA;EACA,6BAAA;;EAEA,SAAA;EACA,YAAA;;EAEA,UAAA;EACA,WAAA;;EAEA,kBAAA;EACA,yBAAA;AACA;AAEA,qBAAA,eAAA;AAAA;AAEA;EACA,8BAAA;AACA;AACA;EACA,WAAA;EACA,YAAA;AACA;AAEA;EACA,kBAAA;EACA,2DAAA;EACA,yDAAA;EACA,8DAAA;EACA,sDAAA;AAEA;AAEA;AACA,KAAA,MAAA;AAAA;AACA,MAAA,UAAA;AAAA;AACA,OAAA,MAAA;AAAA;AACA;AACA;AACA,KAAA,MAAA;AAAA;AACA,MAAA,UAAA;AAAA;AACA,OAAA,MAAA;AAAA;AACA;AACA;AACA,KAAA,MAAA;AAAA;AACA,MAAA,UAAA;AAAA;AACA,OAAA,MAAA;AAAA;AACA;AACA;AACA,KAAA,MAAA;AAAA;AACA,MAAA,UAAA;AAAA;AACA,OAAA,MAAA;AAAA;AACA;AACA;AACA,KAAA,MAAA;AAAA;AACA,MAAA,UAAA;AAAA;AACA,OAAA,MAAA;AAAA;AACA", "file": "SectionNavigationArrowDown.vue", "sourcesContent": ["<template>\n  <button type='button' name='button' v-on:click='animateScrollTo'>\n    <i class='fa fa-chevron-down'></i>\n  </button>\n</template>\n\n<script>\n\n\nexport default {\n  props: {\n    offsetSelector: {\n      type: String,\n      default: ''\n    },\n    debugQ: {\n      type: Boolean,\n      default: false\n    }\n  },\n\n  data: function () {\n    return {\n      offset: 0\n    };\n  },\n  computed: {\n\n  },\n  methods: {\n    setOffset: function() {\n      this.offset = this.offsetSelector == undefined\n        ? 0\n        : $(this.offsetSelector).outerHeight() == undefined\n          ? 0\n          : $(this.offsetSelector).outerHeight();\n    },\n\n    animateScrollTo: function() {\n      this.setOffset();\n      var that = this;\n      if (that.debugQ) { console.group('[section-down]'); }\n      if (that.debugQ) { console.table({currentOffset: curOffset, offsetSelector:that.offsetSelector}); }\n\n      var sections = document.querySelectorAll('section:not([display=\"none\"])');\n      var current = undefined;\n      var curOffset = this.offset;\n\n      if (that.debugQ) { console.group('each section'); }\n\n      sections.forEach(function(s){\n\n        var winScroll = $(window).scrollTop() + 2;\n        var curScroll = $(s).offset().top - curOffset;\n\n        if (that.debugQ) {\n          console.table({\n            windowPosition: winScroll,\n            sectionScrollPosition: curScroll,\n            section: s\n          });\n        }\n\n        if ( winScroll < curScroll && current == undefined)\n        { current = s; }\n      })\n      if (that.debugQ) { console.groupEnd(); }\n      if (that.debugQ) { console.groupEnd(); }\n\n      if (current != undefined) {\n        $('html, body').animate({\n          scrollTop: $(current).offset().top - curOffset\n        }, 1000, function() {});\n      }\n    }\n\n  }\n}\n</script>\n\n<style scoped>\nbutton {\n  background: radial-gradient(rgb(0, 198, 255), rgb(0, 114, 255));\n  background-color: transparent;\n  border: transparent 0px solid;\n\n  margin: 0;\n  padding: 0px;\n\n  width:32px;\n  height:32px;\n\n  border-radius: 50%;\n  transition: all 0.5s ease;\n}\n\ni { font-size: 16px; }\n\nbutton:focus {\n  outline: transparent 0px solid;\n}\nbutton:hover {\n  color:white;\n  opacity: 0.7;\n}\n\nbutton:hover i {\n  position: relative;\n  -moz-animation: bounce 0.5s infinite linear;\n  -o-animation: bounce 0.5s infinite linear;\n  -webkit-animation: bounce 0.5s infinite linear;\n  animation: bounce 0.5s infinite linear;\n\n}\n\n@-webkit-keyframes bounce {\n    0% { top: 0; }\n    50% { top: 0.2em; }\n    100% { top: 0; }\n}\n@-moz-keyframes bounce {\n    0% { top: 0; }\n    50% { top: 0.2em; }\n    100% { top: 0; }\n}\n@-o-keyframes bounce {\n    0% { top: 0; }\n    50% { top: 0.2em; }\n    100% { top: 0; }\n}\n@-ms-keyframes bounce {\n    0% { top: 0; }\n    50% { top: 0.2em; }\n    100% { top: 0; }\n}\n@keyframes bounce {\n    0% { top: 0; }\n    50% { top: 0.2em; }\n    100% { top: 0; }\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$o = "data-v-9c3f1a3a";
  /* module identifier */
  var __vue_module_identifier__$o = undefined;
  /* functional template */
  var __vue_is_functional_template__$o = false;
  /* style inject SSR */

  var SectionNavigationArrowDown = normalizeComponent_1({ render: __vue_render__$o, staticRenderFns: __vue_staticRenderFns__$o }, __vue_inject_styles__$o, __vue_script__$o, __vue_scope_id__$o, __vue_is_functional_template__$o, __vue_module_identifier__$o, browser, undefined);

  //
  //
  //
  //
  //
  //


  var script$p = {
    props: {
      offsetSelector: {
        type: String,
        default: ''
      },
      debugQ: {
        type: Boolean,
        default: false
      }
    },

    data: function data() {
      return { offset: 0 };
    },

    computed: {},

    methods: {
      setOffset: function setOffset() {
        this.offset = this.offsetSelector == undefined ? 0 : $(this.offsetSelector).outerHeight() == undefined ? 0 : $(this.offsetSelector).outerHeight();
      },

      animateScrollTo: function animateScrollTo() {
        this.setOffset();
        var that = this;
        if (that.debugQ) {
          console.group('[section-up]');
        }
        if (that.debugQ) {
          console.table({ currentOffset: curOffset, offsetSelector: that.offsetSelector });
        }

        var sections = document.querySelectorAll('section:not([display="none"])');
        var current = undefined;
        var curOffset = this.offset;

        if (that.debugQ) {
          console.group('each section');
        }
        sections.forEach(function (s) {
          // window position
          var winScroll = $(window).scrollTop() - 0.2;
          // section position
          var curScroll = $(s).offset().top - curOffset;

          if (that.debugQ) {
            console.table({
              windowPosition: winScroll,
              sectionScrollPosition: curScroll,
              section: s
            });
          }

          if (winScroll > curScroll) {
            current = s;
          }
        });
        if (that.debugQ) {
          console.groupEnd();
        }
        if (that.debugQ) {
          console.groupEnd();
        }

        // Scroll to section
        if (current != undefined) {
          $('html, body').animate({
            scrollTop: $(current).offset().top - curOffset
          }, 1000, function () {});
        }
      }
    }
  };

  /* script */
  var __vue_script__$p = script$p;

  /* template */
  var __vue_render__$p = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("button", {
      attrs: { type: "button", name: "button" },
      on: { click: _vm.animateScrollTo }
    }, [_c("i", { staticClass: "fa fa-chevron-up" })]);
  };
  var __vue_staticRenderFns__$p = [];
  __vue_render__$p._withStripped = true;

  /* style */
  var __vue_inject_styles__$p = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-54de3134_0", { source: "\nbutton[data-v-54de3134] {\n  background: radial-gradient(rgb(0, 198, 255), rgb(0, 114, 255));\n  background-color: transparent;\n  border: transparent 0px solid;\n\n  margin: 0;\n  padding: 0px;\n\n  width:32px;\n  height:32px;\n\n  border-radius: 50%;\n  transition: all 0.5s ease;\n}\ni[data-v-54de3134] { font-size: 16px;\n}\nbutton[data-v-54de3134]:focus {\n  outline: transparent 0px solid;\n}\nbutton[data-v-54de3134]:hover {\n  color:white;\n  opacity: 0.7;\n}\nbutton:hover i[data-v-54de3134] {\n  position: relative;\n  -moz-animation: bounce-data-v-54de3134 0.5s infinite linear;\n  -o-animation: bounce-data-v-54de3134 0.5s infinite linear;\n  -webkit-animation: bounce-data-v-54de3134 0.5s infinite linear;\n  animation: bounce-data-v-54de3134 0.5s infinite linear;\n}\n@-webkit-keyframes bounce-data-v-54de3134 {\n0% { top: 0;\n}\n50% { top: -0.2em;\n}\n100% { top: 0;\n}\n}\n@-moz-keyframes bounce-data-v-54de3134 {\n0% { top: 0;\n}\n50% { top: -0.2em;\n}\n100% { top: 0;\n}\n}\n@-o-keyframes bounce-data-v-54de3134 {\n0% { top: 0;\n}\n50% { top: -0.2em;\n}\n100% { top: 0;\n}\n}\n@-ms-keyframes bounce-data-v-54de3134 {\n0% { top: 0;\n}\n50% { top: -0.2em;\n}\n100% { top: 0;\n}\n}\n@keyframes bounce-data-v-54de3134 {\n0% { top: 0;\n}\n50% { top: -0.2em;\n}\n100% { top: 0;\n}\n}\n\n\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/SectionNavigation/SectionNavigationArrowUp/SectionNavigationArrowUp.vue"], "names": [], "mappings": ";AAqFA;EACA,+DAAA;EACA,6BAAA;EACA,6BAAA;;EAEA,SAAA;EACA,YAAA;;EAEA,UAAA;EACA,WAAA;;EAEA,kBAAA;EACA,yBAAA;AAEA;AAEA,qBAAA,eAAA;AAAA;AAEA;EACA,8BAAA;AACA;AAEA;EACA,WAAA;EACA,YAAA;AACA;AAEA;EACA,kBAAA;EACA,2DAAA;EACA,yDAAA;EACA,8DAAA;EACA,sDAAA;AAEA;AAEA;AACA,KAAA,MAAA;AAAA;AACA,MAAA,WAAA;AAAA;AACA,OAAA,MAAA;AAAA;AACA;AACA;AACA,KAAA,MAAA;AAAA;AACA,MAAA,WAAA;AAAA;AACA,OAAA,MAAA;AAAA;AACA;AACA;AACA,KAAA,MAAA;AAAA;AACA,MAAA,WAAA;AAAA;AACA,OAAA,MAAA;AAAA;AACA;AACA;AACA,KAAA,MAAA;AAAA;AACA,MAAA,WAAA;AAAA;AACA,OAAA,MAAA;AAAA;AACA;AACA;AACA,KAAA,MAAA;AAAA;AACA,MAAA,WAAA;AAAA;AACA,OAAA,MAAA;AAAA;AACA", "file": "SectionNavigationArrowUp.vue", "sourcesContent": ["<template>\n  <button type='button' name='button' v-on:click='animateScrollTo'>\n    <i class='fa fa-chevron-up'></i>\n  </button>\n</template>\n\n<script>\n\n\nexport default {\n  props: {\n    offsetSelector: {\n      type: String,\n      default: ''\n    },\n    debugQ: {\n      type: Boolean,\n      default: false\n    }\n  },\n\n  data: function () {\n    return { offset: 0 }\n  },\n\n  computed: {\n\n  },\n\n  methods: {\n    setOffset: function() {\n      this.offset = this.offsetSelector == undefined\n        ? 0\n        : $(this.offsetSelector).outerHeight() == undefined\n          ? 0\n          : $(this.offsetSelector).outerHeight();\n    },\n\n    animateScrollTo: function() {\n      this.setOffset();\n      var that = this;\n      if (that.debugQ) { console.group('[section-up]'); }\n      if (that.debugQ) { console.table({currentOffset: curOffset, offsetSelector:that.offsetSelector}); }\n\n\n      var sections = document.querySelectorAll('section:not([display=\"none\"])');\n      var current = undefined;\n      var curOffset = this.offset;\n\n\n      if (that.debugQ) { console.group('each section'); }\n      sections.forEach(function(s){\n        // window position\n        var winScroll = $(window).scrollTop() - 0.2;\n        // section position\n        var curScroll = $(s).offset().top - curOffset;\n\n\n        if (that.debugQ) {\n          console.table({\n            windowPosition: winScroll,\n            sectionScrollPosition: curScroll,\n            section: s\n          });\n        }\n\n        if ( winScroll > curScroll) { current = s; }\n      })\n      if (that.debugQ) { console.groupEnd(); }\n      if (that.debugQ) { console.groupEnd(); }\n\n\n      // Scroll to section\n      if (current != undefined) {\n        $('html, body').animate({\n          scrollTop: $(current).offset().top - curOffset\n        }, 1000, function() {});\n      }\n\n    }\n  }\n}\n</script>\n\n<style scoped>\nbutton {\n  background: radial-gradient(rgb(0, 198, 255), rgb(0, 114, 255));\n  background-color: transparent;\n  border: transparent 0px solid;\n\n  margin: 0;\n  padding: 0px;\n\n  width:32px;\n  height:32px;\n\n  border-radius: 50%;\n  transition: all 0.5s ease;\n\n}\n\ni { font-size: 16px; }\n\nbutton:focus {\n  outline: transparent 0px solid;\n}\n\nbutton:hover {\n  color:white;\n  opacity: 0.7;\n}\n\nbutton:hover i {\n  position: relative;\n  -moz-animation: bounce 0.5s infinite linear;\n  -o-animation: bounce 0.5s infinite linear;\n  -webkit-animation: bounce 0.5s infinite linear;\n  animation: bounce 0.5s infinite linear;\n\n}\n\n@-webkit-keyframes bounce {\n    0% { top: 0; }\n    50% { top: -0.2em; }\n    100% { top: 0; }\n}\n@-moz-keyframes bounce {\n    0% { top: 0; }\n    50% { top: -0.2em; }\n    100% { top: 0; }\n}\n@-o-keyframes bounce {\n    0% { top: 0; }\n    50% { top: -0.2em; }\n    100% { top: 0; }\n}\n@-ms-keyframes bounce {\n    0% { top: 0; }\n    50% { top: -0.2em; }\n    100% { top: 0; }\n}\n@keyframes bounce {\n    0% { top: 0; }\n    50% { top: -0.2em; }\n    100% { top: 0; }\n}\n\n\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$p = "data-v-54de3134";
  /* module identifier */
  var __vue_module_identifier__$p = undefined;
  /* functional template */
  var __vue_is_functional_template__$p = false;
  /* style inject SSR */

  var SectionNavigationArrowUp = normalizeComponent_1({ render: __vue_render__$p, staticRenderFns: __vue_staticRenderFns__$p }, __vue_inject_styles__$p, __vue_script__$p, __vue_scope_id__$p, __vue_is_functional_template__$p, __vue_module_identifier__$p, browser, undefined);

  //

  var script$q = {
    components: { SectionNavigationArrowDown: SectionNavigationArrowDown, SectionNavigationArrowUp: SectionNavigationArrowUp },
    props: {
      offsetSelector: {
        type: String,
        default: undefined
      },
      debugQ: {
        type: Boolean,
        default: false
      }
    }
  };

  /* script */
  var __vue_script__$q = script$q;

  /* template */
  var __vue_render__$q = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", [_c("section-navigation-arrow-up", {
      attrs: { offsetSelector: _vm.offsetSelector, debugQ: _vm.debugQ }
    }), _vm._v(" "), _c("section-navigation-arrow-down", {
      attrs: { offsetSelector: _vm.offsetSelector, debugQ: _vm.debugQ }
    })], 1);
  };
  var __vue_staticRenderFns__$q = [];
  __vue_render__$q._withStripped = true;

  /* style */
  var __vue_inject_styles__$q = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-0855bcff_0", { source: "\ndiv[data-v-0855bcff] {\n  position: fixed;\n  top:50%;\n  margin-left: 10px;\n}\ndiv > button[data-v-0855bcff] {\n  display: block;\n  margin-bottom: 10px !important;\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/SectionNavigation/SectionNavigation/SectionNavigation.vue"], "names": [], "mappings": ";AA2BA;EACA,eAAA;EACA,OAAA;EACA,iBAAA;AACA;AAEA;EACA,cAAA;EACA,8BAAA;AACA", "file": "SectionNavigation.vue", "sourcesContent": ["<template>\n  <div>\n    <section-navigation-arrow-up :offsetSelector=\"offsetSelector\" :debugQ=\"debugQ\"/>\n    <section-navigation-arrow-down :offsetSelector=\"offsetSelector\" :debugQ=\"debugQ\"/>\n  </div>\n</template>\n\n<script>\nimport SectionNavigationArrowDown from '../SectionNavigationArrowDown/SectionNavigationArrowDown.vue';\nimport SectionNavigationArrowUp from '../SectionNavigationArrowUp/SectionNavigationArrowUp.vue';\n\nexport default {\n  components: { SectionNavigationArrowDown, SectionNavigationArrowUp },\n  props: {\n    offsetSelector: {\n      type:String,\n      default: undefined\n    },\n    debugQ: {\n      type: Boolean,\n      default: false\n    }\n  }\n}\n</script>\n\n<style scoped>\ndiv {\n  position: fixed;\n  top:50%;\n  margin-left: 10px;\n}\n\ndiv > button {\n  display: block;\n  margin-bottom: 10px !important;\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$q = "data-v-0855bcff";
  /* module identifier */
  var __vue_module_identifier__$q = undefined;
  /* functional template */
  var __vue_is_functional_template__$q = false;
  /* style inject SSR */

  var SectionNavigation = normalizeComponent_1({ render: __vue_render__$q, staticRenderFns: __vue_staticRenderFns__$q }, __vue_inject_styles__$q, __vue_script__$q, __vue_scope_id__$q, __vue_is_functional_template__$q, __vue_module_identifier__$q, browser, undefined);

  var sortTokenObject = function sortTokenObject(object) {
    return Object.entries(object).sort(function (a, b) {
      var ka = a[0];
      var kb = b[0];
      return ka > kb || kb.length - ka.length;
    }).reduce(function (o, _ref) {
      var _ref2 = slicedToArray(_ref, 2),
          k = _ref2[0],
          v = _ref2[1];

      o[k] = v;
      return o;
    }, {});
  };

  var conditionalRenders = {
    eq: '=',
    neq: '≠',
    gt: '>',
    lt: '<',
    gte: '≥',
    lte: '≤',
    includes: '⊃'
  };

  var logicRenders = {
    and: '⋀',
    or: '⋁'
  };

  var conditionalTokens = {
    eq: 'eq',
    equal: 'eq',
    '=': 'eq',
    '!=': 'neq',
    '≠': 'neq',
    '>': 'gt',
    '≥': 'gte',
    '>=': 'gte',
    '<': 'lt',
    '≤': 'lte',
    '<=': 'lte',
    'is': 'eq',
    'is equal to': 'eq',
    'is not': 'neq',
    neq: 'neq',
    'not equal to': 'neq',
    'not eq to': 'neq',
    'is not equal to': 'neq',
    gt: 'gt',
    'greater than': 'gt',
    'less than': 'lt',
    lt: 'lt',
    'less than or equal to': 'lte',
    'greater than or equal to': 'gte',
    'member of': 'includes',
    substring: 'includes',
    contains: 'includes',
    includes: 'includes'
  };

  var functionTokens = {
    max: 'max',
    maximum: 'max',
    minimum: 'min',
    min: 'min',
    length: 'length',
    len: 'length',
    mean: 'mean',
    median: 'median',
    average: 'mean',
    identity: 'identity'
  };

  var logicTokens = {
    and: 'and',
    or: 'or'
  };

  functionTokens = sortTokenObject(functionTokens);
  logicTokens = sortTokenObject(logicTokens);
  conditionalTokens = sortTokenObject(conditionalTokens);

  var extractTokens = function extractTokens(text, tokens) {
    var fallback = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : undefined;
    var scope = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : undefined;
    var dropUntilMatchQ = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : false;

    var token = fallback;
    var str = scope === undefined ? text : text.slice(0, scope);
    for (var t in tokens) {
      if (str.includes(t)) {
        token = tokens[t];
        if (dropUntilMatchQ) {
          text = text.slice(str.indexOf(t) + t.length, str.length);
        } else {
          text = text.replace(t, '');
        }
        break;
      }
    }
    return [token, text];
  };

  var fields = function fields(records) {
    var _ref;

    var recordIds = ids(records);
    var allRecordFields = (_ref = []).concat.apply(_ref, toConsumableArray(recordIds.map(function (recordId) {
      return Object.keys(records[recordId]);
    })));
    return [].concat(toConsumableArray(new Set(allRecordFields)));
  };

  var fieldTokens = function fieldTokens(records) {
    var recordFields = fields(records);
    var tokens = {};
    recordFields.map(function (field) {
      tokens[field] = field;
    });
    return tokens;
  };

  var ids = function ids(records) {
    return Object.keys(records);
  };

  var giRegex = function giRegex(text, tokens) {
    var regex = new RegExp(text, 'gi');

    var suggestions = {};

    tokens.filter(function (token) {
      var match = token.match(regex);
      // console.log(match, token);

      var found = !(match == null || match.join('') === '');

      if (found) {
        suggestions[token] = match[0];
      }
      return found;
    });

    suggestions = Object.keys(suggestions).sort(function (a, b) {
      return text.indexOf(suggestions[b]) - text.indexOf(suggestions[a]);
    });
    return suggestions;
  };

  var groupByConjunctiveNormalForm = function groupByConjunctiveNormalForm(filters) {
    var groupped = [];

    for (var i = 0; i < filters.length; i++) {
      var logic = filters[i].logic;
      if (logic == 'and') {
        groupped.push([filters[i]]);
      } else {
        var list = groupped[groupped.length - 1];
        if (list == undefined) {
          filters[i].logic = 'and';
          groupped.push([filters[i]]);
        } else {
          groupped[groupped.length - 1].push(filters[i]);
        }
      }

      // console.log(groupped);
    }

    return groupped;
  };

  var scrubInputText = function scrubInputText(text) {
    if (text[0] === ' ') {
      text = text.slice(1, text.length);
    }
    if (text[text.length - 1] === ' ') {
      text = text.slice(1, text.length - 1);
    }
    return text;
  };

  //

  var script$r = {
    props: ["filter", "index"],
    data: function data() {
      return {
        logicRenders: logicRenders,
        conditionalRenders: conditionalRenders
      };
    },
    methods: {
      clicked: function clicked() {
        this.$emit("filterClick");
      }
    }
  };

  /* script */
  var __vue_script__$r = script$r;

  /* template */
  var __vue_render__$r = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { staticClass: "filter", on: { click: _vm.clicked } }, [_vm.index ? _c("span", { staticClass: "logic", class: _vm.filter.logic }, [_vm._v("\n    " + _vm._s(_vm.logicRenders[_vm.filter.logic]) + "\n  ")]) : _vm._e(), _vm._v(" "), _vm.filter.function !== "identity" ? _c("span", { staticClass: "function" }, [_vm._v("\n    " + _vm._s(_vm.filter.function) + "\n  ")]) : _vm._e(), _vm._v(" "), _c("span", { staticClass: "property" }, [_vm._v(_vm._s(_vm.filter.property))]), _vm._v(" "), _c("span", { staticClass: "conditional" }, [_vm._v("\n    " + _vm._s(_vm.conditionalRenders[_vm.filter.conditional]) + "\n  ")]), _vm._v(" "), _c("span", { staticClass: "input" }, [_vm._v("\n    " + _vm._s(_vm.filter.input) + "\n  ")])]);
  };
  var __vue_staticRenderFns__$r = [];
  __vue_render__$r._withStripped = true;

  /* style */
  var __vue_inject_styles__$r = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-59e2affe_0", { source: ".filter[data-v-59e2affe] {\n  display: inline-block;\n}\n.logic[data-v-59e2affe] {\n  text-transform: uppercase;\n  font-weight: bold;\n  padding-left: 2px;\n}\n.and[data-v-59e2affe] {\n  color: #0072ff;\n}\n.or[data-v-59e2affe] {\n  color: #00c6ff;\n}\n.property[data-v-59e2affe] {\n  /* border-bottom: solid 2px #f83600; */\n  color: #f83600;\n}\n.cnf[data-v-59e2affe] {\n  display: inline-block;\n}\n.filter[data-v-59e2affe]:hover {\n  opacity: 0.5;\n  text-decoration: line-through;\n  color: red;\n}\n", map: undefined, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$r = "data-v-59e2affe";
  /* module identifier */
  var __vue_module_identifier__$r = undefined;
  /* functional template */
  var __vue_is_functional_template__$r = false;
  /* style inject SSR */

  var InputAlphaFilter = normalizeComponent_1({ render: __vue_render__$r, staticRenderFns: __vue_staticRenderFns__$r }, __vue_inject_styles__$r, __vue_script__$r, __vue_scope_id__$r, __vue_is_functional_template__$r, __vue_module_identifier__$r, browser, undefined);

  //

  var script$s = {
    components: { InputAlphaFilter: InputAlphaFilter },
    props: ['filters'],
    data: function data() {
      return {
        logicRenders: logicRenders
      };
    },
    computed: {
      conjunctiveNormalForm: function conjunctiveNormalForm() {
        return groupByConjunctiveNormalForm(this.filters);
      }
    },
    methods: {
      filterClick: function filterClick(index, jndex) {
        var total = 0;
        var that = this;
        for (var i = 0; i < index; i++) {
          total += that.conjunctiveNormalForm[i].length;
        }
        total += jndex;
        // console.log('total', total)
        that.$emit('filterClick', total);
      }
    }
  };

  /* script */
  var __vue_script__$s = script$s;

  /* template */
  var __vue_render__$s = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", _vm._l(_vm.conjunctiveNormalForm, function (cnf, index) {
      return _c("div", { key: cnf.id, staticClass: "cnf" }, [index ? _c("span", { staticClass: "logic and" }, [_vm._v(_vm._s(_vm.logicRenders["and"]))]) : _vm._e(), _vm._v("\n    (\n      "), _vm._l(cnf, function (filter, jndex) {
        return _c("InputAlphaFilter", {
          key: filter.id,
          attrs: { filter: filter, index: jndex },
          on: {
            filterClick: function filterClick($event) {
              return _vm.filterClick(index, jndex);
            }
          }
        });
      }), _vm._v("\n    )\n  ")], 2);
    }), 0);
  };
  var __vue_staticRenderFns__$s = [];
  __vue_render__$s._withStripped = true;

  /* style */
  var __vue_inject_styles__$s = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-f05708b4_0", { source: ".filter[data-v-f05708b4] {\n  display: inline-block;\n}\n.logic[data-v-f05708b4] {\n  text-transform: uppercase;\n  font-weight: bold;\n  padding-left: 2px;\n}\n.and[data-v-f05708b4] {\n  color: #0072ff;\n}\n.or[data-v-f05708b4] {\n  color: #00c6ff;\n}\n.property[data-v-f05708b4] {\n  /* border-bottom: solid 2px #f83600; */\n  color: #f83600;\n}\n.cnf[data-v-f05708b4] {\n  display: inline-block;\n}\n.filter[data-v-f05708b4]:hover {\n  opacity: 0.5;\n  text-decoration: line-through;\n  color: red;\n}\n", map: undefined, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$s = "data-v-f05708b4";
  /* module identifier */
  var __vue_module_identifier__$s = undefined;
  /* functional template */
  var __vue_is_functional_template__$s = false;
  /* style inject SSR */

  var InputAlphaConjunctiveNormalForm = normalizeComponent_1({ render: __vue_render__$s, staticRenderFns: __vue_staticRenderFns__$s }, __vue_inject_styles__$s, __vue_script__$s, __vue_scope_id__$s, __vue_is_functional_template__$s, __vue_module_identifier__$s, browser, undefined);

  //

  var script$t = {
    components: { Alert: Alert, InputAlphaFilter: InputAlphaFilter, InputAlphaConjunctiveNormalForm: InputAlphaConjunctiveNormalForm },
    props: {
      data: {
        type: Object,
        default: function _default() {
          return {};
        }
      }
    },
    data: function data() {
      return {
        input: '',
        func: '',
        propertyTokens: sortTokenObject(fieldTokens(this.data)),
        allTokens: ids(logicTokens).concat(ids(functionTokens)).concat(ids(conditionalTokens)).concat(ids(sortTokenObject(fieldTokens(this.data)))),
        errors: [],
        filters: [],
        suggestions: [],
        CNF: []
      };
    },
    methods: {
      removeFilter: function removeFilter(index) {
        this.filters.splice(index, 1);
      },
      processInput: function processInput() {
        var that = this;
        var text = that.input;
        that.input = '';

        var filter = this.tokenSearch(text);

        var anyUndefinedQ = false;
        for (var key in filter) {
          var undef = filter[key] === undefined;
          if (undef) {
            that.tokenError(key);
            anyUndefinedQ = true;
          }
        }

        if (anyUndefinedQ) {
          return;
        }

        that.filters.push(filter);
        that.CNF = groupByConjunctiveNormalForm(that.filters);
      },
      tokenError: function tokenError(key) {
        this.errors.push({ error: 'Undefined', type: 'warning', message: key + ' is undefined. Filter discarded.' });
      },
      suggest: function suggest() {
        var that = this;
        that.suggestions = giRegex(that.input, that.allTokens);
      },
      userInputChanged: function userInputChanged(event) {
        this.input = event.target.value;
        this.suggest();
      },
      tokenSearch: function tokenSearch(text) {
        var logic = void 0;
        var func = void 0;
        var cond = void 0;
        var prop = void 0;
        var inp = void 0;

        var _extractTokens = extractTokens(text, this.propertyTokens);

        var _extractTokens2 = slicedToArray(_extractTokens, 2);

        prop = _extractTokens2[0];
        text = _extractTokens2[1];

        var _extractTokens3 = extractTokens(text, logicTokens, 'and', 3);

        var _extractTokens4 = slicedToArray(_extractTokens3, 2);

        logic = _extractTokens4[0];
        text = _extractTokens4[1];

        var _extractTokens5 = extractTokens(text, functionTokens, 'identity');

        var _extractTokens6 = slicedToArray(_extractTokens5, 2);

        func = _extractTokens6[0];
        text = _extractTokens6[1];

        var _extractTokens7 = extractTokens(text, conditionalTokens, 'eq', undefined, true);

        var _extractTokens8 = slicedToArray(_extractTokens7, 2);

        cond = _extractTokens8[0];
        text = _extractTokens8[1];

        inp = scrubInputText(text);

        return {
          logic: logic,
          function: func,
          conditional: cond,
          property: prop,
          input: inp
        };
      },
      removeAlert: function removeAlert(index) {
        this.errors.splice(index, 1);
      }
    }
  };

  /* script */
  var __vue_script__$t = script$t;

  /* template */
  var __vue_render__$t = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { staticClass: "alpha" }, [_c("input", {
      directives: [{
        name: "model",
        rawName: "v-model",
        value: _vm.input,
        expression: "input"
      }],
      attrs: { type: "text" },
      domProps: { value: _vm.input },
      on: {
        keyup: function keyup($event) {
          if (!$event.type.indexOf("key") && _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")) {
            return null;
          }
          return _vm.processInput($event);
        },
        input: [function ($event) {
          if ($event.target.composing) {
            return;
          }
          _vm.input = $event.target.value;
        }, _vm.userInputChanged]
      }
    }), _vm._v("\n  " + _vm._s(Object.keys(_vm.propertyTokens)) + "\n\n  "), _c("ol", _vm._l(_vm.suggestions, function (suggestion) {
      return _c("li", { key: suggestion.id }, [_vm._v("\n      " + _vm._s(suggestion) + "\n    ")]);
    }), 0), _vm._v(" "), _c("div", _vm._l(_vm.errors, function (error, index) {
      return _c("Alert", {
        key: error.id,
        attrs: {
          error: error.error,
          message: error.message,
          type: error.type,
          value: _vm.input
        },
        on: {
          remove: function remove($event) {
            return _vm.removeAlert(index);
          },
          input: function input($event) {
            return _vm.userInputChanged($event);
          }
        }
      });
    }), 1), _vm._v(" "), _c("div", { staticClass: "query" }, [_c("InputAlphaConjunctiveNormalForm", {
      attrs: { filters: _vm.filters },
      on: { filterClick: _vm.removeFilter }
    })], 1)]);
  };
  var __vue_staticRenderFns__$t = [];
  __vue_render__$t._withStripped = true;

  /* style */
  var __vue_inject_styles__$t = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-5b47067a_0", { source: "div.alpha[data-v-5b47067a] {\n  text-align: center;\n}\ninput[data-v-5b47067a] {\n  border-width: 0px 0px 2px 0;\n  border-style: solid;\n  border-image: linear-gradient(to right, rgb(0, 198, 255), rgb(0, 114, 255)) 20;\n  -webkit-animation: Gradient-data-v-5b47067a 5s ease infinite;\n  -moz-animation: Gradient-data-v-5b47067a 5s ease infinite;\n  animation: Gradient-data-v-5b47067a 5s ease infinite;\n}\ninput[data-v-5b47067a]:focus {\n  outline: none;\n}\n@-webkit-keyframes Gradient-data-v-5b47067a {\n0% {\n    background-position: 0% 50%;\n}\n100% {\n    background-position: 100% 50%;\n}\n}\n@-moz-keyframes Gradient-data-v-5b47067a {\n0% {\n    background-position: 0% 50%;\n}\n100% {\n    background-position: 100% 50%;\n}\n}\n@keyframes Gradient-data-v-5b47067a {\n0% {\n    background-position: 0% 50%;\n}\n100% {\n    background-position: 100% 50%;\n}\n}\n", map: undefined, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$t = "data-v-5b47067a";
  /* module identifier */
  var __vue_module_identifier__$t = undefined;
  /* functional template */
  var __vue_is_functional_template__$t = false;
  /* style inject SSR */

  var InputAlpha = normalizeComponent_1({ render: __vue_render__$t, staticRenderFns: __vue_staticRenderFns__$t }, __vue_inject_styles__$t, __vue_script__$t, __vue_scope_id__$t, __vue_is_functional_template__$t, __vue_module_identifier__$t, browser, undefined);

  var colors = ["#F44336", "#FF4081", "#9C27B0", "#673AB7", "#3F51B5", "#2196F3", "#03A9F4", "#00BCD4", "#009688", "#4CAF50", "#8BC34A", "#CDDC39", "#FFC107", "#FF9800", "#FF5722", "#795548", "#9E9E9E", "#607D8B"];

  function LightenDarkenColor(col, amt) {
    // https://css-tricks.com/snippets/javascript/lighten-darken-color/
    var usePound = false;

    if (col[0] == "#") {
      col = col.slice(1);
      usePound = true;
    }

    var num = parseInt(col, 16);

    var r = (num >> 16) + amt;

    if (r > 255) r = 255;else if (r < 0) r = 0;

    var b = (num >> 8 & 0x00ff) + amt;

    if (b > 255) b = 255;else if (b < 0) b = 0;

    var g = (num & 0x0000ff) + amt;

    if (g > 255) g = 255;else if (g < 0) g = 0;

    return (usePound ? "#" : "") + (g | b << 8 | r << 16).toString(16);
  }

  function initialsFontSize(size) {
    return Math.floor(size / 2.5);
  }

  function azOnly(string) {
    var filtered = "";
    for (var i = 0; i < string.length; i++) {
      var char = string[i];
      if (/^[a-zA-Z]+$/.test(char)) {
        filtered += char;
      }
    }
    return filtered;
  }

  function captialsIn(string) {
    var caps = [];
    var str = azOnly(string);
    for (var i = 0; i < str.length; i++) {
      if (str[i] === str[i].toUpperCase()) {
        caps.push(str[i]);
      }
    }
    return caps;
  }

  function initialsOf(name) {
    var caps = captialsIn(name.split(" ").join(""));
    var first = caps[0],
        last = "";
    if (caps.length > 1) {
      last = caps[caps.length - 1];
    }
    var inits = first + last;
    if (inits !== "undefined") {
      return inits;
    }
    return name[0].toUpperCase();
  }

  //

  var hrefSignIn = '',
      hrefSignOut = '',
      hrefSignUp = '',
      hrefProfile = '';

  var script$u = {
    name: 'DjangoUser',
    props: {
      loggedInQ: {
        type: Boolean,
        default: false
      },
      dropDirection: {
        type: String,
        default: 'dropleft'
      },
      username: {
        type: String,
        default: 'User Name'
      },
      hrefSignIn: {
        type: String,
        default: hrefSignIn
      },
      hrefSignOut: {
        type: String,
        default: hrefSignOut
      },
      hrefSignUp: {
        type: String,
        default: hrefSignUp
      },
      hrefProfile: {
        type: String,
        default: hrefProfile
      },
      size: {
        type: Number,
        default: 30
      },
      fontColorShift: {
        type: Number,
        default: 80
      }
    },
    data: function data() {
      return {};
    },
    computed: {
      initials: function initials() {
        return initialsOf(this.username);
      },

      avatarStyle: function avatarStyle() {
        var color = colors[Math.floor(Math.random() * colors.length)];
        return {
          width: this.size + 'px !important',
          height: this.size + 'px !important',
          backgroundColor: color,
          color: LightenDarkenColor(color, this.fontColorShift)
        };
      },

      initialsStyle: function initialsStyle() {
        return {
          fontSize: initialsFontSize(this.size) + 'px'
        };
      },

      actionStyle: function actionStyle() {
        var fs = this.size / 2 / 1.5,
            disp = '',
            mr = '0px';
        if (fs < 10) {
          fs = this.size / 2;
          disp = 'inline !important';
          mr = '5px';
        }
        return {
          fontSize: fs + 'px',
          display: disp,
          marginRight: mr
        };
      },

      userActions: function userActions() {
        return this.loggedInQ ? [{ icon: 'fa-sign-out-alt', href: this.hrefSignOut, text: 'Sign-out' }, { icon: 'fa-wrench', href: this.hrefProfile, text: 'Profile' }] : [{ icon: 'fa-sign-in-alt', href: this.hrefSignIn, text: 'Sign-in' }, { icon: 'fa-user-plus', href: this.hrefSignUp, text: 'Sign-up' }];
      }
    },
    methods: {}
  };

  /* script */
  var __vue_script__$u = script$u;

  /* template */
  var __vue_render__$u = function __vue_render__() {
    var _vm = this;
    var _h = _vm.$createElement;
    var _c = _vm._self._c || _h;
    return _c("div", { class: _vm.dropDirection }, [_c("a", {
      staticClass: "dropdown-toggle",
      attrs: {
        href: "#",
        role: "button",
        id: "avatar-dropdown",
        "data-toggle": "dropdown",
        "aria-haspopup": "true",
        "aria-expanded": "false"
      }
    }, [_c("div", { staticClass: "avatar", style: _vm.avatarStyle }, [_c("div", { staticClass: "text-avatar", style: _vm.initialsStyle }, [_vm.loggedInQ ? _c("div", [_vm._v(_vm._s(_vm.initials))]) : _c("div", [_c("i", { staticClass: "fa fa-user" })])])])]), _vm._v(" "), _c("ul", { staticClass: "dropdown-menu" }, _vm._l(_vm.userActions, function (action) {
      return _c("li", {
        key: action.id,
        staticClass: "dropdown-item",
        style: _vm.actionStyle
      }, [_c("a", { attrs: { href: action.href } }, [_c("span", {}, [_c("i", { staticClass: "fa", class: action.icon }), _vm._v("\n            " + _vm._s(action.text) + "\n          ")])])]);
    }), 0)]);
  };
  var __vue_staticRenderFns__$u = [];
  __vue_render__$u._withStripped = true;

  /* style */
  var __vue_inject_styles__$u = function __vue_inject_styles__(inject) {
    if (!inject) return;
    inject("data-v-7a5256b9_0", { source: "\n.avatar[data-v-7a5256b9] {\n  background: rgb(0,191,255);\n  border: 1px solid transparent;\n  border-radius: 50%;\n}\n.text-avatar[data-v-7a5256b9] {\n  position: relative;\n  top: 50%;\n  transform: translateY(-50%);\n  text-align: center;\n  font-weight: bold;\n}\nli a[data-v-7a5256b9] {\n  text-decoration: none;\n}\nli a[data-v-7a5256b9]:visited {\n}\n.dropdown-toggle[data-v-7a5256b9]::after, .dropdown-toggle[data-v-7a5256b9]::before {\n  content: none !important;\n}\n", map: { "version": 3, "sources": ["/home/sumner/Projects/mue/src/components/DjangoUser/DjangoUser.vue"], "names": [], "mappings": ";AA2IA;EACA,0BAAA;EACA,6BAAA;EACA,kBAAA;AACA;AAEA;EACA,kBAAA;EACA,QAAA;EACA,2BAAA;EACA,kBAAA;EACA,iBAAA;AACA;AAEA;EACA,qBAAA;AACA;AAEA;AAEA;AAGA;EACA,wBAAA;AACA", "file": "DjangoUser.vue", "sourcesContent": ["<template>\n\n<div :class=\"dropDirection\">\n  <a class=\"dropdown-toggle\" href=\"#\" role=\"button\" id=\"avatar-dropdown\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">\n    <div class='avatar' :style=\"avatarStyle\">\n      <div class='text-avatar' :style=\"initialsStyle\">\n        <div  v-if=\"loggedInQ\">{{initials}}</div>\n        <div v-else><i class='fa fa-user'></i></div>\n      </div>\n    </div>\n  </a>\n\n    <ul class='dropdown-menu'>\n      <li class=\"dropdown-item\" v-for='action in userActions' :key='action.id' :style=\"actionStyle\">\n        <a :href='action.href'>\n          <span class=\"\">\n            <i class=\"fa\" :class=\"action.icon\"></i>\n            {{action.text}}\n          </span>\n        </a>\n      </li>\n    </ul>\n</div>\n\n\n\n</template>\n\n\n<script>\nimport { colors, LightenDarkenColor, initialsFontSize, azOnly, captialsIn, initialsOf } from './DjangoUserUtils.js';\n\nlet hrefSignIn = '',\nhrefSignOut = '',\nhrefSignUp = '',\nhrefProfile = '';\n\n\nexport default {\n  name: 'DjangoUser',\n  props: {\n    loggedInQ: {\n      type: Boolean,\n      default: false\n    },\n    dropDirection: {\n      type: String,\n      default: 'dropleft'\n    },\n    username: {\n      type: String,\n      default: 'User Name'\n    },\n    hrefSignIn: {\n      type: String,\n      default: hrefSignIn\n    },\n    hrefSignOut: {\n      type: String,\n      default: hrefSignOut\n    },\n    hrefSignUp: {\n      type: String,\n      default: hrefSignUp\n    },\n    hrefProfile: {\n      type: String,\n      default: hrefProfile\n    },\n    size: {\n      type: Number,\n      default: 30\n    },\n    fontColorShift: {\n      type: Number,\n      default: 80\n    }\n  },\n  data: function() {\n    return {\n\n    };\n  },\n  computed: {\n    initials: function() {\n      return initialsOf(this.username)\n    },\n\n    avatarStyle: function () {\n      let color = colors[Math.floor(Math.random() * colors.length)]\n      return {\n        width: this.size + 'px !important',\n        height: this.size + 'px !important',\n        backgroundColor: color,\n        color: LightenDarkenColor(color, this.fontColorShift)\n      }\n    },\n\n    initialsStyle: function () {\n      return {\n        fontSize: initialsFontSize(this.size) + 'px',\n      }\n    },\n\n    actionStyle: function () {\n      var fs = (this.size / 2 / 1.5), disp = '', mr = '0px';\n      if (fs < 10) {\n        fs = this.size / 2;\n        disp = 'inline !important';\n        mr = '5px';\n      }\n      return {\n        fontSize: fs + 'px',\n        display: disp,\n        marginRight: mr\n      }\n    },\n\n    userActions: function() {\n      return this.loggedInQ\n      ? [\n        {icon: 'fa-sign-out-alt', href: this.hrefSignOut, text: 'Sign-out'},\n        {icon: 'fa-wrench', href: this.hrefProfile, text: 'Profile'},\n      ]\n      : [\n        {icon: 'fa-sign-in-alt', href: this.hrefSignIn, text: 'Sign-in'},\n        {icon: 'fa-user-plus', href: this.hrefSignUp, text: 'Sign-up'},\n      ];\n    }\n  },\n  methods: {\n\n  }\n};\n</script>\n\n\n<style scoped>\n\n.avatar {\n  background: rgb(0,191,255);\n  border: 1px solid transparent;\n  border-radius: 50%;\n}\n\n.text-avatar {\n  position: relative;\n  top: 50%;\n  transform: translateY(-50%);\n  text-align: center;\n  font-weight: bold;\n}\n\nli a {\n  text-decoration: none;\n}\n\nli a:visited {\n\n}\n\n\n.dropdown-toggle::after, .dropdown-toggle::before {\n  content: none !important;\n}\n</style>\n"] }, media: undefined });
  };
  /* scoped */
  var __vue_scope_id__$u = "data-v-7a5256b9";
  /* module identifier */
  var __vue_module_identifier__$u = undefined;
  /* functional template */
  var __vue_is_functional_template__$u = false;
  /* style inject SSR */

  var DjangoUser = normalizeComponent_1({ render: __vue_render__$u, staticRenderFns: __vue_staticRenderFns__$u }, __vue_inject_styles__$u, __vue_script__$u, __vue_scope_id__$u, __vue_is_functional_template__$u, __vue_module_identifier__$u, browser, undefined);

  var Mue = {};

  // Mue.ButtonMinusToClose = ButtonMinusToClose;
  // Mue.ButtonPlus = ButtonPlus;

  Mue.Crumbs = Crumbs;
  Mue.AlertBox = Alert;

  // Mue.AdvancedFilterInput = AdvancedFilterInput;
  // Mue.AdvancedFilterRow = AdvancedFilterRow;
  // Mue.AdvancedFilterSelectConditional = AdvancedFilterSelectConditional;
  // Mue.AdvancedFilterSelectFunction = AdvancedFilterSelectFunction;
  // Mue.AdvancedFilterSelectLogic = AdvancedFilterSelectLogic;
  // Mue.AdvancedFilterSelectProperty = AdvancedFilterSelectProperty;
  Mue.AdvancedFilterTable = AdvancedFilterTable;

  Mue.AnchorButton = AnchorButton;

  Mue.DataTable = DataTable;
  // Mue.DataTableCell = DataTableCell;
  // Mue.DataTableRow = DataTableRow;

  Mue.HttpRequest = HttpRequest;

  Mue.SectionNavigation = SectionNavigation;
  // Mue.SectionNavigationArrowUp = SectionNavigationArrowUp;
  // Mue.SectionNavigationArrowDown = SectionNavigationArrowDown;

  Mue.InputAlpha = InputAlpha;
  // Mue.InputAlphaFilter = InputAlphaFilter;

  Mue.DjangoUser = DjangoUser;

  window.Mue = Mue;

}());
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVlLnYwLjAuMC5qcyIsInNvdXJjZXMiOlsiLi4vLi4vc3JjL2NvbXBvbmVudHMvQnV0dG9uTWludXNUb0Nsb3NlL0J1dHRvbk1pbnVzVG9DbG9zZS52dWUiLCIuLi8uLi9ub2RlX21vZHVsZXMvdnVlLXJ1bnRpbWUtaGVscGVycy9kaXN0L25vcm1hbGl6ZS1jb21wb25lbnQuanMiLCIuLi8uLi9ub2RlX21vZHVsZXMvdnVlLXJ1bnRpbWUtaGVscGVycy9kaXN0L2luamVjdC1zdHlsZS9icm93c2VyLmpzIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvQnV0dG9uUGx1cy9CdXR0b25QbHVzLnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0NydW1icy9DcnVtYnMudnVlIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvQWR2YW5jZWRGaWx0ZXIvQWR2YW5jZWRGaWx0ZXJJbnB1dC9BZHZhbmNlZEZpbHRlcklucHV0LnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0FkdmFuY2VkRmlsdGVyL0FkdmFuY2VkRmlsdGVyU2VsZWN0Q29uZGl0aW9uYWwvY29uZGl0aW9uYWxzLmpzIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvQWR2YW5jZWRGaWx0ZXIvQWR2YW5jZWRGaWx0ZXJTZWxlY3RDb25kaXRpb25hbC9BZHZhbmNlZEZpbHRlclNlbGVjdENvbmRpdGlvbmFsLnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0FkdmFuY2VkRmlsdGVyL0FkdmFuY2VkRmlsdGVyU2VsZWN0RnVuY3Rpb24vZnVuY3Rpb25zLmpzIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvQWR2YW5jZWRGaWx0ZXIvQWR2YW5jZWRGaWx0ZXJTZWxlY3RGdW5jdGlvbi9BZHZhbmNlZEZpbHRlclNlbGVjdEZ1bmN0aW9uLnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0FkdmFuY2VkRmlsdGVyL0FkdmFuY2VkRmlsdGVyU2VsZWN0TG9naWMvQWR2YW5jZWRGaWx0ZXJTZWxlY3RMb2dpYy52dWUiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9BZHZhbmNlZEZpbHRlci9BZHZhbmNlZEZpbHRlclNlbGVjdFByb3BlcnR5L0FkdmFuY2VkRmlsdGVyU2VsZWN0UHJvcGVydHkudnVlIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvQWR2YW5jZWRGaWx0ZXIvQWR2YW5jZWRGaWx0ZXJSb3cvQWR2YW5jZWRGaWx0ZXJSb3cudnVlIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvQWR2YW5jZWRGaWx0ZXIvQWR2YW5jZWRGaWx0ZXJUYWJsZS9kZXRlcm1pbmVUeXBlLmpzIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvQWR2YW5jZWRGaWx0ZXIvQWR2YW5jZWRGaWx0ZXJUYWJsZS9BZHZhbmNlZEZpbHRlclRhYmxlLnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0FuY2hvckJ1dHRvbi9BbmNob3JCdXR0b24udnVlIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvQWxlcnRCb3gvQWxlcnRCb3gudnVlIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvRGF0YVRhYmxlL0RhdGFUYWJsZUZpbHRlci9EYXRhVGFibGVGaWx0ZXIudnVlIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvRGF0YVRhYmxlL0RhdGFUYWJsZUNlbGwvRGF0YVRhYmxlQ2VsbC52dWUiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9EYXRhVGFibGUvRGF0YVRhYmxlUm93L0RhdGFUYWJsZVJvdy52dWUiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9EYXRhVGFibGUvRGF0YVRhYmxlSGVhZGVyQ2VsbC9EYXRhVGFibGVIZWFkZXJDZWxsLnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0RhdGFUYWJsZS9EYXRhVGFibGVIZWFkZXIvRGF0YVRhYmxlSGVhZGVyLnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0RhdGFUYWJsZS9EYXRhVGFibGVGb290ZXJDZWxsL0RhdGFUYWJsZUZvb3RlckNlbGwudnVlIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvRGF0YVRhYmxlL0RhdGFUYWJsZUZvb3Rlci9EYXRhVGFibGVGb290ZXIudnVlIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvRGF0YVRhYmxlL0RhdGFUYWJsZVBhZ2luYXRpb24vRGF0YVRhYmxlUGFnaW5hdGlvbi52dWUiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9EYXRhVGFibGUvRGF0YVRhYmxlQ2FwdGlvblNvcnQvRGF0YVRhYmxlQ2FwdGlvblNvcnQudnVlIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvRGF0YVRhYmxlL0RhdGFUYWJsZUNhcHRpb25GaWx0ZXIvRGF0YVRhYmxlQ2FwdGlvbkZpbHRlci52dWUiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9EYXRhVGFibGUvRGF0YVRhYmxlL2hlbHBlcnMuanMiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9EYXRhVGFibGUvRGF0YVRhYmxlL0RhdGFUYWJsZS52dWUiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9IdHRwUmVxdWVzdC9yZXNwb25zZXMuanMiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9IdHRwUmVxdWVzdC9IdHRwUmVxdWVzdC52dWUiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9TZWN0aW9uTmF2aWdhdGlvbi9TZWN0aW9uTmF2aWdhdGlvbkFycm93RG93bi9TZWN0aW9uTmF2aWdhdGlvbkFycm93RG93bi52dWUiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9TZWN0aW9uTmF2aWdhdGlvbi9TZWN0aW9uTmF2aWdhdGlvbkFycm93VXAvU2VjdGlvbk5hdmlnYXRpb25BcnJvd1VwLnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL1NlY3Rpb25OYXZpZ2F0aW9uL1NlY3Rpb25OYXZpZ2F0aW9uL1NlY3Rpb25OYXZpZ2F0aW9uLnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0lucHV0QWxwaGEvSW5wdXRBbHBoYS90b2tlbnMuanMiLCIuLi8uLi9zcmMvY29tcG9uZW50cy9JbnB1dEFscGhhL0lucHV0QWxwaGEvaGVscGVycy5qcyIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0lucHV0QWxwaGEvSW5wdXRBbHBoYUZpbHRlci9JbnB1dEFscGhhRmlsdGVyLnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0lucHV0QWxwaGEvSW5wdXRBbHBoYUNvbmp1bmN0aXZlTm9ybWFsRm9ybS9JbnB1dEFscGhhQ29uanVuY3RpdmVOb3JtYWxGb3JtLnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0lucHV0QWxwaGEvSW5wdXRBbHBoYS9JbnB1dEFscGhhLnZ1ZSIsIi4uLy4uL3NyYy9jb21wb25lbnRzL0RqYW5nb1VzZXIvRGphbmdvVXNlclV0aWxzLmpzIiwiLi4vLi4vc3JjL2NvbXBvbmVudHMvRGphbmdvVXNlci9EamFuZ29Vc2VyLnZ1ZSIsIi4uLy4uL3NyYy9pbmRleC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyI8dGVtcGxhdGU+XG4gIDxkaXZcbiAgICBAY2xpY2s9J21pbnVzJ1xuICA+XG4gICAgPGlcbiAgICAgIGNsYXNzPSdmYSdcbiAgICAgIDpjbGFzcz0nW3tcImZhLW1pbnVzLWNpcmNsZVwiOiAhcmVhZHlRLFwiZmEtdGltZXMtY2lyY2xlXCI6IHJlYWR5UX0sXCJiZy1cIitiYWNrZ3JvdW5kLCBcInRleHQtXCIrY29sb3JdJ1xuICAgICAgPlxuICAgIDwvaT5cbiAgPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICBwcm9wczoge1xuXG4gIH0sXG4gIGRhdGE6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB7XG4gICAgICByZWFkeVE6IGZhbHNlLFxuICAgICAgY29sb3I6ICdkYW5nZXInLFxuICAgICAgYmFja2dyb3VuZDogJ2xpZ2h0J1xuXG5cbiAgICB9O1xuICB9LFxuICBtZXRob2RzOiB7XG4gICAgbWludXM6IGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKCF0aGlzLnJlYWR5USkge1xuICAgICAgICB0aGlzLnJlYWR5USA9IHRydWU7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cbiAgICAgIGVsc2Uge1xuICAgICAgICB0aGlzLnJlYWR5USA9IGZhbHNlO1xuICAgICAgICB0aGlzLiRlbWl0KCdtaW51cycpO1xuICAgICAgfVxuXG4gICAgfVxuICB9XG59O1xuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQ+XG5pIHtcbiAgZm9udC1zaXplOiAyNHB4O1xuICBjb2xvcjonI2RjMzU0NSc7XG59XG5pOmhvdmVyLCBpOmZvY3Vze1xuICBjb2xvcjogJyNjODIzMzMnICFpbXBvcnRhbnQ7XG59XG48L3N0eWxlPlxuIiwiJ3VzZSBzdHJpY3QnO1xuXG5mdW5jdGlvbiBub3JtYWxpemVDb21wb25lbnQodGVtcGxhdGUsIHN0eWxlLCBzY3JpcHQsIHNjb3BlSWQsIGlzRnVuY3Rpb25hbFRlbXBsYXRlLCBtb2R1bGVJZGVudGlmaWVyXG4vKiBzZXJ2ZXIgb25seSAqL1xuLCBzaGFkb3dNb2RlLCBjcmVhdGVJbmplY3RvciwgY3JlYXRlSW5qZWN0b3JTU1IsIGNyZWF0ZUluamVjdG9yU2hhZG93KSB7XG4gIGlmICh0eXBlb2Ygc2hhZG93TW9kZSAhPT0gJ2Jvb2xlYW4nKSB7XG4gICAgY3JlYXRlSW5qZWN0b3JTU1IgPSBjcmVhdGVJbmplY3RvcjtcbiAgICBjcmVhdGVJbmplY3RvciA9IHNoYWRvd01vZGU7XG4gICAgc2hhZG93TW9kZSA9IGZhbHNlO1xuICB9IC8vIFZ1ZS5leHRlbmQgY29uc3RydWN0b3IgZXhwb3J0IGludGVyb3AuXG5cblxuICB2YXIgb3B0aW9ucyA9IHR5cGVvZiBzY3JpcHQgPT09ICdmdW5jdGlvbicgPyBzY3JpcHQub3B0aW9ucyA6IHNjcmlwdDsgLy8gcmVuZGVyIGZ1bmN0aW9uc1xuXG4gIGlmICh0ZW1wbGF0ZSAmJiB0ZW1wbGF0ZS5yZW5kZXIpIHtcbiAgICBvcHRpb25zLnJlbmRlciA9IHRlbXBsYXRlLnJlbmRlcjtcbiAgICBvcHRpb25zLnN0YXRpY1JlbmRlckZucyA9IHRlbXBsYXRlLnN0YXRpY1JlbmRlckZucztcbiAgICBvcHRpb25zLl9jb21waWxlZCA9IHRydWU7IC8vIGZ1bmN0aW9uYWwgdGVtcGxhdGVcblxuICAgIGlmIChpc0Z1bmN0aW9uYWxUZW1wbGF0ZSkge1xuICAgICAgb3B0aW9ucy5mdW5jdGlvbmFsID0gdHJ1ZTtcbiAgICB9XG4gIH0gLy8gc2NvcGVkSWRcblxuXG4gIGlmIChzY29wZUlkKSB7XG4gICAgb3B0aW9ucy5fc2NvcGVJZCA9IHNjb3BlSWQ7XG4gIH1cblxuICB2YXIgaG9vaztcblxuICBpZiAobW9kdWxlSWRlbnRpZmllcikge1xuICAgIC8vIHNlcnZlciBidWlsZFxuICAgIGhvb2sgPSBmdW5jdGlvbiBob29rKGNvbnRleHQpIHtcbiAgICAgIC8vIDIuMyBpbmplY3Rpb25cbiAgICAgIGNvbnRleHQgPSBjb250ZXh0IHx8IC8vIGNhY2hlZCBjYWxsXG4gICAgICB0aGlzLiR2bm9kZSAmJiB0aGlzLiR2bm9kZS5zc3JDb250ZXh0IHx8IC8vIHN0YXRlZnVsXG4gICAgICB0aGlzLnBhcmVudCAmJiB0aGlzLnBhcmVudC4kdm5vZGUgJiYgdGhpcy5wYXJlbnQuJHZub2RlLnNzckNvbnRleHQ7IC8vIGZ1bmN0aW9uYWxcbiAgICAgIC8vIDIuMiB3aXRoIHJ1bkluTmV3Q29udGV4dDogdHJ1ZVxuXG4gICAgICBpZiAoIWNvbnRleHQgJiYgdHlwZW9mIF9fVlVFX1NTUl9DT05URVhUX18gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgIGNvbnRleHQgPSBfX1ZVRV9TU1JfQ09OVEVYVF9fO1xuICAgICAgfSAvLyBpbmplY3QgY29tcG9uZW50IHN0eWxlc1xuXG5cbiAgICAgIGlmIChzdHlsZSkge1xuICAgICAgICBzdHlsZS5jYWxsKHRoaXMsIGNyZWF0ZUluamVjdG9yU1NSKGNvbnRleHQpKTtcbiAgICAgIH0gLy8gcmVnaXN0ZXIgY29tcG9uZW50IG1vZHVsZSBpZGVudGlmaWVyIGZvciBhc3luYyBjaHVuayBpbmZlcmVuY2VcblxuXG4gICAgICBpZiAoY29udGV4dCAmJiBjb250ZXh0Ll9yZWdpc3RlcmVkQ29tcG9uZW50cykge1xuICAgICAgICBjb250ZXh0Ll9yZWdpc3RlcmVkQ29tcG9uZW50cy5hZGQobW9kdWxlSWRlbnRpZmllcik7XG4gICAgICB9XG4gICAgfTsgLy8gdXNlZCBieSBzc3IgaW4gY2FzZSBjb21wb25lbnQgaXMgY2FjaGVkIGFuZCBiZWZvcmVDcmVhdGVcbiAgICAvLyBuZXZlciBnZXRzIGNhbGxlZFxuXG5cbiAgICBvcHRpb25zLl9zc3JSZWdpc3RlciA9IGhvb2s7XG4gIH0gZWxzZSBpZiAoc3R5bGUpIHtcbiAgICBob29rID0gc2hhZG93TW9kZSA/IGZ1bmN0aW9uICgpIHtcbiAgICAgIHN0eWxlLmNhbGwodGhpcywgY3JlYXRlSW5qZWN0b3JTaGFkb3codGhpcy4kcm9vdC4kb3B0aW9ucy5zaGFkb3dSb290KSk7XG4gICAgfSA6IGZ1bmN0aW9uIChjb250ZXh0KSB7XG4gICAgICBzdHlsZS5jYWxsKHRoaXMsIGNyZWF0ZUluamVjdG9yKGNvbnRleHQpKTtcbiAgICB9O1xuICB9XG5cbiAgaWYgKGhvb2spIHtcbiAgICBpZiAob3B0aW9ucy5mdW5jdGlvbmFsKSB7XG4gICAgICAvLyByZWdpc3RlciBmb3IgZnVuY3Rpb25hbCBjb21wb25lbnQgaW4gdnVlIGZpbGVcbiAgICAgIHZhciBvcmlnaW5hbFJlbmRlciA9IG9wdGlvbnMucmVuZGVyO1xuXG4gICAgICBvcHRpb25zLnJlbmRlciA9IGZ1bmN0aW9uIHJlbmRlcldpdGhTdHlsZUluamVjdGlvbihoLCBjb250ZXh0KSB7XG4gICAgICAgIGhvb2suY2FsbChjb250ZXh0KTtcbiAgICAgICAgcmV0dXJuIG9yaWdpbmFsUmVuZGVyKGgsIGNvbnRleHQpO1xuICAgICAgfTtcbiAgICB9IGVsc2Uge1xuICAgICAgLy8gaW5qZWN0IGNvbXBvbmVudCByZWdpc3RyYXRpb24gYXMgYmVmb3JlQ3JlYXRlIGhvb2tcbiAgICAgIHZhciBleGlzdGluZyA9IG9wdGlvbnMuYmVmb3JlQ3JlYXRlO1xuICAgICAgb3B0aW9ucy5iZWZvcmVDcmVhdGUgPSBleGlzdGluZyA/IFtdLmNvbmNhdChleGlzdGluZywgaG9vaykgOiBbaG9va107XG4gICAgfVxuICB9XG5cbiAgcmV0dXJuIHNjcmlwdDtcbn1cblxubW9kdWxlLmV4cG9ydHMgPSBub3JtYWxpemVDb21wb25lbnQ7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1ub3JtYWxpemUtY29tcG9uZW50LmpzLm1hcFxuIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgaXNPbGRJRSA9IHR5cGVvZiBuYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmIC9tc2llIFs2LTldXFxcXGIvLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudC50b0xvd2VyQ2FzZSgpKTtcbmZ1bmN0aW9uIGNyZWF0ZUluamVjdG9yKGNvbnRleHQpIHtcbiAgcmV0dXJuIGZ1bmN0aW9uIChpZCwgc3R5bGUpIHtcbiAgICByZXR1cm4gYWRkU3R5bGUoaWQsIHN0eWxlKTtcbiAgfTtcbn1cbnZhciBIRUFEID0gZG9jdW1lbnQuaGVhZCB8fCBkb2N1bWVudC5nZXRFbGVtZW50c0J5VGFnTmFtZSgnaGVhZCcpWzBdO1xudmFyIHN0eWxlcyA9IHt9O1xuXG5mdW5jdGlvbiBhZGRTdHlsZShpZCwgY3NzKSB7XG4gIHZhciBncm91cCA9IGlzT2xkSUUgPyBjc3MubWVkaWEgfHwgJ2RlZmF1bHQnIDogaWQ7XG4gIHZhciBzdHlsZSA9IHN0eWxlc1tncm91cF0gfHwgKHN0eWxlc1tncm91cF0gPSB7XG4gICAgaWRzOiBuZXcgU2V0KCksXG4gICAgc3R5bGVzOiBbXVxuICB9KTtcblxuICBpZiAoIXN0eWxlLmlkcy5oYXMoaWQpKSB7XG4gICAgc3R5bGUuaWRzLmFkZChpZCk7XG4gICAgdmFyIGNvZGUgPSBjc3Muc291cmNlO1xuXG4gICAgaWYgKGNzcy5tYXApIHtcbiAgICAgIC8vIGh0dHBzOi8vZGV2ZWxvcGVyLmNocm9tZS5jb20vZGV2dG9vbHMvZG9jcy9qYXZhc2NyaXB0LWRlYnVnZ2luZ1xuICAgICAgLy8gdGhpcyBtYWtlcyBzb3VyY2UgbWFwcyBpbnNpZGUgc3R5bGUgdGFncyB3b3JrIHByb3Blcmx5IGluIENocm9tZVxuICAgICAgY29kZSArPSAnXFxuLyojIHNvdXJjZVVSTD0nICsgY3NzLm1hcC5zb3VyY2VzWzBdICsgJyAqLyc7IC8vIGh0dHA6Ly9zdGFja292ZXJmbG93LmNvbS9hLzI2NjAzODc1XG5cbiAgICAgIGNvZGUgKz0gJ1xcbi8qIyBzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtiYXNlNjQsJyArIGJ0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KGNzcy5tYXApKSkpICsgJyAqLyc7XG4gICAgfVxuXG4gICAgaWYgKCFzdHlsZS5lbGVtZW50KSB7XG4gICAgICBzdHlsZS5lbGVtZW50ID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc3R5bGUnKTtcbiAgICAgIHN0eWxlLmVsZW1lbnQudHlwZSA9ICd0ZXh0L2Nzcyc7XG4gICAgICBpZiAoY3NzLm1lZGlhKSBzdHlsZS5lbGVtZW50LnNldEF0dHJpYnV0ZSgnbWVkaWEnLCBjc3MubWVkaWEpO1xuICAgICAgSEVBRC5hcHBlbmRDaGlsZChzdHlsZS5lbGVtZW50KTtcbiAgICB9XG5cbiAgICBpZiAoJ3N0eWxlU2hlZXQnIGluIHN0eWxlLmVsZW1lbnQpIHtcbiAgICAgIHN0eWxlLnN0eWxlcy5wdXNoKGNvZGUpO1xuICAgICAgc3R5bGUuZWxlbWVudC5zdHlsZVNoZWV0LmNzc1RleHQgPSBzdHlsZS5zdHlsZXMuZmlsdGVyKEJvb2xlYW4pLmpvaW4oJ1xcbicpO1xuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgaW5kZXggPSBzdHlsZS5pZHMuc2l6ZSAtIDE7XG4gICAgICB2YXIgdGV4dE5vZGUgPSBkb2N1bWVudC5jcmVhdGVUZXh0Tm9kZShjb2RlKTtcbiAgICAgIHZhciBub2RlcyA9IHN0eWxlLmVsZW1lbnQuY2hpbGROb2RlcztcbiAgICAgIGlmIChub2Rlc1tpbmRleF0pIHN0eWxlLmVsZW1lbnQucmVtb3ZlQ2hpbGQobm9kZXNbaW5kZXhdKTtcbiAgICAgIGlmIChub2Rlcy5sZW5ndGgpIHN0eWxlLmVsZW1lbnQuaW5zZXJ0QmVmb3JlKHRleHROb2RlLCBub2Rlc1tpbmRleF0pO2Vsc2Ugc3R5bGUuZWxlbWVudC5hcHBlbmRDaGlsZCh0ZXh0Tm9kZSk7XG4gICAgfVxuICB9XG59XG5cbm1vZHVsZS5leHBvcnRzID0gY3JlYXRlSW5qZWN0b3I7XG4vLyMgc291cmNlTWFwcGluZ1VSTD1icm93c2VyLmpzLm1hcFxuIiwiPHRlbXBsYXRlPlxuICA8ZGl2XG4gICAgQGNsaWNrPSdwbHVzJ1xuICA+XG4gICAgPGlcbiAgICAgIGNsYXNzPSdmYSBmYS1wbHVzLWNpcmNsZSdcbiAgICAgIDpjbGFzcz0nW1wiYmctXCIrYmFja2dyb3VuZCwgXCJ0ZXh0LVwiK2NvbG9yXSdcbiAgICAgID5cbiAgICA8L2k+XG4gIDwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgcHJvcHM6IHtcbiAgICBjb2xvcjoge1xuICAgICAgZGVmYXVsdDonc3VjY2VzcycsXG4gICAgICB0eXBlOlN0cmluZ1xuICAgIH0sXG4gICAgYmFja2dyb3VuZDoge1xuICAgICAgZGVmYXVsdDonbGlnaHQnLFxuICAgICAgdHlwZTpTdHJpbmdcbiAgICB9XG4gIH0sXG4gIG1ldGhvZHM6IHtcbiAgICBwbHVzOiBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMuJGVtaXQoJ3BsdXMnKTtcbiAgICB9XG4gIH1cbn07XG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cbmkge1xuICBmb250LXNpemU6IDI0cHg7XG4gIGNvbG9yOiAjMjhhNzQ1O1xufVxuXG5pOmhvdmVyLCBpOmZvY3VzIHtcbiAgY29sb3I6ICMyMTg4MzggIWltcG9ydGFudDtcbn1cbjwvc3R5bGU+XG4iLCI8dGVtcGxhdGU+XG4gIDxvbCBjbGFzcz0nYnJlYWRjcnVtYic+XG4gICAgPGxpXG4gICAgICB2LWZvcj0nY3J1bWIgaW4gY3J1bWJzJ1xuICAgICAgY2xhc3M9J2JyZWFkY3J1bWItaXRlbSdcbiAgICAgIDpjbGFzcz0neyBhY3RpdmU6IGNydW1iLmFjdGl2ZVEgfSdcbiAgICA+XG4gICAgICA8YSA6aHJlZj0nY3J1bWIuaHJlZic+e3tjcnVtYi5jcnVtYn19PC9hPlxuICAgIDwvbGk+XG4gIDwvb2w+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuZXhwb3J0IGRlZmF1bHQge1xuICBuYW1lOiAnY3J1bWJzJyxcbiAgcHJvcHM6IHtcbiAgICBob21lOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnJ1xuICAgIH0sXG4gICAgZGVidWdROiB7XG4gICAgICB0eXBlOiBCb29sZWFuLFxuICAgICAgZGVmYXVsdDogZmFsc2VcbiAgICB9XG4gIH0sXG5cbiAgZGF0YTogZnVuY3Rpb24gKCkge1xuICAgIHJldHVybiB7XG4gICAgfTtcbiAgfSxcblxuICBtZXRob2RzOiB7XG4gICAgcmVtb3ZlVHJhaWxpbmdTbGFzaDogZnVuY3Rpb24ocGFydHMpIHtcbiAgICAgIHJldHVybiBwYXJ0c1twYXJ0cy5sZW5ndGggLSAxXSA9PSAnJ1xuICAgICAgPyBwYXJ0cy5zbGljZSgwLCBwYXJ0cy5sZW5ndGgtMSlcbiAgICAgIDogcGFydHM7XG4gICAgfSxcbiAgICByZW1vdmVGaWxlTmFtZTogZnVuY3Rpb24ocGFydHMpIHtcbiAgICAgIHJldHVybiAocGFydHNbcGFydHMubGVuZ3RoIC0gMV0uaW5jbHVkZXMoJy5odG1sJykpID8gcGFydHMuc2xpY2UoMCwgcGFydHMubGVuZ3RoLTEpIDogcGFydHNcbiAgICB9LFxuICAgIHByb2R1Y2VQYXJ0OiBmdW5jdGlvbihwYXJ0LCBpLCBwYXJ0cykge1xuICAgICAgbGV0IHRoYXQgPSB0aGlzXG4gICAgICBsZXQgZmlyc3RRID0gaSA9PSAwXG4gICAgICBsZXQgbGFzdFEgPSBpID09IHBhcnRzLmxlbmd0aCAtIDFcblxuICAgICAgbGV0IGNydW1iID0gZmlyc3RRXG4gICAgICAgID8gdGhhdC5ob21lID09ICcnXG4gICAgICAgICAgPyAnaG9tZSdcbiAgICAgICAgICA6IHRoYXQuaG9tZVxuICAgICAgICA6IHBhcnQ7XG5cbiAgICAgIGxldCBocmVmID0gZmlyc3RRXG4gICAgICAgID8gdGhhdC5ob21lID09ICcnXG4gICAgICAgICAgPyBBcnJheShwYXJ0cy5sZW5ndGgtMSkuZmlsbCgnLi4vJykuam9pbignJylcbiAgICAgICAgICA6IHRoYXQuaG9tZVxuICAgICAgICA6IHBhcnRzLnNsaWNlKDAsIGkrMSkuam9pbignLycpO1xuXG5cbiAgICAgIHJldHVybiB7YWN0aXZlUTogbGFzdFEsIGNydW1iOiBjcnVtYiwgaHJlZjogaHJlZn07XG4gICAgfVxuICB9LFxuXG4gIGNvbXB1dGVkOiB7XG4gICAgcGFydHM6IGZ1bmN0aW9uKCl7XG4gICAgICB2YXIgdGhhdCA9IHRoaXM7XG5cbiAgICAgIHZhciBwYXRobmFtZSA9IHdpbmRvdy5sb2NhdGlvbi5wYXRobmFtZTtcbiAgICAgIHZhciBwYXJ0cyA9IHBhdGhuYW1lLnNwbGl0KCcvJyk7XG5cbiAgICAgIHBhcnRzID0gdGhhdC5yZW1vdmVUcmFpbGluZ1NsYXNoKHBhcnRzKTtcbiAgICAgIHBhcnRzID0gdGhhdC5yZW1vdmVGaWxlTmFtZShwYXJ0cyk7XG4gICAgICByZXR1cm4gcGFydHM7XG4gICAgfSxcblxuICAgIGNydW1iczogZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgICBpZiAodGhhdC5kZWJ1Z1EpIHtjb25zb2xlLmdyb3VwKCdbY3J1bWJzXScpO31cblxuICAgICAgdmFyIGhvbWUgPSB0aGlzLmhvbWU7XG5cbiAgICAgIHZhciBwYXJ0cyA9IHRoYXQucGFydHM7XG5cbiAgICAgIGlmICh0aGF0LmRlYnVnUSkge1xuICAgICAgICBjb25zb2xlLnRhYmxlKHBhcnRzKTtcbiAgICAgICAgY29uc29sZS50YWJsZSh7XG4gICAgICAgICAgaG9tZVByb3A6IHRoYXQuaG9tZSxcbiAgICAgICAgICBob21lOiBob21lLFxuICAgICAgICAgIGhvbWVIYXNJbmRleDBROiBob21lID09IHBhcnRzWzBdXG4gICAgICAgIH0pO1xuICAgICAgfVxuXG5cbiAgICAgIC8vIC8vIHJlbW92ZSBpbnZhbGlkIGhvbWVcbiAgICAgIHdoaWxlIChob21lICE9IHBhcnRzWzBdICYmIHBhcnRzLmxlbmd0aCkgeyBwYXJ0cy5zcGxpY2UoMCwxKTsgfVxuXG4gICAgICB2YXIgZGF0YSA9IHBhcnRzLm1hcChmdW5jdGlvbihwYXJ0LCBpKSB7XG4gICAgICAgIHJldHVybiB0aGF0LnByb2R1Y2VQYXJ0KHBhcnQsIGksIHBhcnRzKTtcbiAgICAgIH0pO1xuXG4gICAgICBpZiAodGhhdC5kZWJ1Z1EpIHtjb25zb2xlLmdyb3VwRW5kKCk7fVxuICAgICAgcmV0dXJuIGRhdGE7XG4gICAgfVxuICB9XG59O1xuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQ+XG5cbjwvc3R5bGU+XG4iLCJcbjx0ZW1wbGF0ZT5cbiAgPGlucHV0IDp2YWx1ZT0ndmFsdWUnIEBpbnB1dD0ndXBkYXRlSW5wdXQoJGV2ZW50KScvPlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgbmFtZTonYWR2YW5jZWQtZmlsdGVyLWlucHV0JyxcbiAgZGF0YTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHZhbHVlOiB0aGlzLnNlbGVjdGVkLFxuICAgIH07XG4gIH0sXG4gIG1ldGhvZHM6IHtcbiAgICB1cGRhdGVJbnB1dDogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgIHRoaXMuJGVtaXQoJ3VwZGF0ZUlucHV0JywgZXZlbnQudGFyZ2V0LnZhbHVlKTtcbiAgICB9XG4gIH1cblxufTtcbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuaW5wdXQge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCA7XG4gIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGJvcmRlci1ib3R0b20tY29sb3I6IFJHQig1MCwgMjAwLCAyNTApO1xuICBwYWRkaW5nOiAwIDVweDtcbiAgY29sb3I6IFJHQigzMiwgMTIyLCAyNDcpXG59XG5cbmlucHV0OmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cbjwvc3R5bGU+XG4iLCIvL+KIiCwg4oiJXG5cbmxldCBjb25kaXRpb25hbHMgPSB7XG4gIC8vIGZhZXE6IHtcbiAgLy8gICB0ZXh0OifiiIAgZSwgZSA9JyxcbiAgLy8gfVxuICBlcToge1xuICAgIHRleHQ6ICc9JyxcbiAgICB0eXBlczogW1xuICAgICAgJ2FycmF5JywgJ2FycmF5c3RyaW5nJywgJ2FycmF5b2JqZWN0JywgJ2FycmF5bnVtYmVyJyxcbiAgICAgICdudW1iZXInLCAnc3RyaW5nJywgJ3VuZGVmaW5lZCdcbiAgICBdLFxuICAgIGNvbmQ6IGZ1bmN0aW9uKGEsIGIpIHtcbiAgICAgIHJldHVybiBhID09IGI7XG4gICAgfVxuICB9LFxuICBuZXE6IHtcbiAgICB0ZXh0OiAn4omgJyxcbiAgICB0eXBlczogW1xuICAgICAgJ2FycmF5JywgJ2FycmF5c3RyaW5nJywgJ2FycmF5b2JqZWN0JywgJ2FycmF5bnVtYmVyJyxcbiAgICAgICdudW1iZXInLCAnc3RyaW5nJywgJ3VuZGVmaW5lZCdcbiAgICBdLFxuICAgIGNvbmQ6IGZ1bmN0aW9uKGEsIGIpIHtcbiAgICAgIHJldHVybiBhICE9IGI7XG4gICAgfVxuICB9LFxuICBsdDoge1xuICAgIHRleHQ6ICc8JyxcbiAgICB0eXBlczogWydudW1iZXInXVxuICAgICxcbiAgICBjb25kOiBmdW5jdGlvbihhLCBiKSB7XG4gICAgICByZXR1cm4gYSA8IGI7XG4gICAgfVxuICB9LFxuICBndDoge1xuICAgIHRleHQ6ICc+JyxcbiAgICB0eXBlczogWydudW1iZXInXVxuICAgICxcbiAgICBjb25kOiBmdW5jdGlvbihhLCBiKSB7XG4gICAgICByZXR1cm4gYSA+IGI7XG4gICAgfVxuICB9LFxuICBsdGU6IHtcbiAgICB0ZXh0OiAn4omkJyxcbiAgICB0eXBlczogWydudW1iZXInXVxuICAgICxcbiAgICBjb25kOiBmdW5jdGlvbihhLCBiKSB7XG4gICAgICByZXR1cm4gYSA8PSBiO1xuICAgIH1cbiAgfSxcbiAgZ3RlOiB7XG4gICAgdGV4dDogJ+KJpScsXG4gICAgdHlwZXM6IFsnbnVtYmVyJ11cbiAgICAsXG4gICAgY29uZDogZnVuY3Rpb24oYSwgYikge1xuICAgICAgcmV0dXJuIGEgPj0gYjtcbiAgICB9XG4gIH0sXG4gIHNzOiB7XG4gICAgdGV4dDogJ+KKgicsXG4gICAgdHlwZXM6IFsnYXJyYXknLCAnYXJyYXlzdHJpbmcnLCAnYXJyYXludW1iZXInLCAnc3RyaW5nJ10sXG4gICAgY29uZDogZnVuY3Rpb24oYSwgYikge1xuICAgICAgcmV0dXJuIGEuaW5jbHVkZXMoYik7XG4gICAgfVxuICB9LFxuICBuc3M6IHtcbiAgICB0ZXh0OiAn4p+IJyxcbiAgICB0eXBlczogWydhcnJheScsICdhcnJheXN0cmluZycsICdhcnJheW51bWJlcicsICdzdHJpbmcnXVxuICAgICxcbiAgICBjb25kOiBmdW5jdGlvbihhLCBiKSB7XG4gICAgICByZXR1cm4gIShhLmluY2x1ZGVzKGIpKTsgXG4gICAgfVxuICB9XG59O1xuXG5leHBvcnQgZGVmYXVsdCBjb25kaXRpb25hbHM7XG4iLCI8dGVtcGxhdGU+XG4gIDxzZWxlY3QgQGlucHV0PSd1cGRhdGVTZWxlY3RlZCgkZXZlbnQpJz5cbiAgICA8b3B0aW9uXG4gICAgICB2LWZvcj0nKHZhbCwga2V5LCBpbmRleCkgaW4gb3B0aW9ucydcbiAgICAgIDp2YWx1ZT0na2V5J1xuICAgICAgOnNlbGVjdGVkPSdrZXkgPT0gdmFsdWUnXG4gICAgICB2LWlmPSd2YWwudHlwZXMuaW5jbHVkZXMoZGF0YVR5cGUpJ1xuICAgID5cbiAgICAgIHt7dmFsLnRleHR9fVxuICAgIDwvb3B0aW9uPlxuICA8L3NlbGVjdD5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5pbXBvcnQgY29uZGl0aW9uYWxzIGZyb20gJy4vY29uZGl0aW9uYWxzLmpzJztcbmV4cG9ydCBkZWZhdWx0IHtcbiAgbmFtZTonYWR2YW5jZWQtZmlsdGVyLXNlbGVjdC1jb25kaXRpb25hbCcsXG4gIHByb3BzOiB7XG4gICAgc2VsZWN0ZWQ6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICdpZGVudGl0eSdcbiAgICB9LFxuICAgIGRhdGFUeXBlOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAndW5kZWZpbmVkJ1xuICAgIH1cbiAgfSxcbiAgZGF0YTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHZhbHVlOiB0aGlzLnNlbGVjdGVkLFxuICAgICAgb3B0aW9uczogY29uZGl0aW9uYWxzXG4gICAgfTtcbiAgfSxcbiAgbWV0aG9kczoge1xuICAgIHVwZGF0ZVNlbGVjdGVkOiBmdW5jdGlvbihldmVudCkge1xuICAgICAgdGhpcy52YWx1ZSA9IGV2ZW50LnRhcmdldC52YWx1ZTtcbiAgICAgIHRoaXMuJGVtaXQoJ3VwZGF0ZVNlbGVjdGVkJywgZXZlbnQudGFyZ2V0LnZhbHVlKTtcbiAgICB9XG4gIH1cbn07XG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cbjwvc3R5bGU+XG4iLCIvLydhcnJheScsICdhcnJheXN0cmluZycsICdhcnJheW9iamVjdCcsICdhcnJheW51bWJlcicsXG4vLydudW1iZXInLCAnc3RyaW5nJywgJ3VuZGVmaW5lZCdcbmxldCBmdW5jdGlvbnMgPSB7XG4gICdpZGVudGl0eSc6IHtcbiAgICB0ZXh0OiAneCDihpIgeCcsXG4gICAgZnVuYzogZnVuY3Rpb24oeCkge1xuICAgICAgcmV0dXJuIHg7XG4gICAgfSxcbiAgICB0eXBlczogW1xuICAgICAgJ251bWJlcicsICdzdHJpbmcnLCAndW5kZWZpbmVkJ1xuICAgIF1cbiAgfSxcbiAgJ21lYW4nOiB7XG4gICAgdGV4dDogJ3gg4oaSIG1lYW4oeCknLFxuICAgIGZ1bmM6IGZ1bmN0aW9uKG51bWJlcnMpIHtcbiAgICAgIHJldHVybiBudW1iZXJzLm1hcCh6ID0+IHovbnVtYmVycy5sZW5ndGgpXG4gICAgICAgIC5yZWR1Y2UoKGFkZGVyLCB2YWx1ZSkgPT4gKGFkZGVyICsgdmFsdWUpKTtcbiAgICB9LFxuICAgIHR5cGVzOiBbJ2FycmF5bnVtYmVyJ11cbiAgfSxcbiAgJ21heCc6IHtcbiAgICB0ZXh0OiAneCDihpIgbWF4KHgpJyxcbiAgICBmdW5jOiBmdW5jdGlvbih4KSB7XG4gICAgICB2YXIgbGcgPSAtSW5maW5pdHk7XG4gICAgICB4Lm1hcChmdW5jdGlvbihkKSB7XG4gICAgICAgIGxnID0gZCA+IGxnID8gZCA6IGxnO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gbGc7XG4gICAgfSxcbiAgICB0eXBlczogWydhcnJheW51bWJlciddXG5cbiAgfSxcbiAgJ21pbic6IHtcbiAgICB0ZXh0OiAneCDihpIgbWluKHgpJyxcbiAgICBmdW5jOiBmdW5jdGlvbih4KSB7XG4gICAgICB2YXIgc20gPSBJbmZpbml0eTtcbiAgICAgIHgubWFwKGZ1bmN0aW9uKGQpIHtcbiAgICAgICAgc20gPSBkIDwgc20gPyBkIDogc207XG4gICAgICB9KTtcbiAgICAgIHJldHVybiBzbTtcbiAgICB9LFxuICAgIHR5cGVzOiBbJ2FycmF5bnVtYmVyJ11cbiAgfSxcbiAgJ2xlbmd0aCc6IHtcbiAgICB0ZXh0OiAneCDihpIgbGVuKHgpJyxcbiAgICBmdW5jOiBmdW5jdGlvbih4KSB7XG4gICAgICByZXR1cm4geC5sZW5ndGg7XG4gICAgfSxcbiAgICB0eXBlczogWydhcnJheScsJ2FycmF5bnVtYmVyJywnYXJyYXlzdHJpbmcnLCdhcnJheW9iamVjdCddXG4gIH1cblxufTtcblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb25zO1xuIiwiPHRlbXBsYXRlPlxuXG4gIDxzZWxlY3QgQGNoYW5nZT0ndXBkYXRlU2VsZWN0ZWQoJGV2ZW50KSc+XG4gICAge3tzZWxlY3RlZH19IHt7dmFsdWV9fVxuICAgIDxvcHRpb25cbiAgICAgIHYtZm9yPScodmFsLCBrZXksIGluZGV4KSBpbiBvcHRpb25zJ1xuICAgICAgOnZhbHVlPSdrZXknXG4gICAgICA6c2VsZWN0ZWQ9J2tleSA9PSBzZWxlY3RlZCdcbiAgICAgIHYtaWY9J3ZhbC50eXBlcy5pbmNsdWRlcyhkYXRhVHlwZSknXG4gICAgPlxuICAgICAge3t2YWwudGV4dH19XG4gICAgPC9vcHRpb24+XG4gIDwvc2VsZWN0PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmltcG9ydCBmdW5jdGlvbnMgZnJvbSAnLi9mdW5jdGlvbnMuanMnO1xuZXhwb3J0IGRlZmF1bHQge1xuICBuYW1lOidhZHZhbmNlZC1maWx0ZXItc2VsZWN0LWZ1bmN0aW9uJyxcbiAgcHJvcHM6IHtcbiAgICBzZWxlY3RlZDoge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogJ2lkZW50aXR5J1xuICAgIH0sXG4gICAgZGF0YVR5cGU6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICd1bmRlZmluZWQnXG4gICAgfVxuICB9LFxuICBkYXRhOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgdmFsdWU6IHRoaXMuc2VsZWN0ZWQsXG4gICAgICBvcHRpb25zOiBmdW5jdGlvbnNcbiAgICB9O1xuICB9LFxuICBtZXRob2RzOiB7XG4gICAgdXBkYXRlU2VsZWN0ZWQ6IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICB0aGlzLnZhbHVlID0gZXZlbnQudGFyZ2V0LnZhbHVlO1xuICAgICAgdGhpcy4kZW1pdCgndXBkYXRlU2VsZWN0ZWQnLCBldmVudC50YXJnZXQudmFsdWUpO1xuICAgIH1cbiAgfVxufTtcbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuPC9zdHlsZT5cbiIsIlxuPHRlbXBsYXRlPlxuICA8c2VsZWN0IEBjaGFuZ2U9J3VwZGF0ZVNlbGVjdGVkKCRldmVudCknPlxuICAgIDxvcHRpb25cbiAgICAgIHYtZm9yPScodmFsLCBrZXksIGluZGV4KSBpbiBvcHRpb25zJ1xuICAgICAgOnZhbHVlPSdrZXknXG4gICAgICA6c2VsZWN0ZWQ9J2tleSA9PSB2YWx1ZSdcbiAgICA+XG4gICAgICB7e3ZhbC50ZXh0fX1cbiAgICA8L29wdGlvbj5cbiAgPC9zZWxlY3Q+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxubGV0IG9wdGlvbnMgPSB7XG4gIGFuZDoge3RleHQ6ICdhbmQnfSxcbiAgb3I6IHt0ZXh0OiAnb3InfVxufTtcblxuZXhwb3J0IGRlZmF1bHQge1xuICBuYW1lOidhZHZhbmNlZC1maWx0ZXItc2VsZWN0LWxvZ2ljJyxcbiAgcHJvcHM6IHtcbiAgICBzZWxlY3RlZDoge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogJ2FuZCdcbiAgICB9XG4gIH0sXG4gIC8vIG1vZGVsOiB7XG4gIC8vICAgcHJvcDogJ3NlbGVjdGVkJyxcbiAgLy8gICBldmVudDogJ3VwZGF0ZVNlbGVjdGVkJ1xuICAvLyB9LFxuICBkYXRhOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgdmFsdWU6IHRoaXMuc2VsZWN0ZWQsXG4gICAgICBvcHRpb25zOiBvcHRpb25zXG4gICAgfTtcbiAgfSxcbiAgbWV0aG9kczoge1xuICAgIHVwZGF0ZVNlbGVjdGVkOiBmdW5jdGlvbihldmVudCkge1xuICAgICAgdGhpcy52YWx1ZSA9IGV2ZW50LnRhcmdldC52YWx1ZTtcbiAgICAgIHRoaXMuJGVtaXQoJ3VwZGF0ZVNlbGVjdGVkJywgZXZlbnQudGFyZ2V0LnZhbHVlKTtcbiAgICB9XG4gIH1cblxufTtcbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuc2VsZWN0IHtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQgO1xuICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xuICBib3JkZXItYm90dG9tLWNvbG9yOiBSR0IoNTAsIDIwMCwgMjUwKTtcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBwYWRkaW5nOiAwIDVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiBSR0IoMzIsIDEyMiwgMjQ3KVxufVxuXG5zZWxlY3Q6Zm9jdXMge1xuICBvdXRsaW5lOiBub25lO1xufVxuXG5vcHRpb24ge1xuICBwYWRkaW5nOiAwIDVweDtcblxufVxuPC9zdHlsZT5cbiIsIjx0ZW1wbGF0ZT5cbiAgPGRpdiBjbGFzcz1cIlwiPlxuICAgIDxzZWxlY3QgQGNoYW5nZT0ndXBkYXRlU2VsZWN0ZWQoJGV2ZW50KSc+XG4gICAgICA8b3B0aW9uXG4gICAgICAgIHYtZm9yPScodmFsLCBrZXksIGluZGV4KSBpbiBvcHRpb25zJ1xuICAgICAgICA6dmFsdWU9J2tleSdcbiAgICAgICAgOnNlbGVjdGVkPSdrZXkgPT0gdmFsdWUnXG4gICAgICA+XG4gICAgICAgIHt7dmFsLnRleHR9fVxuICAgICAgPC9vcHRpb24+XG4gICAgPC9zZWxlY3Q+XG4gICAgPHNwYW4+e3t0eXBlfX08L3NwYW4+XG4gIDwvZGl2PlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXG5leHBvcnQgZGVmYXVsdCB7XG4gIG5hbWU6J2FkdmFuY2VkLWZpbHRlci1zZWxlY3QtcHJvcGVydHknLFxuICBwcm9wczoge1xuICAgIHByb3BlcnRpZXM6IHtcbiAgICAgIHR5cGU6IE9iamVjdCxcbiAgICAgIGRlZmF1bHQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICByZXR1cm4ge307XG4gICAgICB9XG4gICAgfSxcbiAgICBzZWxlY3RlZDoge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogJ2lkZW50aXR5J1xuICAgIH0sXG4gICAgZGF0YVR5cGU6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICcnXG4gICAgfVxuICB9LFxuICBkYXRhOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgdmFsdWU6IHRoaXMuc2VsZWN0ZWQsXG4gICAgICBvcHRpb25zOiB0aGlzLnByb3BlcnRpZXNcbiAgICB9O1xuICB9LFxuICBtZXRob2RzOiB7XG4gICAgdXBkYXRlU2VsZWN0ZWQ6IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICB0aGlzLnZhbHVlID0gZXZlbnQudGFyZ2V0LnZhbHVlO1xuICAgICAgdGhpcy4kZW1pdCgndXBkYXRlU2VsZWN0ZWQnLCBldmVudC50YXJnZXQudmFsdWUpO1xuICAgIH0sXG4gICAgZm9ybWF0VHlwZTogZnVuY3Rpb24oKSB7XG4gICAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgICBsZXQgdHlwZSA9IHRoYXQuZGF0YVR5cGU7XG4gICAgICBsZXQgZm9ybWF0dGVkID0gdHlwZTtcbiAgICAgIGlmICh0eXBlLmluY2x1ZGVzKCdhcnJheScpICYmIHR5cGUubGVuZ3RoID4gNSkge1xuICAgICAgICBmb3JtYXR0ZWQgPSB0eXBlLnJlcGxhY2UoJ2FycmF5JywgJ2FycmF5OiAnKTtcbiAgICAgIH1cbiAgICAgIHJldHVybiBmb3JtYXR0ZWQ7XG4gICAgfVxuICB9LFxuICBjb21wdXRlZDoge1xuICAgIHR5cGU6IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMuZm9ybWF0VHlwZSgpO1xuICAgIH1cbiAgfVxufTtcbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuc3BhbiB7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBjb2xvcjogUkdCKDIwMiwgMTQ5LCAxMDMpO1xuICBmb250LXNpemU6IDEwcHg7XG59XG48L3N0eWxlPlxuIiwiPHRlbXBsYXRlPlxuICA8dHI+XG4gICAgPCEtLSBMb2dpYyAtLT5cbiAgICA8dGQ+XG4gICAgICA8YWR2YW5jZWQtZmlsdGVyLXNlbGVjdC1sb2dpY1xuICAgICAgICAgdi1pZj0naW5kZXghPTAnXG4gICAgICAgICA6c2VsZWN0ZWQ9J2ZpbHRlci5sb2dpYydcbiAgICAgICAgIEB1cGRhdGVTZWxlY3RlZD0ndXBkYXRlU2VsZWN0TG9naWMnXG4gICAgICAvPlxuICAgIDwvdGQ+XG5cbiAgICA8IS0tIGZ1bmN0aW9ucyAtLT5cbiAgICA8dGQ+XG4gICAgICA8YWR2YW5jZWQtZmlsdGVyLXNlbGVjdC1mdW5jdGlvblxuICAgICAgICA6c2VsZWN0ZWQ9J2ZpbHRlci5mdW5jdGlvbidcbiAgICAgICAgOmRhdGEtdHlwZT0ncHJvcGVydHlUeXBlJ1xuICAgICAgICBAdXBkYXRlU2VsZWN0ZWQ9J3VwZGF0ZVNlbGVjdEZ1bmN0aW9uJ1xuICAgICAgLz5cbiAgICA8L3RkPlxuXG4gICAgPCEtLSBwcm9wZXJ0aWVzIC0tPlxuICAgIDx0ZD5cbiAgICAgIDxhZHZhbmNlZC1maWx0ZXItc2VsZWN0LXByb3BlcnR5XG4gICAgICAgIDpzZWxlY3RlZD0nZmlsdGVyLnByb3BlcnR5J1xuICAgICAgICA6cHJvcGVydGllcz0ncHJvcGVydGllcydcbiAgICAgICAgQHVwZGF0ZVNlbGVjdGVkPSd1cGRhdGVTZWxlY3RQcm9wZXJ0eSdcbiAgICAgICAgOmRhdGEtdHlwZT0ncHJvcGVydHlUeXBlJ1xuXG4gICAgICAvPlxuICAgIDwvdGQ+XG5cblxuICAgIDwhLS0gY29uZGl0aW9uYWwgLS0+XG4gICAgPHRkPlxuICAgICAgPGFkdmFuY2VkLWZpbHRlci1zZWxlY3QtY29uZGl0aW9uYWxcbiAgICAgICAgOnNlbGVjdGVkPSdmaWx0ZXIuY29uZGl0aW9uYWwnXG4gICAgICAgIDpkYXRhLXR5cGU9J2FwcGxpY2FibGVDb25kaXRpb25hbFR5cGUnXG4gICAgICAgIEB1cGRhdGVTZWxlY3RlZD0ndXBkYXRlU2VsZWN0Q29uZGl0aW9uYWwnXG4gICAgICAvPlxuICAgIDwvdGQ+XG5cblxuXG4gICAgPCEtLSBpbnB1dCAtLT5cbiAgICA8dGQ+XG4gICAgICA8YWR2YW5jZWQtZmlsdGVyLWlucHV0XG4gICAgICAgIDp2YWx1ZT0nZmlsdGVyLmlucHV0J1xuICAgICAgICBAdXBkYXRlSW5wdXQ9J3VwZGF0ZUlucHV0J1xuICAgICAgLz5cbiAgICA8L3RkPlxuXG4gICAgPCEtLSBjbG9zZSAtLT5cbiAgICA8dGQ+PGJ1dHRvbi1taW51cy10by1jbG9zZSBAbWludXM9J3JlbW92ZU1lJy8+PC90ZD5cbiAgPC90cj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5pbXBvcnQgQWR2YW5jZWRGaWx0ZXJJbnB1dCBmcm9tICcuLi9BZHZhbmNlZEZpbHRlcklucHV0L0FkdmFuY2VkRmlsdGVySW5wdXQudnVlJztcbmltcG9ydCBBZHZhbmNlZEZpbHRlclNlbGVjdENvbmRpdGlvbmFsIGZyb20gJy4uL0FkdmFuY2VkRmlsdGVyU2VsZWN0Q29uZGl0aW9uYWwvQWR2YW5jZWRGaWx0ZXJTZWxlY3RDb25kaXRpb25hbC52dWUnO1xuaW1wb3J0IEFkdmFuY2VkRmlsdGVyU2VsZWN0RnVuY3Rpb24gZnJvbSAnLi4vQWR2YW5jZWRGaWx0ZXJTZWxlY3RGdW5jdGlvbi9BZHZhbmNlZEZpbHRlclNlbGVjdEZ1bmN0aW9uLnZ1ZSc7XG5pbXBvcnQgQWR2YW5jZWRGaWx0ZXJTZWxlY3RMb2dpYyBmcm9tICcuLi9BZHZhbmNlZEZpbHRlclNlbGVjdExvZ2ljL0FkdmFuY2VkRmlsdGVyU2VsZWN0TG9naWMudnVlJztcbmltcG9ydCBBZHZhbmNlZEZpbHRlclNlbGVjdFByb3BlcnR5IGZyb20gJy4uL0FkdmFuY2VkRmlsdGVyU2VsZWN0UHJvcGVydHkvQWR2YW5jZWRGaWx0ZXJTZWxlY3RQcm9wZXJ0eS52dWUnO1xuaW1wb3J0IEJ1dHRvbk1pbnVzVG9DbG9zZSBmcm9tICcuLi8uLi9CdXR0b25NaW51c1RvQ2xvc2UvQnV0dG9uTWludXNUb0Nsb3NlLnZ1ZSc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcblxuICBjb21wb25lbnRzOiB7XG4gICAgQWR2YW5jZWRGaWx0ZXJTZWxlY3RDb25kaXRpb25hbCxcbiAgICBBZHZhbmNlZEZpbHRlclNlbGVjdExvZ2ljLFxuICAgIEFkdmFuY2VkRmlsdGVyU2VsZWN0RnVuY3Rpb24sXG4gICAgQWR2YW5jZWRGaWx0ZXJTZWxlY3RQcm9wZXJ0eSxcbiAgICBBZHZhbmNlZEZpbHRlcklucHV0LFxuICAgIEJ1dHRvbk1pbnVzVG9DbG9zZVxuICB9LFxuXG4gIHByb3BzOiB7XG5cbiAgICBpbmRleDogeyB0eXBlOiBOdW1iZXIgfSxcblxuICAgIGZpbHRlcjoge1xuICAgICAgdHlwZTogT2JqZWN0LFxuICAgICAgZGVmYXVsdDogZnVuY3Rpb24oKSB7IHJldHVybiB7fTsgfSxcbiAgICAgIHZhbGlkYXRvcjogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgdmFyIGtleXMgPSBPYmplY3Qua2V5cyh2YWx1ZSk7XG5cbiAgICAgICAgaWYgKCFrZXlzLmluY2x1ZGVzKCdsb2dpYycpKSB7IHZhbHVlLmxvZ2ljID0gJ2FuZCc7IH1cblxuICAgICAgICBpZiAoIWtleXMuaW5jbHVkZXMoJ2Z1bmN0aW9uJykpIHsgdmFsdWUuZnVuY3Rpb24gPSAnaWRlbnRpdHknOyB9XG5cbiAgICAgICAgaWYgKCFrZXlzLmluY2x1ZGVzKCdjb25kaXRpb25hbCcpKSB7IHZhbHVlLmNvbmRpdGlvbmFsID0gJ2VxJzsgfVxuXG4gICAgICAgIGlmICgha2V5cy5pbmNsdWRlcygnaW5wdXQnKSkgeyB2YWx1ZS5pbnB1dCA9ICcnOyB9XG5cbiAgICAgICAgaWYgKCFrZXlzLmluY2x1ZGVzKCdwcm9wZXJ0eScpKSB7IHZhbHVlLnByb3BlcnR5ID0gJyc7IH1cbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgfSxcblxuICAgIHByb3BlcnRpZXM6IHtcbiAgICAgIHR5cGU6IE9iamVjdCxcbiAgICAgIGRlZmF1bHQ6IGZ1bmN0aW9uICgpIHsgcmV0dXJuIHt9OyB9LFxuICAgICAgdmFsaWRhdG9yOiBmdW5jdGlvbihwcm9wZXJ0aWVzKSB7XG4gICAgICAgIGxldCBwcm9wcyA9IE9iamVjdC5rZXlzKHByb3BlcnRpZXMpO1xuICAgICAgICByZXR1cm4gcHJvcHMuZXZlcnkoZnVuY3Rpb24ocHJvcCkge1xuICAgICAgICAgIGxldCBwcm9wS2V5cyA9IE9iamVjdC5rZXlzKHByb3BlcnRpZXNbcHJvcF0pO1xuICAgICAgICAgIGxldCBjb25kaXRpb25hbCA9IHByb3BLZXlzLmluY2x1ZGVzKCdleGFtcGxlVmFsdWUnKVxuICAgICAgICAgICAgJiYgcHJvcEtleXMuaW5jbHVkZXMoJ2V4YW1wbGVWYWx1ZVR5cGUnKVxuICAgICAgICAgICAgJiYgcHJvcEtleXMuaW5jbHVkZXMoJ3RleHQnKTtcbiAgICAgICAgICByZXR1cm4gY29uZGl0aW9uYWw7XG4gICAgICAgIH0pO1xuICAgICAgfVxuICAgIH1cbiAgfSxcblxuICBkYXRhIDogZnVuY3Rpb24oKXtcbiAgICByZXR1cm4ge1xuICAgICAgbG9naWM6IHRoaXMuZmlsdGVyLmxvZ2ljLFxuICAgICAgZnVuY3Rpb246IHRoaXMuZmlsdGVyLmZ1bmN0aW9uLFxuICAgICAgY29uZGl0aW9uYWw6IHRoaXMuZmlsdGVyLmNvbmRpdGlvbmFsLFxuICAgICAgaW5wdXQ6IHRoaXMuZmlsdGVyLmlucHV0LFxuICAgICAgcHJvcGVydHk6IHRoaXMuZmlsdGVyLnByb3BlcnR5XG4gICAgfTtcbiAgfSxcbiAgbWV0aG9kczoge1xuICAgIHJlbW92ZU1lOiBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMuJGVtaXQoJ3JlbW92ZVRoaXNGaWx0ZXInKTtcbiAgICB9LFxuXG4gICAgYXBwbHlGaWx0ZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgdmFyIGZpbHQgPSB0aGlzLmZpbHRlcjtcbiAgICAgIC8vIHZhciBmID0gZnVuY3Rpb25zW2ZpbHQuZnVuY3Rpb25dLmZ1bmM7XG5cbiAgICAgIGlmICh0aGlzLmZpbHRlci5pbnB1dCA9PT0gJycpIHtcbiAgICAgICAgcmV0dXJuXG4gICAgICB9XG5cbiAgICAgIHRoaXMuJGVtaXQoJ3VwZGF0ZUZpbHRlcicsIGZpbHQpO1xuICAgICAgLy9cbiAgICAgIC8vIGlmIChmaWx0LmlucHV0ICE9ICcnKSB7XG4gICAgICAvLyAgIHRoaXMuJGVtaXQoJ3VwZGF0ZUZpbHRlcicsIGZpbHQpO1xuICAgICAgLy8gfSBlbHNlIHtcbiAgICAgIC8vICAgdGhpcy4kZW1pdCgnbm9JbnB1dCcsIGZpbHQsIHRoaXMuaW5kZXgpXG4gICAgICAvLyB9XG4gICAgfSxcblxuICAgIHVwZGF0ZVNlbGVjdExvZ2ljOiBmdW5jdGlvbihuZXdTZWxlY3RlZCkge1xuICAgICAgdGhpcy5maWx0ZXIubG9naWMgPSBuZXdTZWxlY3RlZDtcbiAgICAgIHRoaXMubG9naWMgPSBuZXdTZWxlY3RlZDtcbiAgICAgIHRoaXMuYXBwbHlGaWx0ZXIoKTtcbiAgICAgIC8vIHRoaXMuJGVtaXQoJ3VwZGF0ZUZpbHRlcicsIHRoaXMuZmlsdGVyLCB0aGlzLmluZGV4KTtcbiAgICB9LFxuXG4gICAgdXBkYXRlU2VsZWN0RnVuY3Rpb246IGZ1bmN0aW9uKG5ld1NlbGVjdGVkKSB7XG4gICAgICB0aGlzLmZpbHRlci5mdW5jdGlvbiA9IG5ld1NlbGVjdGVkO1xuICAgICAgdGhpcy5mdW5jdGlvbiA9IG5ld1NlbGVjdGVkO1xuICAgICAgdGhpcy5hcHBseUZpbHRlcigpO1xuICAgICAgLy8gdGhpcy4kZW1pdCgndXBkYXRlRmlsdGVyJywgdGhpcy5maWx0ZXIsIHRoaXMuaW5kZXgpO1xuICAgIH0sXG5cbiAgICB1cGRhdGVTZWxlY3RQcm9wZXJ0eTogZnVuY3Rpb24obmV3U2VsZWN0ZWQpIHtcbiAgICAgIHRoaXMuZmlsdGVyLnByb3BlcnR5ID0gbmV3U2VsZWN0ZWQ7XG4gICAgICB0aGlzLnByb3BlcnR5ID0gbmV3U2VsZWN0ZWQ7XG4gICAgICB0aGlzLmFwcGx5RmlsdGVyKCk7XG4gICAgICAvLyB0aGlzLiRlbWl0KCd1cGRhdGVGaWx0ZXInLCB0aGlzLmZpbHRlciwgdGhpcy5pbmRleCk7XG4gICAgfSxcblxuICAgIHVwZGF0ZVNlbGVjdENvbmRpdGlvbmFsOiBmdW5jdGlvbihuZXdTZWxlY3RlZCkge1xuICAgICAgdGhpcy5maWx0ZXIuY29uZGl0aW9uYWwgPSBuZXdTZWxlY3RlZDtcbiAgICAgIHRoaXMuY29uZGl0aW9uYWwgPSBuZXdTZWxlY3RlZDtcbiAgICAgIHRoaXMuYXBwbHlGaWx0ZXIoKTtcbiAgICAgIC8vIHRoaXMuJGVtaXQoJ3VwZGF0ZUZpbHRlcicsIHRoaXMuZmlsdGVyLCB0aGlzLmluZGV4KTtcbiAgICB9LFxuXG4gICAgdXBkYXRlSW5wdXQ6IGZ1bmN0aW9uKG5ld0lucHV0KSB7XG4gICAgICB0aGlzLmZpbHRlci5pbnB1dCA9IG5ld0lucHV0O1xuICAgICAgdGhpcy5pbnB1dCA9IG5ld0lucHV0O1xuICAgICAgdGhpcy5hcHBseUZpbHRlcigpO1xuICAgICAgLy8gdGhpcy4kZW1pdCgndXBkYXRlRmlsdGVyJywgdGhpcy5maWx0ZXIsIHRoaXMuaW5kZXgpO1xuICAgIH0sXG4gIH0sXG4gIGNvbXB1dGVkOiB7XG4gICAgcHJvcGVydHlEYXRhOiBmdW5jdGlvbigpIHtcbiAgICAgIGxldCB0aGF0ID0gdGhpcztcbiAgICAgIGxldCBwcm9wID0gdGhhdC5wcm9wZXJ0eTtcbiAgICAgIGxldCBwcm9wRGF0YSA9IHRoYXQucHJvcGVydGllc1twcm9wXTtcbiAgICAgIHJldHVybiBwcm9wRGF0YTtcbiAgICB9LFxuICAgIHByb3BlcnR5VHlwZTogZnVuY3Rpb24oKSB7XG4gICAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgICByZXR1cm4gdGhhdC5wcm9wZXJ0eURhdGEuZXhhbXBsZVZhbHVlVHlwZTtcbiAgICB9LFxuICAgIGFwcGxpY2FibGVDb25kaXRpb25hbFR5cGU6IGZ1bmN0aW9uKCkge1xuICAgICAgbGV0IHRoYXQgPSB0aGlzO1xuICAgICAgbGV0IHR5cGUgPSB0aGF0LnByb3BlcnR5VHlwZTtcbiAgICAgIGlmICh0eXBlLmluY2x1ZGVzKCdhcnJheScpKSB7XG4gICAgICAgIGlmIChbJ21pbicsICdtYXgnLCAnbWVhbicsICdsZW5ndGgnXS5pbmNsdWRlcyh0aGF0LmZ1bmN0aW9uKSkge1xuICAgICAgICAgIHR5cGUgPSAnbnVtYmVyJztcbiAgICAgICAgfVxuICAgICAgfVxuICAgICAgcmV0dXJuIHR5cGU7XG4gICAgfVxuICB9XG59O1xuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQ+XG48L3N0eWxlPlxuIiwiZnVuY3Rpb24gZGV0ZXJtaW5lVHlwZShkYXRhKSB7XG4gIC8vIHN0YXJ0IHdpdGggSlMgYmFzaWNzXG4gIGxldCB0eXBlID0gdHlwZW9mIGRhdGE7XG5cbiAgLypcbiAgbnVtYmVyIGFuZCBzdHJpbmcgYXJlIHN1ZmZpY2llbnQgdG8ga25vdyBmdW5jdGlvbnMgYW5kIGNvbmRpdGlvbmFsc1xuICBjYW4gYmUgYXBwbGllZCB0byB0aGVtLlxuICAqL1xuICBpZiAoWydudW1iZXInLCAnc3RyaW5nJ10uaW5jbHVkZXModHlwZSkpIHsgcmV0dXJuIHR5cGU7IH1cblxuICAvKlxuICBpZiBub3QgYSBudW1iZXIgb3Igc3RyaW5nLCBpdCBtaWdodCBiZSBhbiBhcnJheS5cbiAgV2hhdCB0aGUgZWxlbWVudHMgYXJlIG9mIHRob3NlIGFycmF5IG1pZ2h0IG1vZGlmeSB3aGF0IGZ1bmN0aW9uc1xuICBjYW4gYmUgYXBwbGllZC4gRS5nLiBhbiBhcnJheSBvZiBudW1iZXJzIGFsbG93cyBmb3IgbWluLCBtYXgsIG1lYW4sXG4gIGV0YyB0byBiZSBhcHBsaWVkLiBXaGVyZWFzIGFuIGFycmF5IG9mIHN0cmluZ3MgZG9lcyBub3QuXG4gICovXG4gIGlmICh0eXBlICE9ICdvYmplY3QnKSB7cmV0dXJuIHR5cGU7fVxuXG4gIC8qIG9ubHkgZGVhbGluZyB3aXRoIG9iamVjdCB0eXBlcyBhdCB0aGlzIHBvaW50ICovXG4gIGlmIChBcnJheS5pc0FycmF5KGRhdGEpKSB7XG4gICAgbGV0IHR5cGUgPSAnYXJyYXknO1xuXG4gICAgbGV0IHR5cGVvZkVsZW1lbnRzID0gZGF0YS5tYXAoZnVuY3Rpb24oZSl7cmV0dXJuIHR5cGVvZiBlO30pO1xuICAgIGxldCB0eXBlb2ZFbGVtZW50ID0gdHlwZW9mRWxlbWVudHNbMF07XG4gICAgbGV0IGFsbFNhbWVRID0gdHlwZW9mRWxlbWVudHMuZXZlcnkoZnVuY3Rpb24oZSl7cmV0dXJuIGUgPT0gdHlwZW9mRWxlbWVudDt9KTtcblxuICAgIGlmIChhbGxTYW1lUSkgeyB0eXBlICs9IHR5cGVvZkVsZW1lbnQ7IH1cblxuICAgIHJldHVybiB0eXBlO1xuICB9XG5cbiAgcmV0dXJuIHR5cGU7XG59XG5cbmV4cG9ydCBkZWZhdWx0IGRldGVybWluZVR5cGU7XG4iLCI8dGVtcGxhdGUgPlxuICA8ZGl2IGNsYXNzPSd0YWJsZS1yZXNwb25zaXZlJz5cbiAgICA8dGFibGUgY2xhc3M9J3RhYmxlJz5cbiAgICAgIDx0aGVhZD5cbiAgICAgICAgPHRyPlxuICAgICAgICAgIDx0aCBzY29wZT0nY29sJz48c3BhbiB2LWlmPSdmaWx0ZXJzLmxlbmd0aD4xJz5Mb2dpYzwvc3Bhbj48L3RoPlxuICAgICAgICAgIDx0aCBzY29wZT0nY29sJz5GdW5jdGlvbjwvdGg+XG4gICAgICAgICAgPHRoIHNjb3BlPSdjb2wnPlByb3BlcnR5PC90aD5cbiAgICAgICAgICA8dGggc2NvcGU9J2NvbCc+Q29uZGl0aW9uYWw8L3RoPlxuICAgICAgICAgIDx0aCBzY29wZT0nY29sJz5WYWx1ZTwvdGg+XG4gICAgICAgICAgPHRoIHNjb3BlPSdjb2wnPjxidXR0b24tbWludXMtdG8tY2xvc2UgQG1pbnVzPSdmaWx0ZXJzPVtdJy8+PC90aD5cbiAgICAgICAgPC90cj5cbiAgICAgIDwvdGhlYWQ+XG5cbiAgICAgIDx0Ym9keT5cbiAgICAgICAgPGFkdmFuY2VkLWZpbHRlci1yb3dcbiAgICAgICAgICB2LWZvcj0nKGZpbHRlciwgaW5kZXgpIGluIGZpbHRlcnMnXG4gICAgICAgICAgOmluZGV4PSdpbmRleCdcbiAgICAgICAgICA6a2V5PSdmaWx0ZXIuaWQnXG4gICAgICAgICAgOmZpbHRlcj0nZmlsdGVyJ1xuICAgICAgICAgIDpwcm9wZXJ0aWVzPSdwcm9wZXJ0aWVzJ1xuICAgICAgICAgIEByZW1vdmVUaGlzRmlsdGVyPSdmaWx0ZXJzLnNwbGljZShpbmRleCwxKSdcbiAgICAgICAgICBAdXBkYXRlRmlsdGVyPSd1cGRhdGVGaWx0ZXInXG5cbiAgICAgICAgLz5cblxuICAgICAgICA8dHI+XG4gICAgICAgICAgPHRoPjxidXR0b24tcGx1cyBAcGx1cz0nYWRkRmlsdGVyJy8+PC90aD5cbiAgICAgICAgICA8dGg+PC90aD48dGg+PC90aD48dGg+PC90aD48dGg+PC90aD48dGg+PC90aD5cbiAgICAgICAgPC90cj5cbiAgICAgIDwvdGJvZHk+XG4gICAgICA8Y2FwdGlvbj5OdW1iZXIgb2YgcmVjb3Jkczoge3tudW1iZXJPZlJlY29yZHN9fTwvY2FwdGlvbj5cblxuICAgIDwvdGFibGU+XG5cbiAgICA8ZGl2IHYtZm9yPVwiKGssIGkpIGluIGZpbHRlcmVkXCI+XG4gICAgICB7e2t9fToge3tyZWNvcmRzW2tdfX1cbiAgICA8L2Rpdj5cbiAgPC9kaXY+XG5cblxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuaW1wb3J0IEFkdmFuY2VkRmlsdGVyUm93IGZyb20gJy4uL0FkdmFuY2VkRmlsdGVyUm93L0FkdmFuY2VkRmlsdGVyUm93LnZ1ZSc7XG5pbXBvcnQgQnV0dG9uTWludXNUb0Nsb3NlIGZyb20gJy4uLy4uL0J1dHRvbk1pbnVzVG9DbG9zZS9CdXR0b25NaW51c1RvQ2xvc2UudnVlJztcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gJy4uLy4uL0J1dHRvblBsdXMvQnV0dG9uUGx1cy52dWUnO1xuXG5pbXBvcnQgZGV0ZXJtaW5lVHlwZSBmcm9tICcuL2RldGVybWluZVR5cGUuanMnO1xuXG5pbXBvcnQgZnVuY3Rpb25zIGZyb20gJy4uL0FkdmFuY2VkRmlsdGVyU2VsZWN0RnVuY3Rpb24vZnVuY3Rpb25zLmpzJztcbmltcG9ydCBjb25kaXRpb25hbHMgZnJvbSAnLi4vQWR2YW5jZWRGaWx0ZXJTZWxlY3RDb25kaXRpb25hbC9jb25kaXRpb25hbHMuanMnO1xuXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgY29tcG9uZW50czogeyBBZHZhbmNlZEZpbHRlclJvdywgQnV0dG9uTWludXNUb0Nsb3NlLCBCdXR0b25QbHVzIH0sXG5cbiAgcHJvcHM6IHtcbiAgICBkYXRhOiB7XG4gICAgICBkZWZhdWx0OiAoKSA9PiB7fSxcbiAgICAgIHR5cGU6IE9iamVjdFxuICAgIH1cbiAgfSxcbiAgZGF0YTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGZpbHRlcmVkOiBPYmplY3Qua2V5cyh0aGlzLmRhdGEpLFxuICAgICAgZmlsdGVyczogW10sXG4gICAgICByZWNvcmRzOiB0aGlzLmRhdGFcbiAgICB9O1xuICB9LFxuICBjb21wdXRlZDoge1xuICAgIHJlY29yZEZpZWxkczogZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgdGhhdCA9IHRoaXM7XG5cbiAgICAgIHZhciBhbGxSZWNvcmRGaWVsZHMgPSBbXS5jb25jYXQoXG4gICAgICAgIC4uLnRoYXQucmVjb3JkSWRzLm1hcChcbiAgICAgICAgICBmdW5jdGlvbihyZWNvcmRJZCl7XG4gICAgICAgICAgICByZXR1cm4gT2JqZWN0LmtleXModGhhdC5yZWNvcmRzW3JlY29yZElkXSk7XG4gICAgICAgICAgfSlcbiAgICAgICk7XG4gICAgICByZXR1cm4gWy4uLm5ldyBTZXQoYWxsUmVjb3JkRmllbGRzKV07XG4gICAgfSxcbiAgICByZWNvcmRJZHM6IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIE9iamVjdC5rZXlzKHRoaXMucmVjb3Jkcyk7XG4gICAgfSxcbiAgICBudW1iZXJPZlJlY29yZHM6IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIHRoaXMuZmlsdGVyZWQubGVuZ3RoO1xuICAgIH0sXG4gICAgcHJvcGVydGllczogZnVuY3Rpb24oKSB7XG4gICAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgICBsZXQgcmVjb3JkRmllbGRzID0gdGhhdC5yZWNvcmRGaWVsZHM7XG4gICAgICBsZXQgcmVjb3JkSWRzID0gdGhhdC5yZWNvcmRJZHM7XG4gICAgICBsZXQgcmVjb3JkRmllbGRzRGF0YSA9IHt9O1xuXG4gICAgICByZWNvcmRGaWVsZHMubWFwKGZ1bmN0aW9uKGZpZWxkKSB7XG5cbiAgICAgICAgbGV0IGZpZWxkRGF0YSA9IHt9O1xuXG4gICAgICAgIHZhciBleGFtcGxlUmVjb3JkLCBleGFtcGxlRmllbGRWYWx1ZTtcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCByZWNvcmRJZHMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgICB2YXIgY3VycmVudFJlY29yZElkID0gcmVjb3JkSWRzW2ldO1xuICAgICAgICAgIGV4YW1wbGVSZWNvcmQgPSB0aGF0LnJlY29yZHNbY3VycmVudFJlY29yZElkXTtcbiAgICAgICAgICBleGFtcGxlRmllbGRWYWx1ZSA9IGV4YW1wbGVSZWNvcmRbZmllbGRdO1xuICAgICAgICAgIGlmIChleGFtcGxlRmllbGRWYWx1ZSkgeyBicmVhazsgfVxuICAgICAgICB9XG5cbiAgICAgICAgZmllbGREYXRhLmV4YW1wbGVWYWx1ZSA9IGV4YW1wbGVGaWVsZFZhbHVlO1xuICAgICAgICBmaWVsZERhdGEuZXhhbXBsZVZhbHVlVHlwZSA9IHRoYXQuZGV0ZXJtaW5lVHlwZShleGFtcGxlRmllbGRWYWx1ZSk7XG4gICAgICAgIGZpZWxkRGF0YS50ZXh0ID0gZmllbGQ7XG4gICAgICAgIHJlY29yZEZpZWxkc0RhdGFbZmllbGRdID0gZmllbGREYXRhO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gcmVjb3JkRmllbGRzRGF0YTtcbiAgICB9XG4gIH0sXG4gIG1ldGhvZHM6IHtcbiAgICBhZGRGaWx0ZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgbGV0IHRoYXQgPSB0aGlzO1xuICAgICAgdGhhdC5maWx0ZXJzLnB1c2goe1xuICAgICAgICBwcm9wZXJ0eTogdGhhdC5yZWNvcmRGaWVsZHNbMF1cbiAgICAgIH0pO1xuICAgIH0sXG4gICAgcmVtb3ZlQWxsOiBmdW5jdGlvbigpIHtcblxuICAgIH0sXG4gICAgZmlsdGVyRGF0YTogZnVuY3Rpb24oKSB7XG4gICAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgICBsZXQgcmVjb3JkSWRzID0gdGhhdC5yZWNvcmRJZHM7XG4gICAgICBsZXQgZmlsdGVycyA9IHRoaXMuZmlsdGVycztcbiAgICAgIGxldCBmaWx0ZXJHcm91cHMgPSBbXTtcblxuXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGZpbHRlcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgdmFyIGxvZ2ljID0gZmlsdGVyc1tpXS5sb2dpYztcbiAgICAgICAgaWYgKGxvZ2ljID09ICdhbmQnKSB7ZmlsdGVyR3JvdXBzLnB1c2goW2ZpbHRlcnNbaV1dKTt9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIGZpbHRlckdyb3Vwc1tmaWx0ZXJHcm91cHMubGVuZ3RoLTFdLnB1c2goZmlsdGVyc1tpXSk7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgbGV0IGZpbHRlcmVkID0gcmVjb3JkSWRzLmZpbHRlcihmdW5jdGlvbihpZCkge1xuICAgICAgICB2YXIgcmVjb3JkID0gdGhhdC5yZWNvcmRzW2lkXTtcbiAgICAgICAgdmFyIGFuZCA9IGZpbHRlckdyb3Vwcy5tYXAoZnVuY3Rpb24oZmlsdHMpIHtcbiAgICAgICAgICB2YXIgb3IgPSBmaWx0cy5zb21lKGZ1bmN0aW9uKGZpbHQpIHtcbiAgICAgICAgICAgIHZhciBmID0gZnVuY3Rpb25zW2ZpbHQuZnVuY3Rpb25dLmZ1bmM7XG4gICAgICAgICAgICB2YXIgYyA9IGNvbmRpdGlvbmFsc1tmaWx0LmNvbmRpdGlvbmFsXS5jb25kO1xuICAgICAgICAgICAgdmFyIHAgPSByZWNvcmRbZmlsdC5wcm9wZXJ0eV07XG4gICAgICAgICAgICB2YXIgaSA9IGZpbHQuaW5wdXQ7XG4gICAgICAgICAgICB2YXIgZnggPSBmKHApO1xuICAgICAgICAgICAgcmV0dXJuIGMoZngsIGkpO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiBvcjtcbiAgICAgICAgfSk7XG4gICAgICAgIHJldHVybiBhbmQuZXZlcnkoZnVuY3Rpb24oZSkge3JldHVybiBlO30pO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gZmlsdGVyZWQ7XG4gICAgfSxcbiAgICB1cGRhdGVGaWx0ZXI6IGZ1bmN0aW9uKGZpbHRlcikge1xuICAgICAgZmlsdGVyO1xuICAgICAgLy8gbGV0IGZpbHRlcnMgPSB0aGlzLmZpbHRlcnM7XG4gICAgICB0aGlzLmZpbHRlcmVkID0gdGhpcy5maWx0ZXJEYXRhKCk7XG4gICAgfSxcblxuICAgIGRldGVybWluZVR5cGU6IGRldGVybWluZVR5cGVcbiAgfVxufTtcbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuXG48L3N0eWxlPlxuIiwiPHRlbXBsYXRlPlxuICA8YVxuICAgIGNsYXNzPSdidXR0b24nXG4gICAgOmNsYXNzPSdbeyBcImhhcy1pY29uXCI6IGljb25RfSwgY29sb3JdJ1xuICA+XG4gIDxpbWcgdi1pZj0naWNvblEnIDpzcmM9J3NyYyc+PC9pbWc+XG4gIHt7dGV4dH19XG4gIDwvYT5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gIG5hbWU6J2FuY2hvci1idXR0b24nLFxuICBwcm9wczoge1xuICAgIGljb25ROiB7XG4gICAgICB0eXBlOiBCb29sZWFuLFxuICAgICAgZGVmYXVsdDogZmFsc2VcbiAgICB9LFxuICAgIHNyYzoge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogJydcbiAgICB9LFxuICAgIHRleHQ6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICdCdXR0b24nXG4gICAgfSxcbiAgICBocmVmOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnIydcbiAgICB9LFxuICAgIGNvbG9yOiB7XG4gICAgICB0eXBlOlN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICcnXG4gICAgfVxuICB9LFxufTtcbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuYSB7XG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgY29sb3I6ICMzNDQ5NWU7XG4gIGZvbnQtZmFtaWx5OiAnU291cmNlIFNhbnMgUHJvJywgJ0hlbHZldGljYSBOZXVlJywgQXJpYWwsIHNhbnMtc2VyaWY7XG4gIGZvbnQtc2l6ZTogMS4wNWVtO1xuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbmEuYnV0dG9uIHtcbiAgcGFkZGluZzogMC43NWVtIDJlbTtcbiAgYm9yZGVyLXJhZGl1czogMmVtO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG5cbiAgdHJhbnNpdGlvbjogYWxsIDAuMTVzIGVhc2U7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG4gIGJvcmRlcjogMXB4IHNvbGlkO1xufVxuXG4uaGFzLWljb24ge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIHRleHQtaW5kZW50OiAxLjRlbTtcbiAgd2lkdGg6IDJlbTtcblxufVxuXG5pbWcge1xuICAvKiB3aWR0aDogMzBweDtcbiAgaGVpZ2h0OjMwcHg7ICovXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMC40ZW07XG4gIHRvcDogMC40ZW07XG4gIHdpZHRoOiAyZW07XG59XG5cbi5idXR0b24ge1xuICBtYXJnaW46IDFlbSAwO1xuICBmb250LXNpemU6IDEuMDVlbTtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuMWVtO1xuICBtaW4td2lkdGg6IDhlbTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG5cblxuXG5cbi52dWUtZ3JlZW4ge1xuICBjb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjNGZjMDhkICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1jb2xvcjogIzRmYzA4ZCAhaW1wb3J0YW50O1xufVxuXG4udnVlLXdoaXRlIHtcbiAgY29sb3I6ICM0MmI5ODMgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZiAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6ICM0ZmMwOGQgIWltcG9ydGFudDtcbn1cblxuLnZ1ZS1ncmF5IHtcbiAgY29sb3I6ICM3ZjhjOGQgIWltcG9ydGFudDtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2Y2ZjZmNiAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6ICNmNmY2ZjYgIWltcG9ydGFudDtcbn1cblxuLm1vei1ibHVlIHtcbiAgY29sb3I6ICNmZmYgIWltcG9ydGFudDs7XG4gIGJhY2tncm91bmQ6ICMzMDZlZmUgbGluZWFyLWdyYWRpZW50KDEyMGRlZywjMzBjZGUyIDAsIzMwNmVmZSAxMDAlKSBuby1yZXBlYXQgIWltcG9ydGFudDtcbiAgYm9yZGVyOiAwICFpbXBvcnRhbnQ7XG5cbiAgLyogYm9yZGVyLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50OyAqL1xuICAvKiBib3JkZXItcmFkaXVzOiAxMDBweDsgKi9cbn1cblxuLm1vei1ibHVlLWhvbGxvdyB7XG4gIGNvbG9yOiAjMzA2ZWZlIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjMzA2ZWZlIWltcG9ydGFudDtcbiAgLyogYm9yZGVyLXJhZGl1czogMTAwcHghaW1wb3J0YW50OyAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xufVxuXG4ubW96LXdoaXRlLWhvbGxvdyB7XG4gIGNvbG9yOiAjZmZmIWltcG9ydGFudDtcbiAgYm9yZGVyLWNvbG9yOiAjZmZmIWltcG9ydGFudDtcbiAgLyogYm9yZGVyLXJhZGl1czogMTAwcHghaW1wb3J0YW50OyAqL1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA5MDBweCkgLmJ1dHRvbiB7XG4gIGZvbnQtc2l6ZTogMC45ZW0gIWltcG9ydGFudDtcbiAgaW1nIHtcbiAgICB3aWR0aDogMjBweDtcbiAgICBoZWlnaHQ6MjBweDtcbiAgfVxufVxuXG48L3N0eWxlPlxuIiwiPHRlbXBsYXRlPlxuICA8ZGl2IDpjbGFzcz0nXCJiZy1cIit0eXBlJyByb2xlPSdhbGVydC1ib3gnPlxuICAgIDxzdHJvbmcgdi1odG1sPSdlcnJvcic+PC9zdHJvbmc+XG4gICAgPHNwYW4gdi1odG1sPSdtZXNzYWdlJz48L3NwYW4+XG4gICAgPGJ1dHRvbiBAY2xpY2s9JyRlbWl0KFwicmVtb3ZlXCIpJz4mdGltZXM7PC9idXR0b24+XG4gIDwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgcHJvcHM6IHtcbiAgICB0eXBlOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnc2Vjb25kYXJ5J1xuICAgIH0sXG4gICAgZXJyb3I6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICdlcnJvcidcbiAgICB9LFxuICAgIG1lc3NhZ2U6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICcnXG4gICAgfVxuICB9XG59O1xuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQgc3JjPScuL2FsZXJ0LXN0eWxlcy5jc3MnPlxuXG48L3N0eWxlPlxuIiwiPHRlbXBsYXRlID5cbiAgPGRpdiBjbGFzcz0nZmlsdGVyJz5cbiAgICA8aW5wdXRcbiAgICAgIGNsYXNzPSdkYXRhLXRhYmxlLWZpbHRlcidcbiAgICAgIHR5cGU9J3RleHQnXG4gICAgICA6dmFsdWU9J2ZpbHRlcidcbiAgICAgIEBpbnB1dD0naW5wdXQoJGV2ZW50KSdcbiAgICAgIHBsYWNlaG9sZGVyPSdmaWx0ZXIuLi4nXG4gICAgPlxuICAgIDxzcGFuXG4gICAgICB2LWlmPSdmaWx0ZXInXG4gICAgICBjbGFzcz0nZGF0YS10YWJsZS1maWx0ZXItY2xlYXInXG4gICAgICBAY2xpY2s9J2NsZWFyRmlsdGVyJ1xuICAgID4mdGltZXM7PC9zcGFuPlxuICA8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gIGNvbXBvbmVudHM6IHt9LFxuXG4gIHByb3BzOiBbJ2ZpbHRlclRleHQnXSxcblxuICBkYXRhOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgZmlsdGVyOiB0aGlzLmZpbHRlclRleHRcbiAgICB9O1xuICB9LFxuXG4gIGNvbXB1dGVkOiB7fSxcblxuICBtZXRob2RzOiB7XG4gICAgaW5wdXQ6IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICB0aGlzLmZpbHRlciA9IGV2ZW50LnRhcmdldC52YWx1ZTtcbiAgICAgIHRoaXMuJGVtaXQoJ3VwZGF0ZTpmaWx0ZXJUZXh0JywgZXZlbnQudGFyZ2V0LnZhbHVlKTtcbiAgICB9LFxuICAgIGNsZWFyRmlsdGVyOiBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMuZmlsdGVyID0gJyc7XG4gICAgICB0aGlzLiRlbWl0KCd1cGRhdGU6ZmlsdGVyVGV4dCcsICcnKTtcbiAgICB9XG4gIH1cbn07XG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZCBzcmM9Jy4uL0RhdGFUYWJsZS9kYXRhLXRhYmxlLXN0eWxlLmNzcyc+PC9zdHlsZT5cbjxzdHlsZSBzY29wZWQ+XG5kaXYuZmlsdGVyIHtcbiAgLyogYWxpZ24tc2VsZjogZmxleC1lbmQ7ICovXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5kYXRhLXRhYmxlLWZpbHRlciB7XG4gIHBhZGRpbmc6IDAgMS4yNWVtIDAgMC43NWVtO1xuICBoZWlnaHQ6IDJlbTtcbiAgYm9yZGVyOiBzb2xpZCAycHggI2UwZTBlMDtcbiAgYm9yZGVyLXJhZGl1czogMmVtO1xuICBmb250LXNpemU6IDAuODVlbTtcbn1cblxuLmRhdGEtdGFibGUtZmlsdGVyLWNhcHRpb24ge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiBncmV5O1xufVxuXG4uZGF0YS10YWJsZS1maWx0ZXI6Zm9jdXMge1xuICBvdXRsaW5lOiAwO1xuICBib3JkZXItY29sb3I6ICMwMDc1OTM7XG59XG5cbi5kYXRhLXRhYmxlLWZpbHRlci1jbGVhciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xuICByaWdodDogMDtcbiAgYm90dG9tOiAwO1xuICBkaXNwbGF5OiBmbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgd2lkdGg6IDJlbTtcbiAgY29sb3I6ICMwMDc1OTM7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG48L3N0eWxlPlxuIiwiPHRlbXBsYXRlID5cbiAgPGRpdlxuICAgICB2LWh0bWw9J2h0bWwnXG4gICAgIGNsYXNzPSd0ZCdcbiAgPlxuICA8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gIG5hbWU6ICdkYXRhLXRhYmxlLWNlbGwnLFxuICBwcm9wczoge1xuICAgIHJlY29yZElkOiB7IHR5cGU6IFN0cmluZyB9LFxuICAgIHJlY29yZDogeyB0eXBlOiBPYmplY3QgfSxcbiAgICBmaWVsZDogeyB0eXBlOiBTdHJpbmcgfSxcbiAgICBmaWVsZFJlbmRlckZ1bmN0aW9uczoge1xuICAgICAgZGVmYXVsdDogZnVuY3Rpb24oKSB7cmV0dXJuIHt9O30sXG4gICAgICB0eXBlOiBPYmplY3RcbiAgICB9XG4gIH0sXG4gIGNvbXB1dGVkOiB7XG4gICAgaHRtbDogZnVuY3Rpb24oKSB7XG4gICAgICBsZXQgciA9IHRoaXMucmVjb3JkO1xuICAgICAgbGV0IGYgPSB0aGlzLmZpZWxkO1xuICAgICAgbGV0IGkgPSB0aGlzLnJlY29yZElkO1xuICAgICAgbGV0IGZ1bmNzID0gdGhpcy5maWVsZFJlbmRlckZ1bmN0aW9ucztcblxuICAgICAgaWYgKGZ1bmNzICE9IHVuZGVmaW5lZCAmJiBmIGluIGZ1bmNzKSB7XG4gICAgICAgIHJldHVybiBmdW5jc1tmXShpLCByLCBmKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiByW2ZdO1xuICAgICAgfVxuICAgIH1cbiAgfVxufTtcbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkIHNyYz0nLi4vRGF0YVRhYmxlL2RhdGEtdGFibGUtc3R5bGUuY3NzJz48L3N0eWxlPlxuIiwiPHRlbXBsYXRlPlxuICA8ZGl2XG4gIGNsYXNzPSd0cidcbiAgQGNsaWNrPSckZW1pdChcInJvdzpjbGlja1wiLCByZWNvcmRJZCknPlxuICA8ZGl2IHYtaWY9J3NlbGVjdGFibGVRJyAgY2xhc3M9J3RkIHNlbGVjdGFibGUtY2hlY2tib3gnPlxuICAgIDxpbnB1dCB0eXBlPSdjaGVja2JveCcgbmFtZT0ncmVjb3JkSWQnIDpjaGVja2VkPSdzZWxlY3RlZFEnIEBjaGFuZ2U9JyRlbWl0KFwidG9nZ2xlZFNlbGVjdGVkXCIscmVjb3JkSWQpJz5cbiAgPC9kaXYgPlxuICAgIDxkYXRhLXRhYmxlLWNlbGxcbiAgICAgIHYtZm9yPSdmaWVsZCBpbiBmaWVsZHMnXG4gICAgICA6a2V5PSdmaWVsZC5pZCdcbiAgICAgIDpyZWNvcmQ9J3JlY29yZCdcbiAgICAgIDpyZWNvcmRJZD0ncmVjb3JkSWQnXG4gICAgICA6ZmllbGQ9J2ZpZWxkJ1xuICAgICAgOmZpZWxkUmVuZGVyRnVuY3Rpb25zPSdmaWVsZFJlbmRlckZ1bmN0aW9ucydcbiAgICAvPlxuICA8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5pbXBvcnQgRGF0YVRhYmxlQ2VsbCBmcm9tICcuLi9EYXRhVGFibGVDZWxsL0RhdGFUYWJsZUNlbGwudnVlJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBuYW1lOiAnZGF0YS10YWJsZS1yb3cnLFxuICBwcm9wczoge1xuICAgIHJlY29yZElkOiB7IHR5cGU6IFN0cmluZyB9LFxuICAgIHJlY29yZDogeyB0eXBlOiBPYmplY3QgfSxcbiAgICBmaWVsZHM6IHsgdHlwZTogQXJyYXkgfSxcbiAgICBzZWxlY3RhYmxlUToge3R5cGU6IEJvb2xlYW4sIGRlZmF1bHQ6IGZhbHNlfSxcbiAgICBzZWxlY3RlZDoge1xuICAgICAgdHlwZTogQXJyYXksIGRlZmF1bHQ6IGZ1bmN0aW9uKCl7XG4gICAgICAgIHJldHVybiBbXTtcbiAgICAgIH1cbiAgICB9LFxuICAgIGZpZWxkUmVuZGVyRnVuY3Rpb25zOiB7XG4gICAgICBkZWZhdWx0OiBmdW5jdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHt9O1xuICAgICAgfSxcbiAgICAgIHR5cGU6IE9iamVjdFxuICAgIH1cbiAgfSxcbiAgZGF0YTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICB9O1xuICB9LFxuICBjb21wb25lbnRzOiB7XG4gICAgRGF0YVRhYmxlQ2VsbFxuICB9LFxuICBjb21wdXRlZDoge1xuICAgIHZpc2libGVDb2x1bW5zKCkge1xuICAgICAgcmV0dXJuIHRoaXMuY29sdW1ucy5maWx0ZXIoY29sdW1uID0+ICFjb2x1bW4uaGlkZGVuKTtcbiAgICB9LFxuICAgIHNlbGVjdGVkUSgpIHtcbiAgICAgIGxldCBpZCA9IHRoaXMucmVjb3JkSWQ7XG4gICAgICBsZXQgc2VsZWN0ZWQgPSB0aGlzLnNlbGVjdGVkO1xuICAgICAgcmV0dXJuIHNlbGVjdGVkLmluY2x1ZGVzKGlkKTtcbiAgICB9XG4gIH1cbn07XG48L3NjcmlwdD5cblxuXG48c3R5bGUgc2NvcGVkIHNyYz0nLi4vRGF0YVRhYmxlL2RhdGEtdGFibGUtc3R5bGUuY3NzJz48L3N0eWxlPlxuIiwiPHRlbXBsYXRlPlxuICA8ZGl2XG4gICAgdi1odG1sPSdmaWVsZCdcbiAgICBjbGFzcz0ndGgnXG4gICAgOmNsYXNzPSdjZWxsQ2xhc3MnXG4gICAgQGNsaWNrPSckZW1pdChcImhlYWRlcmNlbGw6Y2xpY2tcIiwgZmllbGQpJ1xuICA+XG4gIDwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgbmFtZTogJ2RhdGEtdGFibGUtaGVhZGVyLWNlbGwnLFxuICBwcm9wczogWydmaWVsZCcsICdzb3J0J10sXG4gIGNvbXB1dGVkOiB7XG4gICAgY2VsbENsYXNzOiBmdW5jdGlvbigpIHtcbiAgICAgIHZhciBmaWVsZCA9IHRoaXMuZmllbGQ7XG4gICAgICB2YXIgc29ydCA9IHRoaXMuc29ydDtcbiAgICAgIHJldHVybiB0aGlzLmNsYXNzT2JqZWN0KGZpZWxkLCBzb3J0KTtcbiAgICB9XG4gIH0sXG4gIG1ldGhvZHM6IHtcbiAgICBjbGFzc09iamVjdDogZnVuY3Rpb24oZmllbGQsIHNvcnQpIHtcbiAgICAgIHZhciB2YWwgPSBzb3J0W2ZpZWxkXTtcbiAgICAgIHZhciBjbHMgPSB7fTtcblxuICAgICAgaWYgKCEoZmllbGQgaW4gc29ydCkpIHtcbiAgICAgICAgcmV0dXJuIGNscztcbiAgICAgIH1cbiAgICAgIGlmICh2YWwub3JkZXIgPT09ICdhc2NlbmRpbmcnKSB7XG4gICAgICAgIGNscy5hc2MgPSB0cnVlO1xuICAgICAgfVxuICAgICAgaWYgKHZhbC5vcmRlciA9PT0gJ2Rlc2NlbmRpbmcnKSB7XG4gICAgICAgIGNscy5kZXMgPSB0cnVlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gY2xzO1xuICAgIH1cbiAgfVxufTtcbjwvc2NyaXB0PlxuXG5cbjxzdHlsZSBzY29wZWQ+XG4uYXNjOmFmdGVyIHtcbiAgY29udGVudDogJ+KGkSc7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG4uZGVzOmFmdGVyIHtcbiAgY29udGVudDogJ+KGkyc7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuPC9zdHlsZT5cbiIsIjx0ZW1wbGF0ZT5cbiAgPGRpdiBjbGFzcz0ndHIgdGhlYWQnPlxuICAgIDxkaXYgdi1pZj0nc2VsZWN0YWJsZVEnICBjbGFzcz0ndGggc2VsZWN0YWJsZS1jaGVja2JveCc+XG4gICAgICA8aW5wdXQgdHlwZT0nY2hlY2tib3gnIG5hbWU9J2FsbCcgOmNoZWNrZWQ9J3NlbGVjdGVkUScgQGNoYW5nZT0nJGVtaXQoXCJzZWxlY3RBbGxcIiknPlxuICAgIDwvZGl2ID5cbiAgICA8ZGF0YS10YWJsZS1oZWFkZXItY2VsbFxuICAgICAgdi1mb3I9J2ZpZWxkIGluIGZpZWxkcydcbiAgICAgIDprZXk9J2ZpZWxkLmlkJ1xuICAgICAgOmZpZWxkPSdmaWVsZCdcbiAgICAgIDpzb3J0PSdzb3J0J1xuICAgICAgQGhlYWRlcmNlbGw6Y2xpY2s9J2hlYWRlckNlbGxDbGljaydcbiAgICAvPlxuICA8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5pbXBvcnQgRGF0YVRhYmxlSGVhZGVyQ2VsbCBmcm9tICcuLi9EYXRhVGFibGVIZWFkZXJDZWxsL0RhdGFUYWJsZUhlYWRlckNlbGwudnVlJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBuYW1lOiAnZGF0YS10YWJsZS1oZWFkZXInLFxuICBwcm9wczogWydmaWVsZHMnLCAnc29ydCcsICdzZWxlY3RhYmxlUSddLFxuICBkYXRhOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgdGltc29ydDogdGhpcy5zb3J0LFxuICAgICAgc2VsZWN0ZWRROiBmYWxzZVxuICAgIH07XG4gIH0sXG4gIGNvbXBvbmVudHM6IHsgRGF0YVRhYmxlSGVhZGVyQ2VsbCB9LFxuXG4gIG1ldGhvZHM6IHtcbiAgICBoZWFkZXJDZWxsQ2xpY2s6IGZ1bmN0aW9uKGZpZWxkKSB7XG4gICAgICBsZXQgdGltc29ydCA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMudGltc29ydCk7XG5cbiAgICAgIGlmIChmaWVsZCBpbiB0aW1zb3J0KSB7XG4gICAgICAgIGxldCBmaWVsZFNvcnQgPSB0aW1zb3J0W2ZpZWxkXTtcbiAgICAgICAgaWYgKGZpZWxkU29ydC5vcmRlciA9PSAnYXNjZW5kaW5nJykge1xuICAgICAgICAgIGZpZWxkU29ydC5vcmRlciA9ICdkZXNjZW5kaW5nJztcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICBkZWxldGUgdGltc29ydFtmaWVsZF07XG4gICAgICAgIH1cbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRpbXNvcnRbZmllbGRdID0geyBvcmRlcjogJ2FzY2VuZGluZycgfTtcbiAgICAgIH1cblxuICAgICAgdGhpcy50aW1zb3J0ID0gdGltc29ydDtcbiAgICAgIHRoaXMuJGVtaXQoJ2hlYWRlcjpjbGljaycsIHRoaXMudGltc29ydCk7XG4gICAgfVxuICB9XG59O1xuPC9zY3JpcHQ+XG5cblxuPHN0eWxlIHNjb3BlZCBzcmM9Jy4uL0RhdGFUYWJsZS9kYXRhLXRhYmxlLXN0eWxlLmNzcyc+PC9zdHlsZT5cbiIsIjx0ZW1wbGF0ZT5cbiAgPGRpdlxuICAgIGNsYXNzPSd0aCdcbiAgICBAY2xpY2s9JyRlbWl0KFwiZm9vdGVyY2VsbDpjbGlja1wiLCBmaWVsZCknXG4gID5cblxuXG4gIDxzZWxlY3RcbiAgICBAY2hhbmdlPSd1cGRhdGVTZWxlY3RlZCgkZXZlbnQpJ1xuICAgIGRhdGEtdG9nZ2xlPSd0b29sdGlwJ1xuICAgIGRhdGEtcGxhY2VtZW50PSdib3R0b20nXG4gICAgOnRpdGxlPSdzZWxlY3RlZCdcbiAgPlxuICAgIDxvcHRpb25cbiAgICAgIHYtZm9yPSdvcHRpb24gaW4gb3B0aW9ucydcbiAgICAgIDp2YWx1ZT0nb3B0aW9uJ1xuICAgICAgOnNlbGVjdGVkPSdvcHRpb24gPT0gc2VsZWN0ZWQnXG4gICAgPlxuICAgICAge3tzeW1ib2wob3B0aW9uKX19XG4gICAgPC9vcHRpb24+XG4gIDwvc2VsZWN0PlxuXG5cbiAgPHNwYW4+e3thZ2dyZWdhdGV9fTwvc3Bhbj5cblxuXG5cbiAgPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxubGV0IHN5bWJvbHMgPSB7XG4gIHN1bTogJ+KIkScsXG4gIG1lYW46ICfwnZyHJyxcbiAgc2V0OiAne30nXG59O1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIG5hbWU6ICdkYXRhLXRhYmxlLWZvb3Rlci1jZWxsJyxcblxuICBwcm9wczogWydmaWVsZCcsICdmaWVsZERhdGEnLCAnd2lkdGgnXSxcblxuICBkYXRhOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4ge1xuICAgICAgdHlwZTogdHlwZW9mIHRoaXMuZmllbGREYXRhWzBdLFxuICAgICAgc2VsZWN0ZWQ6ICcnXG4gICAgfTtcbiAgfSxcblxuICBjb21wdXRlZDoge1xuICAgIGFnZ3JlZ2F0ZTogZnVuY3Rpb24oKSB7XG4gICAgICBsZXQgdHlwZSA9IHRoaXMudHlwZTtcbiAgICAgIGxldCBzZWwgPSB0aGlzLnNlbGVjdGVkO1xuXG4gICAgICB2YXIgZGF0YSA9IHRoaXMuZmllbGREYXRhO1xuXG4gICAgICBpZiAodHlwZSA9PSAnbnVtYmVyJykge1xuICAgICAgICB2YXIgdmFsID0gZGF0YS5yZWR1Y2UoKGEsIGIpID0+IGEgKyBiLCAwKTtcbiAgICAgICAgaWYgKHNlbCA9PSAnc3VtJykge1xuICAgICAgICAgIHJldHVybiB2YWw7XG4gICAgICAgIH0gZWxzZSBpZiAoc2VsID09ICdtZWFuJykge1xuICAgICAgICAgIHZhciBtZWFuID0gdmFsIC8gZGF0YS5sZW5ndGg7XG4gICAgICAgICAgcmV0dXJuIE1hdGgucm91bmQobWVhbiAqIDEwMCkgLyAxMDA7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHZhbDtcbiAgICAgIH0gZWxzZSBpZiAodHlwZSA9PSAnc3RyaW5nJykge1xuICAgICAgICByZXR1cm4gWy4uLm5ldyBTZXQoZGF0YSldLmxlbmd0aDtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHJldHVybiAnJztcbiAgICAgIH1cbiAgICB9LFxuXG4gICAgb3B0aW9uczogZnVuY3Rpb24oKSB7XG4gICAgICBsZXQgdHlwZSA9IHRoaXMudHlwZTtcbiAgICAgIHZhciBvcHRzID0gW107XG5cbiAgICAgIGlmICh0eXBlID09ICdudW1iZXInKSB7XG4gICAgICAgIG9wdHMgPSBbJ3N1bScsICdtZWFuJ107XG4gICAgICB9IGVsc2UgaWYgKHR5cGUgPT0gJ3N0cmluZycpIHtcbiAgICAgICAgb3B0cyA9IFsnc2V0J107XG4gICAgICB9IGVsc2Uge1xuICAgICAgICBvcHRzID0gW107XG4gICAgICB9XG5cbiAgICAgIGlmIChvcHRzLmxlbmd0aCAmJiB0aGlzLnNlbGVjdGVkID09ICcnKSB7XG4gICAgICAgIHRoaXMuc2VsZWN0ZWQgPSBvcHRzWzBdO1xuICAgICAgfVxuICAgICAgcmV0dXJuIG9wdHM7XG4gICAgfVxuICB9LFxuXG4gIG1ldGhvZHM6IHtcbiAgICBzeW1ib2w6IGZ1bmN0aW9uKG9wdGlvbikge1xuICAgICAgcmV0dXJuIHN5bWJvbHNbb3B0aW9uXTtcbiAgICB9LFxuICAgIHVwZGF0ZVNlbGVjdGVkOiBmdW5jdGlvbihldmVudCkge1xuICAgICAgdGhpcy5zZWxlY3RlZCA9IGV2ZW50LnRhcmdldC52YWx1ZTtcbiAgICAgIC8vIGNvbnNvbGUubG9nKHRoaXMuJGVsLmNoaWxkTm9kZXMpO1xuICAgIH1cbiAgfVxufTtcbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkIHNyYz0nLi4vRGF0YVRhYmxlL2RhdGEtdGFibGUtc3R5bGUuY3NzJz48L3N0eWxlPlxuPHN0eWxlIHNjb3BlZD5cbnNlbGVjdCB7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICBib3JkZXItY29sb3I6IHRyYW5zcGFyZW50O1xuICAvKiBib3JkZXItYm90dG9tLWNvbG9yOiBSR0IoNTAsIDIwMCwgMjUwKTsgKi9cbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xuICBwYWRkaW5nOiAwIDVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiBSR0IoMzIsIDEyMiwgMjQ3KTtcbiAgLW1vei1hcHBlYXJhbmNlOm5vbmU7XG59XG5cbnNlbGVjdDpob3ZlciB7XG4gIGNvbG9yOiBSR0IoNTAsIDIwMCwgMjUwKTtcbn1cblxuc2VsZWN0OmZvY3VzIHtcbiAgb3V0bGluZTogbm9uZTtcbn1cblxub3B0aW9uIHtcbiAgcGFkZGluZzogMCA1cHg7XG59XG48L3N0eWxlPlxuIiwiPHRlbXBsYXRlPlxuICA8ZGl2IGNsYXNzPSd0ciB0Zm9vdCcgQGNsaWNrPSckZW1pdChcImZvb3RlcjpjbGlja1wiKSc+XG4gICAgPGRhdGEtdGFibGUtZm9vdGVyLWNlbGxcbiAgICAgIHYtZm9yPScoZGF0YSwgZmllbGQsIGluZGV4KSBpbiBmaWVsZERhdGEnXG4gICAgICA6a2V5PSdmaWVsZC5pZCdcbiAgICAgIDpmaWVsZD0nZmllbGQnXG4gICAgICA6ZmllbGREYXRhPSdkYXRhJ1xuICAgIC8+XG4gIDwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmltcG9ydCBEYXRhVGFibGVGb290ZXJDZWxsIGZyb20gJy4uL0RhdGFUYWJsZUZvb3RlckNlbGwvRGF0YVRhYmxlRm9vdGVyQ2VsbC52dWUnO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIG5hbWU6ICdkYXRhLXRhYmxlLWZvb3RlcicsXG4gIHByb3BzOiBbJ2ZpZWxkRGF0YSddLFxuICBjb21wb25lbnRzOiB7IERhdGFUYWJsZUZvb3RlckNlbGwgfSxcbiAgY29tcHV0ZWQ6IHt9XG59O1xuPC9zY3JpcHQ+XG5cblxuPHN0eWxlIHNjb3BlZCBzcmM9Jy4uL0RhdGFUYWJsZS9kYXRhLXRhYmxlLXN0eWxlLmNzcyc+PC9zdHlsZT5cbiIsIjx0ZW1wbGF0ZT5cbiAgPGRpdiBjbGFzcz1cInRyIHRwYWdlXCI+XG4gICAgPGRpdiBjbGFzcz1cInRyXCI+XG4gICAgICA8ZGl2IGNsYXNzPVwiXCI+XG4gICAgICAgIDxzcGFuPlJlY29yZHMgcGVyIHBhZ2U6Jm5ic3A7PC9zcGFuPlxuICAgICAgICA8c2VsZWN0IDpzZWxlY3RlZD1cInNlbGVjdGVkXCIgQGNoYW5nZT1cIiRlbWl0KCdwYWdlU2l6ZScsICRldmVudC50YXJnZXQudmFsdWUpXCI+XG4gICAgICAgICAgPG9wdGlvblxuICAgICAgICAgIHYtZm9yPVwib3B0IGluIHBhZ2VPcHRpb25zXCJcbiAgICAgICAgICA6a2V5PVwib3B0LmlkXCJcbiAgICAgICAgICA+e3tvcHR9fTwvb3B0aW9uPlxuICAgICAgICA8L3NlbGVjdD5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuXG4gICAgPGRpdiBjbGFzcz1cIlwiIHN0eWxlPSdmbGV4LWdyb3c6MSc+PC9kaXY+XG5cbiAgICA8ZGl2IGNsYXNzPVwicGFnZS1uYXZcIj5cbiAgICAgIDxhIEBjbGljaz1cIiRlbWl0KCdwcmV2JywgLTEpXCIgY2xhc3M9XCJwcmV2aW91cyByb3VuZFwiPiYjODI0OTs8L2E+XG4gICAgICA8ZGl2IGNsYXNzPSdwYWdlLXRleHQnPnBhZ2Uge3twYWdlKzF9fSAvIHt7cGFnZXN9fTwvZGl2PlxuICAgICAgPGEgQGNsaWNrPVwiJGVtaXQoJ25leHQnLCAxKVwiIGNsYXNzPVwibmV4dCByb3VuZFwiPiYjODI1MDs8L2E+XG4gICAgPC9kaXY+XG5cbiAgPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXG5leHBvcnQgZGVmYXVsdCB7XG4gIG5hbWU6ICdkYXRhLXRhYmxlLXBhZ2luYXRpb24nLFxuICBwcm9wczoge1xuICAgIHNlbGVjdGVkOiB7XG4gICAgICBkZWZhdWx0OiAnNSdcbiAgICB9LFxuICAgIHBhZ2U6IHtcbiAgICAgIGRlZmF1bHQ6IDBcbiAgICB9LFxuICAgIHBhZ2VzOiB7XG4gICAgICBkZWZhdWx0OiAwXG4gICAgfSxcbiAgICBwYWdlT3B0aW9uczoge1xuICAgICAgZGVmYXVsdDogWyc1JywgJzEwJywgJzI1JywgJ0FsbCddXG4gICAgfVxuXG4gIH0sXG4gIGNvbXBvbmVudHM6IHsgIH0sXG4gIGNvbXB1dGVkOiB7fSxcblxufTtcbjwvc2NyaXB0PlxuXG5cbjxzdHlsZSBzY29wZWQ+XG5cblxuXG4udHBhZ2Uge1xuICBwYWRkaW5nOiAxMHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbn1cblxuLnBhZ2UtbmF2IHtcbiAgcGFkZGluZy1sZWZ0OiAxMHB4O1xuICBkaXNwbGF5OiBpbmxpbmUtZmxleDtcbiAgZmxleC1kaXJlY3Rpb246IHJvdztcbn1cbi5wYWdlLXRleHR7XG4gIG1hcmdpbi1sZWZ0OiAycHg7XG4gIG1hcmdpbi1yaWdodDogMnB4O1xufVxuXG5hIHtcbiB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG4gZGlzcGxheTogaW5saW5lLWJsb2NrO1xuIHBhZGRpbmc6IDhweCAxNnB4O1xuIC13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7IC8qIENocm9tZSBhbGwgLyBTYWZhcmkgYWxsICovXG4tbW96LXVzZXItc2VsZWN0OiBub25lOyAgICAvKiBGaXJlZm94IGFsbCAgICAgICAgICAgICAqL1xuLW1zLXVzZXItc2VsZWN0OiBub25lOyAgICAgLyogSUUgMTArICAgICAgICAgICAgICAgICAgKi9cbiB1c2VyLXNlbGVjdDogbm9uZTsgICAgICAgIC8qIExpa2VseSBmdXR1cmUgICAgICAgICAgICovXG59XG5cbmE6aG92ZXIge1xuIGJhY2tncm91bmQtY29sb3I6ICNkZGQ7XG4gY29sb3I6IGJsYWNrO1xufVxuXG4ucHJldmlvdXMge1xuIGJhY2tncm91bmQtY29sb3I6ICNmMWYxZjE7XG4gY29sb3I6IGJsYWNrO1xufVxuXG4ubmV4dCB7XG4gYmFja2dyb3VuZC1jb2xvcjogI2YxZjFmMTtcbiBjb2xvcjogd2hpdGU7XG59XG5cbi5yb3VuZCB7XG4gYm9yZGVyLXJhZGl1czogNTAlO1xufVxuXG5zZWxlY3Qge1xuICAgIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50O1xuICAgIGJvcmRlci1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gICAgLXdlYmtpdC1hcHBlYXJhbmNlOiBub25lO1xuICAgIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG4gICAgcGFkZGluZzogMCA1cHg7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gICAgY29sb3I6IFJHQigzMiwgMTIyLCAyNDcpO1xuICAgIC1tb3otYXBwZWFyYW5jZTogbm9uZTtcbiAgfVxuXG48L3N0eWxlPlxuIiwiPHRlbXBsYXRlPlxuICA8b2w+XG4gICAgU29ydGVkIGJ5OlxuICAgIDxsaSB2LWZvcj0nKHNvcnQsIGZpZWxkLCBpKSBpbiB0aW1zb3J0Jz5cbiAgICAgIHt7ZmllbGR9fVxuICAgICAgPHNwYW4gOmNsYXNzPSdzb3J0Lm9yZGVyJz57e3N5bWJvbChzb3J0Lm9yZGVyKX19PC9zcGFuPlxuICAgIDwvbGk+XG4gIDwvb2w+XG5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5sZXQgc3ltYm9scyA9IHtcbiAgYXNjZW5kaW5nOiAn4oaRJyxcbiAgZGVzY2VuZGluZzogJ+KGkydcbn07XG5leHBvcnQgZGVmYXVsdCB7XG4gIG5hbWU6ICdkYXRhLXRhYmxlLWNhcHRpb24tc29ydCcsXG4gIHByb3BzOiBbJ3RpbXNvcnQnXSxcbiAgbWV0aG9kczoge1xuICAgIHN5bWJvbDogZnVuY3Rpb24ob3JkZXIpIHtcbiAgICAgIHJldHVybiBzeW1ib2xzW29yZGVyXTtcbiAgICB9XG4gIH1cbn07XG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZCBzcmM9Jy4uL0RhdGFUYWJsZS9kYXRhLXRhYmxlLXN0eWxlLmNzcyc+PC9zdHlsZT5cbjxzdHlsZSBzY29wZWQ+XG5vbCB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgZm9udC1zaXplOiAxMnB4O1xuICBjb2xvcjogZ3JleTtcbiAgbWFyZ2luOiAwcHg7XG4gIHBhZGRpbmc6IDBweDtcbn1cbmxpIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xuICBsaXN0LXN0eWxlOiBub25lO1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG5cbmxpOm5vdCg6Zmlyc3QtY2hpbGQpIHtcbiAgbWFyZ2luLWxlZnQ6IDFweDtcbn1cblxuLmFzY2VuZGluZyB7XG4gIGNvbG9yOiByZWQ7XG59XG4uZGVzY2VuZGluZyB7XG4gIGNvbG9yOiBibHVlO1xufVxuXG5saTpub3QoOmxhc3QtY2hpbGQpOmFmdGVyIHtcbiAgY29udGVudDogJy8nO1xufVxuPC9zdHlsZT5cbiIsIjx0ZW1wbGF0ZT5cbiAgPGRpdj5cbiAgICBSZWNvcmRzOlxuICAgIDxzcGFuXG4gICAgICB2LWlmPSdmaWx0ZXJlZCAhPSB0b3RhbCdcbiAgICAgIGNsYXNzPSd0b3AnXG4gICAgPlxuICAgICAge3tmaWx0ZXJlZH19XG4gICAgPC9zcGFuPlxuICAgIDxzcGFuPiB7e3RvdGFsfX0gPC9zcGFuPlxuICA8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5leHBvcnQgZGVmYXVsdCB7XG4gIG5hbWU6ICdkYXRhLXRhYmxlLWNhcHRpb24tc29ydCcsXG4gIHByb3BzOiBbJ2ZpbHRlcmVkJywgJ3RvdGFsJ11cbn07XG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZCBzcmM9Jy4uL0RhdGFUYWJsZS9kYXRhLXRhYmxlLXN0eWxlLmNzcyc+PC9zdHlsZT5cbjxzdHlsZSBzY29wZWQ+XG5kaXYge1xuICB0ZXh0LWFsaWduOiByaWdodDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBmb250LXNpemU6IDEycHg7XG4gIGNvbG9yOiBncmV5O1xufVxuLnRvcCB7IH1cbi50b3A6YWZ0ZXIge1xuICBjb250ZW50OiAnLyc7XG59XG48L3N0eWxlPlxuIiwiZXhwb3J0IGZ1bmN0aW9uIHNvcnRhYmxlVmFsdWUocmVjb3JkLCBmaWVsZCkge1xuICBsZXQgdmFsdWUgPSByZWNvcmRbZmllbGRdO1xuICByZXR1cm4gdmFsdWU7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiBmaWx0ZXJhYmxlVmFsdWUocmVjb3JkLCBmaWVsZCkge1xuICBsZXQgdmFsdWUgPSByZWNvcmRbZmllbGRdO1xuICBpZiAoIXZhbHVlKSB7IHJldHVybiAnJzsgfVxuICByZXR1cm4gdmFsdWUudG9TdHJpbmcoKS50b0xvd2VyQ2FzZSgpO1xufVxuXG5cbmZ1bmN0aW9uIHN0cmluZ1NvcnQoYSwgYiwgYXNjUSkge1xuICBsZXQgayA9IGFzY1EgPyAtMSA6IDE7XG4gIGlmIChhID09IGIpIHtyZXR1cm4gMDt9XG4gIHJldHVybiBhIDwgYiA/IDEgKiBrIDogLTEgKiBrO1xufVxuXG5mdW5jdGlvbiB2YWx1ZVEoYSkge1xuICBpZiAoXG4gICAgYSA9PSB1bmRlZmluZWQgfHxcbiAgICB0eXBlb2YgYSA9PSB1bmRlZmluZWQgfHxcbiAgICBpc05hTihhKSAmJiB0eXBlb2YoYSkgIT0gJ3N0cmluZycgfHxcbiAgICBhID09IG51bGxcbiAgKSB7IHJldHVybiBmYWxzZTsgfVxuICByZXR1cm4gdHJ1ZTtcbn1cblxuZnVuY3Rpb24gc3RhbmRhcml6ZVZhbHVlKGEpIHtcbiAgaWYgKCF2YWx1ZVEoYSkpIHtyZXR1cm4gdW5kZWZpbmVkO31cbiAgcmV0dXJuIGE7XG59XG5cbmZ1bmN0aW9uIG51bWJlclNvcnQoYSwgYiwgYXNjUSkge1xuICByZXR1cm4gYXNjUSA/IGEgLSBiIDogYiAtIGE7XG59XG5cbmV4cG9ydCBmdW5jdGlvbiB0YWJsZVRpbXNvcnQoaWRzLCByZWNvcmRzLCBzb3J0LCBzb3J0aW5nRnVuY3Rpb25zKSB7XG4gIHZhciBmaWVsZHMgPSBPYmplY3Qua2V5cyhzb3J0KTtcbiAgaWYgKCFmaWVsZHMubGVuZ3RoKSB7cmV0dXJuIGlkczt9XG5cbiAgdmFyIGZpZWxkRnVuY3Rpb25zID0gZmllbGRzLm1hcChcbiAgICBmdW5jdGlvbihmaWVsZCl7XG5cbiAgICAgIGxldCB0cmFuc2Zvcm0gPSAocmVjb3JkLCBmaWVsZCk9PntyZXR1cm4gcmVjb3JkW2ZpZWxkXTt9O1xuICAgICAgbGV0IGFzY2VuZGluZ1EgPSBzb3J0W2ZpZWxkXS5vcmRlciA9PSAnYXNjZW5kaW5nJztcblxuICAgICAgaWYgKGZpZWxkIGluIHNvcnRpbmdGdW5jdGlvbnMpIHsgdHJhbnNmb3JtID0gc29ydGluZ0Z1bmN0aW9uc1tmaWVsZF07IH1cblxuICAgICAgcmV0dXJuIGZ1bmN0aW9uKGEsIGIpIHtcbiAgICAgICAgbGV0IHggPSBzdGFuZGFyaXplVmFsdWUodHJhbnNmb3JtKGEsIGZpZWxkKSk7XG4gICAgICAgIGxldCB5ID0gc3RhbmRhcml6ZVZhbHVlKHRyYW5zZm9ybShiLCBmaWVsZCkpO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyh7YTogYVtmaWVsZF0sdDogdHJhbnNmb3JtKGEsIGZpZWxkKSxzdjogc3RhbmRhcml6ZVZhbHVlKHRyYW5zZm9ybShhLCBmaWVsZCkpfSk7XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKHtiOiBiW2ZpZWxkXSx0OiB0cmFuc2Zvcm0oYiwgZmllbGQpLHN2OiBzdGFuZGFyaXplVmFsdWUodHJhbnNmb3JtKGIsIGZpZWxkKSl9KTtcblxuICAgICAgICB2YXIgdHlwZTtcbiAgICAgICAgaWYgKFt4LCB5XS5zb21lKGZ1bmN0aW9uKGUpe3JldHVybiB0eXBlb2YgZSA9PSAnc3RyaW5nJzt9KSkge3R5cGU9J3N0cmluZyc7fVxuICAgICAgICBpZiAoW3gsIHldLnNvbWUoZnVuY3Rpb24oZSl7cmV0dXJuIHR5cGVvZiBlID09ICdudW1iZXInO30pKSB7dHlwZT0nbnVtYmVyJzt9XG4gICAgICAgIC8vIGNvbnNvbGUubG9nKCd0eXBlJywgdHlwZSk7XG5cbiAgICAgICAgaWYgKHR5cGUgPT0gJ3N0cmluZycpIHtcbiAgICAgICAgICBpZiAoIXZhbHVlUSh4KSkge3ggPSAnJzt9XG4gICAgICAgICAgaWYgKCF2YWx1ZVEoeSkpIHt5ID0gJyc7fVxuICAgICAgICAgIHJldHVybiBzdHJpbmdTb3J0KHgsIHksIGFzY2VuZGluZ1EpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2UgaWYgKHR5cGUgPT0gJ251bWJlcicpIHtcbiAgICAgICAgICBpZiAoIXZhbHVlUSh4KSkge3ggPSBJbmZpbml0eTt9XG4gICAgICAgICAgaWYgKCF2YWx1ZVEoeSkpIHt5ID0gSW5maW5pdHk7fVxuICAgICAgICAgIHJldHVybiBudW1iZXJTb3J0KHgsIHksIGFzY2VuZGluZ1EpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgIHJldHVybiAtMTtcbiAgICAgICAgfVxuXG4gICAgICB9O1xuICB9KTtcblxuXG4gIGxldCBzb3J0ZWQgPSBpZHMuc29ydChmdW5jdGlvbihhLCBiKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBmaWVsZEZ1bmN0aW9ucy5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGYgPSBmaWVsZEZ1bmN0aW9uc1tpXTtcbiAgICAgIHZhciB2ID0gZihyZWNvcmRzW2FdLCByZWNvcmRzW2JdKTtcbiAgICAgIGlmICghdikgeyBjb250aW51ZTsgfVxuICAgICAgZWxzZSB7YnJlYWs7fVxuICAgIH1cbiAgICByZXR1cm4gdjtcbiAgfSk7XG4gIC8vIGNvbnNvbGUubG9nKCdzb3J0ZWQnLCBzb3J0ZWQpO1xuICByZXR1cm4gc29ydGVkO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2lSZWdleE9uRmllbGRzKGZpbHRlclRleHQsIGlkcywgcmVjb3JkcywgZmllbGRzLCBmaWVsZFJlbmRlckZ1bmN0aW9ucykge1xuICB2YXIgcmVnID0gbmV3IFJlZ0V4cChmaWx0ZXJUZXh0LCAnZ2knKTtcbiAgdmFyIHVzZTtcblxuICBpZiAoZmlsdGVyVGV4dCA9PSAnJykge1xuICAgIHVzZSA9IGlkcztcbiAgfVxuICBlbHNlIHtcbiAgICB1c2UgPSBbXTtcblxuICAgIGlkcy5tYXAoZnVuY3Rpb24oaWQpIHtcblxuICAgICAgdmFyIHJlY29yZCA9IHJlY29yZHNbaWRdO1xuXG4gICAgICB2YXIgZmllbGRKb2luID0gZmllbGRzLm1hcChcbiAgICAgICAgZnVuY3Rpb24oZikge1xuICAgICAgICAgIGlmIChmIGluIGZpZWxkUmVuZGVyRnVuY3Rpb25zKSB7XG4gICAgICAgICAgICByZXR1cm4gZmllbGRSZW5kZXJGdW5jdGlvbnNbZl0oaWQsIHJlY29yZCwgZik7XG4gICAgICAgICAgfVxuICAgICAgICAgIHJldHVybiBmaWx0ZXJhYmxlVmFsdWUocmVjb3JkLCBmKTtcbiAgICAgICAgfVxuICAgICAgKS5qb2luKCcnKTtcblxuICAgICAgdmFyIG1hdGNoID0gZmllbGRKb2luLm1hdGNoKHJlZyk7XG5cbiAgICAgIGlmIChcbiAgICAgICAgIShtYXRjaCA9PSBudWxsIHx8IG1hdGNoLmpvaW4oJycpID09ICcnKVxuICAgICAgKSB7XG4gICAgICAgIHVzZS5wdXNoKGlkKTtcbiAgICAgIH1cblxuICAgIH0pO1xuICB9XG4gIHJldHVybiB1c2U7XG59XG5cblxuZXhwb3J0IGZ1bmN0aW9uIG9iamVjdEZpZWxkcyhpZHMsIHJlY29yZHMpIHtcbiAgdmFyIGFsbFJlY29yZEZpZWxkcyA9IFtdLmNvbmNhdChcbiAgICAuLi5pZHMubWFwKFxuICAgICAgZnVuY3Rpb24ocmVjb3JkSWQpe1xuICAgICAgICByZXR1cm4gT2JqZWN0LmtleXMocmVjb3Jkc1tyZWNvcmRJZF0pO1xuICAgICAgfSlcbiAgKTtcbiAgcmV0dXJuIFsuLi5uZXcgU2V0KGFsbFJlY29yZEZpZWxkcyldO1xufVxuIiwiPHRlbXBsYXRlID5cbiAgPGRpdiBjbGFzcz0nZGF0YS10YWJsZScgOnN0eWxlPVwieyd3aWR0aCc6d2lkdGgrJyUnfVwiPlxuXG4gICAgPGRpdiBjbGFzcz0nY2FwdGlvbi1yb3cnIHYtaWY9XCJpbmNsdWRlRmlsdGVyXCI+XG4gICAgICA8c3BhbiBjbGFzcz0ndGl0bGUnPnt7dGl0bGV9fTwvc3Bhbj5cbiAgICAgIDxkYXRhLXRhYmxlLWZpbHRlclxuICAgICAgICAgIDpmaWx0ZXJUZXh0PSdmaWx0ZXJUZXh0J1xuICAgICAgICAgIDpmaWx0ZXJlZD0nZmlsdGVyZWRSZWNvcmRzLmxlbmd0aCdcbiAgICAgICAgICA6dG90YWw9J3JlY29yZElkcy5sZW5ndGgnXG4gICAgICAgICAgQHVwZGF0ZTpmaWx0ZXJUZXh0PSd1cGRhdGVGaWx0ZXJUZXh0J1xuICAgICAgICAvPlxuICAgIDwvZGl2PlxuXG4gICAgPGRpdiBjbGFzcz0nY2FwdGlvbi1yb3cnID5cbiAgICAgIDxkYXRhLXRhYmxlLWNhcHRpb24tc29ydCA6dGltc29ydD0ndGltc29ydCcgc3R5bGU9J2Zsb2F0OmxlZnQ7Jy8+XG4gICAgICA8ZGF0YS10YWJsZS1jYXB0aW9uLWZpbHRlclxuICAgICAgICA6ZmlsdGVyZWQ9J2ZpbHRlcmVkUmVjb3Jkcy5sZW5ndGgnXG4gICAgICAgIDp0b3RhbD0ncmVjb3JkSWRzLmxlbmd0aCdcbiAgICAgIC8+XG4gICAgPC9kaXY+XG5cblxuICAgIDxkaXYgY2xhc3M9J3RhYmxlLXdyYXBwZXInPlxuICAgICAgPGRpdiBjbGFzcz0ndGFibGUnPlxuICAgICAgICA8ZGF0YS10YWJsZS1oZWFkZXJcbiAgICAgICAgICA6ZmllbGRzPSdmaWVsZHMnXG4gICAgICAgICAgOnNvcnQ9J3RpbXNvcnQnXG4gICAgICAgICAgQGhlYWRlcjpjbGljaz0naGVhZGVyQ2xpY2snXG4gICAgICAgICAgQHNlbGVjdEFsbD0nc2VsZWN0QWxsJ1xuICAgICAgICAgIDpzZWxlY3RhYmxlUT0nc2VsZWN0YWJsZVEnXG4gICAgICAgIC8+XG5cbiAgICAgICAgPGRpdiBjbGFzcz0ndGJvZHknPlxuICAgICAgICAgIDxkYXRhLXRhYmxlLXJvd1xuICAgICAgICAgICAgdi1mb3I9J3JlY29yZElkIGluIGlkc09mUGFnZSdcbiAgICAgICAgICAgIDprZXk9J3JlY29yZElkLmlkJ1xuICAgICAgICAgICAgOnJlY29yZElkPSdyZWNvcmRJZCdcbiAgICAgICAgICAgIDpyZWNvcmQ9J3JlY29yZHNbcmVjb3JkSWRdJ1xuICAgICAgICAgICAgOmZpZWxkcz0nZmllbGRzJ1xuICAgICAgICAgICAgOmZpZWxkUmVuZGVyRnVuY3Rpb25zPSdmaWVsZFJlbmRlckZ1bmN0aW9ucydcbiAgICAgICAgICAgIDpzZWxlY3RhYmxlUT0nc2VsZWN0YWJsZVEnXG4gICAgICAgICAgICA6c2VsZWN0ZWQ9J3NlbGVjdGVkJ1xuICAgICAgICAgICAgQHRvZ2dsZWRTZWxlY3RlZD0ndG9nZ2xlZFNlbGVjdGVkJ1xuICAgICAgICAgICAgQHJvdzpjbGljaz0nJ1xuICAgICAgICAgIC8+XG4gICAgICAgIDwvZGl2PlxuXG4gICAgICAgIDxkYXRhLXRhYmxlLWZvb3RlclxuICAgICAgICA6ZmllbGREYXRhPSdmaWVsZERhdGEnXG4gICAgICAgIC8+XG4gICAgICAgIDxkYXRhLXRhYmxlLXBhZ2luYXRpb25cbiAgICAgICAgICB2LWlmPVwicGFnaW5hdGlvblwiXG4gICAgICAgICAgOnNlbGVjdGVkPVwicGFnZVNpemVcIlxuICAgICAgICAgIDpwYWdlPVwiY3VycmVudFBhZ2VcIlxuICAgICAgICAgIDpwYWdlcz1cIm51bVBhZ2VzXCJcbiAgICAgICAgICBAcHJldj1cImNoYW5nZVBhZ2VcIlxuICAgICAgICAgIEBuZXh0PVwiY2hhbmdlUGFnZVwiXG4gICAgICAgICAgQHBhZ2VTaXplPVwiY2hhbmdlUGFnZVNpemVcIlxuICAgICAgICAgIDpwYWdlT3B0aW9ucz1cInBhZ2VPcHRpb25zXCJcbiAgICAgICAgLz5cbiAgICAgIDwvZGl2PlxuICAgIDwvZGl2PlxuXG4gIDwvZGl2PlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cbmltcG9ydCBEYXRhVGFibGVGaWx0ZXIgZnJvbSAnLi4vRGF0YVRhYmxlRmlsdGVyL0RhdGFUYWJsZUZpbHRlci52dWUnO1xuaW1wb3J0IERhdGFUYWJsZVJvdyBmcm9tICcuLi9EYXRhVGFibGVSb3cvRGF0YVRhYmxlUm93LnZ1ZSc7XG5pbXBvcnQgRGF0YVRhYmxlSGVhZGVyIGZyb20gJy4uL0RhdGFUYWJsZUhlYWRlci9EYXRhVGFibGVIZWFkZXIudnVlJztcbmltcG9ydCBEYXRhVGFibGVGb290ZXIgZnJvbSAnLi4vRGF0YVRhYmxlRm9vdGVyL0RhdGFUYWJsZUZvb3Rlci52dWUnO1xuaW1wb3J0IERhdGFUYWJsZVBhZ2luYXRpb24gZnJvbSAnLi4vRGF0YVRhYmxlUGFnaW5hdGlvbi9EYXRhVGFibGVQYWdpbmF0aW9uLnZ1ZSc7XG5pbXBvcnQgRGF0YVRhYmxlQ2FwdGlvblNvcnQgZnJvbSAnLi4vRGF0YVRhYmxlQ2FwdGlvblNvcnQvRGF0YVRhYmxlQ2FwdGlvblNvcnQudnVlJztcbmltcG9ydCBEYXRhVGFibGVDYXB0aW9uRmlsdGVyIGZyb20gJy4uL0RhdGFUYWJsZUNhcHRpb25GaWx0ZXIvRGF0YVRhYmxlQ2FwdGlvbkZpbHRlci52dWUnO1xuaW1wb3J0IHsgZ2lSZWdleE9uRmllbGRzLCBvYmplY3RGaWVsZHMsIHRhYmxlVGltc29ydCB9IGZyb20gJy4vaGVscGVycy5qcyc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgY29tcG9uZW50czoge1xuICAgIERhdGFUYWJsZVJvdyxcbiAgICBEYXRhVGFibGVGaWx0ZXIsXG4gICAgRGF0YVRhYmxlSGVhZGVyLFxuICAgIERhdGFUYWJsZUNhcHRpb25Tb3J0LFxuICAgIERhdGFUYWJsZUZvb3RlcixcbiAgICBEYXRhVGFibGVDYXB0aW9uRmlsdGVyLFxuICAgIERhdGFUYWJsZVBhZ2luYXRpb25cbiAgfSxcbiAgcHJvcHM6IHtcbiAgICBwYWdpbmF0aW9uOiB7XG4gICAgICB0eXBlOiBCb29sZWFuLFxuICAgICAgZGVmYXVsdDogZmFsc2VcbiAgICB9LFxuICAgIGluY2x1ZGVGaWx0ZXI6IHtcbiAgICAgIHR5cGU6IEJvb2xlYW4sXG4gICAgICBkZWZhdWx0OiB0cnVlXG4gICAgfSxcbiAgICBkYXRhOiB7XG4gICAgICBkZWZhdWx0OiBmdW5jdGlvbigpIHtyZXR1cm4ge307fSxcbiAgICAgIHR5cGU6IE9iamVjdFxuICAgIH0sXG4gICAgd2lkdGg6IHtcbiAgICAgIGRlZmF1bHQ6IDEwMFxuICAgIH0sXG4gICAgZmllbGRSZW5kZXJGdW5jdGlvbnM6IHtcbiAgICAgIGRlZmF1bHQ6IGZ1bmN0aW9uKCkge3JldHVybiB7fTt9LFxuICAgICAgdHlwZTogT2JqZWN0XG4gICAgfSxcbiAgICBmaWVsZFNvcnRpbmdGdW5jdGlvbnM6IHtcbiAgICAgIGRlZmF1bHQ6IGZ1bmN0aW9uKCkge3JldHVybiB7fTt9LFxuICAgICAgdHlwZTogT2JqZWN0XG4gICAgfSxcbiAgICBmaWVsZEFnZ3JlZ2F0ZUZ1bmN0aW9uczoge1xuICAgICAgZGVmYXVsdDogZnVuY3Rpb24oKSB7cmV0dXJuIHt9O30sXG4gICAgICB0eXBlOiBPYmplY3RcbiAgICB9LFxuICAgIHRpdGxlOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnJ1xuICAgIH0sXG4gICAgc2VsZWN0YWJsZVE6IHtcbiAgICAgIHR5cGU6Qm9vbGVhbixcbiAgICAgIGRlZmF1bHQ6IGZhbHNlXG4gICAgfSxcbiAgICBzdGFydGluZ1BhZ2VTaXplOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnNSdcbiAgICB9LFxuICAgIHBhZ2VPcHRpb25zOiB7XG4gICAgICBkZWZhdWx0OiBbJzUnLCAnMTAnLCAnMjUnLCAnQWxsJ11cbiAgICB9XG4gIH0sXG5cbiAgZGF0YTogZnVuY3Rpb24oKSB7XG4gICAgbGV0IGluUGFnZU9wdHMgPSB0aGlzLnBhZ2VPcHRpb25zLmluZGV4T2YodGhpcy5zdGFydGluZ1BhZ2VTaXplKSA+IC0xXG4gICAgcmV0dXJuIHtcbiAgICAgIC8vIGFjdHVhbCBkYXRhXG4gICAgICByZWNvcmRzOiB0aGlzLmRhdGEsXG4gICAgICAvLyBrZXlzIGluIGRhdGFcbiAgICAgIHJlY29yZElkczogT2JqZWN0LmtleXModGhpcy5kYXRhKSxcbiAgICAgIC8vIGZpZWxkcyBhY3Jvc3MgYWxsIHJlY29yZHNcbiAgICAgIGZpZWxkczogb2JqZWN0RmllbGRzKE9iamVjdC5rZXlzKHRoaXMuZGF0YSksIHRoaXMuZGF0YSksXG4gICAgICAvLyB2YWx1ZSBvZiBmaWx0ZXJcbiAgICAgIGZpbHRlclRleHQ6ICcnLFxuXG4gICAgICAvLyBsaXN0IG9mIHNvcnRzXG4gICAgICB0aW1zb3J0OiB7fSxcblxuICAgICAgc29ydGVkUmVjb3JkczogT2JqZWN0LmtleXModGhpcy5kYXRhKSxcbiAgICAgIGZpbHRlcmVkUmVjb3JkczogT2JqZWN0LmtleXModGhpcy5kYXRhKSxcbiAgICAgIHNvcnRlZEFuZEZpbHRlcmVkUmVjb3JkczogT2JqZWN0LmtleXModGhpcy5kYXRhKSxcbiAgICAgIHNlbGVjdGVkOiBbXSxcblxuICAgICAgcGFnZVNpemU6IGluUGFnZU9wdHMgPyB0aGlzLnN0YXJ0aW5nUGFnZVNpemUgOiB0aGlzLnBhZ2VPcHRpb25zWzBdLFxuICAgICAgY3VycmVudFBhZ2U6MCxcbiAgICB9O1xuICB9LFxuXG4gIGNvbXB1dGVkOiB7XG4gICAgaWRzT2ZQYWdlKCkge1xuICAgICAgaWYgKCF0aGlzLnBhZ2luYXRpb24pIHJldHVybiB0aGlzLnNvcnRlZEFuZEZpbHRlcmVkUmVjb3Jkc1xuICAgICAgaWYgKHRoaXMucGFnZVNpemUgPT0gJ0FsbCcpIHJldHVybiB0aGlzLnNvcnRlZEFuZEZpbHRlcmVkUmVjb3Jkc1xuICAgICAgcmV0dXJuIHRoaXMuc29ydGVkQW5kRmlsdGVyZWRSZWNvcmRzLnNsaWNlKHRoaXMuY3VycmVudFBhZ2UgKiB0aGlzLnBhZ2VTaXplLCAodGhpcy5jdXJyZW50UGFnZSsxKSp0aGlzLnBhZ2VTaXplKVxuICAgIH0sXG4gICAgbnVtUGFnZXMoKSB7XG4gICAgICBpZiAodGhpcy5wYWdlU2l6ZSA9PSAnQWxsJykgcmV0dXJuIDFcbiAgICAgIHJldHVybiBNYXRoLmNlaWwodGhpcy5zb3J0ZWRBbmRGaWx0ZXJlZFJlY29yZHMubGVuZ3RoIC8gdGhpcy5wYWdlU2l6ZSlcbiAgICB9LFxuICAgIGZpZWxkRGF0YTogZnVuY3Rpb24oKSB7XG4gICAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgICB2YXIgZmllbGRPYmplY3QgPSB7fTtcblxuICAgICAgaWYgKHRoaXMuc2VsZWN0YWJsZVEpIHtcbiAgICAgICAgbGV0IHNlbGVjdGVkID0gdGhpcy5zZWxlY3RlZDtcbiAgICAgICAgZmllbGRPYmplY3RbJ2NoZWNrZWQnXSA9IHRoaXMuc29ydGVkQW5kRmlsdGVyZWRSZWNvcmRzLm1hcChmdW5jdGlvbihpZCkge1xuICAgICAgICAgIGlmIChzZWxlY3RlZC5pbmNsdWRlcyhpZCkpIHtyZXR1cm4gMTt9XG4gICAgICAgICAgcmV0dXJuIDA7XG4gICAgICAgIH0pO1xuICAgICAgfVxuXG4gICAgICB0aGF0LmZpZWxkcy5tYXAoZnVuY3Rpb24oZmllbGQpIHtcbiAgICAgICAgdGhhdC5zb3J0ZWRBbmRGaWx0ZXJlZFJlY29yZHMubWFwKGZ1bmN0aW9uKGlkKSB7XG4gICAgICAgICAgbGV0IHZhbHVlID0gdGhhdC5yZWNvcmRzW2lkXVtmaWVsZF07XG4gICAgICAgICAgaWYgKGZpZWxkT2JqZWN0W2ZpZWxkXSA9PSB1bmRlZmluZWQpIHtcbiAgICAgICAgICAgIGZpZWxkT2JqZWN0W2ZpZWxkXSA9IFtdO1xuICAgICAgICAgIH1cbiAgICAgICAgICBpZiAoZmllbGQgaW4gdGhhdC5maWVsZFJlbmRlckZ1bmN0aW9ucykge1xuICAgICAgICAgICAgZmllbGRPYmplY3RbZmllbGRdLnB1c2goXG4gICAgICAgICAgICAgIHRoYXQuZmllbGRSZW5kZXJGdW5jdGlvbnNbZmllbGRdKGlkLCB0aGF0LnJlY29yZHNbaWRdLCBmaWVsZClcbiAgICAgICAgICAgICk7XG4gICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIGZpZWxkT2JqZWN0W2ZpZWxkXS5wdXNoKHZhbHVlKTtcbiAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gZmllbGRPYmplY3Q7XG4gICAgfVxuICB9LFxuXG4gIG1ldGhvZHM6IHtcbiAgICBjaGFuZ2VQYWdlKHZhbCkge1xuICAgICAgaWYgKHRoaXMuY3VycmVudFBhZ2UgKyB2YWwgPCAwKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuICAgICAgaWYgKHRoaXMuY3VycmVudFBhZ2UgKyB2YWwgPiB0aGlzLm51bVBhZ2VzKSB7XG4gICAgICAgIHJldHVyblxuICAgICAgfVxuICAgICAgdGhpcy5jdXJyZW50UGFnZSArPSB2YWxcblxuICAgIH0sXG4gICAgY2hhbmdlUGFnZVNpemUodmFsKSB7XG4gICAgICB0aGlzLnBhZ2VTaXplID0gdmFsXG4gICAgICB0aGlzLnBhZ2UgPSAwXG4gICAgICAvLyB1cGRhdGUgcGFnZVxuICAgIH0sXG4gICAgaGVhZGVyQ2xpY2s6IGZ1bmN0aW9uKHNvcnQpIHtcbiAgICAgIHRoaXMudGltc29ydCA9IHNvcnQ7XG4gICAgICAvLyBjb25zb2xlLmxvZygnVElNU09SVCcsIHRoaXMudGltc29ydCk7XG4gICAgICB0aGlzLnNvcnRlZFJlY29yZHMgPSB0aGlzLnNvcnRSZWNvcmRzKHRoaXMucmVjb3JkSWRzKTtcbiAgICAgIHRoaXMuZmlsdGVyZWRSZWNvcmRzID0gdGhpcy5maWx0ZXJSZWNvcmRzKHRoaXMuc29ydGVkUmVjb3Jkcyk7XG4gICAgICB0aGlzLnNvcnRlZEFuZEZpbHRlcmVkUmVjb3JkcyA9IHRoaXMuZmlsdGVyZWRSZWNvcmRzO1xuICAgIH0sXG4gICAgdXBkYXRlRmlsdGVyVGV4dDogZnVuY3Rpb24oZmlsdGVyVGV4dCkge1xuICAgICAgdGhpcy5maWx0ZXJUZXh0ID0gZmlsdGVyVGV4dDtcblxuICAgICAgdGhpcy5maWx0ZXJlZFJlY29yZHMgPSB0aGlzLmZpbHRlclJlY29yZHModGhpcy5zb3J0ZWRSZWNvcmRzKTtcbiAgICAgIHRoaXMuc29ydGVkQW5kRmlsdGVyZWRSZWNvcmRzID0gdGhpcy5maWx0ZXJlZFJlY29yZHM7XG4gICAgfSxcblxuICAgIHNvcnRBbmRGaWx0ZXI6IGZ1bmN0aW9uKCkge1xuICAgICAgbGV0IHNvcnRlZCA9IHRoaXMuc29ydFJlY29yZHModGhpcy5yZWNvcmRJZHMpO1xuICAgICAgcmV0dXJuIHRoaXMuZmlsdGVyZWRSZWNvcmRzKHNvcnRlZCk7XG4gICAgfSxcblxuICAgIGZpbHRlclJlY29yZHM6IGZ1bmN0aW9uKGlkcykge1xuICAgICAgcmV0dXJuIGdpUmVnZXhPbkZpZWxkcyh0aGlzLmZpbHRlclRleHQsIGlkcywgdGhpcy5yZWNvcmRzLCB0aGlzLmZpZWxkcywgdGhpcy5maWVsZFJlbmRlckZ1bmN0aW9ucyk7XG4gICAgfSxcblxuICAgIHNvcnRSZWNvcmRzOiBmdW5jdGlvbihpZHMpIHtcbiAgICAgIHJldHVybiB0YWJsZVRpbXNvcnQoaWRzLCB0aGlzLnJlY29yZHMsIHRoaXMudGltc29ydCwgdGhpcy5maWVsZFNvcnRpbmdGdW5jdGlvbnMpO1xuICAgIH0sXG5cbiAgICBzZWxlY3RBbGw6IGZ1bmN0aW9uKCkge1xuICAgICAgaWYgKHRoaXMuc2VsZWN0ZWQubGVuZ3RoID09IHRoaXMuc29ydGVkQW5kRmlsdGVyZWRSZWNvcmRzLmxlbmd0aCkge1xuICAgICAgICB0aGlzLnNlbGVjdGVkID0gW107XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLnNlbGVjdGVkID0gdGhpcy5zb3J0ZWRBbmRGaWx0ZXJlZFJlY29yZHMuc2xpY2UoKTtcbiAgICAgIH1cbiAgICB9LFxuICAgIHRvZ2dsZWRTZWxlY3RlZDogZnVuY3Rpb24oaWQpIHtcblxuICAgICAgbGV0IHNlbGVjdGVkID0gdGhpcy5zZWxlY3RlZDtcbiAgICAgIGlmIChzZWxlY3RlZC5pbmNsdWRlcyhpZCkpIHtcbiAgICAgICAgc2VsZWN0ZWQuc3BsaWNlKHNlbGVjdGVkLmluZGV4T2YoaWQpLCAxKVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgc2VsZWN0ZWQucHVzaChpZCk7XG4gICAgICB9XG4gICAgfVxuICB9XG59O1xuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQgc3JjPScuL2RhdGEtdGFibGUtc3R5bGUuY3NzJz48L3N0eWxlPlxuIiwiZXhwb3J0IGNvbnN0IGh0dHBSZWFkeVN0YXRlQ29kZXMgPSB7XG4gIDA6ICd1bnNlbnQnLFxuICAxOiAnb3BlbmVkJyxcbiAgMjogJ2hlYWRlcnMnLFxuICAzOiAnbG9hZGluZycsXG4gIDQ6ICdkb25lJ1xufTtcbmV4cG9ydCBjb25zdCBodHRwU3RhdHVzQ29kZXMgPSB7XG4gIDEwMDogJ2NvbnRpbnVlJyxcbiAgMTAxOiAnc3dpdGNoaW5nIHByb3RvY29scycsXG4gIDEwMjogJ3Byb2Nlc3NpbmcnLFxuICAxMDM6ICdlYXJseSBoaW50cycsXG4gIDIwMDogJ29rJyxcbiAgMjAxOiAnY3JlYXRlZCcsXG4gIDIwMjogJ2FjY2VwdGVkJyxcbiAgMjAzOiAnbm9uLWF1dGhvcml0YXRpdmUgaW5mb3JtYXRpb24nLFxuICAyMDQ6ICdubyBjb250ZW50JyxcbiAgMjA1OiAncmVzZXQgY29udGVuJyxcbiAgMjA2OiAncGFydGlhbCBjb250ZW50JyxcbiAgMjA3OiAnbXVsdGktc3RhdHVzJyxcbiAgMjA4OiAnYWxyZWFkeSByZXBvcnRlZCcsXG4gIDIxODogJ3RoaXMgaXMgZmluZScsXG4gIDIyNjogJ0lNIHVzZWQnLFxuICAzMDA6ICdtdWx0aXBsZSBjaG9pY2VzJyxcbiAgMzAxOiAnbW92ZWQgcGVybWFuZW50bHknLFxuICAzMDI6ICdmb3VuZCcsXG4gIDMwMzogJ3NlZSBvdGhlcicsXG4gIDMwNDogJ25vdCBtb3RpZmllZCcsXG4gIDMwNTogJ3VzZWQgcHJveHknLFxuICAzMDY6ICdzd2l0Y2ggcHJveHknLFxuICAzMDc6ICd0ZW1wb3JhcnkgcmVkaXJlY3QnLFxuICAzMDg6ICdwZXJtYW5lbnQgcmVkaXJlY3QnLFxuICA0MDA6ICdiYWQgcmVxdWVzdCcsXG4gIDQwMTogJ3VuYXV0aG9yaXplZCcsXG4gIDQwMjogJ3BheW1lbnQgcmVxdWlyZWQnLFxuICA0MDM6ICdmb3JiaWRkZW4nLFxuICA0MDQ6ICdub3QgZm91bmQnLFxuICA0MDU6ICdtZXRob2Qgbm90IGFsbG93ZWQnLFxuICA0MDY6ICdubyBhY2NlcHRhYmxlJyxcbiAgNDA3OiAncHJveHkgYXV0aGVudGljYXRpb24gcmVxdWlyZWQnLFxuICA0MDg6ICdyZXF1ZXN0ZWQgdGltZW91dCcsXG4gIDQwOTogJ2NvbmZsaWN0JyxcbiAgNDEwOiAnZ29uZScsXG4gIDQxMTogJ2xlbmd0aCByZXF1aXJlZCcsXG4gIDQxMjogJ3ByZWNvbmRpdGlvbiBmYWlsZWQnLFxuICA0MTM6ICdwYXlvdXQgdG9vIGxhcmdlJyxcbiAgNDE0OiAnVVJJIHRvbyBsb25nJyxcbiAgNDE1OiAndW5zdXBwb3J0ZWQgbWVkaWEgdHlwZScsXG4gIDQxNjogJ3JhbmdlIG5vdCBzYXRpc2ZpYWJsZScsXG4gIDQxNzogJ2V4cGVjdGF0aW9uIGZhaWxlZCcsXG4gIDQxODogJ0lcXCdtIGEgdGVhcG90JyxcbiAgNDIxOiAnbWlzZGlyZWN0ZWQgcmVxdWVzdGVkJyxcbiAgNDIyOiAndW5wcm9jZXNzYWJsZSBFbnRpdHknLFxuICA0MjM6ICdsb2NrZWQnLFxuICA0MjQ6ICdmYWlsZWQgZGVwZW5kZW5jeScsXG4gIDQyNjogJ3VwZ3JhZGUgcmVxdWlyZWQnLFxuICA0Mjg6ICdwcmVjb25kaXRpb24gcmVxdWlyZWQnLFxuICA0Mjk6ICd0b28gbWFueSByZXF1ZXN0ZWQnLFxuICA0MzE6ICdyZXF1ZXN0IGhlYWRlciBmaWVsZHMgdG9vIGxhcmdlJyxcbiAgNDUxOiAndW5hdmFpbGFibGUgZm9yIGxlZ2FsIHJlYXNvbnMnLFxuICA1MDA6ICdpbnRlcm5hbCBzZXJ2ZXIgZXJyb3InLFxuICA1MDE6ICdub3QgaW1wbGVtZW50ZWQnLFxuICA1MDI6ICdiYWQgZ2F0ZXdheScsXG4gIDUwMzogJ3NlcnZpY2UgdW5hdmFpbGFibGUnLFxuICA1MDQ6ICdnYXRld2F5IHRpbWVvdXQnLFxuICA1MDU6ICdodHRwIHZlcnNpb24gbm90IHN1cHBvcnRlZCcsXG4gIDUwNjogJ3ZhcmlhbnQgYWxzbyBuZWdvdGlhdGVzJyxcbiAgNTA3OiAnaW5zdWZmaWNpZW50IHN0b3JhZ2UnLFxuICA1MDg6ICdsb29wIGRldGVjdGVkJyxcbiAgNTEwOiAnbm90IGV4dGVuZGVkJyxcbiAgNTExOiAnbmV0d29yayBhdXRoZW50aWNhdGlvbiByZXF1aXJlZCdcbn07XG4iLCI8dGVtcGxhdGUgPlxuICA8ZGl2ICB2LWlmPSdzaG93USc+XG4gICAgPGRpdiBjbGFzcz0ndGV4dC1tdXRlZCc+XG4gICAgICA8c3Bhbj57e2h1bWFuUmVhZGFibGVMZWFkfX08L3NwYW4+XG4gICAgICA8c3BhbiA6Y2xhc3M9J1widGV4dC1cIitjb2xvcic+e3todW1hblJlYWRhYmxlTWVzc2FnZX19PC9zcGFuPlxuICAgICAgPHNwYW4+e3todW1hblJlYWRhYmxlRW5kfX08L3NwYW4+XG4gICAgPC9kaXY+XG4gICAgPGRpdiBjbGFzcz0ncHJvZ3Jlc3MnPlxuICAgICAgPGRpdlxuICAgICAgICBjbGFzcz0ncHJvZ3Jlc3MtYmFyIHByb2dyZXNzLWJhci1zdHJpcGVkIHByb2dyZXNzLWJhci1hbmltYXRlZCdcbiAgICAgICAgOmNsYXNzPSdcImJnLVwiK2NvbG9yJ1xuICAgICAgICByb2xlPSdwcm9ncmVzc2JhcidcbiAgICAgICAgOnN0eWxlPSd7d2lkdGg6IFN0cmluZyhwZXJjZW50KStcIiVcIn0nXG4gICAgICAgIDphcmlhLXZhbHVlbm93PSdwZXJjZW50J1xuICAgICAgICBhcmlhLXZhbHVlbWluPScwJ1xuICAgICAgICBhcmlhLXZhbHVlbWF4PScxMDAnXG4gICAgICA+PC9kaXY+XG4gICAgPC9kaXY+XG5cbiAgICA8ZGl2IGNsYXNzPSd0ZXh0LXJpZ2h0IHRleHQtbXV0ZWQgZm9ybS10ZXh0IHRleHQtc21hbGwnPlxuICAgICAgPHNwYW4gY2xhc3M9J2Zsb2F0LWxlZnQnPnt7eGhyTWVzc2FnZX19PC9zcGFuPlxuICAgICAgPHNwYW5cbiAgICAgICAgY2xhc3M9J2JhZGdlJ1xuICAgICAgICA6Y2xhc3M9J1wiYmFkZ2UtXCIrY29sb3InXG4gICAgICAgIGRhdGEtdG9nZ2xlPSd0b29sdGlwJ1xuICAgICAgICBkYXRhLXBsYWNlbWVudD0ncmlnaHQnXG4gICAgICAgIDp0aXRsZT0ncmVhZHlTdGF0ZVRvb2x0aXAnXG4gICAgICAgID5cbiAgICAgICAgICB7e3hoci5yZWFkeVN0YXRlfX1cbiAgICAgICAgPC9zcGFuPlxuICAgICAgPHNwYW5cbiAgICAgICAgY2xhc3M9J2JhZGdlJ1xuICAgICAgICA6Y2xhc3M9J1wiYmFkZ2UtXCIrY29sb3InXG4gICAgICAgIGRhdGEtdG9nZ2xlPSd0b29sdGlwJ1xuICAgICAgICBkYXRhLXBsYWNlbWVudD0ncmlnaHQnXG4gICAgICAgIDp0aXRsZT0nc3RhdHVzVG9vbHRpcCdcbiAgICAgID5cbiAgICAgICAge3t4aHIuc3RhdHVzfX1cbiAgICAgIDwvc3Bhbj5cbiAgICAgIDxzcGFuXG4gICAgICAgIHYtaWY9J3RyYW5zZmVyQ29tcGxldGUnXG4gICAgICAgIEBjbGljaz0nZ29vZGJ5ZSdcbiAgICAgICAgY2xhc3M9J2JhZGdlIGJhZGdlLXNlY29uZGFyeSdcbiAgICAgICAgZGF0YS10b2dnbGU9J3Rvb2x0aXAnXG4gICAgICAgIGRhdGEtcGxhY2VtZW50PSdyaWdodCdcbiAgICAgICAgdGl0bGU9J0Rpc21pc3MgcHJvZ3Jlc3MgYmFyJ1xuICAgICAgPlxuICAgICAgICAmdGltZXM7XG4gICAgICA8L3NwYW4+XG4gICAgPC9kaXY+XG4gIDwvZGl2PlxuXG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXG5pbXBvcnQge2h0dHBTdGF0dXNDb2RlcywgaHR0cFJlYWR5U3RhdGVDb2Rlc30gZnJvbSAnLi9yZXNwb25zZXMuanMnO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIHByb3BzOiB7XG4gICAgbWV0aG9kOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnR0VUJyxcbiAgICAgIHZhbGlkYXRvcjogZnVuY3Rpb24odmFsdWUpIHtcbiAgICAgICAgcmV0dXJuIFsnR0VUJywgJ1BPU1QnLCAnUFVUJywgJ0RFTEVURSddLmluY2x1ZGVzKHZhbHVlKVxuICAgICAgfVxuICAgIH0sXG4gICAgdXJsOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICByZXF1aXJlZDogdHJ1ZVxuICAgIH0sXG4gICAgYXN5bmM6IHtcbiAgICAgIHR5cGU6IEJvb2xlYW4sXG4gICAgICBkZWZhdWx0OiB0cnVlXG4gICAgfSxcbiAgICBzdWNjZXNzOiB7XG4gICAgICB0eXBlOiBGdW5jdGlvbixcbiAgICAgIGRlZmF1bHQ6IGZ1bmN0aW9uKCkge1xuICAgICAgICBjb25zb2xlLmxvZyh0aGlzLnhoci5yZXNwb25zZSlcbiAgICAgIH1cbiAgICB9LFxuICAgIHJlYWR5c3RhdGVjaGFuZ2U6IHtcbiAgICAgIHR5cGU6IEZ1bmN0aW9uLFxuICAgICAgZGVmYXVsdDogZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgICAgZXZlbnQ7XG4gICAgICB9XG4gICAgfSxcbiAgICBhdXRvbWF0aWNDbG9zZVE6IHtcbiAgICAgIHR5cGU6IEJvb2xlYW4sXG4gICAgICBkZWZhdWx0OiBmYWxzZVxuICAgIH1cbiAgfSxcbiAgZGF0YTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIHhocjogbmV3IFhNTEh0dHBSZXF1ZXN0KCksXG4gICAgICBodHRwU3RhdHVzQ29kZXM6aHR0cFN0YXR1c0NvZGVzLFxuICAgICAgaHR0cFJlYWR5U3RhdGVDb2RlczpodHRwUmVhZHlTdGF0ZUNvZGVzLFxuICAgICAgY29sb3I6ICdwcmltYXJ5JyxcbiAgICAgIHBlcmNlbnQ6IDAsXG4gICAgICBodW1hblJlYWRhYmxlTGVhZDogJycsXG4gICAgICBodW1hblJlYWRhYmxlTWVzc2FnZTogJycsXG4gICAgICBodW1hblJlYWRhYmxlRW5kOiAnJyxcbiAgICAgIHhock1lc3NhZ2U6ICcnLFxuICAgICAgc2hvd1E6IHRydWUsXG4gICAgICBjb21wbGV0ZVE6IGZhbHNlXG4gICAgfVxuICB9LFxuICBjb21wdXRlZDoge1xuICAgIHJlYWR5U3RhdGVUb29sdGlwOiBmdW5jdGlvbigpIHtcbiAgICAgIGxldCBycyA9IHRoaXMueGhyLnJlYWR5U3RhdGU7XG4gICAgICBsZXQgcnNjID0gaHR0cFJlYWR5U3RhdGVDb2Rlc1tyc107XG4gICAgICByZXR1cm4gYFJlYWR5IHN0YXRlICR7cnN9OiAke3JzY31gO1xuICAgIH0sXG4gICAgc3RhdHVzVG9vbHRpcDogZnVuY3Rpb24oKSB7XG4gICAgICB2YXIgcyA9IHRoaXMueGhyLnN0YXR1cztcbiAgICAgIC8vIHMgPSBzID09IDAgPyAyMTggOiBzXG4gICAgICB2YXIgc2MgPSBodHRwU3RhdHVzQ29kZXNbc107XG4gICAgICByZXR1cm4gYFN0YXR1cyAke3N9OiAke3NjfWA7XG4gICAgfSxcbiAgICB0cmFuc2ZlckNvbXBsZXRlOiBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiB0aGlzLmNvbXBsZXRlUTtcbiAgICB9XG4gIH0sXG4gIG1ldGhvZHM6IHtcbiAgICBvcGVuOiBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMueGhyLm9wZW4odGhpcy5tZXRob2QsIHRoaXMudXJsLCB0aGlzLmFzeW5jKTtcbiAgICB9LFxuICAgIHNlbmQ6IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy54aHIuc2VuZCgpO1xuICAgIH0sXG4gICAgZ29vZGJ5ZTogZnVuY3Rpb24oKSB7XG4gICAgICB0aGlzLnNob3dRID0gZmFsc2U7XG4gICAgfVxuICB9LFxuXG4gIGNyZWF0ZWQ6IGZ1bmN0aW9uKCkge1xuICAgIHZhciB0aGF0ID0gdGhpc1xuICAgIHRoYXQub3BlbigpO1xuXG4gICAgdGhhdC54aHIuYWRkRXZlbnRMaXN0ZW5lcignZXJyb3InLCBmdW5jdGlvbihldmVudCkge1xuICAgICAgZXZlbnQ7XG4gICAgICB0aGF0LmNvbG9yID0gJ2Rhbmdlcic7XG4gICAgICB0aGF0Lnhock1lc3NhZ2UgPSAnQW4gZXJyb3IgaGFzIG9jY3VyZWQuJztcbiAgICB9KVxuXG4gICAgdGhpcy54aHIuYWRkRXZlbnRMaXN0ZW5lcigncHJvZ3Jlc3MnLCBmdW5jdGlvbihldmVudCkge1xuICAgICAgZXZlbnQ7XG4gICAgICBpZiAoZXZlbnQubGVuZ3RoQ29tcHV0YWJsZSkge1xuICAgICAgICB2YXIgcGVyY2VudENvbXBsZXRlID0gZXZlbnQubG9hZGVkIC8gZXZlbnQudG90YWwgKiAxMDA7XG4gICAgICAgIHRoYXQucGVyY2VudCA9IHBlcmNlbnRDb21wbGV0ZTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRoYXQucGVyY2VudCA9IDEwMDtcbiAgICAgICAgdGhhdC54aHJNZXNzYWdlID0gJ1VuYWJsZSB0byBjb21wdXRlIHByb2dyZXNzIGluZm9ybWF0aW9uIHNpbmNlIHRoZSB0b3RhbCBzaXplIGlzIHVua25vd24uJztcbiAgICAgIH1cbiAgICB9KVxuXG4gICAgdGhhdC54aHIuYWRkRXZlbnRMaXN0ZW5lcignYWJvcnQnLCBmdW5jdGlvbihldmVudCkge1xuICAgICAgZXZlbnQ7XG4gICAgICB0aGF0LmNvbG9yID0gJ2Rhbmdlcic7XG4gICAgICB0aGF0Lnhock1lc3NhZ2UgPSAnVGhlIHRyYW5zZmVyIGhhcyBiZWVuIGNhbmNlbGVkIGJ5IHRoZSB1c2VyLic7XG4gICAgfSk7XG5cbiAgICB0aGF0Lnhoci5hZGRFdmVudExpc3RlbmVyKCdsb2FkJywgZnVuY3Rpb24oZXZlbnQpIHtcbiAgICAgIGV2ZW50O1xuICAgICAgdGhhdC5jb2xvciA9ICdzdWNjZXNzJztcbiAgICAgIHRoYXQueGhyTWVzc2FnZSA9ICdUcmFuc2ZlciBjb21wbGV0ZS4nO1xuICAgICAgdGhhdC5jb21wbGV0ZVEgPSB0cnVlO1xuICAgICAgaWYgKHRoYXQuYXV0b21hdGljQ2xvc2VRKSB7IHRoYXQuc2hvd1EgPSBmYWxzZTsgfVxuICAgICAgdGhhdC5zdWNjZXNzKCk7XG4gICAgfSlcblxuICAgIHRoYXQueGhyLmFkZEV2ZW50TGlzdGVuZXIoJ3JlYWR5c3RhdGVjaGFuZ2UnLCBmdW5jdGlvbihldmVudCkge1xuICAgICAgZXZlbnQ7XG4gICAgICB0aGF0LnJlYWR5c3RhdGVjaGFuZ2UoZXZlbnQpO1xuICAgIH0pXG5cbiAgICB0aGF0LnNlbmQoKTtcbiAgfVxufVxuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQ+XG48L3N0eWxlPlxuIiwiPHRlbXBsYXRlPlxuICA8YnV0dG9uIHR5cGU9J2J1dHRvbicgbmFtZT0nYnV0dG9uJyB2LW9uOmNsaWNrPSdhbmltYXRlU2Nyb2xsVG8nPlxuICAgIDxpIGNsYXNzPSdmYSBmYS1jaGV2cm9uLWRvd24nPjwvaT5cbiAgPC9idXR0b24+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgcHJvcHM6IHtcbiAgICBvZmZzZXRTZWxlY3Rvcjoge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogJydcbiAgICB9LFxuICAgIGRlYnVnUToge1xuICAgICAgdHlwZTogQm9vbGVhbixcbiAgICAgIGRlZmF1bHQ6IGZhbHNlXG4gICAgfVxuICB9LFxuXG4gIGRhdGE6IGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4ge1xuICAgICAgb2Zmc2V0OiAwXG4gICAgfTtcbiAgfSxcbiAgY29tcHV0ZWQ6IHtcblxuICB9LFxuICBtZXRob2RzOiB7XG4gICAgc2V0T2Zmc2V0OiBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMub2Zmc2V0ID0gdGhpcy5vZmZzZXRTZWxlY3RvciA9PSB1bmRlZmluZWRcbiAgICAgICAgPyAwXG4gICAgICAgIDogJCh0aGlzLm9mZnNldFNlbGVjdG9yKS5vdXRlckhlaWdodCgpID09IHVuZGVmaW5lZFxuICAgICAgICAgID8gMFxuICAgICAgICAgIDogJCh0aGlzLm9mZnNldFNlbGVjdG9yKS5vdXRlckhlaWdodCgpO1xuICAgIH0sXG5cbiAgICBhbmltYXRlU2Nyb2xsVG86IGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5zZXRPZmZzZXQoKTtcbiAgICAgIHZhciB0aGF0ID0gdGhpcztcbiAgICAgIGlmICh0aGF0LmRlYnVnUSkgeyBjb25zb2xlLmdyb3VwKCdbc2VjdGlvbi1kb3duXScpOyB9XG4gICAgICBpZiAodGhhdC5kZWJ1Z1EpIHsgY29uc29sZS50YWJsZSh7Y3VycmVudE9mZnNldDogY3VyT2Zmc2V0LCBvZmZzZXRTZWxlY3Rvcjp0aGF0Lm9mZnNldFNlbGVjdG9yfSk7IH1cblxuICAgICAgdmFyIHNlY3Rpb25zID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnc2VjdGlvbjpub3QoW2Rpc3BsYXk9XCJub25lXCJdKScpO1xuICAgICAgdmFyIGN1cnJlbnQgPSB1bmRlZmluZWQ7XG4gICAgICB2YXIgY3VyT2Zmc2V0ID0gdGhpcy5vZmZzZXQ7XG5cbiAgICAgIGlmICh0aGF0LmRlYnVnUSkgeyBjb25zb2xlLmdyb3VwKCdlYWNoIHNlY3Rpb24nKTsgfVxuXG4gICAgICBzZWN0aW9ucy5mb3JFYWNoKGZ1bmN0aW9uKHMpe1xuXG4gICAgICAgIHZhciB3aW5TY3JvbGwgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgKyAyO1xuICAgICAgICB2YXIgY3VyU2Nyb2xsID0gJChzKS5vZmZzZXQoKS50b3AgLSBjdXJPZmZzZXQ7XG5cbiAgICAgICAgaWYgKHRoYXQuZGVidWdRKSB7XG4gICAgICAgICAgY29uc29sZS50YWJsZSh7XG4gICAgICAgICAgICB3aW5kb3dQb3NpdGlvbjogd2luU2Nyb2xsLFxuICAgICAgICAgICAgc2VjdGlvblNjcm9sbFBvc2l0aW9uOiBjdXJTY3JvbGwsXG4gICAgICAgICAgICBzZWN0aW9uOiBzXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIHdpblNjcm9sbCA8IGN1clNjcm9sbCAmJiBjdXJyZW50ID09IHVuZGVmaW5lZClcbiAgICAgICAgeyBjdXJyZW50ID0gczsgfVxuICAgICAgfSlcbiAgICAgIGlmICh0aGF0LmRlYnVnUSkgeyBjb25zb2xlLmdyb3VwRW5kKCk7IH1cbiAgICAgIGlmICh0aGF0LmRlYnVnUSkgeyBjb25zb2xlLmdyb3VwRW5kKCk7IH1cblxuICAgICAgaWYgKGN1cnJlbnQgIT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICQoJ2h0bWwsIGJvZHknKS5hbmltYXRlKHtcbiAgICAgICAgICBzY3JvbGxUb3A6ICQoY3VycmVudCkub2Zmc2V0KCkudG9wIC0gY3VyT2Zmc2V0XG4gICAgICAgIH0sIDEwMDAsIGZ1bmN0aW9uKCkge30pO1xuICAgICAgfVxuICAgIH1cblxuICB9XG59XG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cbmJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6IHJhZGlhbC1ncmFkaWVudChyZ2IoMCwgMTk4LCAyNTUpLCByZ2IoMCwgMTE0LCAyNTUpKTtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGJvcmRlcjogdHJhbnNwYXJlbnQgMHB4IHNvbGlkO1xuXG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMHB4O1xuXG4gIHdpZHRoOjMycHg7XG4gIGhlaWdodDozMnB4O1xuXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZTtcbn1cblxuaSB7IGZvbnQtc2l6ZTogMTZweDsgfVxuXG5idXR0b246Zm9jdXMge1xuICBvdXRsaW5lOiB0cmFuc3BhcmVudCAwcHggc29saWQ7XG59XG5idXR0b246aG92ZXIge1xuICBjb2xvcjp3aGl0ZTtcbiAgb3BhY2l0eTogMC43O1xufVxuXG5idXR0b246aG92ZXIgaSB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgLW1vei1hbmltYXRpb246IGJvdW5jZSAwLjVzIGluZmluaXRlIGxpbmVhcjtcbiAgLW8tYW5pbWF0aW9uOiBib3VuY2UgMC41cyBpbmZpbml0ZSBsaW5lYXI7XG4gIC13ZWJraXQtYW5pbWF0aW9uOiBib3VuY2UgMC41cyBpbmZpbml0ZSBsaW5lYXI7XG4gIGFuaW1hdGlvbjogYm91bmNlIDAuNXMgaW5maW5pdGUgbGluZWFyO1xuXG59XG5cbkAtd2Via2l0LWtleWZyYW1lcyBib3VuY2Uge1xuICAgIDAlIHsgdG9wOiAwOyB9XG4gICAgNTAlIHsgdG9wOiAwLjJlbTsgfVxuICAgIDEwMCUgeyB0b3A6IDA7IH1cbn1cbkAtbW96LWtleWZyYW1lcyBib3VuY2Uge1xuICAgIDAlIHsgdG9wOiAwOyB9XG4gICAgNTAlIHsgdG9wOiAwLjJlbTsgfVxuICAgIDEwMCUgeyB0b3A6IDA7IH1cbn1cbkAtby1rZXlmcmFtZXMgYm91bmNlIHtcbiAgICAwJSB7IHRvcDogMDsgfVxuICAgIDUwJSB7IHRvcDogMC4yZW07IH1cbiAgICAxMDAlIHsgdG9wOiAwOyB9XG59XG5ALW1zLWtleWZyYW1lcyBib3VuY2Uge1xuICAgIDAlIHsgdG9wOiAwOyB9XG4gICAgNTAlIHsgdG9wOiAwLjJlbTsgfVxuICAgIDEwMCUgeyB0b3A6IDA7IH1cbn1cbkBrZXlmcmFtZXMgYm91bmNlIHtcbiAgICAwJSB7IHRvcDogMDsgfVxuICAgIDUwJSB7IHRvcDogMC4yZW07IH1cbiAgICAxMDAlIHsgdG9wOiAwOyB9XG59XG48L3N0eWxlPlxuIiwiPHRlbXBsYXRlPlxuICA8YnV0dG9uIHR5cGU9J2J1dHRvbicgbmFtZT0nYnV0dG9uJyB2LW9uOmNsaWNrPSdhbmltYXRlU2Nyb2xsVG8nPlxuICAgIDxpIGNsYXNzPSdmYSBmYS1jaGV2cm9uLXVwJz48L2k+XG4gIDwvYnV0dG9uPlxuPC90ZW1wbGF0ZT5cblxuPHNjcmlwdD5cblxuXG5leHBvcnQgZGVmYXVsdCB7XG4gIHByb3BzOiB7XG4gICAgb2Zmc2V0U2VsZWN0b3I6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6ICcnXG4gICAgfSxcbiAgICBkZWJ1Z1E6IHtcbiAgICAgIHR5cGU6IEJvb2xlYW4sXG4gICAgICBkZWZhdWx0OiBmYWxzZVxuICAgIH1cbiAgfSxcblxuICBkYXRhOiBmdW5jdGlvbiAoKSB7XG4gICAgcmV0dXJuIHsgb2Zmc2V0OiAwIH1cbiAgfSxcblxuICBjb21wdXRlZDoge1xuXG4gIH0sXG5cbiAgbWV0aG9kczoge1xuICAgIHNldE9mZnNldDogZnVuY3Rpb24oKSB7XG4gICAgICB0aGlzLm9mZnNldCA9IHRoaXMub2Zmc2V0U2VsZWN0b3IgPT0gdW5kZWZpbmVkXG4gICAgICAgID8gMFxuICAgICAgICA6ICQodGhpcy5vZmZzZXRTZWxlY3Rvcikub3V0ZXJIZWlnaHQoKSA9PSB1bmRlZmluZWRcbiAgICAgICAgICA/IDBcbiAgICAgICAgICA6ICQodGhpcy5vZmZzZXRTZWxlY3Rvcikub3V0ZXJIZWlnaHQoKTtcbiAgICB9LFxuXG4gICAgYW5pbWF0ZVNjcm9sbFRvOiBmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMuc2V0T2Zmc2V0KCk7XG4gICAgICB2YXIgdGhhdCA9IHRoaXM7XG4gICAgICBpZiAodGhhdC5kZWJ1Z1EpIHsgY29uc29sZS5ncm91cCgnW3NlY3Rpb24tdXBdJyk7IH1cbiAgICAgIGlmICh0aGF0LmRlYnVnUSkgeyBjb25zb2xlLnRhYmxlKHtjdXJyZW50T2Zmc2V0OiBjdXJPZmZzZXQsIG9mZnNldFNlbGVjdG9yOnRoYXQub2Zmc2V0U2VsZWN0b3J9KTsgfVxuXG5cbiAgICAgIHZhciBzZWN0aW9ucyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJ3NlY3Rpb246bm90KFtkaXNwbGF5PVwibm9uZVwiXSknKTtcbiAgICAgIHZhciBjdXJyZW50ID0gdW5kZWZpbmVkO1xuICAgICAgdmFyIGN1ck9mZnNldCA9IHRoaXMub2Zmc2V0O1xuXG5cbiAgICAgIGlmICh0aGF0LmRlYnVnUSkgeyBjb25zb2xlLmdyb3VwKCdlYWNoIHNlY3Rpb24nKTsgfVxuICAgICAgc2VjdGlvbnMuZm9yRWFjaChmdW5jdGlvbihzKXtcbiAgICAgICAgLy8gd2luZG93IHBvc2l0aW9uXG4gICAgICAgIHZhciB3aW5TY3JvbGwgPSAkKHdpbmRvdykuc2Nyb2xsVG9wKCkgLSAwLjI7XG4gICAgICAgIC8vIHNlY3Rpb24gcG9zaXRpb25cbiAgICAgICAgdmFyIGN1clNjcm9sbCA9ICQocykub2Zmc2V0KCkudG9wIC0gY3VyT2Zmc2V0O1xuXG5cbiAgICAgICAgaWYgKHRoYXQuZGVidWdRKSB7XG4gICAgICAgICAgY29uc29sZS50YWJsZSh7XG4gICAgICAgICAgICB3aW5kb3dQb3NpdGlvbjogd2luU2Nyb2xsLFxuICAgICAgICAgICAgc2VjdGlvblNjcm9sbFBvc2l0aW9uOiBjdXJTY3JvbGwsXG4gICAgICAgICAgICBzZWN0aW9uOiBzXG4gICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoIHdpblNjcm9sbCA+IGN1clNjcm9sbCkgeyBjdXJyZW50ID0gczsgfVxuICAgICAgfSlcbiAgICAgIGlmICh0aGF0LmRlYnVnUSkgeyBjb25zb2xlLmdyb3VwRW5kKCk7IH1cbiAgICAgIGlmICh0aGF0LmRlYnVnUSkgeyBjb25zb2xlLmdyb3VwRW5kKCk7IH1cblxuXG4gICAgICAvLyBTY3JvbGwgdG8gc2VjdGlvblxuICAgICAgaWYgKGN1cnJlbnQgIT0gdW5kZWZpbmVkKSB7XG4gICAgICAgICQoJ2h0bWwsIGJvZHknKS5hbmltYXRlKHtcbiAgICAgICAgICBzY3JvbGxUb3A6ICQoY3VycmVudCkub2Zmc2V0KCkudG9wIC0gY3VyT2Zmc2V0XG4gICAgICAgIH0sIDEwMDAsIGZ1bmN0aW9uKCkge30pO1xuICAgICAgfVxuXG4gICAgfVxuICB9XG59XG48L3NjcmlwdD5cblxuPHN0eWxlIHNjb3BlZD5cbmJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6IHJhZGlhbC1ncmFkaWVudChyZ2IoMCwgMTk4LCAyNTUpLCByZ2IoMCwgMTE0LCAyNTUpKTtcbiAgYmFja2dyb3VuZC1jb2xvcjogdHJhbnNwYXJlbnQ7XG4gIGJvcmRlcjogdHJhbnNwYXJlbnQgMHB4IHNvbGlkO1xuXG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZzogMHB4O1xuXG4gIHdpZHRoOjMycHg7XG4gIGhlaWdodDozMnB4O1xuXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbiAgdHJhbnNpdGlvbjogYWxsIDAuNXMgZWFzZTtcblxufVxuXG5pIHsgZm9udC1zaXplOiAxNnB4OyB9XG5cbmJ1dHRvbjpmb2N1cyB7XG4gIG91dGxpbmU6IHRyYW5zcGFyZW50IDBweCBzb2xpZDtcbn1cblxuYnV0dG9uOmhvdmVyIHtcbiAgY29sb3I6d2hpdGU7XG4gIG9wYWNpdHk6IDAuNztcbn1cblxuYnV0dG9uOmhvdmVyIGkge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIC1tb3otYW5pbWF0aW9uOiBib3VuY2UgMC41cyBpbmZpbml0ZSBsaW5lYXI7XG4gIC1vLWFuaW1hdGlvbjogYm91bmNlIDAuNXMgaW5maW5pdGUgbGluZWFyO1xuICAtd2Via2l0LWFuaW1hdGlvbjogYm91bmNlIDAuNXMgaW5maW5pdGUgbGluZWFyO1xuICBhbmltYXRpb246IGJvdW5jZSAwLjVzIGluZmluaXRlIGxpbmVhcjtcblxufVxuXG5ALXdlYmtpdC1rZXlmcmFtZXMgYm91bmNlIHtcbiAgICAwJSB7IHRvcDogMDsgfVxuICAgIDUwJSB7IHRvcDogLTAuMmVtOyB9XG4gICAgMTAwJSB7IHRvcDogMDsgfVxufVxuQC1tb3ota2V5ZnJhbWVzIGJvdW5jZSB7XG4gICAgMCUgeyB0b3A6IDA7IH1cbiAgICA1MCUgeyB0b3A6IC0wLjJlbTsgfVxuICAgIDEwMCUgeyB0b3A6IDA7IH1cbn1cbkAtby1rZXlmcmFtZXMgYm91bmNlIHtcbiAgICAwJSB7IHRvcDogMDsgfVxuICAgIDUwJSB7IHRvcDogLTAuMmVtOyB9XG4gICAgMTAwJSB7IHRvcDogMDsgfVxufVxuQC1tcy1rZXlmcmFtZXMgYm91bmNlIHtcbiAgICAwJSB7IHRvcDogMDsgfVxuICAgIDUwJSB7IHRvcDogLTAuMmVtOyB9XG4gICAgMTAwJSB7IHRvcDogMDsgfVxufVxuQGtleWZyYW1lcyBib3VuY2Uge1xuICAgIDAlIHsgdG9wOiAwOyB9XG4gICAgNTAlIHsgdG9wOiAtMC4yZW07IH1cbiAgICAxMDAlIHsgdG9wOiAwOyB9XG59XG5cblxuPC9zdHlsZT5cbiIsIjx0ZW1wbGF0ZT5cbiAgPGRpdj5cbiAgICA8c2VjdGlvbi1uYXZpZ2F0aW9uLWFycm93LXVwIDpvZmZzZXRTZWxlY3Rvcj1cIm9mZnNldFNlbGVjdG9yXCIgOmRlYnVnUT1cImRlYnVnUVwiLz5cbiAgICA8c2VjdGlvbi1uYXZpZ2F0aW9uLWFycm93LWRvd24gOm9mZnNldFNlbGVjdG9yPVwib2Zmc2V0U2VsZWN0b3JcIiA6ZGVidWdRPVwiZGVidWdRXCIvPlxuICA8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5pbXBvcnQgU2VjdGlvbk5hdmlnYXRpb25BcnJvd0Rvd24gZnJvbSAnLi4vU2VjdGlvbk5hdmlnYXRpb25BcnJvd0Rvd24vU2VjdGlvbk5hdmlnYXRpb25BcnJvd0Rvd24udnVlJztcbmltcG9ydCBTZWN0aW9uTmF2aWdhdGlvbkFycm93VXAgZnJvbSAnLi4vU2VjdGlvbk5hdmlnYXRpb25BcnJvd1VwL1NlY3Rpb25OYXZpZ2F0aW9uQXJyb3dVcC52dWUnO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIGNvbXBvbmVudHM6IHsgU2VjdGlvbk5hdmlnYXRpb25BcnJvd0Rvd24sIFNlY3Rpb25OYXZpZ2F0aW9uQXJyb3dVcCB9LFxuICBwcm9wczoge1xuICAgIG9mZnNldFNlbGVjdG9yOiB7XG4gICAgICB0eXBlOlN0cmluZyxcbiAgICAgIGRlZmF1bHQ6IHVuZGVmaW5lZFxuICAgIH0sXG4gICAgZGVidWdROiB7XG4gICAgICB0eXBlOiBCb29sZWFuLFxuICAgICAgZGVmYXVsdDogZmFsc2VcbiAgICB9XG4gIH1cbn1cbjwvc2NyaXB0PlxuXG48c3R5bGUgc2NvcGVkPlxuZGl2IHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICB0b3A6NTAlO1xuICBtYXJnaW4tbGVmdDogMTBweDtcbn1cblxuZGl2ID4gYnV0dG9uIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbi1ib3R0b206IDEwcHggIWltcG9ydGFudDtcbn1cbjwvc3R5bGU+XG4iLCJsZXQgc29ydFRva2VuT2JqZWN0ID0gZnVuY3Rpb24ob2JqZWN0KSB7XG4gIHJldHVybiBPYmplY3QuZW50cmllcyhvYmplY3QpLnNvcnQoZnVuY3Rpb24oYSwgYikge1xuICAgIGxldCBrYSA9IGFbMF07XG4gICAgbGV0IGtiID0gYlswXTtcbiAgICByZXR1cm4gKGthID4ga2IgfHwga2IubGVuZ3RoIC0ga2EubGVuZ3RoKTtcbiAgfSkucmVkdWNlKGZ1bmN0aW9uKG8sIFtrLCB2XSkge1xuICAgIG9ba10gPSB2O1xuICAgIHJldHVybiBvO1xuICB9LCB7fSk7XG59O1xuXG5sZXQgY29uZGl0aW9uYWxSZW5kZXJzID0ge1xuICBlcTogJz0nLFxuICBuZXE6ICfiiaAnLFxuICBndDogJz4nLFxuICBsdDogJzwnLFxuICBndGU6ICfiiaUnLFxuICBsdGU6ICfiiaQnLFxuICBpbmNsdWRlczogJ+KKgycsXG59O1xuXG5sZXQgbG9naWNSZW5kZXJzID0ge1xuICBhbmQ6ICfii4AnLFxuICBvcjogJ+KLgSdcbn07XG5cbmxldCBjb25kaXRpb25hbFRva2VucyA9IHtcbiAgZXE6ICdlcScsXG4gIGVxdWFsOiAnZXEnLFxuICAnPSc6ICdlcScsXG4gICchPSc6ICduZXEnLFxuICAn4omgJzogJ25lcScsXG4gICc+JzogJ2d0JyxcbiAgJ+KJpSc6ICdndGUnLFxuICAnPj0nOiAnZ3RlJyxcbiAgJzwnOiAnbHQnLFxuICAn4omkJzogJ2x0ZScsXG4gICc8PSc6ICdsdGUnLFxuICAnaXMnOiAnZXEnLFxuICAnaXMgZXF1YWwgdG8nOiAnZXEnLFxuICAnaXMgbm90JzogJ25lcScsXG4gIG5lcTogJ25lcScsXG4gICdub3QgZXF1YWwgdG8nOiAnbmVxJyxcbiAgJ25vdCBlcSB0byc6ICduZXEnLFxuICAnaXMgbm90IGVxdWFsIHRvJzogJ25lcScsXG4gIGd0OiAnZ3QnLFxuICAnZ3JlYXRlciB0aGFuJzogJ2d0JyxcbiAgJ2xlc3MgdGhhbic6ICdsdCcsXG4gIGx0OiAnbHQnLFxuICAnbGVzcyB0aGFuIG9yIGVxdWFsIHRvJzogJ2x0ZScsXG4gICdncmVhdGVyIHRoYW4gb3IgZXF1YWwgdG8nOiAnZ3RlJyxcbiAgJ21lbWJlciBvZic6ICdpbmNsdWRlcycsXG4gIHN1YnN0cmluZzogJ2luY2x1ZGVzJyxcbiAgY29udGFpbnM6ICdpbmNsdWRlcycsXG4gIGluY2x1ZGVzOiAnaW5jbHVkZXMnXG59O1xuXG5cbmxldCBmdW5jdGlvblRva2VucyA9IHtcbiAgbWF4OiAnbWF4JyxcbiAgbWF4aW11bTogJ21heCcsXG4gIG1pbmltdW06ICdtaW4nLFxuICBtaW46ICdtaW4nLFxuICBsZW5ndGg6ICdsZW5ndGgnLFxuICBsZW46ICdsZW5ndGgnLFxuICBtZWFuOiAnbWVhbicsXG4gIG1lZGlhbjogJ21lZGlhbicsXG4gIGF2ZXJhZ2U6ICdtZWFuJyxcbiAgaWRlbnRpdHk6ICdpZGVudGl0eSdcbn07XG5cbmxldCBsb2dpY1Rva2VucyA9IHtcbiAgYW5kOiAnYW5kJyxcbiAgb3I6ICdvcidcbn07XG5cblxuZnVuY3Rpb25Ub2tlbnMgPSBzb3J0VG9rZW5PYmplY3QoZnVuY3Rpb25Ub2tlbnMpO1xubG9naWNUb2tlbnMgPSBzb3J0VG9rZW5PYmplY3QobG9naWNUb2tlbnMpO1xuY29uZGl0aW9uYWxUb2tlbnMgPSBzb3J0VG9rZW5PYmplY3QoY29uZGl0aW9uYWxUb2tlbnMpO1xuXG5sZXQgZXh0cmFjdFRva2VucyA9IGZ1bmN0aW9uKHRleHQsIHRva2VucywgZmFsbGJhY2sgPSB1bmRlZmluZWQsIHNjb3BlID0gdW5kZWZpbmVkLCBkcm9wVW50aWxNYXRjaFEgPSBmYWxzZSkge1xuICBsZXQgdG9rZW4gPSBmYWxsYmFjaztcbiAgbGV0IHN0ciA9IHNjb3BlID09PSB1bmRlZmluZWQgPyB0ZXh0IDogdGV4dC5zbGljZSgwLCBzY29wZSk7XG4gIGZvciAodmFyIHQgaW4gdG9rZW5zKSB7XG4gICAgaWYgKHN0ci5pbmNsdWRlcyh0KSkge1xuICAgICAgdG9rZW4gPSB0b2tlbnNbdF07XG4gICAgICBpZiAoZHJvcFVudGlsTWF0Y2hRKSB7XG4gICAgICAgIHRleHQgPSB0ZXh0LnNsaWNlKHN0ci5pbmRleE9mKHQpICsgdC5sZW5ndGgsIHN0ci5sZW5ndGgpO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGV4dCA9IHRleHQucmVwbGFjZSh0LCAnJyk7XG4gICAgICB9XG4gICAgICBicmVhaztcbiAgICB9XG4gIH1cbiAgcmV0dXJuIFt0b2tlbiwgdGV4dF07XG59O1xuXG5leHBvcnQge1xuICBmdW5jdGlvblRva2VucyxcbiAgbG9naWNUb2tlbnMsXG4gIGV4dHJhY3RUb2tlbnMsXG4gIHNvcnRUb2tlbk9iamVjdCxcbiAgY29uZGl0aW9uYWxUb2tlbnMsXG4gIGNvbmRpdGlvbmFsUmVuZGVycyxcbiAgbG9naWNSZW5kZXJzXG59O1xuIiwibGV0IGZpZWxkcyA9IGZ1bmN0aW9uKHJlY29yZHMpIHtcbiAgbGV0IHJlY29yZElkcyA9IGlkcyhyZWNvcmRzKTtcbiAgdmFyIGFsbFJlY29yZEZpZWxkcyA9IFtdLmNvbmNhdChcbiAgICAuLi5yZWNvcmRJZHMubWFwKGZ1bmN0aW9uKHJlY29yZElkKSB7XG4gICAgICByZXR1cm4gT2JqZWN0LmtleXMocmVjb3Jkc1tyZWNvcmRJZF0pO1xuICAgIH0pXG4gICk7XG4gIHJldHVybiBbLi4ubmV3IFNldChhbGxSZWNvcmRGaWVsZHMpXTtcbn07XG5cbmxldCBmaWVsZFRva2VucyA9IGZ1bmN0aW9uKHJlY29yZHMpIHtcbiAgbGV0IHJlY29yZEZpZWxkcyA9IGZpZWxkcyhyZWNvcmRzKTtcbiAgbGV0IHRva2VucyA9IHt9O1xuICByZWNvcmRGaWVsZHMubWFwKGZ1bmN0aW9uKGZpZWxkKSB7XG4gICAgdG9rZW5zW2ZpZWxkXSA9IGZpZWxkO1xuICB9KTtcbiAgcmV0dXJuIHRva2Vucztcbn07XG5cbmxldCBpZHMgPSBmdW5jdGlvbihyZWNvcmRzKSB7XG4gIHJldHVybiBPYmplY3Qua2V5cyhyZWNvcmRzKTtcbn07XG5cbmxldCBnaVJlZ2V4ID0gZnVuY3Rpb24odGV4dCwgdG9rZW5zKSB7XG4gIGxldCByZWdleCA9IG5ldyBSZWdFeHAodGV4dCwgJ2dpJyk7XG5cbiAgbGV0IHN1Z2dlc3Rpb25zID0ge307XG5cbiAgdG9rZW5zLmZpbHRlcihmdW5jdGlvbih0b2tlbikge1xuICAgIHZhciBtYXRjaCA9IHRva2VuLm1hdGNoKHJlZ2V4KTtcbiAgICAvLyBjb25zb2xlLmxvZyhtYXRjaCwgdG9rZW4pO1xuXG4gICAgbGV0IGZvdW5kID0gIShtYXRjaCA9PSBudWxsIHx8IG1hdGNoLmpvaW4oJycpID09PSAnJyk7XG5cbiAgICBpZiAoZm91bmQpIHtcbiAgICAgIHN1Z2dlc3Rpb25zW3Rva2VuXSA9IG1hdGNoWzBdO1xuICAgIH1cbiAgICByZXR1cm4gZm91bmQ7XG4gIH0pO1xuXG4gIHN1Z2dlc3Rpb25zID0gT2JqZWN0LmtleXMoc3VnZ2VzdGlvbnMpLnNvcnQoZnVuY3Rpb24oYSwgYikge1xuICAgIHJldHVybiB0ZXh0LmluZGV4T2Yoc3VnZ2VzdGlvbnNbYl0pIC0gdGV4dC5pbmRleE9mKHN1Z2dlc3Rpb25zW2FdKTtcbiAgfSk7XG4gIHJldHVybiBzdWdnZXN0aW9ucztcbn07XG5cbmxldCBncm91cEJ5Q29uanVuY3RpdmVOb3JtYWxGb3JtID0gZnVuY3Rpb24oZmlsdGVycykge1xuICB2YXIgZ3JvdXBwZWQgPSBbXTtcblxuICBmb3IgKHZhciBpID0gMDsgaSA8IGZpbHRlcnMubGVuZ3RoOyBpKyspIHtcbiAgICB2YXIgbG9naWMgPSBmaWx0ZXJzW2ldLmxvZ2ljO1xuICAgIGlmIChsb2dpYyA9PSAnYW5kJykge1xuICAgICAgZ3JvdXBwZWQucHVzaChbZmlsdGVyc1tpXV0pO1xuICAgIH0gZWxzZSB7XG4gICAgICB2YXIgbGlzdCA9IGdyb3VwcGVkW2dyb3VwcGVkLmxlbmd0aCAtIDFdO1xuICAgICAgaWYgKGxpc3QgPT0gdW5kZWZpbmVkKSB7XG4gICAgICAgIGZpbHRlcnNbaV0ubG9naWMgPSAnYW5kJztcbiAgICAgICAgZ3JvdXBwZWQucHVzaChbZmlsdGVyc1tpXV0pO1xuICAgICAgfSBlbHNlIHtcbiAgICAgICAgZ3JvdXBwZWRbZ3JvdXBwZWQubGVuZ3RoIC0gMV0ucHVzaChmaWx0ZXJzW2ldKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBjb25zb2xlLmxvZyhncm91cHBlZCk7XG4gIH1cblxuICByZXR1cm4gZ3JvdXBwZWQ7XG59O1xuXG5sZXQgc2NydWJJbnB1dFRleHQgPSBmdW5jdGlvbih0ZXh0KSB7XG4gIGlmICh0ZXh0WzBdID09PSAnICcpIHtcbiAgICB0ZXh0ID0gdGV4dC5zbGljZSgxLCB0ZXh0Lmxlbmd0aCk7XG4gIH1cbiAgaWYgKHRleHRbdGV4dC5sZW5ndGggLSAxXSA9PT0gJyAnKSB7XG4gICAgdGV4dCA9IHRleHQuc2xpY2UoMSwgdGV4dC5sZW5ndGggLSAxKTtcbiAgfVxuICByZXR1cm4gdGV4dDtcbn07XG5leHBvcnQge1xuICBpZHMsXG4gIGZpZWxkVG9rZW5zLFxuICBmaWVsZHMsXG4gIGdyb3VwQnlDb25qdW5jdGl2ZU5vcm1hbEZvcm0sXG4gIHNjcnViSW5wdXRUZXh0LFxuICBnaVJlZ2V4XG59O1xuIiwiPHRlbXBsYXRlPlxuICA8ZGl2IGNsYXNzPVwiZmlsdGVyXCIgQGNsaWNrPSdjbGlja2VkJz5cbiAgICA8c3BhblxuICAgICAgdi1pZj1cImluZGV4XCJcbiAgICAgIGNsYXNzPSdsb2dpYydcbiAgICAgIDpjbGFzcz0nZmlsdGVyLmxvZ2ljJ1xuICAgICAgPlxuICAgICAge3sgbG9naWNSZW5kZXJzW2ZpbHRlci5sb2dpY10gfX1cbiAgICA8L3NwYW4+XG5cblxuICAgIDxzcGFuXG4gICAgICB2LWlmPVwiZmlsdGVyLmZ1bmN0aW9uIT09J2lkZW50aXR5J1wiXG4gICAgICBjbGFzcz0nZnVuY3Rpb24nXG4gICAgPlxuICAgICAge3tmaWx0ZXIuZnVuY3Rpb259fVxuICAgIDwvc3Bhbj5cblxuXG4gICAgPHNwYW4gY2xhc3M9J3Byb3BlcnR5Jz57e2ZpbHRlci5wcm9wZXJ0eX19PC9zcGFuPlxuXG5cbiAgICA8IS0tIGNvbmRpdGlvbmFsIC0tPlxuICAgIDxzcGFuIGNsYXNzPSdjb25kaXRpb25hbCc+XG4gICAgICB7eyBjb25kaXRpb25hbFJlbmRlcnNbZmlsdGVyLmNvbmRpdGlvbmFsXSB9fVxuICAgIDwvc3Bhbj5cblxuICAgIDwhLS0gdXNlciBpbnB1dCAtLT5cbiAgICA8c3BhbiBjbGFzcz0naW5wdXQnPlxuICAgICAge3tmaWx0ZXIuaW5wdXR9fVxuICAgIDwvc3Bhbj5cbiAgPC9kaXY+XG48L3RlbXBsYXRlPlxuXG48c2NyaXB0PlxuaW1wb3J0IHtsb2dpY1JlbmRlcnMsIGNvbmRpdGlvbmFsUmVuZGVyc30gZnJvbSAnLi4vSW5wdXRBbHBoYS90b2tlbnMuanMnO1xuXG5leHBvcnQgZGVmYXVsdCB7XG4gIHByb3BzOiBbXCJmaWx0ZXJcIiwgXCJpbmRleFwiXSxcbiAgZGF0YTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGxvZ2ljUmVuZGVyczogbG9naWNSZW5kZXJzLFxuICAgICAgY29uZGl0aW9uYWxSZW5kZXJzOiBjb25kaXRpb25hbFJlbmRlcnNcbiAgICB9O1xuICB9LFxuICBtZXRob2RzOiB7XG4gICAgY2xpY2tlZDogZnVuY3Rpb24oKSB7XG4gICAgICB0aGlzLiRlbWl0KFwiZmlsdGVyQ2xpY2tcIik7XG4gICAgfVxuICB9XG59O1xuPC9zY3JpcHQ+XG5cbjxzdHlsZSBzY29wZWQgc3JjPScuLi9JbnB1dEFscGhhQ29uanVuY3RpdmVOb3JtYWxGb3JtL3N0eWxlcy5jc3MnPlxuXG48L3N0eWxlPlxuIiwiPHRlbXBsYXRlPlxuICA8ZGl2PlxuICAgIDxkaXZcbiAgICAgIGNsYXNzPSdjbmYnXG4gICAgICB2LWZvcj0nKGNuZiwgaW5kZXgpIGluIGNvbmp1bmN0aXZlTm9ybWFsRm9ybSdcbiAgICAgIDprZXk9J2NuZi5pZCdcbiAgICA+XG4gICAgICA8c3BhbiB2LWlmPSdpbmRleCcgY2xhc3M9J2xvZ2ljIGFuZCc+e3tsb2dpY1JlbmRlcnNbJ2FuZCddfX08L3NwYW4+XG4gICAgICAoXG4gICAgICAgIDxJbnB1dEFscGhhRmlsdGVyIHYtZm9yPScoZmlsdGVyLCBqbmRleCkgaW4gY25mJ1xuICAgICAgICA6a2V5PSdmaWx0ZXIuaWQnXG4gICAgICAgIDpmaWx0ZXI9J2ZpbHRlcidcbiAgICAgICAgOmluZGV4PSdqbmRleCdcbiAgICAgICAgQGZpbHRlckNsaWNrPSdmaWx0ZXJDbGljayhpbmRleCwgam5kZXgpJ1xuICAgICAgICAvPlxuICAgICAgKVxuICAgIDwvZGl2PlxuICA8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5pbXBvcnQgSW5wdXRBbHBoYUZpbHRlciBmcm9tICcuLi9JbnB1dEFscGhhRmlsdGVyL0lucHV0QWxwaGFGaWx0ZXIudnVlJztcbmltcG9ydCB7Z3JvdXBCeUNvbmp1bmN0aXZlTm9ybWFsRm9ybX0gZnJvbSAnLi4vSW5wdXRBbHBoYS9oZWxwZXJzLmpzJztcbmltcG9ydCB7bG9naWNSZW5kZXJzfSBmcm9tICcuLi9JbnB1dEFscGhhL3Rva2Vucy5qcyc7XG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgY29tcG9uZW50czogeyBJbnB1dEFscGhhRmlsdGVyIH0sXG4gIHByb3BzOiBbJ2ZpbHRlcnMnXSxcbiAgZGF0YTogZnVuY3Rpb24oKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgIGxvZ2ljUmVuZGVyczogbG9naWNSZW5kZXJzXG4gICAgfTtcbiAgfSxcbiAgY29tcHV0ZWQ6IHtcbiAgICBjb25qdW5jdGl2ZU5vcm1hbEZvcm06IGZ1bmN0aW9uKCkge1xuICAgICAgcmV0dXJuIGdyb3VwQnlDb25qdW5jdGl2ZU5vcm1hbEZvcm0odGhpcy5maWx0ZXJzKVxuICAgIH1cbiAgfSxcbiAgbWV0aG9kczoge1xuICAgIGZpbHRlckNsaWNrOiBmdW5jdGlvbihpbmRleCwgam5kZXgpIHtcbiAgICAgIHZhciB0b3RhbCA9IDA7XG4gICAgICBsZXQgdGhhdCA9IHRoaXM7XG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IGluZGV4OyBpKyspIHtcbiAgICAgICAgdG90YWwgKz0gdGhhdC5jb25qdW5jdGl2ZU5vcm1hbEZvcm1baV0ubGVuZ3RoO1xuICAgICAgfVxuICAgICAgdG90YWwgKz0gam5kZXg7XG4gICAgICAvLyBjb25zb2xlLmxvZygndG90YWwnLCB0b3RhbClcbiAgICAgIHRoYXQuJGVtaXQoJ2ZpbHRlckNsaWNrJywgdG90YWwpO1xuICAgIH1cbiAgfVxufTtcbjwvc2NyaXB0PlxuXG5cbjxzdHlsZSBzY29wZWQgc3JjPScuL3N0eWxlcy5jc3MnPlxuXG48L3N0eWxlPlxuIiwiPHRlbXBsYXRlPlxuICA8ZGl2IGNsYXNzPSdhbHBoYSc+XG4gICAgPGlucHV0XG4gICAgICB0eXBlPSd0ZXh0J1xuICAgICAgdi1tb2RlbD0naW5wdXQnXG4gICAgICBAa2V5dXAuZW50ZXI9J3Byb2Nlc3NJbnB1dCgkZXZlbnQpJ1xuICAgICAgQGlucHV0PSd1c2VySW5wdXRDaGFuZ2VkJ1xuICAgICAgPlxuICAgIHt7T2JqZWN0LmtleXMocHJvcGVydHlUb2tlbnMpfX1cblxuICAgIDxvbD5cbiAgICAgIDxsaSB2LWZvcj0nc3VnZ2VzdGlvbiBpbiBzdWdnZXN0aW9ucycgOmtleT0nc3VnZ2VzdGlvbi5pZCc+XG4gICAgICAgIHt7c3VnZ2VzdGlvbn19XG4gICAgICA8L2xpPlxuICAgIDwvb2w+XG5cbiAgICA8ZGl2PlxuICAgICAgPEFsZXJ0IHYtZm9yPScoZXJyb3IsIGluZGV4KSBpbiBlcnJvcnMnXG4gICAgICAgIDplcnJvcj0nZXJyb3IuZXJyb3InXG4gICAgICAgIDptZXNzYWdlPSdlcnJvci5tZXNzYWdlJ1xuICAgICAgICA6dHlwZT0nZXJyb3IudHlwZSdcbiAgICAgICAgOmtleT0nZXJyb3IuaWQnXG4gICAgICAgIEByZW1vdmU9J3JlbW92ZUFsZXJ0KGluZGV4KSdcbiAgICAgICAgOnZhbHVlPSdpbnB1dCdcbiAgICAgICAgQGlucHV0PSd1c2VySW5wdXRDaGFuZ2VkKCRldmVudCknXG4gICAgICAvPlxuICAgIDwvZGl2PlxuXG4gICAgPGRpdiBjbGFzcz1cInF1ZXJ5XCI+XG4gICAgICA8SW5wdXRBbHBoYUNvbmp1bmN0aXZlTm9ybWFsRm9ybVxuICAgICAgICA6ZmlsdGVycz0nZmlsdGVycydcbiAgICAgICAgQGZpbHRlckNsaWNrPSdyZW1vdmVGaWx0ZXInXG4gICAgICAvPlxuICAgIDwvZGl2PlxuICA8L2Rpdj5cbjwvdGVtcGxhdGU+XG5cbjxzY3JpcHQ+XG5pbXBvcnQge1xuICBmdW5jdGlvblRva2VucyxcbiAgbG9naWNUb2tlbnMsXG4gIGV4dHJhY3RUb2tlbnMsXG4gIGNvbmRpdGlvbmFsVG9rZW5zLFxuICBzb3J0VG9rZW5PYmplY3Rcbn0gZnJvbSAnLi90b2tlbnMuanMnO1xuaW1wb3J0IHtcbiAgaWRzLFxuICBmaWVsZFRva2VucyxcbiAgZ3JvdXBCeUNvbmp1bmN0aXZlTm9ybWFsRm9ybSxcbiAgc2NydWJJbnB1dFRleHQsXG4gIGdpUmVnZXhcbn0gZnJvbSAnLi9oZWxwZXJzLmpzJztcbmltcG9ydCBBbGVydCBmcm9tICcuLi8uLi9BbGVydEJveC9BbGVydEJveC52dWUnO1xuaW1wb3J0IElucHV0QWxwaGFGaWx0ZXIgZnJvbSAnLi4vSW5wdXRBbHBoYUZpbHRlci9JbnB1dEFscGhhRmlsdGVyLnZ1ZSc7XG5pbXBvcnQgSW5wdXRBbHBoYUNvbmp1bmN0aXZlTm9ybWFsRm9ybSBmcm9tICcuLi9JbnB1dEFscGhhQ29uanVuY3RpdmVOb3JtYWxGb3JtL0lucHV0QWxwaGFDb25qdW5jdGl2ZU5vcm1hbEZvcm0udnVlJztcblxuZXhwb3J0IGRlZmF1bHQge1xuICBjb21wb25lbnRzOiB7IEFsZXJ0LCBJbnB1dEFscGhhRmlsdGVyLElucHV0QWxwaGFDb25qdW5jdGl2ZU5vcm1hbEZvcm0gfSxcbiAgcHJvcHM6IHtcbiAgICBkYXRhOiB7XG4gICAgICB0eXBlOiBPYmplY3QsXG4gICAgICBkZWZhdWx0OiAoKSA9PiB7XG4gICAgICAgIHJldHVybiB7fTtcbiAgICAgIH1cbiAgICB9XG4gIH0sXG4gIGRhdGE6IGZ1bmN0aW9uKCkge1xuICAgIHJldHVybiB7XG4gICAgICBpbnB1dDogJycsXG4gICAgICBmdW5jOiAnJyxcbiAgICAgIHByb3BlcnR5VG9rZW5zOiBzb3J0VG9rZW5PYmplY3QoZmllbGRUb2tlbnModGhpcy5kYXRhKSksXG4gICAgICBhbGxUb2tlbnM6IGlkcyhsb2dpY1Rva2VucylcbiAgICAgICAgLmNvbmNhdChpZHMoZnVuY3Rpb25Ub2tlbnMpKVxuICAgICAgICAuY29uY2F0KGlkcyhjb25kaXRpb25hbFRva2VucykpXG4gICAgICAgIC5jb25jYXQoaWRzKHNvcnRUb2tlbk9iamVjdChmaWVsZFRva2Vucyh0aGlzLmRhdGEpKSkpLFxuICAgICAgZXJyb3JzOiBbXSxcbiAgICAgIGZpbHRlcnM6IFtdLFxuICAgICAgc3VnZ2VzdGlvbnM6IFtdLFxuICAgICAgQ05GOiBbXVxuICAgIH07XG4gIH0sXG4gIG1ldGhvZHM6IHtcbiAgICByZW1vdmVGaWx0ZXI6IGZ1bmN0aW9uKGluZGV4KSB7XG4gICAgICB0aGlzLmZpbHRlcnMuc3BsaWNlKGluZGV4LDEpO1xuICAgIH0sXG4gICAgcHJvY2Vzc0lucHV0OiBmdW5jdGlvbigpIHtcbiAgICAgIGxldCB0aGF0ID0gdGhpcztcbiAgICAgIGxldCB0ZXh0ID0gdGhhdC5pbnB1dDtcbiAgICAgIHRoYXQuaW5wdXQgPSAnJztcblxuICAgICAgbGV0IGZpbHRlciA9IHRoaXMudG9rZW5TZWFyY2godGV4dCk7XG5cbiAgICAgIGxldCBhbnlVbmRlZmluZWRRID0gZmFsc2U7XG4gICAgICBmb3IgKHZhciBrZXkgaW4gZmlsdGVyKSB7XG4gICAgICAgIGxldCB1bmRlZiA9IGZpbHRlcltrZXldID09PSB1bmRlZmluZWQ7XG4gICAgICAgIGlmICh1bmRlZikge1xuICAgICAgICAgIHRoYXQudG9rZW5FcnJvcihrZXkpO1xuICAgICAgICAgIGFueVVuZGVmaW5lZFEgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChhbnlVbmRlZmluZWRRKSB7XG4gICAgICAgIHJldHVybjtcbiAgICAgIH1cblxuICAgICAgdGhhdC5maWx0ZXJzLnB1c2goZmlsdGVyKTtcbiAgICAgIHRoYXQuQ05GID0gZ3JvdXBCeUNvbmp1bmN0aXZlTm9ybWFsRm9ybSh0aGF0LmZpbHRlcnMpO1xuICAgIH0sXG4gICAgdG9rZW5FcnJvcjogZnVuY3Rpb24oa2V5KSB7XG4gICAgICB0aGlzLmVycm9ycy5wdXNoKHtlcnJvcjogJ1VuZGVmaW5lZCcsIHR5cGU6ICd3YXJuaW5nJyxtZXNzYWdlOiBgJHtrZXl9IGlzIHVuZGVmaW5lZC4gRmlsdGVyIGRpc2NhcmRlZC5gfSk7XG4gICAgfSxcbiAgICBzdWdnZXN0OiBmdW5jdGlvbigpIHtcbiAgICAgIGxldCB0aGF0ID0gdGhpcztcbiAgICAgIHRoYXQuc3VnZ2VzdGlvbnMgPSBnaVJlZ2V4KHRoYXQuaW5wdXQsIHRoYXQuYWxsVG9rZW5zKTtcbiAgICB9LFxuICAgIHVzZXJJbnB1dENoYW5nZWQ6IGZ1bmN0aW9uKGV2ZW50KSB7XG4gICAgICB0aGlzLmlucHV0ID0gZXZlbnQudGFyZ2V0LnZhbHVlO1xuICAgICAgdGhpcy5zdWdnZXN0KCk7XG4gICAgfSxcbiAgICB0b2tlblNlYXJjaDogZnVuY3Rpb24odGV4dCkge1xuICAgICAgbGV0IGxvZ2ljO1xuICAgICAgbGV0IGZ1bmM7XG4gICAgICBsZXQgY29uZDtcbiAgICAgIGxldCBwcm9wO1xuICAgICAgbGV0IGlucDtcblxuICAgICAgW3Byb3AsIHRleHRdID0gZXh0cmFjdFRva2Vucyh0ZXh0LCB0aGlzLnByb3BlcnR5VG9rZW5zKTtcbiAgICAgIFtsb2dpYywgdGV4dF0gPSBleHRyYWN0VG9rZW5zKHRleHQsIGxvZ2ljVG9rZW5zLCAnYW5kJywgMyk7XG4gICAgICBbZnVuYywgdGV4dF0gPSBleHRyYWN0VG9rZW5zKHRleHQsIGZ1bmN0aW9uVG9rZW5zLCAnaWRlbnRpdHknKTtcbiAgICAgIFtjb25kLCB0ZXh0XSA9IGV4dHJhY3RUb2tlbnModGV4dCwgY29uZGl0aW9uYWxUb2tlbnMsICdlcScsIHVuZGVmaW5lZCwgdHJ1ZSk7XG4gICAgICBpbnAgPSBzY3J1YklucHV0VGV4dCh0ZXh0KTtcblxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgbG9naWM6IGxvZ2ljLFxuICAgICAgICBmdW5jdGlvbjogZnVuYyxcbiAgICAgICAgY29uZGl0aW9uYWw6IGNvbmQsXG4gICAgICAgIHByb3BlcnR5OiBwcm9wLFxuICAgICAgICBpbnB1dDogaW5wXG4gICAgICB9O1xuICAgIH0sXG4gICAgcmVtb3ZlQWxlcnQ6IGZ1bmN0aW9uKGluZGV4KSB7XG4gICAgICB0aGlzLmVycm9ycy5zcGxpY2UoaW5kZXgsIDEpO1xuICAgIH1cbiAgfVxufTtcbjwvc2NyaXB0PlxuXG5cbjxzdHlsZSBzY29wZWQgc3JjPScuL2lucHV0LXN0eWxlcy5jc3MnPlxuXG48L3N0eWxlPlxuIiwibGV0IGNvbG9ycyA9IFtcbiAgXCIjRjQ0MzM2XCIsXG4gIFwiI0ZGNDA4MVwiLFxuICBcIiM5QzI3QjBcIixcbiAgXCIjNjczQUI3XCIsXG4gIFwiIzNGNTFCNVwiLFxuICBcIiMyMTk2RjNcIixcbiAgXCIjMDNBOUY0XCIsXG4gIFwiIzAwQkNENFwiLFxuICBcIiMwMDk2ODhcIixcbiAgXCIjNENBRjUwXCIsXG4gIFwiIzhCQzM0QVwiLFxuICBcIiNDRERDMzlcIixcbiAgXCIjRkZDMTA3XCIsXG4gIFwiI0ZGOTgwMFwiLFxuICBcIiNGRjU3MjJcIixcbiAgXCIjNzk1NTQ4XCIsXG4gIFwiIzlFOUU5RVwiLFxuICBcIiM2MDdEOEJcIlxuXTtcblxuZnVuY3Rpb24gTGlnaHRlbkRhcmtlbkNvbG9yKGNvbCwgYW10KSB7XG4gIC8vIGh0dHBzOi8vY3NzLXRyaWNrcy5jb20vc25pcHBldHMvamF2YXNjcmlwdC9saWdodGVuLWRhcmtlbi1jb2xvci9cbiAgdmFyIHVzZVBvdW5kID0gZmFsc2U7XG5cbiAgaWYgKGNvbFswXSA9PSBcIiNcIikge1xuICAgIGNvbCA9IGNvbC5zbGljZSgxKTtcbiAgICB1c2VQb3VuZCA9IHRydWU7XG4gIH1cblxuICB2YXIgbnVtID0gcGFyc2VJbnQoY29sLCAxNik7XG5cbiAgdmFyIHIgPSAobnVtID4+IDE2KSArIGFtdDtcblxuICBpZiAociA+IDI1NSkgciA9IDI1NTtcbiAgZWxzZSBpZiAociA8IDApIHIgPSAwO1xuXG4gIHZhciBiID0gKChudW0gPj4gOCkgJiAweDAwZmYpICsgYW10O1xuXG4gIGlmIChiID4gMjU1KSBiID0gMjU1O1xuICBlbHNlIGlmIChiIDwgMCkgYiA9IDA7XG5cbiAgdmFyIGcgPSAobnVtICYgMHgwMDAwZmYpICsgYW10O1xuXG4gIGlmIChnID4gMjU1KSBnID0gMjU1O1xuICBlbHNlIGlmIChnIDwgMCkgZyA9IDA7XG5cbiAgcmV0dXJuICh1c2VQb3VuZCA/IFwiI1wiIDogXCJcIikgKyAoZyB8IChiIDw8IDgpIHwgKHIgPDwgMTYpKS50b1N0cmluZygxNik7XG59XG5cbmZ1bmN0aW9uIGluaXRpYWxzRm9udFNpemUoc2l6ZSkge1xuICByZXR1cm4gTWF0aC5mbG9vcihzaXplIC8gMi41KTtcbn1cblxuZnVuY3Rpb24gYXpPbmx5KHN0cmluZykge1xuICB2YXIgZmlsdGVyZWQgPSBcIlwiO1xuICBmb3IgKHZhciBpID0gMDsgaSA8IHN0cmluZy5sZW5ndGg7IGkrKykge1xuICAgIGxldCBjaGFyID0gc3RyaW5nW2ldO1xuICAgIGlmICgvXlthLXpBLVpdKyQvLnRlc3QoY2hhcikpIHtcbiAgICAgIGZpbHRlcmVkICs9IGNoYXI7XG4gICAgfVxuICB9XG4gIHJldHVybiBmaWx0ZXJlZDtcbn1cblxuZnVuY3Rpb24gY2FwdGlhbHNJbihzdHJpbmcpIHtcbiAgdmFyIGNhcHMgPSBbXTtcbiAgbGV0IHN0ciA9IGF6T25seShzdHJpbmcpO1xuICBmb3IgKHZhciBpID0gMDsgaSA8IHN0ci5sZW5ndGg7IGkrKykge1xuICAgIGlmIChzdHJbaV0gPT09IHN0cltpXS50b1VwcGVyQ2FzZSgpKSB7XG4gICAgICBjYXBzLnB1c2goc3RyW2ldKTtcbiAgICB9XG4gIH1cbiAgcmV0dXJuIGNhcHM7XG59XG5cbmZ1bmN0aW9uIGluaXRpYWxzT2YobmFtZSkge1xuICBsZXQgY2FwcyA9IGNhcHRpYWxzSW4obmFtZS5zcGxpdChcIiBcIikuam9pbihcIlwiKSk7XG4gIHZhciBmaXJzdCA9IGNhcHNbMF0sXG4gICAgbGFzdCA9IFwiXCI7XG4gIGlmIChjYXBzLmxlbmd0aCA+IDEpIHtcbiAgICBsYXN0ID0gY2Fwc1tjYXBzLmxlbmd0aCAtIDFdO1xuICB9XG4gIHZhciBpbml0cyA9IGZpcnN0ICsgbGFzdDtcbiAgaWYgKGluaXRzICE9PSBcInVuZGVmaW5lZFwiKSB7XG4gICAgcmV0dXJuIGluaXRzO1xuICB9XG4gIHJldHVybiBuYW1lWzBdLnRvVXBwZXJDYXNlKCk7XG59XG5cbmV4cG9ydCB7XG4gIGNvbG9ycyxcbiAgTGlnaHRlbkRhcmtlbkNvbG9yLFxuICBpbml0aWFsc0ZvbnRTaXplLFxuICBhek9ubHksXG4gIGNhcHRpYWxzSW4sXG4gIGluaXRpYWxzT2Zcbn07XG4iLCI8dGVtcGxhdGU+XG5cbjxkaXYgOmNsYXNzPVwiZHJvcERpcmVjdGlvblwiPlxuICA8YSBjbGFzcz1cImRyb3Bkb3duLXRvZ2dsZVwiIGhyZWY9XCIjXCIgcm9sZT1cImJ1dHRvblwiIGlkPVwiYXZhdGFyLWRyb3Bkb3duXCIgZGF0YS10b2dnbGU9XCJkcm9wZG93blwiIGFyaWEtaGFzcG9wdXA9XCJ0cnVlXCIgYXJpYS1leHBhbmRlZD1cImZhbHNlXCI+XG4gICAgPGRpdiBjbGFzcz0nYXZhdGFyJyA6c3R5bGU9XCJhdmF0YXJTdHlsZVwiPlxuICAgICAgPGRpdiBjbGFzcz0ndGV4dC1hdmF0YXInIDpzdHlsZT1cImluaXRpYWxzU3R5bGVcIj5cbiAgICAgICAgPGRpdiAgdi1pZj1cImxvZ2dlZEluUVwiPnt7aW5pdGlhbHN9fTwvZGl2PlxuICAgICAgICA8ZGl2IHYtZWxzZT48aSBjbGFzcz0nZmEgZmEtdXNlcic+PC9pPjwvZGl2PlxuICAgICAgPC9kaXY+XG4gICAgPC9kaXY+XG4gIDwvYT5cblxuICAgIDx1bCBjbGFzcz0nZHJvcGRvd24tbWVudSc+XG4gICAgICA8bGkgY2xhc3M9XCJkcm9wZG93bi1pdGVtXCIgdi1mb3I9J2FjdGlvbiBpbiB1c2VyQWN0aW9ucycgOmtleT0nYWN0aW9uLmlkJyA6c3R5bGU9XCJhY3Rpb25TdHlsZVwiPlxuICAgICAgICA8YSA6aHJlZj0nYWN0aW9uLmhyZWYnPlxuICAgICAgICAgIDxzcGFuIGNsYXNzPVwiXCI+XG4gICAgICAgICAgICA8aSBjbGFzcz1cImZhXCIgOmNsYXNzPVwiYWN0aW9uLmljb25cIj48L2k+XG4gICAgICAgICAgICB7e2FjdGlvbi50ZXh0fX1cbiAgICAgICAgICA8L3NwYW4+XG4gICAgICAgIDwvYT5cbiAgICAgIDwvbGk+XG4gICAgPC91bD5cbjwvZGl2PlxuXG5cblxuPC90ZW1wbGF0ZT5cblxuXG48c2NyaXB0PlxuaW1wb3J0IHsgY29sb3JzLCBMaWdodGVuRGFya2VuQ29sb3IsIGluaXRpYWxzRm9udFNpemUsIGF6T25seSwgY2FwdGlhbHNJbiwgaW5pdGlhbHNPZiB9IGZyb20gJy4vRGphbmdvVXNlclV0aWxzLmpzJztcblxubGV0IGhyZWZTaWduSW4gPSAnJyxcbmhyZWZTaWduT3V0ID0gJycsXG5ocmVmU2lnblVwID0gJycsXG5ocmVmUHJvZmlsZSA9ICcnO1xuXG5cbmV4cG9ydCBkZWZhdWx0IHtcbiAgbmFtZTogJ0RqYW5nb1VzZXInLFxuICBwcm9wczoge1xuICAgIGxvZ2dlZEluUToge1xuICAgICAgdHlwZTogQm9vbGVhbixcbiAgICAgIGRlZmF1bHQ6IGZhbHNlXG4gICAgfSxcbiAgICBkcm9wRGlyZWN0aW9uOiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiAnZHJvcGxlZnQnXG4gICAgfSxcbiAgICB1c2VybmFtZToge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogJ1VzZXIgTmFtZSdcbiAgICB9LFxuICAgIGhyZWZTaWduSW46IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6IGhyZWZTaWduSW5cbiAgICB9LFxuICAgIGhyZWZTaWduT3V0OiB7XG4gICAgICB0eXBlOiBTdHJpbmcsXG4gICAgICBkZWZhdWx0OiBocmVmU2lnbk91dFxuICAgIH0sXG4gICAgaHJlZlNpZ25VcDoge1xuICAgICAgdHlwZTogU3RyaW5nLFxuICAgICAgZGVmYXVsdDogaHJlZlNpZ25VcFxuICAgIH0sXG4gICAgaHJlZlByb2ZpbGU6IHtcbiAgICAgIHR5cGU6IFN0cmluZyxcbiAgICAgIGRlZmF1bHQ6IGhyZWZQcm9maWxlXG4gICAgfSxcbiAgICBzaXplOiB7XG4gICAgICB0eXBlOiBOdW1iZXIsXG4gICAgICBkZWZhdWx0OiAzMFxuICAgIH0sXG4gICAgZm9udENvbG9yU2hpZnQ6IHtcbiAgICAgIHR5cGU6IE51bWJlcixcbiAgICAgIGRlZmF1bHQ6IDgwXG4gICAgfVxuICB9LFxuICBkYXRhOiBmdW5jdGlvbigpIHtcbiAgICByZXR1cm4ge1xuXG4gICAgfTtcbiAgfSxcbiAgY29tcHV0ZWQ6IHtcbiAgICBpbml0aWFsczogZnVuY3Rpb24oKSB7XG4gICAgICByZXR1cm4gaW5pdGlhbHNPZih0aGlzLnVzZXJuYW1lKVxuICAgIH0sXG5cbiAgICBhdmF0YXJTdHlsZTogZnVuY3Rpb24gKCkge1xuICAgICAgbGV0IGNvbG9yID0gY29sb3JzW01hdGguZmxvb3IoTWF0aC5yYW5kb20oKSAqIGNvbG9ycy5sZW5ndGgpXVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgd2lkdGg6IHRoaXMuc2l6ZSArICdweCAhaW1wb3J0YW50JyxcbiAgICAgICAgaGVpZ2h0OiB0aGlzLnNpemUgKyAncHggIWltcG9ydGFudCcsXG4gICAgICAgIGJhY2tncm91bmRDb2xvcjogY29sb3IsXG4gICAgICAgIGNvbG9yOiBMaWdodGVuRGFya2VuQ29sb3IoY29sb3IsIHRoaXMuZm9udENvbG9yU2hpZnQpXG4gICAgICB9XG4gICAgfSxcblxuICAgIGluaXRpYWxzU3R5bGU6IGZ1bmN0aW9uICgpIHtcbiAgICAgIHJldHVybiB7XG4gICAgICAgIGZvbnRTaXplOiBpbml0aWFsc0ZvbnRTaXplKHRoaXMuc2l6ZSkgKyAncHgnLFxuICAgICAgfVxuICAgIH0sXG5cbiAgICBhY3Rpb25TdHlsZTogZnVuY3Rpb24gKCkge1xuICAgICAgdmFyIGZzID0gKHRoaXMuc2l6ZSAvIDIgLyAxLjUpLCBkaXNwID0gJycsIG1yID0gJzBweCc7XG4gICAgICBpZiAoZnMgPCAxMCkge1xuICAgICAgICBmcyA9IHRoaXMuc2l6ZSAvIDI7XG4gICAgICAgIGRpc3AgPSAnaW5saW5lICFpbXBvcnRhbnQnO1xuICAgICAgICBtciA9ICc1cHgnO1xuICAgICAgfVxuICAgICAgcmV0dXJuIHtcbiAgICAgICAgZm9udFNpemU6IGZzICsgJ3B4JyxcbiAgICAgICAgZGlzcGxheTogZGlzcCxcbiAgICAgICAgbWFyZ2luUmlnaHQ6IG1yXG4gICAgICB9XG4gICAgfSxcblxuICAgIHVzZXJBY3Rpb25zOiBmdW5jdGlvbigpIHtcbiAgICAgIHJldHVybiB0aGlzLmxvZ2dlZEluUVxuICAgICAgPyBbXG4gICAgICAgIHtpY29uOiAnZmEtc2lnbi1vdXQtYWx0JywgaHJlZjogdGhpcy5ocmVmU2lnbk91dCwgdGV4dDogJ1NpZ24tb3V0J30sXG4gICAgICAgIHtpY29uOiAnZmEtd3JlbmNoJywgaHJlZjogdGhpcy5ocmVmUHJvZmlsZSwgdGV4dDogJ1Byb2ZpbGUnfSxcbiAgICAgIF1cbiAgICAgIDogW1xuICAgICAgICB7aWNvbjogJ2ZhLXNpZ24taW4tYWx0JywgaHJlZjogdGhpcy5ocmVmU2lnbkluLCB0ZXh0OiAnU2lnbi1pbid9LFxuICAgICAgICB7aWNvbjogJ2ZhLXVzZXItcGx1cycsIGhyZWY6IHRoaXMuaHJlZlNpZ25VcCwgdGV4dDogJ1NpZ24tdXAnfSxcbiAgICAgIF07XG4gICAgfVxuICB9LFxuICBtZXRob2RzOiB7XG5cbiAgfVxufTtcbjwvc2NyaXB0PlxuXG5cbjxzdHlsZSBzY29wZWQ+XG5cbi5hdmF0YXIge1xuICBiYWNrZ3JvdW5kOiByZ2IoMCwxOTEsMjU1KTtcbiAgYm9yZGVyOiAxcHggc29saWQgdHJhbnNwYXJlbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcbn1cblxuLnRleHQtYXZhdGFyIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICB0b3A6IDUwJTtcbiAgdHJhbnNmb3JtOiB0cmFuc2xhdGVZKC01MCUpO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG5saSBhIHtcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuXG5saSBhOnZpc2l0ZWQge1xuXG59XG5cblxuLmRyb3Bkb3duLXRvZ2dsZTo6YWZ0ZXIsIC5kcm9wZG93bi10b2dnbGU6OmJlZm9yZSB7XG4gIGNvbnRlbnQ6IG5vbmUgIWltcG9ydGFudDtcbn1cbjwvc3R5bGU+XG4iLCJcbmltcG9ydCBCdXR0b25NaW51c1RvQ2xvc2UgZnJvbSAnLi9jb21wb25lbnRzL0J1dHRvbk1pbnVzVG9DbG9zZS9CdXR0b25NaW51c1RvQ2xvc2UudnVlJztcbmltcG9ydCBCdXR0b25QbHVzIGZyb20gJy4vY29tcG9uZW50cy9CdXR0b25QbHVzL0J1dHRvblBsdXMudnVlJztcblxuaW1wb3J0IENydW1icyBmcm9tICcuL2NvbXBvbmVudHMvQ3J1bWJzL0NydW1icy52dWUnO1xuXG5cblxuaW1wb3J0IEFkdmFuY2VkRmlsdGVySW5wdXQgZnJvbSAnLi9jb21wb25lbnRzL0FkdmFuY2VkRmlsdGVyL0FkdmFuY2VkRmlsdGVySW5wdXQvQWR2YW5jZWRGaWx0ZXJJbnB1dC52dWUnO1xuaW1wb3J0IEFkdmFuY2VkRmlsdGVyUm93IGZyb20gJy4vY29tcG9uZW50cy9BZHZhbmNlZEZpbHRlci9BZHZhbmNlZEZpbHRlclJvdy9BZHZhbmNlZEZpbHRlclJvdy52dWUnO1xuaW1wb3J0IEFkdmFuY2VkRmlsdGVyU2VsZWN0Q29uZGl0aW9uYWwgZnJvbSAnLi9jb21wb25lbnRzL0FkdmFuY2VkRmlsdGVyL0FkdmFuY2VkRmlsdGVyU2VsZWN0Q29uZGl0aW9uYWwvQWR2YW5jZWRGaWx0ZXJTZWxlY3RDb25kaXRpb25hbC52dWUnO1xuaW1wb3J0IEFkdmFuY2VkRmlsdGVyU2VsZWN0RnVuY3Rpb24gZnJvbSAnLi9jb21wb25lbnRzL0FkdmFuY2VkRmlsdGVyL0FkdmFuY2VkRmlsdGVyU2VsZWN0RnVuY3Rpb24vQWR2YW5jZWRGaWx0ZXJTZWxlY3RGdW5jdGlvbi52dWUnO1xuaW1wb3J0IEFkdmFuY2VkRmlsdGVyU2VsZWN0TG9naWMgZnJvbSAnLi9jb21wb25lbnRzL0FkdmFuY2VkRmlsdGVyL0FkdmFuY2VkRmlsdGVyU2VsZWN0TG9naWMvQWR2YW5jZWRGaWx0ZXJTZWxlY3RMb2dpYy52dWUnO1xuaW1wb3J0IEFkdmFuY2VkRmlsdGVyU2VsZWN0UHJvcGVydHkgZnJvbSAnLi9jb21wb25lbnRzL0FkdmFuY2VkRmlsdGVyL0FkdmFuY2VkRmlsdGVyU2VsZWN0UHJvcGVydHkvQWR2YW5jZWRGaWx0ZXJTZWxlY3RQcm9wZXJ0eS52dWUnO1xuaW1wb3J0IEFkdmFuY2VkRmlsdGVyVGFibGUgZnJvbSAnLi9jb21wb25lbnRzL0FkdmFuY2VkRmlsdGVyL0FkdmFuY2VkRmlsdGVyVGFibGUvQWR2YW5jZWRGaWx0ZXJUYWJsZS52dWUnO1xuXG5pbXBvcnQgQW5jaG9yQnV0dG9uIGZyb20gJy4vY29tcG9uZW50cy9BbmNob3JCdXR0b24vQW5jaG9yQnV0dG9uLnZ1ZSc7XG5pbXBvcnQgQWxlcnRCb3ggZnJvbSAnLi9jb21wb25lbnRzL0FsZXJ0Qm94L0FsZXJ0Qm94LnZ1ZSc7XG5cbmltcG9ydCBEYXRhVGFibGUgZnJvbSAnLi9jb21wb25lbnRzL0RhdGFUYWJsZS9EYXRhVGFibGUvRGF0YVRhYmxlLnZ1ZSc7XG5pbXBvcnQgRGF0YVRhYmxlQ2VsbCBmcm9tICcuL2NvbXBvbmVudHMvRGF0YVRhYmxlL0RhdGFUYWJsZUNlbGwvRGF0YVRhYmxlQ2VsbC52dWUnO1xuaW1wb3J0IERhdGFUYWJsZVJvdyBmcm9tICcuL2NvbXBvbmVudHMvRGF0YVRhYmxlL0RhdGFUYWJsZVJvdy9EYXRhVGFibGVSb3cudnVlJztcblxuXG5pbXBvcnQgSHR0cFJlcXVlc3QgZnJvbSAnLi9jb21wb25lbnRzL0h0dHBSZXF1ZXN0L0h0dHBSZXF1ZXN0LnZ1ZSc7XG5cblxuaW1wb3J0IFNlY3Rpb25OYXZpZ2F0aW9uIGZyb20gJy4vY29tcG9uZW50cy9TZWN0aW9uTmF2aWdhdGlvbi9TZWN0aW9uTmF2aWdhdGlvbi9TZWN0aW9uTmF2aWdhdGlvbi52dWUnO1xuaW1wb3J0IFNlY3Rpb25OYXZpZ2F0aW9uQXJyb3dVcCBmcm9tICcuL2NvbXBvbmVudHMvU2VjdGlvbk5hdmlnYXRpb24vU2VjdGlvbk5hdmlnYXRpb25BcnJvd1VwL1NlY3Rpb25OYXZpZ2F0aW9uQXJyb3dVcC52dWUnO1xuaW1wb3J0IFNlY3Rpb25OYXZpZ2F0aW9uQXJyb3dEb3duIGZyb20gJy4vY29tcG9uZW50cy9TZWN0aW9uTmF2aWdhdGlvbi9TZWN0aW9uTmF2aWdhdGlvbkFycm93RG93bi9TZWN0aW9uTmF2aWdhdGlvbkFycm93RG93bi52dWUnO1xuXG5cbmltcG9ydCBJbnB1dEFscGhhIGZyb20gJy4vY29tcG9uZW50cy9JbnB1dEFscGhhL0lucHV0QWxwaGEvSW5wdXRBbHBoYS52dWUnO1xuaW1wb3J0IElucHV0QWxwaGFGaWx0ZXIgZnJvbSAnLi9jb21wb25lbnRzL0lucHV0QWxwaGEvSW5wdXRBbHBoYUZpbHRlci9JbnB1dEFscGhhRmlsdGVyLnZ1ZSc7XG5cbmltcG9ydCBEamFuZ29Vc2VyIGZyb20gJy4vY29tcG9uZW50cy9EamFuZ29Vc2VyL0RqYW5nb1VzZXIudnVlJztcblxuXG52YXIgTXVlID0ge307XG5cbi8vIE11ZS5CdXR0b25NaW51c1RvQ2xvc2UgPSBCdXR0b25NaW51c1RvQ2xvc2U7XG4vLyBNdWUuQnV0dG9uUGx1cyA9IEJ1dHRvblBsdXM7XG5cbk11ZS5DcnVtYnMgPSBDcnVtYnM7XG5NdWUuQWxlcnRCb3ggPSBBbGVydEJveDtcblxuLy8gTXVlLkFkdmFuY2VkRmlsdGVySW5wdXQgPSBBZHZhbmNlZEZpbHRlcklucHV0O1xuLy8gTXVlLkFkdmFuY2VkRmlsdGVyUm93ID0gQWR2YW5jZWRGaWx0ZXJSb3c7XG4vLyBNdWUuQWR2YW5jZWRGaWx0ZXJTZWxlY3RDb25kaXRpb25hbCA9IEFkdmFuY2VkRmlsdGVyU2VsZWN0Q29uZGl0aW9uYWw7XG4vLyBNdWUuQWR2YW5jZWRGaWx0ZXJTZWxlY3RGdW5jdGlvbiA9IEFkdmFuY2VkRmlsdGVyU2VsZWN0RnVuY3Rpb247XG4vLyBNdWUuQWR2YW5jZWRGaWx0ZXJTZWxlY3RMb2dpYyA9IEFkdmFuY2VkRmlsdGVyU2VsZWN0TG9naWM7XG4vLyBNdWUuQWR2YW5jZWRGaWx0ZXJTZWxlY3RQcm9wZXJ0eSA9IEFkdmFuY2VkRmlsdGVyU2VsZWN0UHJvcGVydHk7XG5NdWUuQWR2YW5jZWRGaWx0ZXJUYWJsZSA9IEFkdmFuY2VkRmlsdGVyVGFibGU7XG5cbk11ZS5BbmNob3JCdXR0b24gPSBBbmNob3JCdXR0b247XG5cbk11ZS5EYXRhVGFibGUgPSBEYXRhVGFibGU7XG4vLyBNdWUuRGF0YVRhYmxlQ2VsbCA9IERhdGFUYWJsZUNlbGw7XG4vLyBNdWUuRGF0YVRhYmxlUm93ID0gRGF0YVRhYmxlUm93O1xuXG5NdWUuSHR0cFJlcXVlc3QgPSBIdHRwUmVxdWVzdDtcblxuTXVlLlNlY3Rpb25OYXZpZ2F0aW9uID0gU2VjdGlvbk5hdmlnYXRpb247XG4vLyBNdWUuU2VjdGlvbk5hdmlnYXRpb25BcnJvd1VwID0gU2VjdGlvbk5hdmlnYXRpb25BcnJvd1VwO1xuLy8gTXVlLlNlY3Rpb25OYXZpZ2F0aW9uQXJyb3dEb3duID0gU2VjdGlvbk5hdmlnYXRpb25BcnJvd0Rvd247XG5cbk11ZS5JbnB1dEFscGhhID0gSW5wdXRBbHBoYTtcbi8vIE11ZS5JbnB1dEFscGhhRmlsdGVyID0gSW5wdXRBbHBoYUZpbHRlcjtcblxuTXVlLkRqYW5nb1VzZXIgPSBEamFuZ29Vc2VyO1xuXG53aW5kb3cuTXVlID0gTXVlO1xuIl0sIm5hbWVzIjpbImNvbmRpdGlvbmFscyIsImVxIiwidGV4dCIsInR5cGVzIiwiY29uZCIsImEiLCJiIiwibmVxIiwibHQiLCJndCIsImx0ZSIsImd0ZSIsInNzIiwiaW5jbHVkZXMiLCJuc3MiLCJmdW5jdGlvbnMiLCJmdW5jIiwieCIsIm51bWJlcnMiLCJtYXAiLCJ6IiwibGVuZ3RoIiwicmVkdWNlIiwiYWRkZXIiLCJ2YWx1ZSIsImxnIiwiSW5maW5pdHkiLCJkIiwic20iLCJkZXRlcm1pbmVUeXBlIiwiZGF0YSIsInR5cGUiLCJBcnJheSIsImlzQXJyYXkiLCJ0eXBlb2ZFbGVtZW50cyIsImUiLCJ0eXBlb2ZFbGVtZW50IiwiYWxsU2FtZVEiLCJldmVyeSIsImZpbHRlcmFibGVWYWx1ZSIsInJlY29yZCIsImZpZWxkIiwidG9TdHJpbmciLCJ0b0xvd2VyQ2FzZSIsInN0cmluZ1NvcnQiLCJhc2NRIiwiayIsInZhbHVlUSIsInVuZGVmaW5lZCIsImlzTmFOIiwic3RhbmRhcml6ZVZhbHVlIiwibnVtYmVyU29ydCIsInRhYmxlVGltc29ydCIsImlkcyIsInJlY29yZHMiLCJzb3J0Iiwic29ydGluZ0Z1bmN0aW9ucyIsImZpZWxkcyIsIk9iamVjdCIsImtleXMiLCJmaWVsZEZ1bmN0aW9ucyIsInRyYW5zZm9ybSIsImFzY2VuZGluZ1EiLCJvcmRlciIsInkiLCJzb21lIiwic29ydGVkIiwiaSIsImYiLCJ2IiwiZ2lSZWdleE9uRmllbGRzIiwiZmlsdGVyVGV4dCIsImZpZWxkUmVuZGVyRnVuY3Rpb25zIiwicmVnIiwiUmVnRXhwIiwidXNlIiwiaWQiLCJmaWVsZEpvaW4iLCJqb2luIiwibWF0Y2giLCJwdXNoIiwib2JqZWN0RmllbGRzIiwiYWxsUmVjb3JkRmllbGRzIiwiY29uY2F0IiwicmVjb3JkSWQiLCJTZXQiLCJodHRwUmVhZHlTdGF0ZUNvZGVzIiwiaHR0cFN0YXR1c0NvZGVzIiwic29ydFRva2VuT2JqZWN0Iiwib2JqZWN0IiwiZW50cmllcyIsImthIiwia2IiLCJvIiwiY29uZGl0aW9uYWxSZW5kZXJzIiwibG9naWNSZW5kZXJzIiwiYW5kIiwib3IiLCJjb25kaXRpb25hbFRva2VucyIsImVxdWFsIiwic3Vic3RyaW5nIiwiY29udGFpbnMiLCJmdW5jdGlvblRva2VucyIsIm1heCIsIm1heGltdW0iLCJtaW5pbXVtIiwibWluIiwibGVuIiwibWVhbiIsIm1lZGlhbiIsImF2ZXJhZ2UiLCJpZGVudGl0eSIsImxvZ2ljVG9rZW5zIiwiZXh0cmFjdFRva2VucyIsInRva2VucyIsImZhbGxiYWNrIiwic2NvcGUiLCJkcm9wVW50aWxNYXRjaFEiLCJ0b2tlbiIsInN0ciIsInNsaWNlIiwidCIsImluZGV4T2YiLCJyZXBsYWNlIiwicmVjb3JkSWRzIiwiZmllbGRUb2tlbnMiLCJyZWNvcmRGaWVsZHMiLCJnaVJlZ2V4IiwicmVnZXgiLCJzdWdnZXN0aW9ucyIsImZpbHRlciIsImZvdW5kIiwiZ3JvdXBCeUNvbmp1bmN0aXZlTm9ybWFsRm9ybSIsImZpbHRlcnMiLCJncm91cHBlZCIsImxvZ2ljIiwibGlzdCIsInNjcnViSW5wdXRUZXh0IiwiY29sb3JzIiwiTGlnaHRlbkRhcmtlbkNvbG9yIiwiY29sIiwiYW10IiwidXNlUG91bmQiLCJudW0iLCJwYXJzZUludCIsInIiLCJnIiwiaW5pdGlhbHNGb250U2l6ZSIsInNpemUiLCJNYXRoIiwiZmxvb3IiLCJhek9ubHkiLCJzdHJpbmciLCJmaWx0ZXJlZCIsImNoYXIiLCJ0ZXN0IiwiY2FwdGlhbHNJbiIsImNhcHMiLCJ0b1VwcGVyQ2FzZSIsImluaXRpYWxzT2YiLCJuYW1lIiwic3BsaXQiLCJmaXJzdCIsImxhc3QiLCJpbml0cyIsImhyZWZTaWduT3V0IiwiaHJlZlNpZ25VcCIsImhyZWZQcm9maWxlIiwiTXVlIiwiQ3J1bWJzIiwiQWxlcnRCb3giLCJBZHZhbmNlZEZpbHRlclRhYmxlIiwiQW5jaG9yQnV0dG9uIiwiRGF0YVRhYmxlIiwiSHR0cFJlcXVlc3QiLCJTZWN0aW9uTmF2aWdhdGlvbiIsIklucHV0QWxwaGEiLCJEamFuZ29Vc2VyIiwid2luZG93Il0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7O0FBYUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQUFBOztFQ1hBLFNBQVMsa0JBQWtCLENBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLGdCQUFnQjs7SUFFbEcsVUFBVSxFQUFFLGNBQWMsRUFBRSxpQkFBaUIsRUFBRSxvQkFBb0IsRUFBRTtJQUNyRSxJQUFJLE9BQU8sVUFBVSxLQUFLLFNBQVMsRUFBRTtNQUNuQyxpQkFBaUIsR0FBRyxjQUFjLENBQUM7TUFDbkMsY0FBYyxHQUFHLFVBQVUsQ0FBQztNQUM1QixVQUFVLEdBQUcsS0FBSyxDQUFDO0tBQ3BCOzs7SUFHRCxJQUFJLE9BQU8sR0FBRyxPQUFPLE1BQU0sS0FBSyxVQUFVLEdBQUcsTUFBTSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7O0lBRXJFLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEVBQUU7TUFDL0IsT0FBTyxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDO01BQ2pDLE9BQU8sQ0FBQyxlQUFlLEdBQUcsUUFBUSxDQUFDLGVBQWUsQ0FBQztNQUNuRCxPQUFPLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQzs7TUFFekIsSUFBSSxvQkFBb0IsRUFBRTtRQUN4QixPQUFPLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztPQUMzQjtLQUNGOzs7SUFHRCxJQUFJLE9BQU8sRUFBRTtNQUNYLE9BQU8sQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO0tBQzVCOztJQUVELElBQUksSUFBSSxDQUFDOztJQUVULElBQUksZ0JBQWdCLEVBQUU7O01BRXBCLElBQUksR0FBRyxTQUFTLElBQUksQ0FBQyxPQUFPLEVBQUU7O1FBRTVCLE9BQU8sR0FBRyxPQUFPO1FBQ2pCLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVO1FBQ3JDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDOzs7UUFHbkUsSUFBSSxDQUFDLE9BQU8sSUFBSSxPQUFPLG1CQUFtQixLQUFLLFdBQVcsRUFBRTtVQUMxRCxPQUFPLEdBQUcsbUJBQW1CLENBQUM7U0FDL0I7OztRQUdELElBQUksS0FBSyxFQUFFO1VBQ1QsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsaUJBQWlCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztTQUM5Qzs7O1FBR0QsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLHFCQUFxQixFQUFFO1VBQzVDLE9BQU8sQ0FBQyxxQkFBcUIsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUNyRDtPQUNGLENBQUM7Ozs7TUFJRixPQUFPLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztLQUM3QixNQUFNLElBQUksS0FBSyxFQUFFO01BQ2hCLElBQUksR0FBRyxVQUFVLEdBQUcsWUFBWTtRQUM5QixLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO09BQ3hFLEdBQUcsVUFBVSxPQUFPLEVBQUU7UUFDckIsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7T0FDM0MsQ0FBQztLQUNIOztJQUVELElBQUksSUFBSSxFQUFFO01BQ1IsSUFBSSxPQUFPLENBQUMsVUFBVSxFQUFFOztRQUV0QixJQUFJLGNBQWMsR0FBRyxPQUFPLENBQUMsTUFBTSxDQUFDOztRQUVwQyxPQUFPLENBQUMsTUFBTSxHQUFHLFNBQVMsd0JBQXdCLENBQUMsQ0FBQyxFQUFFLE9BQU8sRUFBRTtVQUM3RCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1VBQ25CLE9BQU8sY0FBYyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQztTQUNuQyxDQUFDO09BQ0gsTUFBTTs7UUFFTCxJQUFJLFFBQVEsR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDO1FBQ3BDLE9BQU8sQ0FBQyxZQUFZLEdBQUcsUUFBUSxHQUFHLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7T0FDdEU7S0FDRjs7SUFFRCxPQUFPLE1BQU0sQ0FBQztHQUNmOztFQUVELHdCQUFjLEdBQUcsa0JBQWtCLENBQUM7O0VDbkZwQyxJQUFJLE9BQU8sR0FBRyxPQUFPLFNBQVMsS0FBSyxXQUFXLElBQUksZUFBZSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUM7RUFDMUcsU0FBUyxjQUFjLENBQUMsT0FBTyxFQUFFO0lBQy9CLE9BQU8sVUFBVSxFQUFFLEVBQUUsS0FBSyxFQUFFO01BQzFCLE9BQU8sUUFBUSxDQUFDLEVBQUUsRUFBRSxLQUFLLENBQUMsQ0FBQztLQUM1QixDQUFDO0dBQ0g7RUFDRCxJQUFJLElBQUksR0FBRyxRQUFRLENBQUMsSUFBSSxJQUFJLFFBQVEsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztFQUNyRSxJQUFJLE1BQU0sR0FBRyxFQUFFLENBQUM7O0VBRWhCLFNBQVMsUUFBUSxDQUFDLEVBQUUsRUFBRSxHQUFHLEVBQUU7SUFDekIsSUFBSSxLQUFLLEdBQUcsT0FBTyxHQUFHLEdBQUcsQ0FBQyxLQUFLLElBQUksU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUNsRCxJQUFJLEtBQUssR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHO01BQzVDLEdBQUcsRUFBRSxJQUFJLEdBQUcsRUFBRTtNQUNkLE1BQU0sRUFBRSxFQUFFO0tBQ1gsQ0FBQyxDQUFDOztJQUVILElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsRUFBRTtNQUN0QixLQUFLLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsQ0FBQztNQUNsQixJQUFJLElBQUksR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDOztNQUV0QixJQUFJLEdBQUcsQ0FBQyxHQUFHLEVBQUU7OztRQUdYLElBQUksSUFBSSxrQkFBa0IsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7O1FBRXhELElBQUksSUFBSSxzREFBc0QsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQztPQUN0STs7TUFFRCxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTtRQUNsQixLQUFLLENBQUMsT0FBTyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDaEQsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEdBQUcsVUFBVSxDQUFDO1FBQ2hDLElBQUksR0FBRyxDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO09BQ2pDOztNQUVELElBQUksWUFBWSxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7UUFDakMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEIsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztPQUM1RSxNQUFNO1FBQ0wsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLEdBQUcsQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0MsSUFBSSxLQUFLLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUM7UUFDckMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDMUQsSUFBSSxLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLEtBQUssQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDO09BQy9HO0tBQ0Y7R0FDRjs7RUFFRCxXQUFjLEdBQUcsY0FBYyxDQUFDOzs7QUZsRGhDLEVBRUEsMkJBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUdXQTs7Ozs7Ozs7Ozs7Ozs7OztHQUFBOzs7QUFiQSxFQUVBLCtCQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDV0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBQUE7OztBQWJBLEVBRUEsK0JBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDSUE7Ozs7Ozs7Ozs7Ozs7R0FBQTs7O0FBTkEsRUFFQSwrQkFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQ0ZBOztFQUVBLElBQUlBLGVBQWU7RUFDakI7RUFDQTtFQUNBO0VBQ0FDLE1BQUk7RUFDRkMsVUFBTSxHQURKO0VBRUZDLFdBQU8sQ0FDTCxPQURLLEVBQ0ksYUFESixFQUNtQixhQURuQixFQUNrQyxhQURsQyxFQUVMLFFBRkssRUFFSyxRQUZMLEVBRWUsV0FGZixDQUZMO0VBTUZDLFVBQU0sY0FBU0MsQ0FBVCxFQUFZQyxDQUFaLEVBQWU7RUFDbkIsYUFBT0QsS0FBS0MsQ0FBWjtFQUNEO0VBUkMsR0FKYTtFQWNqQkMsT0FBSztFQUNITCxVQUFNLEdBREg7RUFFSEMsV0FBTyxDQUNMLE9BREssRUFDSSxhQURKLEVBQ21CLGFBRG5CLEVBQ2tDLGFBRGxDLEVBRUwsUUFGSyxFQUVLLFFBRkwsRUFFZSxXQUZmLENBRko7RUFNSEMsVUFBTSxjQUFTQyxDQUFULEVBQVlDLENBQVosRUFBZTtFQUNuQixhQUFPRCxLQUFLQyxDQUFaO0VBQ0Q7RUFSRSxHQWRZO0VBd0JqQkUsTUFBSTtFQUNGTixVQUFNLEdBREo7RUFFRkMsV0FBTyxDQUFDLFFBQUQsQ0FGTDs7RUFJRkMsVUFBTSxjQUFTQyxDQUFULEVBQVlDLENBQVosRUFBZTtFQUNuQixhQUFPRCxJQUFJQyxDQUFYO0VBQ0Q7RUFOQyxHQXhCYTtFQWdDakJHLE1BQUk7RUFDRlAsVUFBTSxHQURKO0VBRUZDLFdBQU8sQ0FBQyxRQUFELENBRkw7O0VBSUZDLFVBQU0sY0FBU0MsQ0FBVCxFQUFZQyxDQUFaLEVBQWU7RUFDbkIsYUFBT0QsSUFBSUMsQ0FBWDtFQUNEO0VBTkMsR0FoQ2E7RUF3Q2pCSSxPQUFLO0VBQ0hSLFVBQU0sR0FESDtFQUVIQyxXQUFPLENBQUMsUUFBRCxDQUZKOztFQUlIQyxVQUFNLGNBQVNDLENBQVQsRUFBWUMsQ0FBWixFQUFlO0VBQ25CLGFBQU9ELEtBQUtDLENBQVo7RUFDRDtFQU5FLEdBeENZO0VBZ0RqQkssT0FBSztFQUNIVCxVQUFNLEdBREg7RUFFSEMsV0FBTyxDQUFDLFFBQUQsQ0FGSjs7RUFJSEMsVUFBTSxjQUFTQyxDQUFULEVBQVlDLENBQVosRUFBZTtFQUNuQixhQUFPRCxLQUFLQyxDQUFaO0VBQ0Q7RUFORSxHQWhEWTtFQXdEakJNLE1BQUk7RUFDRlYsVUFBTSxHQURKO0VBRUZDLFdBQU8sQ0FBQyxPQUFELEVBQVUsYUFBVixFQUF5QixhQUF6QixFQUF3QyxRQUF4QyxDQUZMO0VBR0ZDLFVBQU0sY0FBU0MsQ0FBVCxFQUFZQyxDQUFaLEVBQWU7RUFDbkIsYUFBT0QsRUFBRVEsUUFBRixDQUFXUCxDQUFYLENBQVA7RUFDRDtFQUxDLEdBeERhO0VBK0RqQlEsT0FBSztFQUNIWixVQUFNLEdBREg7RUFFSEMsV0FBTyxDQUFDLE9BQUQsRUFBVSxhQUFWLEVBQXlCLGFBQXpCLEVBQXdDLFFBQXhDLENBRko7O0VBSUhDLFVBQU0sY0FBU0MsQ0FBVCxFQUFZQyxDQUFaLEVBQWU7RUFDbkIsYUFBTyxDQUFFRCxFQUFFUSxRQUFGLENBQVdQLENBQVgsQ0FBVDtFQUNEO0VBTkU7RUEvRFksQ0FBbkI7OztBQ2FBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FBQTs7O0FBZkEsRUFFQSwrQkFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUNGQTtFQUNBO0VBQ0EsSUFBSVMsWUFBWTtFQUNkLGNBQVk7RUFDVmIsVUFBTSxPQURJO0VBRVZjLFVBQU0sY0FBU0MsQ0FBVCxFQUFZO0VBQ2hCLGFBQU9BLENBQVA7RUFDRCxLQUpTO0VBS1ZkLFdBQU8sQ0FDTCxRQURLLEVBQ0ssUUFETCxFQUNlLFdBRGY7RUFMRyxHQURFO0VBVWQsVUFBUTtFQUNORCxVQUFNLGFBREE7RUFFTmMsVUFBTSxjQUFTRSxPQUFULEVBQWtCO0VBQ3RCLGFBQU9BLFFBQVFDLEdBQVIsQ0FBWTtFQUFBLGVBQUtDLElBQUVGLFFBQVFHLE1BQWY7RUFBQSxPQUFaLEVBQ0pDLE1BREksQ0FDRyxVQUFDQyxLQUFELEVBQVFDLEtBQVI7RUFBQSxlQUFtQkQsUUFBUUMsS0FBM0I7RUFBQSxPQURILENBQVA7RUFFRCxLQUxLO0VBTU5yQixXQUFPLENBQUMsYUFBRDtFQU5ELEdBVk07RUFrQmQsU0FBTztFQUNMRCxVQUFNLFlBREQ7RUFFTGMsVUFBTSxjQUFTQyxDQUFULEVBQVk7RUFDaEIsVUFBSVEsS0FBSyxDQUFDQyxRQUFWO0VBQ0FULFFBQUVFLEdBQUYsQ0FBTSxVQUFTUSxDQUFULEVBQVk7RUFDaEJGLGFBQUtFLElBQUlGLEVBQUosR0FBU0UsQ0FBVCxHQUFhRixFQUFsQjtFQUNELE9BRkQ7RUFHQSxhQUFPQSxFQUFQO0VBQ0QsS0FSSTtFQVNMdEIsV0FBTyxDQUFDLGFBQUQ7O0VBVEYsR0FsQk87RUE4QmQsU0FBTztFQUNMRCxVQUFNLFlBREQ7RUFFTGMsVUFBTSxjQUFTQyxDQUFULEVBQVk7RUFDaEIsVUFBSVcsS0FBS0YsUUFBVDtFQUNBVCxRQUFFRSxHQUFGLENBQU0sVUFBU1EsQ0FBVCxFQUFZO0VBQ2hCQyxhQUFLRCxJQUFJQyxFQUFKLEdBQVNELENBQVQsR0FBYUMsRUFBbEI7RUFDRCxPQUZEO0VBR0EsYUFBT0EsRUFBUDtFQUNELEtBUkk7RUFTTHpCLFdBQU8sQ0FBQyxhQUFEO0VBVEYsR0E5Qk87RUF5Q2QsWUFBVTtFQUNSRCxVQUFNLFlBREU7RUFFUmMsVUFBTSxjQUFTQyxDQUFULEVBQVk7RUFDaEIsYUFBT0EsRUFBRUksTUFBVDtFQUNELEtBSk87RUFLUmxCLFdBQU8sQ0FBQyxPQUFELEVBQVMsYUFBVCxFQUF1QixhQUF2QixFQUFxQyxhQUFyQztFQUxDOztFQXpDSSxDQUFoQjs7O0FDZUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQUFBOzs7QUFqQkEsRUFFQSwrQkFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQ1lBOzs7R0FBQTs7QUFLQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQUFBOzs7QUFuQkEsRUFFQSwrQkFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNnQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBQUE7OztBQWxCQSxFQUVBLCtCQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDOERBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FBQTs7O0FBaEVBLEVBRUEsK0JBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUNGQSxTQUFTMEIsYUFBVCxDQUF1QkMsSUFBdkIsRUFBNkI7RUFDM0I7RUFDQSxNQUFJQyxjQUFjRCxJQUFkLHlDQUFjQSxJQUFkLENBQUo7O0VBRUE7Ozs7RUFJQSxNQUFJLENBQUMsUUFBRCxFQUFXLFFBQVgsRUFBcUJqQixRQUFyQixDQUE4QmtCLElBQTlCLENBQUosRUFBeUM7RUFBRSxXQUFPQSxJQUFQO0VBQWM7O0VBRXpEOzs7Ozs7RUFNQSxNQUFJQSxRQUFRLFFBQVosRUFBc0I7RUFBQyxXQUFPQSxJQUFQO0VBQWE7O0VBRXBDO0VBQ0EsTUFBSUMsTUFBTUMsT0FBTixDQUFjSCxJQUFkLENBQUosRUFBeUI7RUFDdkIsUUFBSUMsUUFBTyxPQUFYOztFQUVBLFFBQUlHLGlCQUFpQkosS0FBS1gsR0FBTCxDQUFTLFVBQVNnQixDQUFULEVBQVc7RUFBQyxvQkFBY0EsQ0FBZCx5Q0FBY0EsQ0FBZDtFQUFpQixLQUF0QyxDQUFyQjtFQUNBLFFBQUlDLGdCQUFnQkYsZUFBZSxDQUFmLENBQXBCO0VBQ0EsUUFBSUcsV0FBV0gsZUFBZUksS0FBZixDQUFxQixVQUFTSCxDQUFULEVBQVc7RUFBQyxhQUFPQSxLQUFLQyxhQUFaO0VBQTJCLEtBQTVELENBQWY7O0VBRUEsUUFBSUMsUUFBSixFQUFjO0VBQUVOLGVBQVFLLGFBQVI7RUFBd0I7O0VBRXhDLFdBQU9MLEtBQVA7RUFDRDs7RUFFRCxTQUFPQSxJQUFQO0VBQ0Q7Ozs7QUN1QkQ7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBQUE7OztBQXZEQSxFQUVBLCtCQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDU0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQUFBOzs7QUFYQSxFQUVBLCtCQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNPQTs7Ozs7Ozs7Ozs7Ozs7O0dBQUE7OztBQVRBLEVBRUEsK0JBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2dCQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FBQTs7O0FBbEJBLEVBRUEsK0JBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ09BOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FBQTs7O0FBVEEsRUFFQSwrQkFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNtQkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FBQTs7O0FBckJBLEVBRUEsK0JBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1NBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBQUE7OztBQVhBLEVBRUEsK0JBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDZ0JBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FBQTs7O0FBbEJBLEVBRUEsK0JBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQzZCQTs7OztHQUFBOztBQU1BOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQUFBOzs7QUFyQ0EsRUFFQSwrQkFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1lBOzs7OztHQUFBOzs7QUFkQSxFQUVBLCtCQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN5QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBQUE7OztBQTNCQSxFQUVBLCtCQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQ1VBOzs7R0FBQTtBQUlBOzs7Ozs7OztHQUFBOzs7QUFoQkEsRUFFQSwrQkFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ1lBOzs7R0FBQTs7O0FBZEEsRUFFQSwrQkFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VDR08sU0FBU1EsZUFBVCxDQUF5QkMsTUFBekIsRUFBaUNDLEtBQWpDLEVBQXdDO0VBQzdDLE1BQUlqQixRQUFRZ0IsT0FBT0MsS0FBUCxDQUFaO0VBQ0EsTUFBSSxDQUFDakIsS0FBTCxFQUFZO0VBQUUsV0FBTyxFQUFQO0VBQVk7RUFDMUIsU0FBT0EsTUFBTWtCLFFBQU4sR0FBaUJDLFdBQWpCLEVBQVA7RUFDRDs7RUFHRCxTQUFTQyxVQUFULENBQW9CdkMsQ0FBcEIsRUFBdUJDLENBQXZCLEVBQTBCdUMsSUFBMUIsRUFBZ0M7RUFDOUIsTUFBSUMsSUFBSUQsT0FBTyxDQUFDLENBQVIsR0FBWSxDQUFwQjtFQUNBLE1BQUl4QyxLQUFLQyxDQUFULEVBQVk7RUFBQyxXQUFPLENBQVA7RUFBVTtFQUN2QixTQUFPRCxJQUFJQyxDQUFKLEdBQVEsSUFBSXdDLENBQVosR0FBZ0IsQ0FBQyxDQUFELEdBQUtBLENBQTVCO0VBQ0Q7O0VBRUQsU0FBU0MsTUFBVCxDQUFnQjFDLENBQWhCLEVBQW1CO0VBQ2pCLE1BQ0VBLEtBQUsyQyxTQUFMLElBQ0EsUUFBTzNDLENBQVAseUNBQU9BLENBQVAsTUFBWTJDLFNBRFosSUFFQUMsTUFBTTVDLENBQU4sS0FBWSxPQUFPQSxDQUFQLElBQWEsUUFGekIsSUFHQUEsS0FBSyxJQUpQLEVBS0U7RUFBRSxXQUFPLEtBQVA7RUFBZTtFQUNuQixTQUFPLElBQVA7RUFDRDs7RUFFRCxTQUFTNkMsZUFBVCxDQUF5QjdDLENBQXpCLEVBQTRCO0VBQzFCLE1BQUksQ0FBQzBDLE9BQU8xQyxDQUFQLENBQUwsRUFBZ0I7RUFBQyxXQUFPMkMsU0FBUDtFQUFrQjtFQUNuQyxTQUFPM0MsQ0FBUDtFQUNEOztFQUVELFNBQVM4QyxVQUFULENBQW9COUMsQ0FBcEIsRUFBdUJDLENBQXZCLEVBQTBCdUMsSUFBMUIsRUFBZ0M7RUFDOUIsU0FBT0EsT0FBT3hDLElBQUlDLENBQVgsR0FBZUEsSUFBSUQsQ0FBMUI7RUFDRDs7QUFFRCxFQUFPLFNBQVMrQyxZQUFULENBQXNCQyxHQUF0QixFQUEyQkMsT0FBM0IsRUFBb0NDLElBQXBDLEVBQTBDQyxnQkFBMUMsRUFBNEQ7RUFDakUsTUFBSUMsU0FBU0MsT0FBT0MsSUFBUCxDQUFZSixJQUFaLENBQWI7RUFDQSxNQUFJLENBQUNFLE9BQU9wQyxNQUFaLEVBQW9CO0VBQUMsV0FBT2dDLEdBQVA7RUFBWTs7RUFFakMsTUFBSU8saUJBQWlCSCxPQUFPdEMsR0FBUCxDQUNuQixVQUFTc0IsS0FBVCxFQUFlOztFQUViLFFBQUlvQixZQUFZLG1CQUFDckIsTUFBRCxFQUFTQyxLQUFULEVBQWlCO0VBQUMsYUFBT0QsT0FBT0MsS0FBUCxDQUFQO0VBQXNCLEtBQXhEO0VBQ0EsUUFBSXFCLGFBQWFQLEtBQUtkLEtBQUwsRUFBWXNCLEtBQVosSUFBcUIsV0FBdEM7O0VBRUEsUUFBSXRCLFNBQVNlLGdCQUFiLEVBQStCO0VBQUVLLGtCQUFZTCxpQkFBaUJmLEtBQWpCLENBQVo7RUFBc0M7O0VBRXZFLFdBQU8sVUFBU3BDLENBQVQsRUFBWUMsQ0FBWixFQUFlO0VBQ3BCLFVBQUlXLElBQUlpQyxnQkFBZ0JXLFVBQVV4RCxDQUFWLEVBQWFvQyxLQUFiLENBQWhCLENBQVI7RUFDQSxVQUFJdUIsSUFBSWQsZ0JBQWdCVyxVQUFVdkQsQ0FBVixFQUFhbUMsS0FBYixDQUFoQixDQUFSO0VBQ0E7RUFDQTs7RUFFQSxVQUFJVixJQUFKO0VBQ0EsVUFBSSxDQUFDZCxDQUFELEVBQUkrQyxDQUFKLEVBQU9DLElBQVAsQ0FBWSxVQUFTOUIsQ0FBVCxFQUFXO0VBQUMsZUFBTyxPQUFPQSxDQUFQLElBQVksUUFBbkI7RUFBNkIsT0FBckQsQ0FBSixFQUE0RDtFQUFDSixlQUFLLFFBQUw7RUFBZTtFQUM1RSxVQUFJLENBQUNkLENBQUQsRUFBSStDLENBQUosRUFBT0MsSUFBUCxDQUFZLFVBQVM5QixDQUFULEVBQVc7RUFBQyxlQUFPLE9BQU9BLENBQVAsSUFBWSxRQUFuQjtFQUE2QixPQUFyRCxDQUFKLEVBQTREO0VBQUNKLGVBQUssUUFBTDtFQUFlO0VBQzVFOztFQUVBLFVBQUlBLFFBQVEsUUFBWixFQUFzQjtFQUNwQixZQUFJLENBQUNnQixPQUFPOUIsQ0FBUCxDQUFMLEVBQWdCO0VBQUNBLGNBQUksRUFBSjtFQUFRO0VBQ3pCLFlBQUksQ0FBQzhCLE9BQU9pQixDQUFQLENBQUwsRUFBZ0I7RUFBQ0EsY0FBSSxFQUFKO0VBQVE7RUFDekIsZUFBT3BCLFdBQVczQixDQUFYLEVBQWMrQyxDQUFkLEVBQWlCRixVQUFqQixDQUFQO0VBQ0QsT0FKRCxNQUtLLElBQUkvQixRQUFRLFFBQVosRUFBc0I7RUFDekIsWUFBSSxDQUFDZ0IsT0FBTzlCLENBQVAsQ0FBTCxFQUFnQjtFQUFDQSxjQUFJUyxRQUFKO0VBQWM7RUFDL0IsWUFBSSxDQUFDcUIsT0FBT2lCLENBQVAsQ0FBTCxFQUFnQjtFQUFDQSxjQUFJdEMsUUFBSjtFQUFjO0VBQy9CLGVBQU95QixXQUFXbEMsQ0FBWCxFQUFjK0MsQ0FBZCxFQUFpQkYsVUFBakIsQ0FBUDtFQUNELE9BSkksTUFLQTtFQUNILGVBQU8sQ0FBQyxDQUFSO0VBQ0Q7RUFFRixLQXpCRDtFQTBCSCxHQWxDb0IsQ0FBckI7O0VBcUNBLE1BQUlJLFNBQVNiLElBQUlFLElBQUosQ0FBUyxVQUFTbEQsQ0FBVCxFQUFZQyxDQUFaLEVBQWU7RUFDbkMsU0FBSyxJQUFJNkQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJUCxlQUFldkMsTUFBbkMsRUFBMkM4QyxHQUEzQyxFQUFnRDtFQUM5QyxVQUFJQyxJQUFJUixlQUFlTyxDQUFmLENBQVI7RUFDQSxVQUFJRSxJQUFJRCxFQUFFZCxRQUFRakQsQ0FBUixDQUFGLEVBQWNpRCxRQUFRaEQsQ0FBUixDQUFkLENBQVI7RUFDQSxVQUFJLENBQUMrRCxDQUFMLEVBQVE7RUFBRTtFQUFXLE9BQXJCLE1BQ0s7RUFBQztFQUFPO0VBQ2Q7RUFDRCxXQUFPQSxDQUFQO0VBQ0QsR0FSWSxDQUFiO0VBU0E7RUFDQSxTQUFPSCxNQUFQO0VBQ0Q7O0FBRUQsRUFBTyxTQUFTSSxlQUFULENBQXlCQyxVQUF6QixFQUFxQ2xCLEdBQXJDLEVBQTBDQyxPQUExQyxFQUFtREcsTUFBbkQsRUFBMkRlLG9CQUEzRCxFQUFpRjtFQUN0RixNQUFJQyxNQUFNLElBQUlDLE1BQUosQ0FBV0gsVUFBWCxFQUF1QixJQUF2QixDQUFWO0VBQ0EsTUFBSUksR0FBSjs7RUFFQSxNQUFJSixjQUFjLEVBQWxCLEVBQXNCO0VBQ3BCSSxVQUFNdEIsR0FBTjtFQUNELEdBRkQsTUFHSztFQUNIc0IsVUFBTSxFQUFOOztFQUVBdEIsUUFBSWxDLEdBQUosQ0FBUSxVQUFTeUQsRUFBVCxFQUFhOztFQUVuQixVQUFJcEMsU0FBU2MsUUFBUXNCLEVBQVIsQ0FBYjs7RUFFQSxVQUFJQyxZQUFZcEIsT0FBT3RDLEdBQVAsQ0FDZCxVQUFTaUQsQ0FBVCxFQUFZO0VBQ1YsWUFBSUEsS0FBS0ksb0JBQVQsRUFBK0I7RUFDN0IsaUJBQU9BLHFCQUFxQkosQ0FBckIsRUFBd0JRLEVBQXhCLEVBQTRCcEMsTUFBNUIsRUFBb0M0QixDQUFwQyxDQUFQO0VBQ0Q7RUFDRCxlQUFPN0IsZ0JBQWdCQyxNQUFoQixFQUF3QjRCLENBQXhCLENBQVA7RUFDRCxPQU5hLEVBT2RVLElBUGMsQ0FPVCxFQVBTLENBQWhCOztFQVNBLFVBQUlDLFFBQVFGLFVBQVVFLEtBQVYsQ0FBZ0JOLEdBQWhCLENBQVo7O0VBRUEsVUFDRSxFQUFFTSxTQUFTLElBQVQsSUFBaUJBLE1BQU1ELElBQU4sQ0FBVyxFQUFYLEtBQWtCLEVBQXJDLENBREYsRUFFRTtFQUNBSCxZQUFJSyxJQUFKLENBQVNKLEVBQVQ7RUFDRDtFQUVGLEtBckJEO0VBc0JEO0VBQ0QsU0FBT0QsR0FBUDtFQUNEOztBQUdELEVBQU8sU0FBU00sWUFBVCxDQUFzQjVCLEdBQXRCLEVBQTJCQyxPQUEzQixFQUFvQztFQUFBOztFQUN6QyxNQUFJNEIsa0JBQWtCLFlBQUdDLE1BQUgsK0JBQ2pCOUIsSUFBSWxDLEdBQUosQ0FDRCxVQUFTaUUsUUFBVCxFQUFrQjtFQUNoQixXQUFPMUIsT0FBT0MsSUFBUCxDQUFZTCxRQUFROEIsUUFBUixDQUFaLENBQVA7RUFDRCxHQUhBLENBRGlCLEVBQXRCO0VBTUEscUNBQVcsSUFBSUMsR0FBSixDQUFRSCxlQUFSLENBQVg7RUFDRDs7OztBQzVERDs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQUFBOzs7QUE1RUEsRUFFQSwrQkFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7RUNGTyxJQUFNSSxzQkFBc0I7RUFDakMsS0FBRyxRQUQ4QjtFQUVqQyxLQUFHLFFBRjhCO0VBR2pDLEtBQUcsU0FIOEI7RUFJakMsS0FBRyxTQUo4QjtFQUtqQyxLQUFHO0VBTDhCLENBQTVCO0FBT1AsRUFBTyxJQUFNQyxrQkFBa0I7RUFDN0IsT0FBSyxVQUR3QjtFQUU3QixPQUFLLHFCQUZ3QjtFQUc3QixPQUFLLFlBSHdCO0VBSTdCLE9BQUssYUFKd0I7RUFLN0IsT0FBSyxJQUx3QjtFQU03QixPQUFLLFNBTndCO0VBTzdCLE9BQUssVUFQd0I7RUFRN0IsT0FBSywrQkFSd0I7RUFTN0IsT0FBSyxZQVR3QjtFQVU3QixPQUFLLGNBVndCO0VBVzdCLE9BQUssaUJBWHdCO0VBWTdCLE9BQUssY0Fad0I7RUFhN0IsT0FBSyxrQkFid0I7RUFjN0IsT0FBSyxjQWR3QjtFQWU3QixPQUFLLFNBZndCO0VBZ0I3QixPQUFLLGtCQWhCd0I7RUFpQjdCLE9BQUssbUJBakJ3QjtFQWtCN0IsT0FBSyxPQWxCd0I7RUFtQjdCLE9BQUssV0FuQndCO0VBb0I3QixPQUFLLGNBcEJ3QjtFQXFCN0IsT0FBSyxZQXJCd0I7RUFzQjdCLE9BQUssY0F0QndCO0VBdUI3QixPQUFLLG9CQXZCd0I7RUF3QjdCLE9BQUssb0JBeEJ3QjtFQXlCN0IsT0FBSyxhQXpCd0I7RUEwQjdCLE9BQUssY0ExQndCO0VBMkI3QixPQUFLLGtCQTNCd0I7RUE0QjdCLE9BQUssV0E1QndCO0VBNkI3QixPQUFLLFdBN0J3QjtFQThCN0IsT0FBSyxvQkE5QndCO0VBK0I3QixPQUFLLGVBL0J3QjtFQWdDN0IsT0FBSywrQkFoQ3dCO0VBaUM3QixPQUFLLG1CQWpDd0I7RUFrQzdCLE9BQUssVUFsQ3dCO0VBbUM3QixPQUFLLE1BbkN3QjtFQW9DN0IsT0FBSyxpQkFwQ3dCO0VBcUM3QixPQUFLLHFCQXJDd0I7RUFzQzdCLE9BQUssa0JBdEN3QjtFQXVDN0IsT0FBSyxjQXZDd0I7RUF3QzdCLE9BQUssd0JBeEN3QjtFQXlDN0IsT0FBSyx1QkF6Q3dCO0VBMEM3QixPQUFLLG9CQTFDd0I7RUEyQzdCLE9BQUssZUEzQ3dCO0VBNEM3QixPQUFLLHVCQTVDd0I7RUE2QzdCLE9BQUssc0JBN0N3QjtFQThDN0IsT0FBSyxRQTlDd0I7RUErQzdCLE9BQUssbUJBL0N3QjtFQWdEN0IsT0FBSyxrQkFoRHdCO0VBaUQ3QixPQUFLLHVCQWpEd0I7RUFrRDdCLE9BQUssb0JBbER3QjtFQW1EN0IsT0FBSyxpQ0FuRHdCO0VBb0Q3QixPQUFLLCtCQXBEd0I7RUFxRDdCLE9BQUssdUJBckR3QjtFQXNEN0IsT0FBSyxpQkF0RHdCO0VBdUQ3QixPQUFLLGFBdkR3QjtFQXdEN0IsT0FBSyxxQkF4RHdCO0VBeUQ3QixPQUFLLGlCQXpEd0I7RUEwRDdCLE9BQUssNEJBMUR3QjtFQTJEN0IsT0FBSyx5QkEzRHdCO0VBNEQ3QixPQUFLLHNCQTVEd0I7RUE2RDdCLE9BQUssZUE3RHdCO0VBOEQ3QixPQUFLLGNBOUR3QjtFQStEN0IsT0FBSztFQS9Ed0IsQ0FBeEI7Ozs7QUNtRFA7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBQUE7OztBQTFEQSxFQUVBLCtCQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNPQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQUFBOzs7QUFUQSxFQUVBLCtCQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ09BOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBQUE7OztBQVRBLEVBRUEsK0JBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDU0E7Ozs7Ozs7Ozs7OztHQUFBOzs7QUFYQSxFQUVBLCtCQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VDRkEsSUFBSUMsa0JBQWtCLFNBQWxCQSxlQUFrQixDQUFTQyxNQUFULEVBQWlCO0VBQ3JDLFNBQU8vQixPQUFPZ0MsT0FBUCxDQUFlRCxNQUFmLEVBQXVCbEMsSUFBdkIsQ0FBNEIsVUFBU2xELENBQVQsRUFBWUMsQ0FBWixFQUFlO0VBQ2hELFFBQUlxRixLQUFLdEYsRUFBRSxDQUFGLENBQVQ7RUFDQSxRQUFJdUYsS0FBS3RGLEVBQUUsQ0FBRixDQUFUO0VBQ0EsV0FBUXFGLEtBQUtDLEVBQUwsSUFBV0EsR0FBR3ZFLE1BQUgsR0FBWXNFLEdBQUd0RSxNQUFsQztFQUNELEdBSk0sRUFJSkMsTUFKSSxDQUlHLFVBQVN1RSxDQUFULFFBQW9CO0VBQUE7RUFBQSxRQUFQL0MsQ0FBTztFQUFBLFFBQUp1QixDQUFJOztFQUM1QndCLE1BQUUvQyxDQUFGLElBQU91QixDQUFQO0VBQ0EsV0FBT3dCLENBQVA7RUFDRCxHQVBNLEVBT0osRUFQSSxDQUFQO0VBUUQsQ0FURDs7RUFXQSxJQUFJQyxxQkFBcUI7RUFDdkI3RixNQUFJLEdBRG1CO0VBRXZCTSxPQUFLLEdBRmtCO0VBR3ZCRSxNQUFJLEdBSG1CO0VBSXZCRCxNQUFJLEdBSm1CO0VBS3ZCRyxPQUFLLEdBTGtCO0VBTXZCRCxPQUFLLEdBTmtCO0VBT3ZCRyxZQUFVO0VBUGEsQ0FBekI7O0VBVUEsSUFBSWtGLGVBQWU7RUFDakJDLE9BQUssR0FEWTtFQUVqQkMsTUFBSTtFQUZhLENBQW5COztFQUtBLElBQUlDLG9CQUFvQjtFQUN0QmpHLE1BQUksSUFEa0I7RUFFdEJrRyxTQUFPLElBRmU7RUFHdEIsT0FBSyxJQUhpQjtFQUl0QixRQUFNLEtBSmdCO0VBS3RCLE9BQUssS0FMaUI7RUFNdEIsT0FBSyxJQU5pQjtFQU90QixPQUFLLEtBUGlCO0VBUXRCLFFBQU0sS0FSZ0I7RUFTdEIsT0FBSyxJQVRpQjtFQVV0QixPQUFLLEtBVmlCO0VBV3RCLFFBQU0sS0FYZ0I7RUFZdEIsUUFBTSxJQVpnQjtFQWF0QixpQkFBZSxJQWJPO0VBY3RCLFlBQVUsS0FkWTtFQWV0QjVGLE9BQUssS0FmaUI7RUFnQnRCLGtCQUFnQixLQWhCTTtFQWlCdEIsZUFBYSxLQWpCUztFQWtCdEIscUJBQW1CLEtBbEJHO0VBbUJ0QkUsTUFBSSxJQW5Ca0I7RUFvQnRCLGtCQUFnQixJQXBCTTtFQXFCdEIsZUFBYSxJQXJCUztFQXNCdEJELE1BQUksSUF0QmtCO0VBdUJ0QiwyQkFBeUIsS0F2Qkg7RUF3QnRCLDhCQUE0QixLQXhCTjtFQXlCdEIsZUFBYSxVQXpCUztFQTBCdEI0RixhQUFXLFVBMUJXO0VBMkJ0QkMsWUFBVSxVQTNCWTtFQTRCdEJ4RixZQUFVO0VBNUJZLENBQXhCOztFQWdDQSxJQUFJeUYsaUJBQWlCO0VBQ25CQyxPQUFLLEtBRGM7RUFFbkJDLFdBQVMsS0FGVTtFQUduQkMsV0FBUyxLQUhVO0VBSW5CQyxPQUFLLEtBSmM7RUFLbkJyRixVQUFRLFFBTFc7RUFNbkJzRixPQUFLLFFBTmM7RUFPbkJDLFFBQU0sTUFQYTtFQVFuQkMsVUFBUSxRQVJXO0VBU25CQyxXQUFTLE1BVFU7RUFVbkJDLFlBQVU7RUFWUyxDQUFyQjs7RUFhQSxJQUFJQyxjQUFjO0VBQ2hCaEIsT0FBSyxLQURXO0VBRWhCQyxNQUFJO0VBRlksQ0FBbEI7O0VBTUFLLGlCQUFpQmQsZ0JBQWdCYyxjQUFoQixDQUFqQjtFQUNBVSxjQUFjeEIsZ0JBQWdCd0IsV0FBaEIsQ0FBZDtFQUNBZCxvQkFBb0JWLGdCQUFnQlUsaUJBQWhCLENBQXBCOztFQUVBLElBQUllLGdCQUFnQixTQUFoQkEsYUFBZ0IsQ0FBUy9HLElBQVQsRUFBZWdILE1BQWYsRUFBeUY7RUFBQSxNQUFsRUMsUUFBa0UsdUVBQXZEbkUsU0FBdUQ7RUFBQSxNQUE1Q29FLEtBQTRDLHVFQUFwQ3BFLFNBQW9DO0VBQUEsTUFBekJxRSxlQUF5Qix1RUFBUCxLQUFPOztFQUMzRyxNQUFJQyxRQUFRSCxRQUFaO0VBQ0EsTUFBSUksTUFBTUgsVUFBVXBFLFNBQVYsR0FBc0I5QyxJQUF0QixHQUE2QkEsS0FBS3NILEtBQUwsQ0FBVyxDQUFYLEVBQWNKLEtBQWQsQ0FBdkM7RUFDQSxPQUFLLElBQUlLLENBQVQsSUFBY1AsTUFBZCxFQUFzQjtFQUNwQixRQUFJSyxJQUFJMUcsUUFBSixDQUFhNEcsQ0FBYixDQUFKLEVBQXFCO0VBQ25CSCxjQUFRSixPQUFPTyxDQUFQLENBQVI7RUFDQSxVQUFJSixlQUFKLEVBQXFCO0VBQ25CbkgsZUFBT0EsS0FBS3NILEtBQUwsQ0FBV0QsSUFBSUcsT0FBSixDQUFZRCxDQUFaLElBQWlCQSxFQUFFcEcsTUFBOUIsRUFBc0NrRyxJQUFJbEcsTUFBMUMsQ0FBUDtFQUNELE9BRkQsTUFFTztFQUNMbkIsZUFBT0EsS0FBS3lILE9BQUwsQ0FBYUYsQ0FBYixFQUFnQixFQUFoQixDQUFQO0VBQ0Q7RUFDRDtFQUNEO0VBQ0Y7RUFDRCxTQUFPLENBQUNILEtBQUQsRUFBUXBILElBQVIsQ0FBUDtFQUNELENBZkQ7O0VDakZBLElBQUl1RCxTQUFTLFNBQVRBLE1BQVMsQ0FBU0gsT0FBVCxFQUFrQjtFQUFBOztFQUM3QixNQUFJc0UsWUFBWXZFLElBQUlDLE9BQUosQ0FBaEI7RUFDQSxNQUFJNEIsa0JBQWtCLFlBQUdDLE1BQUgsK0JBQ2pCeUMsVUFBVXpHLEdBQVYsQ0FBYyxVQUFTaUUsUUFBVCxFQUFtQjtFQUNsQyxXQUFPMUIsT0FBT0MsSUFBUCxDQUFZTCxRQUFROEIsUUFBUixDQUFaLENBQVA7RUFDRCxHQUZFLENBRGlCLEVBQXRCO0VBS0EscUNBQVcsSUFBSUMsR0FBSixDQUFRSCxlQUFSLENBQVg7RUFDRCxDQVJEOztFQVVBLElBQUkyQyxjQUFjLFNBQWRBLFdBQWMsQ0FBU3ZFLE9BQVQsRUFBa0I7RUFDbEMsTUFBSXdFLGVBQWVyRSxPQUFPSCxPQUFQLENBQW5CO0VBQ0EsTUFBSTRELFNBQVMsRUFBYjtFQUNBWSxlQUFhM0csR0FBYixDQUFpQixVQUFTc0IsS0FBVCxFQUFnQjtFQUMvQnlFLFdBQU96RSxLQUFQLElBQWdCQSxLQUFoQjtFQUNELEdBRkQ7RUFHQSxTQUFPeUUsTUFBUDtFQUNELENBUEQ7O0VBU0EsSUFBSTdELE1BQU0sU0FBTkEsR0FBTSxDQUFTQyxPQUFULEVBQWtCO0VBQzFCLFNBQU9JLE9BQU9DLElBQVAsQ0FBWUwsT0FBWixDQUFQO0VBQ0QsQ0FGRDs7RUFJQSxJQUFJeUUsVUFBVSxTQUFWQSxPQUFVLENBQVM3SCxJQUFULEVBQWVnSCxNQUFmLEVBQXVCO0VBQ25DLE1BQUljLFFBQVEsSUFBSXRELE1BQUosQ0FBV3hFLElBQVgsRUFBaUIsSUFBakIsQ0FBWjs7RUFFQSxNQUFJK0gsY0FBYyxFQUFsQjs7RUFFQWYsU0FBT2dCLE1BQVAsQ0FBYyxVQUFTWixLQUFULEVBQWdCO0VBQzVCLFFBQUl2QyxRQUFRdUMsTUFBTXZDLEtBQU4sQ0FBWWlELEtBQVosQ0FBWjtFQUNBOztFQUVBLFFBQUlHLFFBQVEsRUFBRXBELFNBQVMsSUFBVCxJQUFpQkEsTUFBTUQsSUFBTixDQUFXLEVBQVgsTUFBbUIsRUFBdEMsQ0FBWjs7RUFFQSxRQUFJcUQsS0FBSixFQUFXO0VBQ1RGLGtCQUFZWCxLQUFaLElBQXFCdkMsTUFBTSxDQUFOLENBQXJCO0VBQ0Q7RUFDRCxXQUFPb0QsS0FBUDtFQUNELEdBVkQ7O0VBWUFGLGdCQUFjdkUsT0FBT0MsSUFBUCxDQUFZc0UsV0FBWixFQUF5QjFFLElBQXpCLENBQThCLFVBQVNsRCxDQUFULEVBQVlDLENBQVosRUFBZTtFQUN6RCxXQUFPSixLQUFLd0gsT0FBTCxDQUFhTyxZQUFZM0gsQ0FBWixDQUFiLElBQStCSixLQUFLd0gsT0FBTCxDQUFhTyxZQUFZNUgsQ0FBWixDQUFiLENBQXRDO0VBQ0QsR0FGYSxDQUFkO0VBR0EsU0FBTzRILFdBQVA7RUFDRCxDQXJCRDs7RUF1QkEsSUFBSUcsK0JBQStCLFNBQS9CQSw0QkFBK0IsQ0FBU0MsT0FBVCxFQUFrQjtFQUNuRCxNQUFJQyxXQUFXLEVBQWY7O0VBRUEsT0FBSyxJQUFJbkUsSUFBSSxDQUFiLEVBQWdCQSxJQUFJa0UsUUFBUWhILE1BQTVCLEVBQW9DOEMsR0FBcEMsRUFBeUM7RUFDdkMsUUFBSW9FLFFBQVFGLFFBQVFsRSxDQUFSLEVBQVdvRSxLQUF2QjtFQUNBLFFBQUlBLFNBQVMsS0FBYixFQUFvQjtFQUNsQkQsZUFBU3RELElBQVQsQ0FBYyxDQUFDcUQsUUFBUWxFLENBQVIsQ0FBRCxDQUFkO0VBQ0QsS0FGRCxNQUVPO0VBQ0wsVUFBSXFFLE9BQU9GLFNBQVNBLFNBQVNqSCxNQUFULEdBQWtCLENBQTNCLENBQVg7RUFDQSxVQUFJbUgsUUFBUXhGLFNBQVosRUFBdUI7RUFDckJxRixnQkFBUWxFLENBQVIsRUFBV29FLEtBQVgsR0FBbUIsS0FBbkI7RUFDQUQsaUJBQVN0RCxJQUFULENBQWMsQ0FBQ3FELFFBQVFsRSxDQUFSLENBQUQsQ0FBZDtFQUNELE9BSEQsTUFHTztFQUNMbUUsaUJBQVNBLFNBQVNqSCxNQUFULEdBQWtCLENBQTNCLEVBQThCMkQsSUFBOUIsQ0FBbUNxRCxRQUFRbEUsQ0FBUixDQUFuQztFQUNEO0VBQ0Y7O0VBRUQ7RUFDRDs7RUFFRCxTQUFPbUUsUUFBUDtFQUNELENBckJEOztFQXVCQSxJQUFJRyxpQkFBaUIsU0FBakJBLGNBQWlCLENBQVN2SSxJQUFULEVBQWU7RUFDbEMsTUFBSUEsS0FBSyxDQUFMLE1BQVksR0FBaEIsRUFBcUI7RUFDbkJBLFdBQU9BLEtBQUtzSCxLQUFMLENBQVcsQ0FBWCxFQUFjdEgsS0FBS21CLE1BQW5CLENBQVA7RUFDRDtFQUNELE1BQUluQixLQUFLQSxLQUFLbUIsTUFBTCxHQUFjLENBQW5CLE1BQTBCLEdBQTlCLEVBQW1DO0VBQ2pDbkIsV0FBT0EsS0FBS3NILEtBQUwsQ0FBVyxDQUFYLEVBQWN0SCxLQUFLbUIsTUFBTCxHQUFjLENBQTVCLENBQVA7RUFDRDtFQUNELFNBQU9uQixJQUFQO0VBQ0QsQ0FSRDs7OztBQ2hDQTs7Ozs7Ozs7Ozs7OztHQUFBOzs7QUFyQ0EsRUFFQSwrQkFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUN1QkE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7R0FBQTs7O0FBekJBLEVBRUEsK0JBQUE7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDc0RBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0dBQUE7OztBQXhEQSxFQUVBLCtCQUFBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0VDRkEsSUFBSXdJLFNBQVMsQ0FDWCxTQURXLEVBRVgsU0FGVyxFQUdYLFNBSFcsRUFJWCxTQUpXLEVBS1gsU0FMVyxFQU1YLFNBTlcsRUFPWCxTQVBXLEVBUVgsU0FSVyxFQVNYLFNBVFcsRUFVWCxTQVZXLEVBV1gsU0FYVyxFQVlYLFNBWlcsRUFhWCxTQWJXLEVBY1gsU0FkVyxFQWVYLFNBZlcsRUFnQlgsU0FoQlcsRUFpQlgsU0FqQlcsRUFrQlgsU0FsQlcsQ0FBYjs7RUFxQkEsU0FBU0Msa0JBQVQsQ0FBNEJDLEdBQTVCLEVBQWlDQyxHQUFqQyxFQUFzQztFQUNwQztFQUNBLE1BQUlDLFdBQVcsS0FBZjs7RUFFQSxNQUFJRixJQUFJLENBQUosS0FBVSxHQUFkLEVBQW1CO0VBQ2pCQSxVQUFNQSxJQUFJcEIsS0FBSixDQUFVLENBQVYsQ0FBTjtFQUNBc0IsZUFBVyxJQUFYO0VBQ0Q7O0VBRUQsTUFBSUMsTUFBTUMsU0FBU0osR0FBVCxFQUFjLEVBQWQsQ0FBVjs7RUFFQSxNQUFJSyxJQUFJLENBQUNGLE9BQU8sRUFBUixJQUFjRixHQUF0Qjs7RUFFQSxNQUFJSSxJQUFJLEdBQVIsRUFBYUEsSUFBSSxHQUFKLENBQWIsS0FDSyxJQUFJQSxJQUFJLENBQVIsRUFBV0EsSUFBSSxDQUFKOztFQUVoQixNQUFJM0ksSUFBSSxDQUFFeUksT0FBTyxDQUFSLEdBQWEsTUFBZCxJQUF3QkYsR0FBaEM7O0VBRUEsTUFBSXZJLElBQUksR0FBUixFQUFhQSxJQUFJLEdBQUosQ0FBYixLQUNLLElBQUlBLElBQUksQ0FBUixFQUFXQSxJQUFJLENBQUo7O0VBRWhCLE1BQUk0SSxJQUFJLENBQUNILE1BQU0sUUFBUCxJQUFtQkYsR0FBM0I7O0VBRUEsTUFBSUssSUFBSSxHQUFSLEVBQWFBLElBQUksR0FBSixDQUFiLEtBQ0ssSUFBSUEsSUFBSSxDQUFSLEVBQVdBLElBQUksQ0FBSjs7RUFFaEIsU0FBTyxDQUFDSixXQUFXLEdBQVgsR0FBaUIsRUFBbEIsSUFBd0IsQ0FBQ0ksSUFBSzVJLEtBQUssQ0FBVixHQUFnQjJJLEtBQUssRUFBdEIsRUFBMkJ2RyxRQUEzQixDQUFvQyxFQUFwQyxDQUEvQjtFQUNEOztFQUVELFNBQVN5RyxnQkFBVCxDQUEwQkMsSUFBMUIsRUFBZ0M7RUFDOUIsU0FBT0MsS0FBS0MsS0FBTCxDQUFXRixPQUFPLEdBQWxCLENBQVA7RUFDRDs7RUFFRCxTQUFTRyxNQUFULENBQWdCQyxNQUFoQixFQUF3QjtFQUN0QixNQUFJQyxXQUFXLEVBQWY7RUFDQSxPQUFLLElBQUl0RixJQUFJLENBQWIsRUFBZ0JBLElBQUlxRixPQUFPbkksTUFBM0IsRUFBbUM4QyxHQUFuQyxFQUF3QztFQUN0QyxRQUFJdUYsT0FBT0YsT0FBT3JGLENBQVAsQ0FBWDtFQUNBLFFBQUksY0FBY3dGLElBQWQsQ0FBbUJELElBQW5CLENBQUosRUFBOEI7RUFDNUJELGtCQUFZQyxJQUFaO0VBQ0Q7RUFDRjtFQUNELFNBQU9ELFFBQVA7RUFDRDs7RUFFRCxTQUFTRyxVQUFULENBQW9CSixNQUFwQixFQUE0QjtFQUMxQixNQUFJSyxPQUFPLEVBQVg7RUFDQSxNQUFJdEMsTUFBTWdDLE9BQU9DLE1BQVAsQ0FBVjtFQUNBLE9BQUssSUFBSXJGLElBQUksQ0FBYixFQUFnQkEsSUFBSW9ELElBQUlsRyxNQUF4QixFQUFnQzhDLEdBQWhDLEVBQXFDO0VBQ25DLFFBQUlvRCxJQUFJcEQsQ0FBSixNQUFXb0QsSUFBSXBELENBQUosRUFBTzJGLFdBQVAsRUFBZixFQUFxQztFQUNuQ0QsV0FBSzdFLElBQUwsQ0FBVXVDLElBQUlwRCxDQUFKLENBQVY7RUFDRDtFQUNGO0VBQ0QsU0FBTzBGLElBQVA7RUFDRDs7RUFFRCxTQUFTRSxVQUFULENBQW9CQyxJQUFwQixFQUEwQjtFQUN4QixNQUFJSCxPQUFPRCxXQUFXSSxLQUFLQyxLQUFMLENBQVcsR0FBWCxFQUFnQm5GLElBQWhCLENBQXFCLEVBQXJCLENBQVgsQ0FBWDtFQUNBLE1BQUlvRixRQUFRTCxLQUFLLENBQUwsQ0FBWjtFQUFBLE1BQ0VNLE9BQU8sRUFEVDtFQUVBLE1BQUlOLEtBQUt4SSxNQUFMLEdBQWMsQ0FBbEIsRUFBcUI7RUFDbkI4SSxXQUFPTixLQUFLQSxLQUFLeEksTUFBTCxHQUFjLENBQW5CLENBQVA7RUFDRDtFQUNELE1BQUkrSSxRQUFRRixRQUFRQyxJQUFwQjtFQUNBLE1BQUlDLFVBQVUsV0FBZCxFQUEyQjtFQUN6QixXQUFPQSxLQUFQO0VBQ0Q7RUFDRCxTQUFPSixLQUFLLENBQUwsRUFBUUYsV0FBUixFQUFQO0VBQ0Q7Ozs7RUN4REQsbUJBQUE7RUFBQSxJQUNBTyxnQkFEQTtFQUFBLElBRUFDLGVBRkE7RUFBQSxJQUdBQyxnQkFIQTs7QUFNQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztHQUFBOzs7QUF0Q0EsRUFFQSwrQkFBQTs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztFQ29DQSxJQUFJQyxNQUFNLEVBQVY7O0VBRUE7RUFDQTs7RUFFQUEsSUFBSUMsTUFBSixHQUFhQSxNQUFiO0VBQ0FELElBQUlFLFFBQUosR0FBZUEsS0FBZjs7RUFFQTtFQUNBO0VBQ0E7RUFDQTtFQUNBO0VBQ0E7RUFDQUYsSUFBSUcsbUJBQUosR0FBMEJBLG1CQUExQjs7RUFFQUgsSUFBSUksWUFBSixHQUFtQkEsWUFBbkI7O0VBRUFKLElBQUlLLFNBQUosR0FBZ0JBLFNBQWhCO0VBQ0E7RUFDQTs7RUFFQUwsSUFBSU0sV0FBSixHQUFrQkEsV0FBbEI7O0VBRUFOLElBQUlPLGlCQUFKLEdBQXdCQSxpQkFBeEI7RUFDQTtFQUNBOztFQUVBUCxJQUFJUSxVQUFKLEdBQWlCQSxVQUFqQjtFQUNBOztFQUVBUixJQUFJUyxVQUFKLEdBQWlCQSxVQUFqQjs7RUFFQUMsT0FBT1YsR0FBUCxHQUFhQSxHQUFiOzs7OyJ9
