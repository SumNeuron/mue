'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _uglifyjsWebpackPlugin = require('uglifyjs-webpack-plugin');

var _uglifyjsWebpackPlugin2 = _interopRequireDefault(_uglifyjsWebpackPlugin);

var _webpackNodeExternals = require('webpack-node-externals');

var _webpackNodeExternals2 = _interopRequireDefault(_webpackNodeExternals);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _getFilesList = require('./getFilesList');

var _getFilesList2 = _interopRequireDefault(_getFilesList);

var _webpack = require('webpack');

var _webpack2 = _interopRequireDefault(_webpack);

var _vueLoader = require('vue-loader');

var _vueLoader2 = _interopRequireDefault(_vueLoader);

var _vueTemplateCompiler = require('vue-template-compiler');

var _vueTemplateCompiler2 = _interopRequireDefault(_vueTemplateCompiler);

var _vueStyleLoader = require('vue-style-loader');

var _vueStyleLoader2 = _interopRequireDefault(_vueStyleLoader);

var _cssLoader = require('css-loader');

var _cssLoader2 = _interopRequireDefault(_cssLoader);

var _sassLoader = require('sass-loader');

var _sassLoader2 = _interopRequireDefault(_sassLoader);

var _nodeSass = require('node-sass');

var _nodeSass2 = _interopRequireDefault(_nodeSass);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// Import so it will be added as a dependency on Bit.
var modulesPath = _path2.default.normalize('' + __dirname + _path2.default.sep + '..' + _path2.default.sep + 'node_modules');

var getConfig = function getConfig(files, distPath) {
  return {
    entry: (0, _getFilesList2.default)(files),
    context: '' + __dirname + _path2.default.sep + '..$',
    resolve: {
      modules: [modulesPath, "node_modules"]
    },
    resolveLoader: {
      modules: [modulesPath, "node_modules"]
    },
    // Don't bundle node modules
    externals: [(0, _webpackNodeExternals2.default)()],
    output: {
      path: _path2.default.resolve(distPath),
      filename: '[name].js',
      libraryTarget: 'umd'
    },
    plugins: [new _uglifyjsWebpackPlugin2.default({
      sourceMap: true
    }), new _webpack2.default.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production')
    })],
    module: {
      loaders: [{
        test: /\.js$/,
        loader: 'vue-loader',
        include: __dirname,
        exclude: /node_modules/
      }, {
        test: /\.vue$/,
        loader: 'vue-loader'
      }, {
        test: /\.s[a|c]ss$/,
        loader: 'style!css!sass'
      }]
    }
  };
};

exports.default = getConfig;
module.exports = exports['default'];

//# sourceMappingURL=webpack.config.js.map