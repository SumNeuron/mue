let DataTable = Mue.DataTable;

// //<kbd class='code-version'>VERSION: 0.0.0</kbd>
let records = {
    a: {x: 1,  y: "a"},
    b: {x: 1,  y: "b"},
    c: {x: 1,  y: "c"},
    // d: {x: 1,  y: "d"},
    // e: {x: 2,  y: "? qwerty"},
    // f: {x: 4,  y: "asdasdas"},
    // g: {x: 4,  y: "asd asds"},
    // h: {x: 5,  y: "12 "},
    // i: {x: 1,  y: "12a "},
    // j: {x: 2,  y: "asd1 "},
    // k: {x: 3,  y: "s3 "},
    // l: {x: 4,  y: "asdaskdga "},
    // m: {x: 5,  y: "asd1kdga "}
}

// plots.datasets.plot.fieldFunction(
//       function(record, columnKey, recordValue) {
//
//         else if (['disease', 'organism', 'tissue', 'cell type', 'cell line'].includes(columnKey)) {
//           return  d3.keys(record[columnKey]).map(function(c, i){
//             return '<a target="_blank" href="'+record[columnKey][c]+'">'+c+'</a>'
//           }).join(', ')
//         }
//         else {return recordValue}
//       }
//     )

let ontologyFieldFunction = function(id, rec, field) {
  let keys = Object.keys(rec[field]).sort();
  let as = keys.map(function(col) {
    let href = rec[field][col];
    let text = col;
    return `<a target="_blank" href="${href}">${text}</a>`
  });
  return as.join(', ');
};

let fieldRenderFunctions = {
  'sRNA DE': function(id, rec, field) {
    let s = rec[field];
    let t = "DE Results";
    return `<a target="_blank" href='/comparison/de/${s}'>${t}</a>`;
  },
  'Pathogen DE': function(id, rec, field) {
    let s = rec[field];
    let t = "Pathogen Results";
    return `<a target="_blank" href='/comparison/pde/${s}'>${t}</a>`;
  },
  'sRNA Classification': function(id, rec, field) {
    let s = rec[field];
    let t = "Classification Results";
    return `<a target="_blank" href='/comparison/cl/${s}'>${t}</a>`;
  },
  'Oasis Output': function(id, rec, field) {
    let s = rec[field];
    let t = 'Results';
    return `<a target="_blank" href='https://oasis.dzne.de/share/GEO3_unzip/${s}/summary.html'>${t}</a>`;
  },
  'Disease': ontologyFieldFunction,
  'Organism': ontologyFieldFunction,
  'Tissue': ontologyFieldFunction,
  'Cell Type': ontologyFieldFunction,
  'Cell Line': ontologyFieldFunction,
};

let ontologyFieldSortingFunciton = function(record, field) {
  return Object.keys(record[field]).sort().join('');
}



let fieldSortingFunctions = {
  'sRNA DE': function(rec, field) { return "DE Results"; },
  'Pathogen DE': function(rec, field) { return "Pathogen Results"; },
  'sRNA Classification': function(rec, field) { return "Classification Results"; },
  'Oasis Output': function(rec, field) {return `Results`;},
  'Disease': ontologyFieldSortingFunciton,
  'Organism': ontologyFieldSortingFunciton,
  'Tissue': ontologyFieldSortingFunciton,
  'Cell Type': ontologyFieldSortingFunciton,
  'Cell Line': ontologyFieldSortingFunciton,
};


//  records = {
//    "GSE72193_fffdb92d34ab8cf7c09111c58369955b":{
//       "Dataset":"GSE72193",
//       "Group A":"3day",
//       "Group B":"120day",
//       "Covariates":"age-gender",
//       "sRNA DE":"GSE72193_3day_120day_age-gender_1",
//       "Pathogen DE":"GSE72193_3day_120day_age-gender_1",
//       "sRNA Classification":"GSE72193_3day_120day_1"
//    },
//    "GSE53600_9980a6601147bd61fe9260e841994916":{
//       "Dataset":"GSE53600",
//       "Group A":"healthy",
//       "Group B":"Merkel-cell-carcinoma",
//       "Covariates":"none",
//       "sRNA DE":"GSE53600_healthy_Merkel-cell-carcinoma_none_1",
//       "Pathogen DE":"GSE53600_healthy_Merkel-cell-carcinoma_none_1"
//    },
//    "GSE34137_8db132cd8c06cdbb79c1bf85fcb11f5a":{
//       "Dataset":"GSE34137",
//       "Group A":"Merkel-cell-carcinoma",
//       "Group B":"squamous-cell-carcinoma",
//       "Covariates":"none",
//       "sRNA DE":"GSE34137_Merkel-cell-carcinoma_squamous-cell-carcinoma_none_1",
//       "Pathogen DE":"GSE34137_Merkel-cell-carcinoma_squamous-cell-carcinoma_none_1"
//    },
//    "GSE31037_3f56946029b3001d32ee23f726e59441":{
//       "Dataset":"GSE31037",
//       "Group A":"psoriasis:healthy-tissue",
//       "Group B":"psoriasis:diseased-tissue",
//       "Covariates":"none",
//       "sRNA DE":"GSE31037_psoriasis:healthy-tissue_psoriasis:diseased-tissue_none_1",
//       "Pathogen DE":"GSE31037_psoriasis:healthy-tissue_psoriasis:diseased-tissue_none_1",
//       "sRNA Classification":"GSE31037_psoriasis:healthy-tissue_psoriasis:diseased-tissue_1"
//    },
//    "GSE46131_20c57f24636ef2636ddfb7eeefe5abf9":{
//       "Dataset":"GSE46131",
//       "Group A":"braak-stage-6",
//       "Group B":"TDP43-positive",
//       "Covariates":"age-gender",
//       "sRNA DE":"GSE46131_braak-stage-6_TDP43-positive_age-gender_1",
//       "Pathogen DE":"GSE46131_braak-stage-6_TDP43-positive_age-gender_1",
//       "sRNA Classification":"GSE46131_braak-stage-6_TDP43-positive_1"
//    },
//    "GSE34137_fafac4239ac8a937ad4c5032335f5770":{
//       "Dataset":"GSE34137",
//       "Group A":"healthy",
//       "Group B":"basal-cell-carcinoma",
//       "Covariates":"none",
//       "sRNA DE":"GSE34137_healthy_basal-cell-carcinoma_none_1",
//       "Pathogen DE":"GSE34137_healthy_basal-cell-carcinoma_none_1",
//       "sRNA Classification":"GSE34137_healthy_basal-cell-carcinoma_1"
//    },
//    "GSE72193_4b41cd71d437ad98c6926cfcbfab3553":{
//       "Dataset":"GSE72193",
//       "Group A":"Diphencyprone:14day",
//       "Group B":"Placebo:14day",
//       "Covariates":"age-gender",
//       "sRNA DE":"GSE72193_Diphencyprone:14day_Placebo:14day_age-gender_1",
//       "Pathogen DE":"GSE72193_Diphencyprone:14day_Placebo:14day_age-gender_1",
//       "sRNA Classification":"GSE72193_Diphencyprone:14day_Placebo:14day_1"
//    },
//    "GSE34137_c059795661ac36edea86c9e3b1d3f102":{
//       "Dataset":"GSE34137",
//       "Group A":"healthy",
//       "Group B":"Merkel-cell-carcinoma",
//       "Covariates":"none",
//       "sRNA DE":"GSE34137_healthy_Merkel-cell-carcinoma_none_1",
//       "Pathogen DE":"GSE34137_healthy_Merkel-cell-carcinoma_none_1",
//       "sRNA Classification":"GSE34137_healthy_Merkel-cell-carcinoma_1"
//    },
//    "GSE30286_5aab58ed2742385157296caad1438a01":{
//       "Dataset":"GSE30286",
//       "Group A":"neocortex:somatostatin-positive-interneuron:immunoprecipitation-against-Myc",
//       "Group B":"neocortex:pyramidal-neuron:immunoprecipitation-against-Myc",
//       "Covariates":"none",
//       "sRNA DE":"GSE30286_neocortex:somatostatin-positive-interneuron:immunoprecipitation-against-Myc_neocortex:pyramidal-neuron:immunoprecipitation-against-Myc_none_1",
//       "Pathogen DE":"GSE30286_neocortex:somatostatin-positive-interneuron:immunoprecipitation-against-Myc_neocortex:pyramidal-neuron:immunoprecipitation-against-Myc_none_1"
//    },
//    "GSE46131_dfd4b0d95dc5f5dd769cf268bfabd855":{
//       "Dataset":"GSE46131",
//       "Group A":"Alzheimer's-disease",
//       "Group B":"frontotemporal-dementia",
//       "Covariates":"age-gender",
//       "sRNA DE":"GSE46131_Alzheimer's-disease_frontotemporal-dementia_age-gender_1",
//       "Pathogen DE":"GSE46131_Alzheimer's-disease_frontotemporal-dementia_age-gender_1",
//       "sRNA Classification":"GSE46131_Alzheimer's-disease_frontotemporal-dementia_1"
//    },
//    "GSE46131_50428a6640e2ad35af0f6639ed5879c6":{
//       "Dataset":"GSE46131",
//       "Group A":"Alzheimer's-disease",
//       "Group B":"Lewy-body-dementia",
//       "Covariates":"age-gender",
//       "sRNA DE":"GSE46131_Alzheimer's-disease_Lewy-body-dementia_age-gender_1",
//       "Pathogen DE":"GSE46131_Alzheimer's-disease_Lewy-body-dementia_age-gender_1",
//       "sRNA Classification":"GSE46131_Alzheimer's-disease_Lewy-body-dementia_1"
//    },
//    "GSE57012_f49ea5275587d5687e036cc723f5f537":{
//       "Dataset":"GSE57012",
//       "Group A":"skin:healthy-tissue",
//       "Group B":"epidermis:diseased-tissue",
//       "Covariates":"age",
//       "sRNA DE":"GSE57012_skin:healthy-tissue_epidermis:diseased-tissue_age_1",
//       "Pathogen DE":"GSE57012_skin:healthy-tissue_epidermis:diseased-tissue_age_1",
//       "sRNA Classification":"GSE57012_skin:healthy-tissue_epidermis:diseased-tissue_1"
//    },
//    "GSE57012_2cb0578f23362fe0de122e392e0d57b9":{
//       "Dataset":"GSE57012",
//       "Group A":"skin:diseased-tissue",
//       "Group B":"epidermis:healthy-tissue",
//       "Covariates":"age",
//       "sRNA DE":"GSE57012_skin:diseased-tissue_epidermis:healthy-tissue_age_1",
//       "Pathogen DE":"GSE57012_skin:diseased-tissue_epidermis:healthy-tissue_age_1",
//       "sRNA Classification":"GSE57012_skin:diseased-tissue_epidermis:healthy-tissue_1"
//    },
//    "GSE57012_0d184741f490bfbcc6f766e60265b007":{
//       "Dataset":"GSE57012",
//       "Group A":"skin:healthy-tissue",
//       "Group B":"epidermis:healthy-tissue",
//       "Covariates":"age",
//       "sRNA DE":"GSE57012_skin:healthy-tissue_epidermis:healthy-tissue_age_1",
//       "Pathogen DE":"GSE57012_skin:healthy-tissue_epidermis:healthy-tissue_age_1",
//       "sRNA Classification":"GSE57012_skin:healthy-tissue_epidermis:healthy-tissue_1"
//    },
//    "GSE72193_d8a76a2198b8e24e3b0c8d3abc0615ce":{
//       "Dataset":"GSE72193",
//       "Group A":"Diphencyprone:120day",
//       "Group B":"Diphencyprone:3day",
//       "Covariates":"age-gender",
//       "sRNA DE":"GSE72193_Diphencyprone:120day_Diphencyprone:3day_age-gender_1",
//       "Pathogen DE":"GSE72193_Diphencyprone:120day_Diphencyprone:3day_age-gender_1",
//       "sRNA Classification":"GSE72193_Diphencyprone:120day_Diphencyprone:3day_1"
//    },
//    "GSE57012_b09a830ff30cea037e7577636521737f":{
//       "Dataset":"GSE57012",
//       "Group A":"skin:healthy-tissue",
//       "Group B":"dermis:diseased-tissue",
//       "Covariates":"age",
//       "sRNA DE":"GSE57012_skin:healthy-tissue_dermis:diseased-tissue_age_1",
//       "Pathogen DE":"GSE57012_skin:healthy-tissue_dermis:diseased-tissue_age_1",
//       "sRNA Classification":"GSE57012_skin:healthy-tissue_dermis:diseased-tissue_1"
//    }
//  }
//
//
// records = {
//    "GSM1709861":{
//       "organism":"homo sapiens",
//       "cell line":"HEp-2",
//       "disease":"carcinoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497",
//       "tissue from cell line":"cervical epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hep2"
//    },
//    "GSM1709860":{
//       "organism":"homo sapiens",
//       "cell line":"HEp-2",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497",
//       "tissue from cell line":"cervical epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hep2"
//    },
//    "GSM1709843":{
//       "age units":"year",
//       "gender":"female organism",
//       "age":"6",
//       "disease":"healthy",
//       "new sample group":"disease+cell line+virus",
//       "tissue":"conjunctival epithelium",
//       "organism":"homo sapiens",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"in vivo"
//    },
//    "GSM1709842":{
//       "age units":"year",
//       "gender":"male organism",
//       "age":"5",
//       "disease":"healthy",
//       "new sample group":"disease+cell line+virus",
//       "tissue":"conjunctival epithelium",
//       "organism":"homo sapiens",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"in vivo"
//    },
//    "GSM1709841":{
//       "age units":"year",
//       "gender":"male organism",
//       "age":"5",
//       "disease":"healthy",
//       "new sample group":"disease+cell line+virus",
//       "tissue":"conjunctival epithelium",
//       "organism":"homo sapiens",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"in vivo"
//    },
//    "GSM1709840":{
//       "age units":"year",
//       "gender":"female organism",
//       "age":"9",
//       "disease":"healthy",
//       "new sample group":"disease+cell line+virus",
//       "tissue":"conjunctival epithelium",
//       "organism":"homo sapiens",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"in vivo"
//    },
//    "GSM1709847":{
//       "organism":"homo sapiens",
//       "cell line":"HCjE",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497P-",
//       "tissue from cell line":"conjunctival epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hcje"
//    },
//    "GSM1709846":{
//       "organism":"homo sapiens",
//       "cell line":"HCjE",
//       "disease":"healthy",
//       "new sample group":"disease+cell line+virus",
//       "tissue from cell line":"conjunctival epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hcje"
//    },
//    "GSM1709845":{
//       "organism":"homo sapiens",
//       "cell line":"HCjE",
//       "disease":"healthy",
//       "new sample group":"disease+cell line+virus",
//       "tissue from cell line":"conjunctival epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hcje"
//    },
//    "GSM1709844":{
//       "organism":"homo sapiens",
//       "cell line":"HCjE",
//       "disease":"healthy",
//       "new sample group":"disease+cell line+virus",
//       "tissue from cell line":"conjunctival epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hcje"
//    },
//    "GSM1709849":{
//       "organism":"homo sapiens",
//       "cell line":"HCjE",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497P-",
//       "tissue from cell line":"conjunctival epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hcje"
//    },
//    "GSM1709848":{
//       "organism":"homo sapiens",
//       "cell line":"HCjE",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497P-",
//       "tissue from cell line":"conjunctival epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hcje"
//    },
//    "GSM1709850":{
//       "organism":"homo sapiens",
//       "cell line":"HCjE",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497",
//       "tissue from cell line":"conjunctival epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hcje"
//    },
//    "GSM1709854":{
//       "organism":"homo sapiens",
//       "cell line":"HEp-2",
//       "disease":"carcinoma",
//       "new sample group":"disease+cell line+virus",
//       "tissue from cell line":"cervical epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hep2"
//    },
//    "GSM1709853":{
//       "organism":"homo sapiens",
//       "cell line":"HEp-2",
//       "disease":"carcinoma",
//       "new sample group":"disease+cell line+virus",
//       "tissue from cell line":"cervical epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hep2"
//    },
//    "GSM1709852":{
//       "organism":"homo sapiens",
//       "cell line":"HCjE",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497",
//       "tissue from cell line":"conjunctival epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hcje"
//    },
//    "GSM1709851":{
//       "organism":"homo sapiens",
//       "cell line":"HCjE",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497",
//       "tissue from cell line":"conjunctival epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hcje"
//    },
//    "GSM1709858":{
//       "organism":"homo sapiens",
//       "cell line":"HEp-2",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497P-",
//       "tissue from cell line":"cervical epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hep2"
//    },
//    "GSM1709836":{
//       "age units":"year",
//       "gender":"male organism",
//       "age":"8",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis",
//       "tissue":"conjunctival epithelium",
//       "organism":"homo sapiens",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"in vivo"
//    },
//    "GSM1709835":{
//       "new sample group":"disease+cell line+virus",
//       "gender":"male organism",
//       "age":"10",
//       "disease":"trachoma",
//       "age units":"year",
//       "virus":"Chlamydia trachomatis",
//       "tissue":"conjunctival epithelium",
//       "organism":"homo sapiens",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"in vivo"
//    },
//    "GSM1709857":{
//       "organism":"homo sapiens",
//       "cell line":"HEp-2",
//       "disease":"carcinoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497P-",
//       "tissue from cell line":"cervical epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hep2"
//    },
//    "GSM1709834":{
//       "age units":"year",
//       "gender":"female organism",
//       "age":"3",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis",
//       "tissue":"conjunctival epithelium",
//       "organism":"homo sapiens",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"in vivo"
//    },
//    "GSM1709856":{
//       "organism":"homo sapiens",
//       "cell line":"HEp-2",
//       "disease":"carcinoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497P-",
//       "tissue from cell line":"cervical epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hep2"
//    },
//    "GSM1709855":{
//       "organism":"homo sapiens",
//       "cell line":"HEp-2",
//       "disease":"carcinoma",
//       "new sample group":"disease+cell line+virus",
//       "tissue from cell line":"cervical epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hep2"
//    },
//    "GSM1709839":{
//       "age units":"year",
//       "gender":"female organism",
//       "age":"7",
//       "disease":"healthy",
//       "new sample group":"disease+cell line+virus",
//       "tissue":"conjunctival epithelium",
//       "organism":"homo sapiens",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"in vivo"
//    },
//    "GSM1709838":{
//       "age units":"year",
//       "gender":"female organism",
//       "age":"6",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis",
//       "tissue":"conjunctival epithelium",
//       "organism":"homo sapiens",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"in vivo"
//    },
//    "GSM1709837":{
//       "age units":"year",
//       "gender":"male organism",
//       "age":"4",
//       "disease":"trachoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis",
//       "tissue":"conjunctival epithelium",
//       "organism":"homo sapiens",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"in vivo"
//    },
//    "GSM1709859":{
//       "organism":"homo sapiens",
//       "cell line":"HEp-2",
//       "disease":"carcinoma",
//       "new sample group":"disease+cell line+virus",
//       "virus":"Chlamydia trachomatis A2497",
//       "tissue from cell line":"cervical epithelium",
//       "cell type":"epithelial cell",
//       "extracted molecule":"total rna",
//       "sample group":"hep2"
//    }
// }

records = {
   "GSE33665":{
      "Dataset":"GSE33665",
      "Number Of Samples":30,
      "Oasis Output":"GSE33665_5ae0427445fb9",
      "Tissue":{
         "skin":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0001253"
      },
      "Cell Line":{

      },
      "Organism":{
         "homo sapiens":"https://www.ebi.ac.uk/ols/ontologies/ncbitaxon/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FNCBITaxon_9606"
      },
      "Disease":{
         "infiltrative basal cell carcinoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_4299",
         "nodular basal cell carcinoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_4280"
      },
      "Cell Type":{
         "basal cell":"https://www.ebi.ac.uk/ols/ontologies/cl/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FCL_0000646"
      }
   },
   "GSE31037":{
      "Dataset":"GSE31037",
      "Number Of Samples":67,
      "Oasis Output":"GSE31037_5adb7471ca2a5",
      "Tissue":{
         "skin":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0001253"
      },
      "Cell Line":{

      },
      "Organism":{
         "homo sapiens":"https://www.ebi.ac.uk/ols/ontologies/ncbitaxon/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FNCBITaxon_9606"
      },
      "Disease":{
         "psoriasis":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_8893",
         "healthy":"https://www.ebi.ac.uk/ols/ontologies//terms?iri="
      },
      "Cell Type":{

      }
   },
   "GSE84193":{
      "Dataset":"GSE84193",
      "Number Of Samples":27,
      "Oasis Output":"GSE84193_5af41961707f9",
      "Tissue":{
         "skin":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0001253"
      },
      "Cell Line":{

      },
      "Organism":{
         "homo sapiens":"https://www.ebi.ac.uk/ols/ontologies/ncbitaxon/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FNCBITaxon_9606"
      },
      "Disease":{
         "cutaneous squamous cell carcinoma":"https://www.ebi.ac.uk/ols/ontologies/efo/terms?iri=http%3A%2F%2Fwww.ebi.ac.uk%2Fefo%2FEFO_1001927"
      },
      "Cell Type":{

      }
   },
   "GSE72193":{
      "Dataset":"GSE72193",
      "Number Of Samples":34,
      "Oasis Output":"GSE72193_5adf11e87bab0",
      "Tissue":{
         "skin":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0001253"
      },
      "Cell Line":{

      },
      "Organism":{
         "homo sapiens":"https://www.ebi.ac.uk/ols/ontologies/ncbitaxon/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FNCBITaxon_9606"
      },
      "Disease":{

      },
      "Cell Type":{

      }
   },
   "GSE34137":{
      "Dataset":"GSE34137",
      "Number Of Samples":40,
      "Oasis Output":"GSE34137_5ae0ad58e042c",
      "Tissue":{
         "skin":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0001253"
      },
      "Cell Line":{
         "MCC13":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "MKL-2":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "MKL-1a":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "UISO":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "MKL-1c":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "MKL-1b":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "NU4":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "MCC26":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "NU3":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "MS-1":"https://www.ebi.ac.uk/ols/ontologies//terms?iri="
      },
      "Organism":{
         "homo sapiens":"https://www.ebi.ac.uk/ols/ontologies/ncbitaxon/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FNCBITaxon_9606"
      },
      "Disease":{
         "Merkel cell carcinoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_3965",
         "healthy":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "basal cell carcinoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_2513",
         "squamous cell carcinoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_1749"
      },
      "Cell Type":{

      }
   },
   "GSE36236":{
      "Dataset":"GSE36236",
      "Number Of Samples":31,
      "Oasis Output":"GSE36236_5af54f597eac6",
      "Tissue":{
         "nevus":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "lymph node":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0000784",
         "skin":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0001253"
      },
      "Cell Line":{
         "C32 cell":"https://www.ebi.ac.uk/ols/ontologies/clo/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FCLO_0002113",
         "A2058 cell":"https://www.ebi.ac.uk/ols/ontologies/clo/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FCLO_0001566",
         "WM35 cell":"https://www.ebi.ac.uk/ols/ontologies/clo/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FCLO_0009617",
         "WM1552C cell":"https://www.ebi.ac.uk/ols/ontologies/clo/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FCLO_0009614",
         "A-375P cell":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "A-375SM cell":"https://www.ebi.ac.uk/ols/ontologies//terms?iri="
      },
      "Organism":{
         "homo sapiens":"https://www.ebi.ac.uk/ols/ontologies/ncbitaxon/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FNCBITaxon_9606"
      },
      "Disease":{
         "cutaneous melanoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_8923",
         "healthy":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "metastatic melanoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_1909",
         "melanoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_1909",
         "acral lentiginous melanoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_6367"
      },
      "Cell Type":{
         "dark melanocyte":"https://www.ebi.ac.uk/ols/ontologies/cl/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FCL_0002566",
         "melanocyte":"https://www.ebi.ac.uk/ols/ontologies/cl/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FCL_0000148",
         "light melanocyte":"https://www.ebi.ac.uk/ols/ontologies/cl/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FCL_0002567"
      }
   },
   "GSE53600":{
      "Dataset":"GSE53600",
      "Number Of Samples":8,
      "Oasis Output":"GSE53600_5adcfc960c631",
      "Tissue":{
         "lymph node":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0000784",
         "skin":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0001253"
      },
      "Cell Line":{
         "MS-1":"https://www.ebi.ac.uk/ols/ontologies/efo/terms?iri=http%3A%2F%2Fwww.ebi.ac.uk%2Fefo%2FEFO_0002836"
      },
      "Organism":{
         "homo sapiens":"https://www.ebi.ac.uk/ols/ontologies/ncbitaxon/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FNCBITaxon_9606"
      },
      "Disease":{
         "Merkel cell carcinoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_3965",
         "healthy":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "basal cell carcinoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_2513",
         "melanoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_1909",
         "squamous cell carcinoma":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_1749"
      },
      "Cell Type":{

      }
   },
   "GSE46131":{
      "Dataset":"GSE46131",
      "Number Of Samples":20,
      "Oasis Output":"GSE46131_5adcc36be8a4e",
      "Tissue":{
         "neocortex":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0000920"
      },
      "Cell Line":{

      },
      "Organism":{
         "homo sapiens":"https://www.ebi.ac.uk/ols/ontologies/ncbitaxon/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FNCBITaxon_9606"
      },
      "Disease":{
         "healthy":"https://www.ebi.ac.uk/ols/ontologies//terms?iri=",
         "Lewy body dementia":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_12217",
         "hippocampal sclerosis of aging":"https://www.ebi.ac.uk/ols/ontologies/efo/terms?iri=http%3A%2F%2Fwww.ebi.ac.uk%2Fefo%2FEFO_0005678",
         "frontotemporal dementia":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_9255",
         "Alzheimer's disease":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_10652",
         "progressive supranuclear palsy":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_678"
      },
      "Cell Type":{

      }
   },
   "GSE57012":{
      "Dataset":"GSE57012",
      "Number Of Samples":36,
      "Oasis Output":"GSE57012_5ae06e7d6ed0d",
      "Tissue":{
         "skin":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0001253",
         "dermis":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0000294",
         "epidermis":"https://www.ebi.ac.uk/ols/ontologies/bto/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FBTO_0000404"
      },
      "Cell Line":{

      },
      "Organism":{
         "homo sapiens":"https://www.ebi.ac.uk/ols/ontologies/ncbitaxon/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FNCBITaxon_9606"
      },
      "Disease":{
         "psoriasis":"https://www.ebi.ac.uk/ols/ontologies/doid/terms?iri=http%3A%2F%2Fpurl.obolibrary.org%2Fobo%2FDOID_8893"
      },
      "Cell Type":{

      }
   }
};
let selectableQ = true;
var App = new Vue({
  el: '#app',
  components: {
    DataTable
  },
  template: `<data-table
  :data='records'
  :width='80'
  :fieldRenderFunctions='fieldRenderFunctions'
  :fieldSortingFunctions='fieldSortingFunctions'
  :selectableQ='selectableQ'
  :startingPageSize="'10'"
  :pagination="true"
  :pageOptions="['5', '15', 'All']"
  />`,
  data: function(){
    return {
      records: records,
      fieldRenderFunctions: fieldRenderFunctions,
      fieldSortingFunctions: fieldSortingFunctions,
      selectableQ:selectableQ
    };
  }
})
