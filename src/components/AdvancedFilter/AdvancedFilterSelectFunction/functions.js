//'array', 'arraystring', 'arrayobject', 'arraynumber',
//'number', 'string', 'undefined'
let functions = {
  'identity': {
    text: 'x → x',
    func: function(x) {
      return x;
    },
    types: [
      'number', 'string', 'undefined'
    ]
  },
  'mean': {
    text: 'x → mean(x)',
    func: function(numbers) {
      return numbers.map(z => z/numbers.length)
        .reduce((adder, value) => (adder + value));
    },
    types: ['arraynumber']
  },
  'max': {
    text: 'x → max(x)',
    func: function(x) {
      var lg = -Infinity;
      x.map(function(d) {
        lg = d > lg ? d : lg;
      });
      return lg;
    },
    types: ['arraynumber']

  },
  'min': {
    text: 'x → min(x)',
    func: function(x) {
      var sm = Infinity;
      x.map(function(d) {
        sm = d < sm ? d : sm;
      });
      return sm;
    },
    types: ['arraynumber']
  },
  'length': {
    text: 'x → len(x)',
    func: function(x) {
      return x.length;
    },
    types: ['array','arraynumber','arraystring','arrayobject']
  }

};

export default functions;
