function determineType(data) {
  // start with JS basics
  let type = typeof data;

  /*
  number and string are sufficient to know functions and conditionals
  can be applied to them.
  */
  if (['number', 'string'].includes(type)) { return type; }

  /*
  if not a number or string, it might be an array.
  What the elements are of those array might modify what functions
  can be applied. E.g. an array of numbers allows for min, max, mean,
  etc to be applied. Whereas an array of strings does not.
  */
  if (type != 'object') {return type;}

  /* only dealing with object types at this point */
  if (Array.isArray(data)) {
    let type = 'array';

    let typeofElements = data.map(function(e){return typeof e;});
    let typeofElement = typeofElements[0];
    let allSameQ = typeofElements.every(function(e){return e == typeofElement;});

    if (allSameQ) { type += typeofElement; }

    return type;
  }

  return type;
}

export default determineType;
