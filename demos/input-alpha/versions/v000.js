//<kbd class='code-version'>VERSION: 0.0.0</kbd>
let InputAlpha = Mue.InputAlpha;
let records = {
   "GSM1709861":{
      "organism":"homo sapiens",
      "cell line":"HEp-2",
      "disease":"carcinoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497",
      "tissue from cell line":"cervical epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hep2"
   },
   "GSM1709860":{
      "organism":"homo sapiens",
      "cell line":"HEp-2",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497",
      "tissue from cell line":"cervical epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hep2"
   },
   "GSM1709843":{
      "age units":"year",
      "gender":"female organism",
      "age":"6",
      "disease":"healthy",
      "new sample group":"disease+cell line+virus",
      "tissue":"conjunctival epithelium",
      "organism":"homo sapiens",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"in vivo"
   },
   "GSM1709842":{
      "age units":"year",
      "gender":"male organism",
      "age":"5",
      "disease":"healthy",
      "new sample group":"disease+cell line+virus",
      "tissue":"conjunctival epithelium",
      "organism":"homo sapiens",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"in vivo"
   },
   "GSM1709841":{
      "age units":"year",
      "gender":"male organism",
      "age":"5",
      "disease":"healthy",
      "new sample group":"disease+cell line+virus",
      "tissue":"conjunctival epithelium",
      "organism":"homo sapiens",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"in vivo"
   },
   "GSM1709840":{
      "age units":"year",
      "gender":"female organism",
      "age":"9",
      "disease":"healthy",
      "new sample group":"disease+cell line+virus",
      "tissue":"conjunctival epithelium",
      "organism":"homo sapiens",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"in vivo"
   },
   "GSM1709847":{
      "organism":"homo sapiens",
      "cell line":"HCjE",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497P-",
      "tissue from cell line":"conjunctival epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hcje"
   },
   "GSM1709846":{
      "organism":"homo sapiens",
      "cell line":"HCjE",
      "disease":"healthy",
      "new sample group":"disease+cell line+virus",
      "tissue from cell line":"conjunctival epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hcje"
   },
   "GSM1709845":{
      "organism":"homo sapiens",
      "cell line":"HCjE",
      "disease":"healthy",
      "new sample group":"disease+cell line+virus",
      "tissue from cell line":"conjunctival epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hcje"
   },
   "GSM1709844":{
      "organism":"homo sapiens",
      "cell line":"HCjE",
      "disease":"healthy",
      "new sample group":"disease+cell line+virus",
      "tissue from cell line":"conjunctival epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hcje"
   },
   "GSM1709849":{
      "organism":"homo sapiens",
      "cell line":"HCjE",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497P-",
      "tissue from cell line":"conjunctival epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hcje"
   },
   "GSM1709848":{
      "organism":"homo sapiens",
      "cell line":"HCjE",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497P-",
      "tissue from cell line":"conjunctival epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hcje"
   },
   "GSM1709850":{
      "organism":"homo sapiens",
      "cell line":"HCjE",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497",
      "tissue from cell line":"conjunctival epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hcje"
   },
   "GSM1709854":{
      "organism":"homo sapiens",
      "cell line":"HEp-2",
      "disease":"carcinoma",
      "new sample group":"disease+cell line+virus",
      "tissue from cell line":"cervical epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hep2"
   },
   "GSM1709853":{
      "organism":"homo sapiens",
      "cell line":"HEp-2",
      "disease":"carcinoma",
      "new sample group":"disease+cell line+virus",
      "tissue from cell line":"cervical epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hep2"
   },
   "GSM1709852":{
      "organism":"homo sapiens",
      "cell line":"HCjE",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497",
      "tissue from cell line":"conjunctival epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hcje"
   },
   "GSM1709851":{
      "organism":"homo sapiens",
      "cell line":"HCjE",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497",
      "tissue from cell line":"conjunctival epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hcje"
   },
   "GSM1709858":{
      "organism":"homo sapiens",
      "cell line":"HEp-2",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497P-",
      "tissue from cell line":"cervical epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hep2"
   },
   "GSM1709836":{
      "age units":"year",
      "gender":"male organism",
      "age":"8",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis",
      "tissue":"conjunctival epithelium",
      "organism":"homo sapiens",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"in vivo"
   },
   "GSM1709835":{
      "new sample group":"disease+cell line+virus",
      "gender":"male organism",
      "age":"10",
      "disease":"trachoma",
      "age units":"year",
      "virus":"Chlamydia trachomatis",
      "tissue":"conjunctival epithelium",
      "organism":"homo sapiens",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"in vivo"
   },
   "GSM1709857":{
      "organism":"homo sapiens",
      "cell line":"HEp-2",
      "disease":"carcinoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497P-",
      "tissue from cell line":"cervical epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hep2"
   },
   "GSM1709834":{
      "age units":"year",
      "gender":"female organism",
      "age":"3",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis",
      "tissue":"conjunctival epithelium",
      "organism":"homo sapiens",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"in vivo"
   },
   "GSM1709856":{
      "organism":"homo sapiens",
      "cell line":"HEp-2",
      "disease":"carcinoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497P-",
      "tissue from cell line":"cervical epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hep2"
   },
   "GSM1709855":{
      "organism":"homo sapiens",
      "cell line":"HEp-2",
      "disease":"carcinoma",
      "new sample group":"disease+cell line+virus",
      "tissue from cell line":"cervical epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hep2"
   },
   "GSM1709839":{
      "age units":"year",
      "gender":"female organism",
      "age":"7",
      "disease":"healthy",
      "new sample group":"disease+cell line+virus",
      "tissue":"conjunctival epithelium",
      "organism":"homo sapiens",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"in vivo"
   },
   "GSM1709838":{
      "age units":"year",
      "gender":"female organism",
      "age":"6",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis",
      "tissue":"conjunctival epithelium",
      "organism":"homo sapiens",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"in vivo"
   },
   "GSM1709837":{
      "age units":"year",
      "gender":"male organism",
      "age":"4",
      "disease":"trachoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis",
      "tissue":"conjunctival epithelium",
      "organism":"homo sapiens",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"in vivo"
   },
   "GSM1709859":{
      "organism":"homo sapiens",
      "cell line":"HEp-2",
      "disease":"carcinoma",
      "new sample group":"disease+cell line+virus",
      "virus":"Chlamydia trachomatis A2497",
      "tissue from cell line":"cervical epithelium",
      "cell type":"epithelial cell",
      "extracted molecule":"total rna",
      "sample group":"hep2"
   }
};


var App = new Vue({
  el: '#app',
  components: { InputAlpha },
  template: `<input-alpha :data='records'/>`,
  data: function(){
    return {
      records: records
    };
  }
})
