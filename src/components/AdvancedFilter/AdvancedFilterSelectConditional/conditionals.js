//∈, ∉

let conditionals = {
  // faeq: {
  //   text:'∀ e, e =',
  // }
  eq: {
    text: '=',
    types: [
      'array', 'arraystring', 'arrayobject', 'arraynumber',
      'number', 'string', 'undefined'
    ],
    cond: function(a, b) {
      return a == b;
    }
  },
  neq: {
    text: '≠',
    types: [
      'array', 'arraystring', 'arrayobject', 'arraynumber',
      'number', 'string', 'undefined'
    ],
    cond: function(a, b) {
      return a != b;
    }
  },
  lt: {
    text: '<',
    types: ['number']
    ,
    cond: function(a, b) {
      return a < b;
    }
  },
  gt: {
    text: '>',
    types: ['number']
    ,
    cond: function(a, b) {
      return a > b;
    }
  },
  lte: {
    text: '≤',
    types: ['number']
    ,
    cond: function(a, b) {
      return a <= b;
    }
  },
  gte: {
    text: '≥',
    types: ['number']
    ,
    cond: function(a, b) {
      return a >= b;
    }
  },
  ss: {
    text: '⊂',
    types: ['array', 'arraystring', 'arraynumber', 'string'],
    cond: function(a, b) {
      return a.includes(b);
    }
  },
  nss: {
    text: '⟈',
    types: ['array', 'arraystring', 'arraynumber', 'string']
    ,
    cond: function(a, b) {
      return !(a.includes(b)); 
    }
  }
};

export default conditionals;
