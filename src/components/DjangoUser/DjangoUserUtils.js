let colors = [
  "#F44336",
  "#FF4081",
  "#9C27B0",
  "#673AB7",
  "#3F51B5",
  "#2196F3",
  "#03A9F4",
  "#00BCD4",
  "#009688",
  "#4CAF50",
  "#8BC34A",
  "#CDDC39",
  "#FFC107",
  "#FF9800",
  "#FF5722",
  "#795548",
  "#9E9E9E",
  "#607D8B"
];

function LightenDarkenColor(col, amt) {
  // https://css-tricks.com/snippets/javascript/lighten-darken-color/
  var usePound = false;

  if (col[0] == "#") {
    col = col.slice(1);
    usePound = true;
  }

  var num = parseInt(col, 16);

  var r = (num >> 16) + amt;

  if (r > 255) r = 255;
  else if (r < 0) r = 0;

  var b = ((num >> 8) & 0x00ff) + amt;

  if (b > 255) b = 255;
  else if (b < 0) b = 0;

  var g = (num & 0x0000ff) + amt;

  if (g > 255) g = 255;
  else if (g < 0) g = 0;

  return (usePound ? "#" : "") + (g | (b << 8) | (r << 16)).toString(16);
}

function initialsFontSize(size) {
  return Math.floor(size / 2.5);
}

function azOnly(string) {
  var filtered = "";
  for (var i = 0; i < string.length; i++) {
    let char = string[i];
    if (/^[a-zA-Z]+$/.test(char)) {
      filtered += char;
    }
  }
  return filtered;
}

function captialsIn(string) {
  var caps = [];
  let str = azOnly(string);
  for (var i = 0; i < str.length; i++) {
    if (str[i] === str[i].toUpperCase()) {
      caps.push(str[i]);
    }
  }
  return caps;
}

function initialsOf(name) {
  let caps = captialsIn(name.split(" ").join(""));
  var first = caps[0],
    last = "";
  if (caps.length > 1) {
    last = caps[caps.length - 1];
  }
  var inits = first + last;
  if (inits !== "undefined") {
    return inits;
  }
  return name[0].toUpperCase();
}

export {
  colors,
  LightenDarkenColor,
  initialsFontSize,
  azOnly,
  captialsIn,
  initialsOf
};
