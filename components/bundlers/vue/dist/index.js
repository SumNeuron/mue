'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _vinyl = require('vinyl');

var _vinyl2 = _interopRequireDefault(_vinyl);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _webpack = require('webpack');

var _webpack2 = _interopRequireDefault(_webpack);

var _memoryFs = require('memory-fs');

var _memoryFs2 = _interopRequireDefault(_memoryFs);

var _webpack3 = require('./webpack.config');

var _webpack4 = _interopRequireDefault(_webpack3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var runWebpack = function runWebpack(files, distPath) {
  return new Promise(function (resolve, reject) {
    var fs = new _memoryFs2.default();
    var webPackConfig = (0, _webpack4.default)(files, distPath);
    var outputFiles = [];

    // Init webpack with config
    var compiler = (0, _webpack2.default)(webPackConfig);

    // Don't write to file system. Write in-memory instead.
    compiler.outputFileSystem = fs;

    compiler.run(function (err, stats) {
      if (err) {
        reject(err);
      }

      if (stats.hasErrors()) {
        if (stats.errors && stats.errors.length > 0) {
          reject(stats.errors[0]);
        } else if (stats.compilation.errors && stats.compilation.errors.length > 0) {
          reject(stats.compilation.errors[0]);
        }
      }

      fs.readdirSync(distPath).forEach(function (distFileName) {
        var fileContent = fs.readFileSync(_path2.default.join(distPath, distFileName));

        outputFiles.push(new _vinyl2.default({
          contents: fileContent,
          base: distPath,
          path: _path2.default.join(distPath, distFileName),
          basename: distFileName,
          // TODO: How to recognize spec files properly?
          test: distFileName.indexOf('.spec.') >= 0
        }));
      });

      return resolve(outputFiles);
    });
  });
};

var compile = function compile(files, distPath) {
  if (files.length === 0) {
    return files;
  }
  return runWebpack(files, distPath).then(function (compiled) {
    return compiled;
  });
};

exports.default = {
  compile: compile
};
module.exports = exports['default'];

//# sourceMappingURL=index.js.map