let fields = function(records) {
  let recordIds = ids(records);
  var allRecordFields = [].concat(
    ...recordIds.map(function(recordId) {
      return Object.keys(records[recordId]);
    })
  );
  return [...new Set(allRecordFields)];
};

let fieldTokens = function(records) {
  let recordFields = fields(records);
  let tokens = {};
  recordFields.map(function(field) {
    tokens[field] = field;
  });
  return tokens;
};

let ids = function(records) {
  return Object.keys(records);
};

let giRegex = function(text, tokens) {
  let regex = new RegExp(text, 'gi');

  let suggestions = {};

  tokens.filter(function(token) {
    var match = token.match(regex);
    // console.log(match, token);

    let found = !(match == null || match.join('') === '');

    if (found) {
      suggestions[token] = match[0];
    }
    return found;
  });

  suggestions = Object.keys(suggestions).sort(function(a, b) {
    return text.indexOf(suggestions[b]) - text.indexOf(suggestions[a]);
  });
  return suggestions;
};

let groupByConjunctiveNormalForm = function(filters) {
  var groupped = [];

  for (var i = 0; i < filters.length; i++) {
    var logic = filters[i].logic;
    if (logic == 'and') {
      groupped.push([filters[i]]);
    } else {
      var list = groupped[groupped.length - 1];
      if (list == undefined) {
        filters[i].logic = 'and';
        groupped.push([filters[i]]);
      } else {
        groupped[groupped.length - 1].push(filters[i]);
      }
    }

    // console.log(groupped);
  }

  return groupped;
};

let scrubInputText = function(text) {
  if (text[0] === ' ') {
    text = text.slice(1, text.length);
  }
  if (text[text.length - 1] === ' ') {
    text = text.slice(1, text.length - 1);
  }
  return text;
};
export {
  ids,
  fieldTokens,
  fields,
  groupByConjunctiveNormalForm,
  scrubInputText,
  giRegex
};
