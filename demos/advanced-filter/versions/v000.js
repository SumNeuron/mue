let AdvancedFilterInput = Mue.AdvancedFilterInput;
let AdvancedFilterRow = Mue.AdvancedFilterRow;
let AdvancedFilterSelectConditional = Mue.AdvancedFilterSelectConditional;
let AdvancedFilterSelectLogic = Mue.AdvancedFilterSelectLogic;
let AdvancedFilterSelectFunction = Mue.AdvancedFilterSelectFunction;
let AdvancedFilterSelectProperty = Mue.AdvancedFilterSelectProperty;
let AdvancedFilterTable = Mue.AdvancedFilterTable;

// //<kbd class='code-version'>VERSION: 0.0.0</kbd>
//
let records = {
    a: {x: 1,  y: "a string", z: [1,2,3,4]},
    b: {x: 2,  y: "strange?", z: [1,2,4]},
    c: {x: 3,  y: "starts w", z: [1,2,3,4]},
    d: {x: 10, y: "some let", z: [1,2,4,5,6,7,8]},
    e: {x: 2,  y: "? qwerty", z: [1,40]}
}

// var filteredRecords = []
// var filters = [
//   {
//     logic: "and",
//     function: function(x){return x},
//     property: 'a',
//     conditional: function(a, b) { return a == b },
//     input: 1
//   },
//   {
//     logic: "or",
//     function: function(x){return x},
//     property: 'a',
//     conditional: function(a, b) { return a == b },
//     input: 2
//   },
//   {
//     logic: "and",
//     function: function(x){return x},
//     property: 'b',
//     conditional: function(a, b) { return a.includes(b) },
//     input: 3
//   }
// ]
//
// var rKeys = Object.keys(records)
// filters.map(function(filt, i){
//   var
//   l = filt.logic,
//   f = filt.function,
//   p = filt.property,
//   c = filt.conditional,
//   i = filt.input
//
//   var results = rKeys.filter(function(k) {
//     var record = records[k];
//     var value = record[p];
//     value = f(value)
//     return c(value, i)
//   })
//   console.log(results)
// })

//
//
// let dataFilter = vdsm.dataFilter
//
// var progress = new Vue({
//   el: '#data-filter',
//   components: { dataFilter },
//   data: { records: records }
// })

var App = new Vue({
  el: '#app',
  components: {
    AdvancedFilterTable
  },
  template: `<advanced-filter-table :data='records'/>`,
  data: function(){
    return {
      records: records
    };
  }
})
